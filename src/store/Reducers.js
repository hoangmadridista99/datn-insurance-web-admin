const reducers = (state, action) => {
  switch (action.type) {
    case 'LOADING':
      return {
        ...state,
        notify: action.payload,
      }
    default:
      return state
  }
}

export default reducers
