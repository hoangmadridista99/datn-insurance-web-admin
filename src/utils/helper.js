import Image from 'next/image'
import RatingStarYellow from '/public/images/ratings/RatingStarYellow.png'
import RatingStarNone from '/public/images/ratings/RatingStarNone.png'
import { formatCurrency, formatNumber } from '@helpers/formatCurrency'

export const timeCreateRating = (rating) => {
  return rating?.slice(0, 10)
}

export const convertRatingStar = (rating) => {
  const ratingStar = [
    {
      id: 1,
      rating: (rating >= 1 && (
        <div>
          <Image src={RatingStarYellow} alt="RatingStarYellow" />
        </div>
      )) || (
        <div>
          <Image src={RatingStarNone} alt="RatingStarNone" />
        </div>
      ),
    },
    {
      id: 2,
      rating: (rating >= 2 && (
        <div>
          <Image src={RatingStarYellow} alt="RatingStarYellow" />
        </div>
      )) || (
        <div>
          <Image src={RatingStarNone} alt="RatingStarNone" />
        </div>
      ),
    },
    {
      id: 3,
      rating: (rating >= 3 && (
        <div>
          <Image src={RatingStarYellow} alt="RatingStarYellow" />
        </div>
      )) || (
        <div>
          <Image src={RatingStarNone} alt="RatingStarNone" />
        </div>
      ),
    },
    {
      id: 4,
      rating: (rating >= 4 && (
        <div>
          <Image src={RatingStarYellow} alt="RatingStarYellow" />
        </div>
      )) || (
        <div>
          <Image src={RatingStarNone} alt="RatingStarNone" />
        </div>
      ),
    },
    {
      id: 5,
      rating: (rating >= 5 && (
        <div>
          <Image src={RatingStarYellow} alt="RatingStarYellow" />
        </div>
      )) || (
        <div>
          <Image src={RatingStarNone} alt="RatingStarNone" />
        </div>
      ),
    },
  ]
  return (
    <div className="flex gap-1">
      {ratingStar.map((value) => (
        <div key={value.id}>{value?.rating}</div>
      ))}
    </div>
  )
}

export const getIconByLevel = (level) => {
  switch (level) {
    case 'high':
      return (
        <Image
          width={18}
          height={18}
          src="/svg/check-success.svg"
          alt="Check Success"
        />
      )
    case 'medium':
      return (
        <Image
          width={18}
          height={18}
          src="/svg/check-warning.svg"
          alt="Check Warning"
        />
      )
    case 'not-support':
      return <Image width={18} height={18} src="/svg/close.svg" alt="close" />
    case 'none':
      return (
        <Image width={18} height={18} src="/svg/warning.svg" alt="warning" />
      )
    default:
      return null
  }
}

export const formatPrices = (fromValue, toValue, timeUnit) => {
  return (
    <p className="text-2xl font-bold text-ultramarine-blue">
      {fromValue && toValue ? (
        <>
          {formatCurrency(fromValue)}
          <span className="mx-1">-</span>
          {formatCurrency(toValue)}
        </>
      ) : (
        formatCurrency(fromValue || toValue)
      )}
      <span className="mx-2">VNĐ</span>
      <span className="text-base text-metallic">/{timeUnit}</span>
    </p>
  )
}

export const handleFormatter = (value) => {
  if (isNaN(value)) return undefined

  return value ? formatCurrency(value) : ''
}

export const handleFormatterWithCommas = (value) => {
  if (isNaN(value)) return undefined

  return value ? formatNumber(value) : ''
}

export const handleParser = (value) => {
  const formatValue = parseInt(value.replace(/\./g, ''))

  if (isNaN(formatValue)) return

  return parseInt(value.replace(/\./g, ''))
}
