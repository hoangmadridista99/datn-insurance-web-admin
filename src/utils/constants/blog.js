export const OptionBlogStatus = ['approved', 'pending', 'rejected']

export const CREATE_AT = 'created_at'

export const KEY_MODAL = {
  CREATE: 'create',
  UPDATE: 'update',
  PREVIEW: 'preview',
  REMOVE: 'remove',
  REJECT: 'reject',
}
