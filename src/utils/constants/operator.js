export const OptionStatus = [
  {
    value: false,
    label: 'unlocked',
  },
  {
    value: true,
    label: 'locked',
  },
]

export const USER_ROLE = {
  SUPER_ADMIN: 'super-admin',
  ADMIN: 'admin',
}
