export const INSURANCE_TYPES_DEFAULT = [
  'Bảo hiểm nhân thọ',
  'Bảo hiểm sức khỏe',
]

export const LIFE_INSURANCE_FIELDS = {
  details: [
    'company_id',
    'name',
    'description_insurance',
    'objective_of_insurance',
  ],
  terms: [
    'age_eligibility',
    'deadline_for_deal',
    'insurance_minimum_fee',
    'insured_person',
    'profession',
    'deadline_for_payment',
    'age_of_contract_termination',
    'termination_conditions',
    'total_sum_insured',
    'monthly_fee',
  ],
  benefits: ['key_benefits'],
  additional_benefits: [
    'death_or_disability',
    'serious_illnesses',
    'health_care',
    'investment_benefit',
    'increasing_value_bonus',
    'for_child',
    'flexible_and_diverse',
    'termination_benefits',
    'expiration_benefits',
    'fee_exemption',
  ],
  customer_orientation: [
    'acceptance_rate',
    'completion_time_deal',
    'end_of_process',
    'withdrawal_time',
    'reception_and_processing_time',
  ],
  document: ['documentation_url', 'benefits_illustration_table'],
}

export const HEALTH_INSURANCE_FIELDS = {
  details: ['company_id', 'name', 'description_insurance'],
  terms: [
    'age_eligibility',
    'deadline_for_deal',
    'insurance_minimum_fee',
    'insured_person',
    'profession',
    'deadline_for_payment',
    'total_sum_insured',
    'monthly_fee',
    'customer_segment',
    'age_of_contract_termination',
  ],
  inpatient: [
    'room_type',
    'for_cancer',
    'for_illnesses',
    'for_accidents',
    'for_surgical',
    'for_hospitalization',
    'for_intensive_care',
    'for_administrative',
    'for_organ_transplant',
  ],
  outpatient: [
    'examination_and_treatment',
    'testing_and_diagnosis',
    'home_care',
    'due_to_accident',
    'due_to_illness',
    'due_to_cancer',
    'restore_functionality',
  ],
  additional_benefits: ['title', 'dental', 'obstetric', 'existed_services'],
  dental: [
    'examination_and_diagnosis',
    'gingivitis',
    'xray_and_diagnostic_imaging',
    'filling_teeth_basic',
    'root_canal_treatment',
    'dental_pathology',
    'dental_calculus',
  ],
  obstetric: [
    'give_birth_normally',
    'caesarean_section',
    'obstetric_complication',
    'give_birth_abnormality',
    'after_give_birth_fee',
    'before_discharged',
    'postpartum_childcare_cost',
  ],
  health_customer_orientation: [
    'insurance_scope',
    'waiting_period',
    'compensation_process',
    'reception_and_processing_time',
  ],
  document: ['documentation_url', 'benefits_illustration_table'],
}

export const CAR_INSURANCE_FIELDS = {
  details: [
    'company_name',
    'insurance_name',
    'insurance_description',
    'insurance_type',
    'company_logo',
  ],
}
export const LIFE_FORM_INITIAL_VALUES = {
  benefits_illustration_table: null,
  documentation_url: null,
  additional_benefits: {
    death_or_disability: {
      text: null,
      level: 'none',
    },
    serious_illnesses: {
      text: null,
      level: 'none',
    },
    health_care: {
      text: null,
      level: 'none',
    },
    investment_benefit: {
      text: null,
      level: 'none',
    },
    increasing_value_bonus: {
      text: null,
      level: 'none',
    },
    for_child: {
      text: null,
      level: 'none',
    },
    flexible_and_diverse: {
      text: null,
      level: 'none',
    },
    termination_benefits: {
      text: null,
      level: 'none',
    },
    expiration_benefits: {
      text: null,
      level: 'none',
    },
    fee_exemption: {
      text: null,
      level: 'none',
    },
  },
  terms: {
    age_eligibility: {
      to: null,
      from: null,
      level: 'none',
      description: null,
    },
    deadline_for_deal: {
      to: null,
      from: null,
      type: 'year',
      level: 'none',
      value: null,
      description: null,
    },
    insurance_minimum_fee: {
      level: 'none',
      value: null,
      description: null,
    },
    deadline_for_payment: {
      level: 'none',
      value: null,
      description: null,
    },
    insured_person: {
      level: 'none',
      value: null,
      description: null,
    },
    profession: {
      level: 'none',
      text: null,
    },
    age_of_contract_termination: {
      to: null,
      from: null,
      type: null,
      level: 'none',
      description: null,
    },
    termination_conditions: {
      text: null,
      level: 'none',
    },
    total_sum_insured: {
      to: null,
      from: null,
      level: 'none',
      description: null,
    },
    monthly_fee: {
      to: null,
      from: null,
      level: 'none',
      description: null,
    },
  },
  customer_orientation: {
    acceptance_rate: {
      text: null,
      level: 'none',
    },
    completion_time_deal: {
      text: null,
      level: 'none',
    },
    end_of_process: {
      text: null,
      level: 'none',
    },
    withdrawal_time: {
      text: null,
      level: 'none',
    },
    reception_and_processing_time: {
      text: null,
      level: 'none',
    },
  },
}

export const HEALTH_FORM_INITIAL_VALUES = {
  company_id: null,
  name: null,
  description_insurance: null,
  terms: {
    age_eligibility: {
      level: 'none',
      from: null,
      to: null,
      description: null,
    },
    deadline_for_deal: {
      level: 'none',
      type: 'year',
      from: null,
      to: null,
      value: null,
      description: null,
    },
    insurance_minimum_fee: {
      level: 'none',
      value: null,
      description: null,
    },
    insured_person: {
      level: 'none',
      value: null,
      description: null,
    },
    profession: {
      level: 'none',
      text: null,
    },
    deadline_for_payment: {
      level: 'none',
      value: null,
      description: null,
    },
    customer_segment: { level: 'none', text: null },
    total_sum_insured: {
      level: 'none',
      from: null,
      to: null,
      description: null,
    },
    monthly_fee: {
      level: 'none',
      from: null,
      to: null,
      description: null,
    },
    age_of_contract_termination: {
      level: 'none',
      from: null,
      to: null,
      description: null,
      type: null,
    },
  },
  customer_orientation: {
    insurance_scope: {
      level: 'none',
      values: null,
    },
    waiting_period: {
      level: 'none',
      from: null,
      to: null,
      description: null,
    },
    compensation_process: {
      level: 'none',
      description: null,
    },
    reception_and_processing_time: {
      level: 'none',
      text: null,
    },
  },
  inpatient: {
    room_type: {
      level: 'none',
      values: null,
      description: null,
    },
    for_cancer: { level: 'none', value: null, description: null },
    for_illnesses: { level: 'none', value: null, description: null },
    for_accidents: { level: 'none', value: null, description: null },
    for_surgical: { level: 'none', value: null, description: null },
    for_hospitalization: {
      level: 'none',
      value: null,
      type: null,
    },
    for_intensive_care: { level: 'none', value: null, description: null },
    for_administrative: { level: 'none', value: null, description: null },
    for_organ_transplant: { level: 'none', value: null, description: null },
  },
  outpatient: {
    examination_and_treatment: {
      level: 'none',
      value: null,
      description: null,
    },
    testing_and_diagnosis: { level: 'none', value: null, description: null },
    home_care: { level: 'none', value: null, description: null },
    due_to_accident: {
      level: 'none',
      value: null,
      description: null,
    },
    due_to_illness: { level: 'none', value: null, description: null },
    due_to_cancer: { level: 'none', value: null, description: null },
    restore_functionality: { level: 'none', value: null, description: null },
  },
  dental: {
    examination_and_diagnosis: {
      level: 'none',
      value: null,
      description: null,
    },
    gingivitis: { level: 'none', value: null, description: null },
    xray_and_diagnostic_imaging: {
      level: 'none',
      value: null,
      description: null,
    },
    filling_teeth_basic: { level: 'none', value: null, description: null },
    root_canal_treatment: { level: 'none', value: null, description: null },
    dental_pathology: { level: 'none', value: null, description: null },
    dental_calculus: { level: 'none', value: null, description: null },
  },
  obstetric: {
    give_birth_normally: {
      level: 'none',
      value: null,
      description: null,
    },
    caesarean_section: { level: 'none', value: null, description: null },
    obstetric_complication: { level: 'none', value: null, description: null },
    give_birth_abnormality: { level: 'none', value: null, description: null },
    after_give_birth_fee: { level: 'none', value: null, description: null },
    before_discharged: { level: 'none', value: null, description: null },
    postpartum_childcare_cost: {
      level: 'none',
      value: null,
      description: null,
    },
  },
}

export const FORM_LEVEL = [
  {
    value: 'high',
    icon: 'check-success.svg',
    alt: 'Level Hight',
  },
  {
    value: 'medium',
    icon: 'check-warning.svg',
    alt: 'Level Medium',
  },
  {
    value: 'not-support',
    icon: 'warning.svg',
    alt: 'Level Not Support',
  },
  {
    value: 'none',
    icon: 'close.svg',
    alt: 'Level None',
  },
]

export const LIST_TYPE = ['year', 'old']
export const LIST_PERSON = ['one-self', 'other-person']
export const LIST_PROFESSION = [
  'EMPLOYER',
  'EMPLOYEE',
  'STUDENT',
  'OTHER_PROFESSION',
]
export const LIST_PAY = ['monthly', 'yearly', 'flexible', 'once']
export const CUSTOMER_SEGMENT = [
  'INDIVIDUAL',
  'FAMILY',
  'ORGANIZATION',
  'BUSINESS',
]

export const CUSTOMER_SEGMENT_I18N = (t) => [
  { value: 'INDIVIDUAL', label: t('insurances:fields:individual') },
  { value: 'FAMILY', label: t('insurances:fields:family') },
  { value: 'ORGANIZATION', label: t('insurances:fields:organization') },
  { value: 'BUSINESS', label: t('insurances:fields:business') },
]
export const INSURANCE_STATUSES = [
  {
    value: 'pending',
    label: 'Đang chờ xác nhận',
  },
  {
    value: 'approved',
    label: 'Đã xác nhận',
  },
  {
    value: 'rejected',
    label: 'Đã từ chối',
  },
]
export const OPTIONS_TYPE = [
  { value: 'year', label: 'Theo năm' },
  { value: 'old', label: 'Theo tuổi' },
]
export const APPRAISAL_STATUS = {
  PENDING: 'pending',
  APPROVED: 'approved',
  REJECTED: 'rejected',
  PAID: 'paid',
}

export const PERSON_I18N_OPTIONS = (t) => [
  { value: 'one-self', label: t('insurances:fields:buy_self') },
  { value: 'other-person', label: t('insurances:fields:buy_loved') },
]

export const PROFESSION_I18N_OPTIONS = (t) => [
  { value: 'EMPLOYEE', label: t('insurances:fields:wage_earner') },
  { value: 'EMPLOYER', label: t('insurances:fields:own') },
  { value: 'STUDENT', label: t('insurances:fields:student') },
  { value: 'OTHER_PROFESSION', label: t('insurances:fields:other_jobs') },
]

export const PAY_I18N_OPTIONS = (t) => [
  { value: 'monthly', label: t('insurances:fields:pay_month') },
  { value: 'yearly', label: t('insurances:fields:pay_year') },
  { value: 'flexible', label: t('insurances:fields:pay_flexible') },
  { value: 'once', label: t('insurances:fields:pay_once') },
]

export const HEALTH_INSURANCE = 'health'
export const LIFE_INSURANCE = 'life'
export const CAR_INSURANCE = 'car'
export const DENTAL = 'dental'
export const OBSTETRIC = 'obstetric'
export const NONE = 'none'
export const HOSPITALIZATION_LIST = [
  { id: 1, value: 'by-day', label: 'byDay' },
  { id: 2, value: 'by-year', label: 'byYear' },
]
export const INSURANCE_SCOPE_LIST = [
  'death',
  'inpatient',
  'outpatient',
  'dental',
  'obstetric',
]

export const INSURANCE_SCOPES_I18N = (t) => [
  { value: 'death', label: t('insurances:fields:death') },
  { value: 'inpatient', label: t('insurances:fields:inpatient') },
  { value: 'outpatient', label: t('insurances:fields:outpatient') },
  { value: 'dental', label: t('insurances:fields:dental') },
  { value: 'obstetric', label: t('insurances:fields:obstetric') },
]

export const ROOM_TYPES = [
  { id: 1, value: 'one-bed', label: 'singleRoom' },
  { id: 2, value: 'two-beds', label: 'doubleRoom' },
  { id: 3, value: 'multi-beds', label: 'multiBedRoom' },
  { id: 4, value: 'vip-room', label: 'vipRoom' },
  { id: 5, value: 'special-room', label: 'specialCareRoom' },
]

export const HEALTH_DEFAULT_TABS = {
  customer_orientation: [],
  terms: [],
  inpatient: [],
  obstetric: [],
  dental: [],
  outpatient: [],
}

export const LIFE_DEFAULT_TABS = {
  customer_orientation: [],
  terms: [],
  additional_benefits: [],
  details: [],
  benefits: [],
}

export const STATUSES = {
  REJECTED: 'rejected',
  PENDING: 'pending',
  APPROVED: 'approved',
}

export const VENDOR_STATUSES = {
  REJECTED: 'rejected',
  PENDING: 'pending',
  ACCEPTED: 'accepted',
}

export const ROLES = {
  ADMIN: 'admin',
  SUPER_ADMIN: 'super-admin',
  VENDOR: 'vendor',
}

export const INSURANCE_PRICE_TYPE = {
  COMPULSORY_CAR: 'compulsory_car',
  PHYSICAL_CAR: 'physical_car',
}

export const PHYSICAL_CAR_TYPE = {
  PERCENTAGE: 'percentage',
  COST: 'cost',
}

export const CAR_INSURANCE_TYPE = {
  PHYSICAL: 'physical',
  BOTH: 'both',
  MANDATORY: 'mandatory',
}

export const FORM_PHYSICAL_CAR_INSURANCE_DEFAULT = {
  cost_of_purchasing: {
    type: PHYSICAL_CAR_TYPE.COST,
    value: null,
  },
  choosing_service_center: {
    type: PHYSICAL_CAR_TYPE.COST,
    value: null,
  },
  component_vehicle_theft: {
    type: PHYSICAL_CAR_TYPE.COST,
    value: null,
  },
  no_depreciation_cost: {
    type: PHYSICAL_CAR_TYPE.COST,
    value: null,
  },
  water_damage: {
    type: PHYSICAL_CAR_TYPE.COST,
    value: null,
  },
  insured_for_each_person: {
    type: PHYSICAL_CAR_TYPE.COST,
    value: null,
  },
}

export const TARGET_USE_BUSINESS = [
  { value: 'family-car', label: 'family_car' },
  { value: 'contracted-service', label: 'contracted_service' },
  { value: 'technology-business', label: 'technology_business' },
]

export const TYPE_INVOICE = [
  { value: 'business', label: 'business' },
  { value: 'individual', label: 'individual' },
]

export const IMAGE_FALLBACK =
  'https://raw.githubusercontent.com/koehlersimon/fallback/master/Resources/Public/Images/placeholder.jpg'
