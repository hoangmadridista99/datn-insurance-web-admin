export const typeOption = [
  { value: 'year ', label: 'Theo năm' },
  { value: 'age', label: 'Theo tuổi' },
]

export const object = [
  { value: 'one-self', label: 'Mua cho mình' },
  { value: 'other-person', label: 'Mua cho người thân' },
]

export const optionsFee = [
  { value: 'monthly', label: 'Thanh toán hàng tháng' },
  { value: 'yearly', label: 'Thanh toán hàng năm' },
  { value: 'flexible', label: 'Thanh toán linh hoạt' },
  { value: 'once', label: 'Đóng phí 1 lần' },
]

export const optionsProfession = [
  { value: 'hire', label: 'Làm thuê' },
  { value: 'own', label: 'Làm chủ' },
  { value: 'student', label: 'Sinh viên' },
  { value: 'otherWorks', label: 'Ngành nghề khác' },
]

export const PAGINATION_INITIAL_VALUES = {
  currentPage: 1,
  totalPages: 1,
  totalItems: 0,
  itemsPerPage: 10,
}

export const SORTING = {
  DESC: 'DESC',
  ASC: 'ASC',
}
