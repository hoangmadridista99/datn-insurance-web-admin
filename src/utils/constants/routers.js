import {
  ArchiveBookActiveSvg,
  ArchiveBookSvg,
  BillSvg,
  CubeSvg,
  // DocumentCloudSvg,
  DocumentFavoriteActiveSvg,
  DocumentFavoriteSvg,
  DocumentSketchActiveSvg,
  DocumentSketchSvg,
  // DocumentTextSvg,
  NoteSvg,
  StickyNoteSvg,
} from '@utils/icons'

export const routerChildrenBlog = [
  {
    key: '/blogs/admin',
    link: '/blogs/admin',
    i18n: 'sidebar.blogs.admin',
  },
  {
    key: '/blogs/vendor',
    link: '/blogs/vendor',
    i18n: 'sidebar.blogs.vendor',
  },
]

export const routerChildrenCarInsurance = [
  {
    key: '/car-insurances/create',
    link: '/car-insurances/create',
    i18n: 'sidebar.car.create',
  },
  {
    key: '/car-insurances/mandatory-price',
    link: '/car-insurances/mandatory-price',
    i18n: 'sidebar.car.mandatory',
  },
  {
    key: '/car-insurances',
    link: '/car-insurances',
    i18n: 'sidebar.car.list',
  },
  {
    key: '/car-insurances/appraisal',
    link: '/car-insurances/appraisal',
    i18n: 'sidebar.appraisal',
  },
  {
    key: '/car-insurances/ratings',
    link: '/car-insurances/ratings',
    i18n: 'sidebar.ratings_insurance',
  },
]

export const routerChildrenMotorInsurance = [
  {
    key: '/car-insurances/create',
    link: '/car-insurances/create',
    i18n: 'sidebar.car.create',
  },
]

export const routerChildrenTravelInsurance = [
  {
    key: '/car-insurances/create',
    link: '/car-insurances/create',
    i18n: 'sidebar.car.create',
  },
]

export const routerChildrenLifeAndHealthInsurances = [
  {
    key: '/insurances/create',
    link: '/insurances/create',
    i18n: 'sidebar.createInsurance',
  },
  {
    key: '/insurances/admin',
    link: '/insurances/admin',
    i18n: 'sidebar.insurances.admin',
  },
  {
    key: '/insurances/vendor',
    link: '/insurances/vendor',
    i18n: 'sidebar.insurances.vendor',
  },
  {
    key: '/insurances/favorite',
    link: '/insurances/favorite',
    i18n: 'sidebar.favorite',
  },
  {
    key: '/insurances/ratings',
    link: '/insurances/ratings',
    i18n: 'sidebar.ratings_insurance',
  },
]

export const routers = [
  {
    key: '/dashboard',
    link: '/dashboard',
    i18n: 'sidebar.dashboard',
    icon: <CubeSvg />,
  },
  {
    isTitle: true,
    link: null,
    i18n: 'sidebar.account_manage',
  },
  {
    key: '/vendors',
    link: '/vendors',
    i18n: 'sidebar.vendors',
    icon: <NoteSvg />,
  },
  {
    key: '/operators',
    link: '/operators',
    i18n: 'sidebar.operators',
    icon: <StickyNoteSvg />,
  },
  {
    isTitle: true,
    link: null,
    i18n: 'sidebar.insurance_system_manage',
  },
  {
    key: 'life-health-insurances',
    i18n: 'sidebar.life_and_health',
    children: routerChildrenLifeAndHealthInsurances,
    icon: <DocumentFavoriteSvg />,
    activeIcon: <DocumentFavoriteActiveSvg />,
  },
  {
    key: 'car-insurances',
    i18n: 'sidebar.car.title',
    children: routerChildrenCarInsurance,
    icon: <DocumentSketchSvg />,
    activeIcon: <DocumentSketchActiveSvg />,
  },
  // {
  //   key: 'motorcycle-insurance',
  //   i18n: 'sidebar.motor.title',
  //   children: routerChildrenMotorInsurance,
  //   icon: <DocumentTextSvg />,
  //   activeIcon: <DocumentSketchActiveSvg />,
  // },
  // {
  //   key: 'travel-insurance',
  //   i18n: 'sidebar.travel.title',
  //   children: routerChildrenTravelInsurance,
  //   icon: <DocumentCloudSvg />,
  //   activeIcon: <DocumentSketchActiveSvg />,
  // },
  {
    isTitle: true,
    link: null,
    i18n: 'sidebar.blog_manage',
  },
  {
    key: 'blogs',
    i18n: 'sidebar.blogs.title',
    children: routerChildrenBlog,
    icon: <ArchiveBookSvg />,
    activeIcon: <ArchiveBookActiveSvg />,
  },
  {
    key: '/blogs/categories',
    link: '/blogs/categories',
    i18n: 'sidebar.blog-categories',
    icon: <BillSvg />,
  },
]

export const ROUTERS = {
  DASHBOARD: '/dashboard',
  CREATE_INSURANCE: '/insurances/create',
  RATING_INSURANCES: '/insurances/ratings',
  VENDORS: '/vendors',
  BLOG_CATEGORIES: '/blogs/categories',
  OPERATORS: '/operators',
  FAVORITE: '/favorite',
  APPRAISAL: '/appraisal',
  PRICE_INSURANCE: '/insurance-price',
  CAR_INSURANCES: '/car-insurances',
  CREATE_CAR_INSURANCE: '/car-insurances/create',
  INSURANCES: '/insurances',
  BLOGS: '/blogs',
}
