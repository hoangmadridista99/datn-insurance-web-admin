import axios from 'axios'
import { getSession } from 'next-auth/react'

const BaseUrl = process.env.NEXT_PUBLIC_BASE_URL

export const getAxios = () => {
  const instance = axios.create({ baseURL: BaseUrl })

  instance.interceptors.request.use(async (req) => {
    const session = await getSession()

    if (session) {
      req.headers.Authorization = `Bearer ${session.accessToken}`
    }

    return req
  })
  instance.interceptors.response.use(
    async (res) => res,
    (error) => Promise.reject(error)
  )

  return instance
}
