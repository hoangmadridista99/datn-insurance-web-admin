import { useState, useEffect, useCallback } from 'react'
import {
  createCategory,
  getCategories,
  removeCategory,
  updateCategory,
} from '@services/blog-categories'
import { PAGINATION_INITIAL_VALUES } from '@constants/common'
import { isEmpty } from 'lodash'

const FILTER_INITIAL_VALUES = {
  category_name: null,
}

const useBlogCategories = () => {
  // state
  const [isLoading, setIsLoading] = useState(false)
  const [blogCategories, setBlogCategories] = useState([])
  const [categoryName, setCategoryName] = useState(null)
  const [categoryDetails, setCategoryDetails] = useState(null)
  const [filterValues, setFilterValues] = useState(FILTER_INITIAL_VALUES)
  const [pagination, setPagination] = useState(PAGINATION_INITIAL_VALUES)
  const [isCreateModalVisible, setIsCreateModalVisible] = useState(false)
  const [isUpdateModalVisible, setIsUpdateModalVisible] = useState(false)

  // handle open/close create modal
  const handleOpenCreateModal = () => setIsCreateModalVisible(true)
  const handleCloseCreateModal = () => {
    setIsCreateModalVisible(false)
  }

  // handle open/close update modal
  const handleOpenUpdateModal = (category) => {
    setCategoryDetails(category)
    setIsUpdateModalVisible(true)
  }
  const handleCloseUpdateModal = () => {
    setCategoryDetails(null)
    setIsUpdateModalVisible(false)
  }

  // fetch categories
  const fetchBlogCategories = useCallback(
    async (signal) => {
      try {
        setIsLoading(true)
        const response = await getCategories(
          pagination.currentPage,
          filterValues,
          signal
        )

        if (!isEmpty(response)) {
          setBlogCategories(!isEmpty(response.data) ? response.data : [])
          setPagination(
            !isEmpty(response.meta) ? response.meta : PAGINATION_INITIAL_VALUES
          )
        }
      } catch (error) {
        console.log('Error', error)
      } finally {
        setIsLoading(false)
      }
    },
    [filterValues, pagination.currentPage]
  )

  // handle change input category name
  const handleChangeCategoryName = (event) => {
    const {
      target: { value },
    } = event

    setCategoryName(value)
  }

  // handle submit create form
  const handleSubmitCreate = async (params) => {
    try {
      await createCategory(params)

      handleCloseCreateModal()
      fetchBlogCategories()
    } catch (error) {
      console.log('Error', error)
    }
  }

  // handle submit update form
  const handleSubmitUpdate = async (params) => {
    try {
      await updateCategory(categoryDetails.id, params)

      handleCloseUpdateModal()
      fetchBlogCategories()
    } catch (error) {
      console.log('Error', error)
    }
  }

  // handle submit delete form
  const handleDeleteCategory = useCallback(
    async (category) => {
      try {
        await removeCategory(category.id)

        fetchBlogCategories()
      } catch (error) {
        console.log('Error', error)
      }
    },
    [fetchBlogCategories]
  )

  // handle change page
  const handleChangePage = (value) =>
    setPagination((prevState) => ({ ...prevState, currentPage: value }))

  useEffect(() => {
    const controller = new AbortController()

    fetchBlogCategories(controller.signal)

    return () => controller.abort()
  }, [fetchBlogCategories])

  // set timeout when enter category name to filter
  useEffect(() => {
    let timer = null

    if (categoryName !== null)
      timer = setTimeout(() => {
        setFilterValues((prevState) => ({
          ...prevState,
          category_name: categoryName,
        }))
      }, 1000)

    return () => clearTimeout(timer)
  }, [categoryName])

  return {
    isLoading,
    blogCategories,
    categoryName,
    isCreateModalVisible,
    isUpdateModalVisible,
    categoryDetails,
    pagination,
    handleChangeCategoryName,
    handleOpenCreateModal,
    handleCloseCreateModal,
    handleSubmitCreate,
    handleOpenUpdateModal,
    handleCloseUpdateModal,
    handleSubmitUpdate,
    handleDeleteCategory,
    handleChangePage,
  }
}

export default useBlogCategories
