import { useState, useEffect, useCallback } from 'react'
import { isEmpty } from 'lodash'
import { PAGINATION_INITIAL_VALUES } from '@constants/common'
import { useTranslation } from 'react-i18next'
import { getLifeHealthUnverifiedRatings } from '@services/ratings'
import { handleError } from 'vue'
import { getInsuranceCategories } from '@services/insurances'

const FILTER_INITIAL_VALUES = {
  insurance_name: null,
  insurance_category_label: null,
}

export const useLifeAndHealthRatingLogic = () => {
  const { t } = useTranslation(['vendor', 'common', 'insurances'])

  const [isLoading, setIsLoading] = useState(false)
  const [insuranceCategories, setInsuranceCategories] = useState(null)
  const [pendingRatings, setPendingRatings] = useState([])
  const [insuranceName, setInsuranceName] = useState(null)
  const [pagination, setPagination] = useState(PAGINATION_INITIAL_VALUES)
  const [filterValues, setFilterValues] = useState(FILTER_INITIAL_VALUES)

  const handleOptions = useCallback(
    (list) => {
      if (!list || list.length < 1) return

      return list.map((item) => ({
        value: item.label,
        label: t(`insurances:form:options:${item.label}`),
      }))
    },
    [t]
  )

  const fetchPendingRatings = useCallback(async () => {
    try {
      setIsLoading(true)
      const params = {
        ...filterValues,
        page: pagination.currentPage,
        limit: pagination.itemsPerPage,
      }

      const response = await getLifeHealthUnverifiedRatings(params)

      if (!isEmpty(response)) {
        setPendingRatings(!isEmpty(response.data) ? response.data : [])
        setPagination(
          !isEmpty(response.meta) ? response.meta : PAGINATION_INITIAL_VALUES
        )
      }
    } catch (error) {
      console.log('Error', error)
      handleError(error)
    } finally {
      setIsLoading(false)
    }
  }, [filterValues, pagination.currentPage, pagination.itemsPerPage])

  const fetchInsuranceCategories = useCallback(async () => {
    try {
      const response = await getInsuranceCategories()

      setInsuranceCategories(response)
    } catch (error) {
      console.log('Error', error)
    }
  }, [])

  const handleChangeInsuranceName = (event) => {
    const {
      target: { value },
    } = event

    setInsuranceName(value)
  }

  const handleChangeInsuranceType = (value) => {
    setFilterValues((prevState) => ({
      ...prevState,
      insurance_category_label: value,
    }))
  }

  useEffect(() => {
    let timer = null

    if (insuranceName !== null)
      timer = setTimeout(() => {
        setFilterValues((prevState) => ({
          ...prevState,
          insurance_name: insuranceName,
        }))
      }, 1000)

    return () => clearTimeout(timer)
  }, [insuranceName])

  const handleChangePage = (value) =>
    setPagination((prevState) => ({ ...prevState, currentPage: value }))

  const handleResetFilter = () => {
    setInsuranceName(null)
    setFilterValues(FILTER_INITIAL_VALUES)
  }

  useEffect(() => {
    fetchPendingRatings()
  }, [fetchPendingRatings])

  useEffect(() => {
    fetchInsuranceCategories()
  }, [fetchInsuranceCategories])

  return {
    isLoading,
    pendingRatings,
    insuranceCategories,
    insuranceName,
    filterValues,
    pagination,
    handleChangeInsuranceName,
    handleOptions,
    handleChangeInsuranceType,
    handleResetFilter,
    handleChangePage,
  }
}
