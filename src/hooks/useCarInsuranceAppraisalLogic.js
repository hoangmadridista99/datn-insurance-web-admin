import { useState, useEffect, useCallback } from 'react'
import { isEmpty, isNil } from 'lodash'
import { PAGINATION_INITIAL_VALUES } from '@constants/common'
import { getAppraisals } from '@services/appraisals'
import { useTranslation } from 'next-i18next'

const FILTER_INITIAL_VALUES = {
  insurance_code: null,
  form_status: null,
}

export const useCarInsuranceAppraisalLogic = () => {
  const { t } = useTranslation(['vendor', 'common', 'insurances'])

  const [isLoading, setIsLoading] = useState(false)
  const [appraisalList, setAppraisalList] = useState([])
  const [pagination, setPagination] = useState(PAGINATION_INITIAL_VALUES)
  const [insuranceCode, setInsuranceCode] = useState(null)
  const [filterValues, setFilterValues] = useState(FILTER_INITIAL_VALUES)

  const fetchAppraisalList = useCallback(async () => {
    try {
      setIsLoading(true)
      const params = {
        ...filterValues,
        page: pagination.currentPage,
        limit: pagination.itemsPerPage,
      }

      const response = await getAppraisals(params)

      if (!isEmpty(response)) {
        setAppraisalList(!isEmpty(response.data) ? response.data : [])
        setPagination(
          !isEmpty(response.meta) ? response.meta : PAGINATION_INITIAL_VALUES
        )
      }
    } catch (error) {
      console.log('Error', error)
    } finally {
      setIsLoading(false)
    }
  }, [filterValues, pagination.currentPage, pagination.itemsPerPage])

  const handleOptions = useCallback(
    (list) => {
      if (!list || list.length < 1) return

      return list.map((item) => ({
        value: item,
        label: t(`insurances:form:options:${item}`),
      }))
    },
    [t]
  )

  const handleChangeInsuranceCode = (event) => {
    const {
      target: { value },
    } = event

    setInsuranceCode(value)
  }

  const handleSelectValue = (value, name) =>
    setFilterValues({ ...filterValues, [name]: value })

  const handleResetFilter = () => setFilterValues(FILTER_INITIAL_VALUES)

  const handleChangePage = (value) =>
    setPagination((prevState) => ({ ...prevState, currentPage: value }))

  useEffect(() => {
    let timer = null

    if (!isNil(insuranceCode))
      timer = setTimeout(() => {
        setFilterValues((prevState) => ({
          ...prevState,
          insurance_code: insuranceCode,
        }))
      }, 1000)

    return () => clearTimeout(timer)
  }, [insuranceCode])

  useEffect(() => {
    fetchAppraisalList()
  }, [fetchAppraisalList])

  return {
    isLoading,
    insuranceCode,
    filterValues,
    appraisalList,
    pagination,
    handleChangeInsuranceCode,
    handleOptions,
    handleSelectValue,
    handleResetFilter,
    handleChangePage,
  }
}
