import { useState, useEffect, useCallback, useMemo } from 'react'
import { getBlogs } from '@services/blogs'
import { getCategoriesFromClient } from '@services/blog-categories'
import { isEmpty, isNil } from 'lodash'
import { CREATE_AT } from '@utils/constants/blog'
import { PAGINATION_INITIAL_VALUES } from '@constants/common'

const FILTER_INITIAL_VALUES = {
  title: null,
  created_at: null,
  blog_category_id: null,
  status: null,
}

const MODAL_STATUS_INITIAL_VALUES = {
  create: false,
  update: false,
  preview: false,
  reject: false,
  remove: false,
}

export const useBlogsLogic = ({ role }) => {
  const [blog, setBlog] = useState(null)
  const [blogs, setBlogs] = useState([])
  const [categories, setCategories] = useState([])
  const [isShowFilter, setIsShowFilter] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [blogName, setBlogName] = useState(null)
  const [filterValues, setFilterValues] = useState(FILTER_INITIAL_VALUES)
  const [pagination, setPagination] = useState(PAGINATION_INITIAL_VALUES)
  const [openModals, setOpenModals] = useState(MODAL_STATUS_INITIAL_VALUES)

  const handleToggleFilter = () => setIsShowFilter(!isShowFilter)

  const handleOpenModal = (modalKey, data = null) => {
    setOpenModals((preState) => ({ ...preState, [modalKey]: true }))
    setBlog(data)
  }

  const handleCloseModal = (modalKey) => {
    setOpenModals((preState) => ({ ...preState, [modalKey]: false }))
    setBlog(null)
  }

  const fetchBlogs = useCallback(async () => {
    try {
      setIsLoading(true)
      const newParams = {
        ...filterValues,
        role,
        page: pagination.currentPage,
        limit: pagination.itemsPerPage,
      }

      const response = await getBlogs(newParams)

      if (!isEmpty(response)) {
        setBlogs(!isEmpty(response.data) ? response.data : [])
        setPagination(
          !isEmpty(response.meta) ? response.meta : PAGINATION_INITIAL_VALUES
        )
      }
    } catch (error) {
      console.log('Error', error)
    } finally {
      setIsLoading(false)
    }
  }, [filterValues, pagination.currentPage, pagination.itemsPerPage, role])

  const fetchCategories = useCallback(async (signal) => {
    try {
      const response = await getCategoriesFromClient(signal)

      const updateResponse = response.map((item) => ({
        value: item.id,
        label: `${item.vn_label} - ${item.en_label}`,
      }))

      setCategories(updateResponse)
    } catch (error) {
      console.log('Error', error)
    }
  }, [])

  const handleChangeBlogName = (event) => {
    const {
      target: { value },
    } = event

    setBlogName(value)
  }

  const handleSelectFilter = (key, value) => {
    return setFilterValues((prevState) => ({
      ...prevState,
      [key]: key === CREATE_AT ? value.toISOString() : value,
    }))
  }

  const handleClickResetFilter = () => {
    setFilterValues(FILTER_INITIAL_VALUES)
    setBlogName(null)
  }

  const handleUpdateSuccess = (id, body) => {
    const updateBlogs = blogs.map((item) => {
      if (item.id === id) return { ...item, ...body }
      return item
    })

    setBlogs(updateBlogs)
  }

  const handleChangePage = async (value) =>
    setPagination((prevState) => ({ ...prevState, currentPage: value }))

  // Validate
  const isDisableButtonReset = useMemo(
    () => Object.values(filterValues).every((item) => !item),
    [filterValues]
  )

  useEffect(() => {
    fetchBlogs()
  }, [fetchBlogs])

  useEffect(() => {
    fetchCategories()
  }, [fetchCategories])

  useEffect(() => {
    let timer = null

    if (!isNil(blogName)) {
      timer = setTimeout(
        () =>
          setFilterValues((preFilter) => ({ ...preFilter, title: blogName })),
        1000
      )
    }

    return () => clearTimeout(timer)
  }, [blogName])

  return {
    isLoading,
    isShowFilter,
    isDisableButtonReset,
    blog,
    blogs,
    categories,
    blogName,
    filterValues,
    pagination,
    openModals,
    onOpenModal: handleOpenModal,
    onCloseModal: handleCloseModal,
    onToggleFilter: handleToggleFilter,
    onClickResetFilter: handleClickResetFilter,
    onUpdateSuccess: handleUpdateSuccess,
    onChangeTitle: handleChangeBlogName,
    onSelectFilter: handleSelectFilter,
    onChangePage: handleChangePage,
    handleGetBlogs: fetchBlogs,
  }
}
