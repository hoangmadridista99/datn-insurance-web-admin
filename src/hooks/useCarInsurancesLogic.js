import { useState, useEffect, useCallback } from 'react'
import { isEmpty } from 'lodash'
import { PAGINATION_INITIAL_VALUES } from '@constants/common'
import { handleError } from 'vue'
import { getCarInsurances } from '@services/car-insurances'

export const useCarInsurancesLogic = () => {
  const [isLoading, setIsLoading] = useState(false)
  const [isMandatory, setIsMandatory] = useState(false)
  const [carInsurances, setCarInsurances] = useState([])
  const [previewCarInsuranceId, setPreviewCarInsuranceId] = useState(null)
  const [pagination, setPagination] = useState(PAGINATION_INITIAL_VALUES)

  const handleGetCarInsurances = useCallback(
    async (signal) => {
      try {
        setIsLoading(true)
        const params = {
          page: pagination.currentPage,
          limit: pagination.itemsPerPage,
        }
        const response = await getCarInsurances(params, signal)

        if (!isEmpty(response)) {
          setCarInsurances(!isEmpty(response.data) ? response.data : [])
          setPagination(
            !isEmpty(response.meta) ? response.meta : PAGINATION_INITIAL_VALUES
          )
          setIsMandatory(!!response.is_mandatory)
        }
      } catch (error) {
        handleError(error)
      } finally {
        setIsLoading(false)
      }
    },
    [pagination.currentPage, pagination.itemsPerPage]
  )

  const handleChangePage = (value) =>
    setPagination((prevState) => ({ ...prevState, currentPage: value }))

  const handleClickPreview = (id) => setPreviewCarInsuranceId(id)

  const handleClickClosePreview = () => setPreviewCarInsuranceId(null)

  useEffect(() => {
    const controller = new AbortController()

    handleGetCarInsurances(controller.signal)

    return () => controller.abort()
  }, [handleGetCarInsurances])

  return {
    isLoading,
    carInsurances,
    isMandatory,
    pagination,
    previewCarInsuranceId,
    handleClickPreview,
    handleChangePage,
    handleClickClosePreview,
  }
}
