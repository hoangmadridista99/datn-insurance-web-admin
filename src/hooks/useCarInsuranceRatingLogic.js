import { useState, useEffect, useCallback } from 'react'
import { isEmpty } from 'lodash'
import { PAGINATION_INITIAL_VALUES } from '@constants/common'
import { getCarUnverifiedRatings } from '@services/ratings'
import { handleError } from 'vue'

const FILTER_INITIAL_VALUES = {
  insurance_name: null,
  rating_score: null,
}

export const useCarInsuranceRatingLogic = () => {
  const [isLoading, setIsLoading] = useState(false)
  const [pendingRatings, setPendingRatings] = useState([])
  const [pagination, setPagination] = useState(PAGINATION_INITIAL_VALUES)
  const [insuranceName, setInsuranceName] = useState(null)
  const [filterValues, setFilterValues] = useState(FILTER_INITIAL_VALUES)

  const handleOptions = useCallback((list) => {
    if (!list || list.length < 1) return

    return list.map((item) => ({
      value: item,
      label: item,
    }))
  }, [])

  const fetchPendingRatings = useCallback(async () => {
    try {
      setIsLoading(true)
      const params = {
        page: pagination.currentPage,
        limit: pagination.itemsPerPage,
        ...filterValues,
      }

      const response = await getCarUnverifiedRatings(params)

      if (!isEmpty(response)) {
        setPendingRatings(!isEmpty(response.data) ? response.data : [])
        setPagination(
          !isEmpty(response.meta) ? response.meta : PAGINATION_INITIAL_VALUES
        )
      }
    } catch (error) {
      console.log('Error', error)
      handleError(error)
    } finally {
      setIsLoading(false)
    }
  }, [filterValues, pagination.currentPage, pagination.itemsPerPage])

  const handleChangeInsuranceName = (event) => {
    const {
      target: { value },
    } = event

    setInsuranceName(value)
  }

  const handleChangeInsuranceType = (value) =>
    setFilterValues((prevState) => ({ ...prevState, rating_score: value }))

  useEffect(() => {
    let timer = null

    if (insuranceName !== null)
      timer = setTimeout(() => {
        setFilterValues((prevState) => ({
          ...prevState,
          insurance_name: insuranceName,
        }))
      }, 1000)

    return () => clearTimeout(timer)
  }, [insuranceName])

  const handleChangePage = (value) =>
    setPagination((prevState) => ({ ...prevState, currentPage: value }))

  const handleResetFilter = () => {
    setInsuranceName(null)
    setFilterValues(FILTER_INITIAL_VALUES)
  }

  useEffect(() => {
    fetchPendingRatings()
  }, [fetchPendingRatings])

  return {
    isLoading,
    insuranceName,
    filterValues,
    pendingRatings,
    pagination,
    handleChangeInsuranceName,
    handleOptions,
    handleChangeInsuranceType,
    handleResetFilter,
    handleChangePage,
  }
}
