import { useState, useEffect, useCallback } from 'react'
import { PAGINATION_INITIAL_VALUES } from '@constants/common'
import {
  getBlogComments,
  removeBlogComment,
  updateBlogComment,
} from '@services/blog-comments'
import { handleError } from 'vue'
import { getBlogDetails } from '@services/blogs'
import { isEmpty } from 'lodash'

export const useBlogPreviewModalLogic = ({ blog, onClose }) => {
  const [blogDetails, setBlogDetails] = useState(null)
  const [comments, setComments] = useState(null)
  const [pagination, setPagination] = useState(PAGINATION_INITIAL_VALUES)

  const handleGetBlogDetails = useCallback(async () => {
    try {
      if (blog) {
        const response = await getBlogDetails(blog.id)

        if (!isEmpty(response)) {
          setBlogDetails(response)
        }
      }
    } catch (error) {
      handleError(error)
    }
  }, [blog])

  const handleGetBlogComments = useCallback(
    async (signal) => {
      try {
        if (blog) {
          const params = {
            limit: pagination.itemsPerPage,
            page: pagination.currentPage,
            blog_id: blog.id,
          }

          const response = await getBlogComments(params, signal)

          setComments(response?.data ?? [])
          setPagination((prevState) => ({
            ...prevState,
            totalPages: response?.meta?.totalPages ?? 1,
            itemsPerPage: response?.meta?.itemsPerPage ?? 5,
          }))
        }
      } catch (error) {
        handleError(error)
      }
    },
    [blog, pagination.currentPage, pagination.itemsPerPage]
  )

  useEffect(() => {
    handleGetBlogDetails()
  }, [handleGetBlogDetails])

  useEffect(() => {
    const controller = new AbortController()

    handleGetBlogComments(controller.signal)

    return () => controller.abort()
  }, [handleGetBlogComments])

  const handleCloseModal = () => {
    setComments(null)
    setBlogDetails(null)
    setPagination(PAGINATION_INITIAL_VALUES)
    onClose()
  }

  const handleClickUpdateComment = async (id, isHide) => {
    try {
      if (comments) {
        const body = { is_hide: isHide }
        await updateBlogComment(id, body)
        const updateComments = comments.map((item) => {
          if (item.id === id) return { ...item, is_hide: isHide }
          return item
        })
        setComments(updateComments)
      }
    } catch (error) {
      handleError(error)
    }
  }

  const handleClickRemoveComment = async (id) => {
    try {
      await removeBlogComment(id)

      setPagination((prevState) => ({ ...prevState, currentPage: 1 }))
    } catch (error) {
      handleError(error)
    }
  }

  const handleClickPageComments = (page) =>
    setPagination((prevState) => ({ ...prevState, currentPage: page }))

  return {
    blogDetails,
    comments,
    pagination,
    handleCloseModal,
    handleClickUpdateComment,
    handleClickRemoveComment,
    handleClickPageComments,
  }
}
