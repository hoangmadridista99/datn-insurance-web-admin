import { isEmpty, isNil } from 'lodash'
import { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'next-i18next'
import { getAdmins } from '@services/admin'
import { PAGINATION_INITIAL_VALUES } from '@constants/common'

const FILTER_INITIAL_VALUES = {
  account: null,
  is_blocked: null,
}

export const useOperatorLogic = () => {
  const { t } = useTranslation(['operators', 'common', 'insurances'])

  const [isLoading, setIsLoading] = useState(false)
  const [isModalCreate, setIsModalCreate] = useState(false)
  const [isModalUpdate, setIsModalUpdate] = useState(false)
  const [operators, setOperators] = useState([])
  const [operatorDetails, setOperatorDetails] = useState(null)
  const [pagination, setPagination] = useState(PAGINATION_INITIAL_VALUES)
  const [username, setUsername] = useState()
  const [filterValues, setFilterValues] = useState(FILTER_INITIAL_VALUES)

  const handleGetData = useCallback(async () => {
    try {
      setIsLoading(true)

      const params = {
        ...filterValues,
        is_blocked:
          filterValues.is_blocked !== null ? filterValues.is_blocked : null,
        page: pagination.currentPage,
        limit: pagination.itemsPerPage,
      }

      const response = await getAdmins(params)

      if (!isEmpty(response)) {
        setOperators(!isEmpty(response.data) ? response.data : [])
        setPagination(
          !isEmpty(response.meta) ? response.meta : PAGINATION_INITIAL_VALUES
        )
      }
    } catch (error) {
      console.log('Error', error)
    } finally {
      setIsLoading(false)
    }
  }, [filterValues, pagination.currentPage, pagination.itemsPerPage])

  const handleResetFilter = () => {
    setUsername(null)
    setFilterValues(FILTER_INITIAL_VALUES)
  }

  const handleUpdateSuccess = (id, body) => {
    const updateData = operators.map((item) => {
      if (item.id === id)
        return {
          ...item,
          ...body,
        }

      return item
    })

    setOperators(updateData)
  }

  const handleOpenModalCreate = () => setIsModalCreate(true)
  const handleCloseModalCreate = () => setIsModalCreate(false)

  const handleOpenModalUpdate = (item) => {
    setIsModalUpdate(true)
    setOperatorDetails(item)
  }

  const handleCloseModalUpdate = () => {
    setIsModalUpdate(false)
    setOperatorDetails(null)
  }

  const handleChangePage = (value) =>
    setPagination((prevState) => ({
      ...prevState,
      currentPage: value,
    }))

  const handleChangeAccount = (e) => setUsername(e.target.value)

  const handleSelectStatus = (value) =>
    setFilterValues((prevState) => ({ ...prevState, is_blocked: value }))

  const handleOptions = (list) =>
    list.map((item) => ({
      value: item.value,
      label: t(`operators:fields:${item.label}`),
    }))

  useEffect(() => {
    handleGetData()
  }, [handleGetData])

  useEffect(() => {
    let timer = null

    if (!isNil(username))
      timer = setTimeout(() => {
        setFilterValues((prevState) => ({
          ...prevState,
          account: username,
        }))
      }, 1000)

    return () => clearTimeout(timer)
  }, [username])

  return {
    isLoading,
    operators,
    username,
    filterValues,
    pagination,
    isModalCreate,
    isModalUpdate,
    operatorDetails,
    handleOpenModalCreate,
    handleChangeAccount,
    handleOptions,
    handleSelectStatus,
    handleResetFilter,
    handleOpenModalUpdate,
    handleUpdateSuccess,
    handleGetData,
    handleChangePage,
    handleCloseModalCreate,
    handleCloseModalUpdate,
  }
}
