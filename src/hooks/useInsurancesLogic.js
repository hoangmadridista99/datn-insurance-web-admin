import { useState, useEffect, useCallback } from 'react'
import { getInsuranceCategories, getInsurances } from '@services/insurances'
import { formatCurrency } from '@helpers/formatCurrency'
import { useTranslation } from 'next-i18next'
import { isEmpty, isNil } from 'lodash'
import { PAGINATION_INITIAL_VALUES } from '@constants/common'

const FILTER_INITIAL_VALUES = {
  insurance_name: null,
  insurance_type: null,
  price: null,
  status: null,
}

const INPUT_INITIAL_VALUES = {
  insuranceName: null,
  price: null,
}

function useInsurancesLogic(role) {
  const { t } = useTranslation(['insurances'])

  const [isLoading, setIsLoading] = useState(false)
  const [isFilter, setIsFilter] = useState(false)
  const [insurances, setInsurances] = useState([])
  const [insuranceCategories, setInsuranceCategories] = useState(null)
  const [inputFieldValues, setInputFieldValues] = useState(INPUT_INITIAL_VALUES)
  const [pagination, setPagination] = useState(PAGINATION_INITIAL_VALUES)
  const [filterValues, setFilterValues] = useState(FILTER_INITIAL_VALUES)

  const fetchInsurances = useCallback(async () => {
    try {
      setIsLoading(true)
      const params = {
        ...filterValues,
        page: pagination.currentPage,
        limit: pagination.itemsPerPage,
        role,
      }

      const response = await getInsurances(params)

      if (!isEmpty(response)) {
        setInsurances(!isEmpty(response.data) ? response.data : [])
        setPagination(
          !isEmpty(response.meta) ? response.meta : PAGINATION_INITIAL_VALUES
        )
      }
    } catch (error) {
      console.log('Error', error)
    } finally {
      setIsLoading(false)
    }
  }, [filterValues, pagination.currentPage, pagination.itemsPerPage, role])

  const handleOptions = useCallback(
    (list) => {
      if (!list || list.length < 1) return

      return list.map((item) => ({
        value: item.label,
        label: t(`insurances:form:options:${item.label}`),
      }))
    },
    [t]
  )

  const fetchInsuranceCategories = useCallback(async () => {
    try {
      const response = await getInsuranceCategories()

      setInsuranceCategories(handleOptions(response))
    } catch (error) {
      console.log('Error', error)
    }
  }, [handleOptions])

  const handleOpenMoreFilters = () => setIsFilter((prevState) => !prevState)

  const handleResetFilters = () => {
    setFilterValues(FILTER_INITIAL_VALUES)
    setInputFieldValues(INPUT_INITIAL_VALUES)
  }

  const handleChangePage = (value) =>
    setPagination((prevState) => ({ ...prevState, currentPage: value }))

  const handleChangeInputField = (event) => {
    const {
      target: { value, name },
    } = event

    switch (name) {
      case 'name':
        return setInputFieldValues((prevState) => ({
          ...prevState,
          insuranceName: value,
        }))

      default:
        return setInputFieldValues((prevState) => ({
          ...prevState,
          price: formatCurrency(value.replace(/\./g, '')),
        }))
    }
  }

  const handleSelectValue = (value, _, type) => {
    switch (type) {
      case 'status':
        return setFilterValues((prevState) => ({
          ...prevState,
          status: value,
        }))

      default:
        return setFilterValues((prevState) => ({
          ...prevState,
          insurance_type: value,
        }))
    }
  }

  useEffect(() => {
    fetchInsuranceCategories()
  }, [fetchInsuranceCategories])

  useEffect(() => {
    fetchInsurances()
  }, [fetchInsurances])

  useEffect(() => {
    let timer = null
    if (
      !isNil(inputFieldValues.insuranceName) &&
      !isNil(inputFieldValues.price)
    ) {
      timer = setTimeout(() => {
        setFilterValues((prevState) => ({
          ...prevState,
          insurance_name: inputFieldValues.insuranceName,
        }))

        if (inputFieldValues.price && inputFieldValues.price.length > 0)
          setFilterValues((prevState) => ({
            ...prevState,
            price: inputFieldValues.price.replace(/\./g, ''),
          }))
      }, 1000)
    }

    return () => clearTimeout(timer)
  }, [inputFieldValues])

  // Handles
  const handleUpdateStatusSuccess = (id, body) => {
    const updateData = insurances.map((item) => {
      if (item.id === id) return { ...item, ...body }

      return item
    })
    setInsurances(updateData)
  }

  const handleRemoveSuccess = (id) => {
    const updateData = insurances.filter((item) => item.id !== id)

    setInsurances(updateData)
  }

  return {
    insurances,
    pagination,
    isLoading,
    inputFieldValues,
    isFilter,
    filterValues,
    insuranceCategories,
    handleChangePage,
    handleChangeInputField,
    handleSelectValue,
    handleOpenMoreFilters,
    handleResetFilters,
    onUpdateStatusSuccess: handleUpdateStatusSuccess,
    onRemoveSuccess: handleRemoveSuccess,
  }
}

export default useInsurancesLogic
