import { useState, useEffect, useCallback } from 'react'
import { isEmpty } from 'lodash'
import { PAGINATION_INITIAL_VALUES } from '@constants/common'
import { useTranslation } from 'react-i18next'
import { getInsuranceCategories } from '@services/insurances'
import { getFavoriteList } from '@services/favorite'

const FILTER_INITIAL_VALUES = {
  total_interest: 'DESC',
  insurance_name: null,
  insurance_category_label: null,
}

export const useLifeAndHealthFavoriteLogic = () => {
  const { t } = useTranslation(['vendor', 'common', 'insurances'])

  const [isLoading, setIsLoading] = useState(false)
  const [interestList, setInterestList] = useState()
  const [insuranceCategories, setInsuranceCategories] = useState(null)
  const [insuranceName, setInsuranceName] = useState(null)
  const [filterValues, setFilterValues] = useState(FILTER_INITIAL_VALUES)
  const [pagination, setPagination] = useState(PAGINATION_INITIAL_VALUES)

  const handleOptions = useCallback(
    (list) => {
      if (!list || list.length < 1) return

      return list.map((item) => ({
        value: item.label,
        label: t(`insurances:form:options:${item.label}`),
      }))
    },
    [t]
  )

  const fetchInsuranceCategories = useCallback(async () => {
    try {
      const response = await getInsuranceCategories()

      setInsuranceCategories(response)
    } catch (error) {
      console.log('Error', error)
    }
  }, [])

  const fetchFavoriteList = useCallback(async () => {
    try {
      setIsLoading(true)
      const params = {
        ...filterValues,
        page: pagination.currentPage,
        limit: pagination.itemsPerPage,
      }

      const response = await getFavoriteList(params)

      if (!isEmpty(response)) {
        setInterestList(!isEmpty(response.data) ? response.data : [])
        setPagination(
          !isEmpty(response.meta) ? response.meta : PAGINATION_INITIAL_VALUES
        )
      }
    } catch (error) {
      console.log('Error', error)
    } finally {
      setIsLoading(false)
    }
  }, [filterValues, pagination.currentPage, pagination.itemsPerPage])

  const handleChangeInsuranceName = (event) => {
    const {
      target: { value },
    } = event
    setInsuranceName(value)
  }

  const handleChangeInsuranceType = (value) =>
    setFilterValues((prevState) => ({
      ...prevState,
      insurance_category_label: value,
    }))

  useEffect(() => {
    let timer = null

    if (insuranceName !== null)
      timer = setTimeout(() => {
        setFilterValues((prevState) => ({
          ...prevState,
          insurance_name: insuranceName,
        }))
      }, 1000)

    return () => clearTimeout(timer)
  }, [insuranceName])

  useEffect(() => {
    fetchFavoriteList()
  }, [fetchFavoriteList])

  useEffect(() => {
    fetchInsuranceCategories()
  }, [fetchInsuranceCategories])

  const handleChangePage = (value) =>
    setPagination((prevState) => ({ ...prevState, currentPage: value }))

  const handleResetFilter = () => {
    setInsuranceName(null)
    setFilterValues(FILTER_INITIAL_VALUES)
  }

  const handleSortValues = () => {
    if (interestList.length < 2) return

    switch (filterValues.total_interest) {
      case 'ASC':
        return setFilterValues((prevState) => ({
          ...prevState,
          total_interest: 'DESC',
        }))

      default:
        return setFilterValues((prevState) => ({
          ...prevState,
          total_interest: 'ASC',
        }))
    }
  }

  return {
    isLoading,
    insuranceName,
    insuranceCategories,
    filterValues,
    interestList,
    pagination,
    handleChangeInsuranceName,
    handleOptions,
    handleChangeInsuranceType,
    handleSortValues,
    handleResetFilter,
    handleChangePage,
  }
}
