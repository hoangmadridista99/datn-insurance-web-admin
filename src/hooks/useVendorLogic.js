import { useState, useEffect, useMemo, useCallback } from 'react'
import { getVendors } from '@services/vendor'
import { useTranslation } from 'next-i18next'
import { PAGINATION_INITIAL_VALUES } from '@constants/common'
import { isEmpty } from 'lodash'

const formatData = (data) =>
  [...(data ?? [])].map(({ first_name, last_name, ...result }) => ({
    ...result,
    fullName: `${first_name} ${last_name}`,
  }))

const FILTER_INITIAL_VALUES = {
  company_name: null,
  vendor_status: null,
}

function useVendorLogic() {
  const { t } = useTranslation(['vendor'])

  const [isLoading, setIsLoading] = useState(false)
  const [vendors, setVendors] = useState([])
  const [companyName, setCompanyName] = useState(null)
  const [filterValues, setFilterValues] = useState(FILTER_INITIAL_VALUES)
  const [pagination, setPagination] = useState(PAGINATION_INITIAL_VALUES)

  const handleGetVendors = useCallback(async () => {
    try {
      setIsLoading(true)
      const params = {
        ...filterValues,
        page: pagination.currentPage,
        limit: pagination.itemsPerPage,
      }

      const response = await getVendors(params)

      if (!isEmpty(response)) {
        setVendors(!isEmpty(response.data) ? formatData(response.data) : [])
        setPagination(
          !isEmpty(response.meta) ? response.meta : PAGINATION_INITIAL_VALUES
        )
      }
    } catch (error) {
      console.log('Error', error)
    } finally {
      setIsLoading(false)
    }
  }, [filterValues, pagination.currentPage, pagination.itemsPerPage])

  const handleChangeCompanyName = (event) => {
    const {
      target: { value },
    } = event
    setCompanyName(value)
  }

  const handleSelectVendorStatus = (value) => {
    setFilterValues((prevState) => ({ ...prevState, vendor_status: value }))
  }

  const handleChangePage = (value) =>
    setPagination((prevState) => ({ ...prevState, currentPage: value }))

  const handleResetFilter = () =>
    setFilterValues((prevState) => ({
      ...prevState,
      company_name: null,
      vendor_status: null,
    }))

  const convertStatusOptions = useCallback(
    (list) => {
      if (!list || list.length < 1) return

      return list.map((item) => ({
        value: item,
        label: t(`vendor:select:${item}`),
      }))
    },
    [t]
  )

  useEffect(() => {
    let timer = null

    if (companyName !== null)
      timer = setTimeout(() => {
        setFilterValues((prevState) => ({
          ...prevState,
          company_name: companyName,
        }))
      }, 1000)

    return () => clearTimeout(timer)
  }, [companyName])

  useEffect(() => {
    handleGetVendors()
  }, [handleGetVendors])

  return useMemo(
    () => ({
      isLoading,
      vendors,
      pagination,
      companyName,
      filterValues,
      convertStatusOptions,
      onChangeName: handleChangeCompanyName,
      onResetFilter: handleResetFilter,
      onSelect: handleSelectVendorStatus,
      handleChangePage,
      handleGetVendors,
    }),
    [
      isLoading,
      vendors,
      pagination,
      companyName,
      filterValues,
      convertStatusOptions,
      handleGetVendors,
    ]
  )
}

export default useVendorLogic
