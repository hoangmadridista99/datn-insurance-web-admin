import { ROLES } from '@utils/constants/insurances'
import { withAuth } from 'next-auth/middleware'
import { NextResponse } from 'next/server'

export const config = {
  matcher: [
    '/blogs/:path*',
    '/dashboard',
    '/insurances/:path*',
    '/operators',
    '/ratings-insurance',
    '/vendors',
    '/settings',
  ],
}

export default withAuth(
  function middleware(req) {
    const { token } = req.nextauth

    if (!token) {
      const url = req.nextUrl.clone()
      url.pathname = '/auth/login'

      NextResponse.redirect(url)

      return
    }

    return NextResponse.next()
  },
  {
    callbacks: {
      authorized: ({ token }) =>
        token?.role === ROLES.ADMIN || token?.role === ROLES.SUPER_ADMIN,
    },
  }
)
