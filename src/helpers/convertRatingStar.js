const RATING_SCORE = 5

export const convertRatingStar = (score) => {
  if (!score || typeof score !== 'number' || score < 1)
    return {
      star: 0,
      nonStar: RATING_SCORE,
    }

  if (score >= 5)
    return {
      star: RATING_SCORE,
      nonStar: 0,
    }

  const star = Math.floor(score)
  const nonStar = RATING_SCORE - star

  return {
    star,
    nonStar,
  }
}
