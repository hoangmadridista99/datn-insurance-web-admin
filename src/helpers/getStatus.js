export function getStatus(status) {
  switch (status) {
    case 'accepted':
    case 'approved':
      return 'approved'
    case 'rejected':
      return 'rejected'
    case 'pending':
      return 'pending'
  }
}

export const getAppraisalStatus = (status) => {
  switch (status) {
    case 'approved':
      return 'Thẩm định thành công'
    case 'rejected':
      return 'Thẩm định thất bại'
    case 'pending':
      return 'Chờ thẩm định'
    case 'paid':
      return 'Đã thanh toán'
  }
}
