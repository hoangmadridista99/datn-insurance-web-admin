export const formatCurrency = (value) => {
  const formatter = new Intl.NumberFormat('vi-VN')

  return formatter.format(value)
}

export const formatNumber = (value) => {
  if (!value) return

  const formatter = new Intl.NumberFormat('ja-JP')

  return formatter.format(value)
}
