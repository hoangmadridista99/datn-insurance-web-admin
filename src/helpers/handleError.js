import { notification } from 'antd'
import { signOut } from 'next-auth/react'

export const handleError = (error) => {
  if (error.name === 'AxiosError') {
    const { response } = error

    if (response.status === 401 || response.status === 403) return signOut()

    notification.error({ message: response?.data?.error_code ?? 'Lỗi máy chủ' })
  }
}
