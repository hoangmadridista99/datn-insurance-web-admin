export const convertUrl = (url) => {
  const key = '?key=pdf/'
  const [homepage, name] = url.split(key)
  const link = `${homepage}/download${key}${name}`

  return { name, link }
}
