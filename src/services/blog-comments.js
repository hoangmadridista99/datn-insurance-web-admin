import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'

const axios = getAxios()

export const getBlogComments = async (params, signal) => {
  const response = await axios.get(
    ENDPOINTS.BLOG.COMMENTS,
    {
      params: { ...params },
    },
    { signal }
  )

  return response.data
}

export const updateBlogComment = async (id, body, signal) => {
  const url = `${ENDPOINTS.BLOG.COMMENTS}/${id}`
  return await axios.patch(url, body, { signal })
}

export const removeBlogComment = async (id, signal) => {
  const url = `${ENDPOINTS.BLOG.COMMENTS}/${id}`
  return await axios.delete(url, { signal })
}
