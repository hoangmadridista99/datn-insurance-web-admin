import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'

const axios = getAxios()

export const createBlog = async (body, signal) => {
  return await axios.post(ENDPOINTS.BLOGS, body, { signal })
}

export const getBlogs = async (params) => {
  const response = await axios.get(ENDPOINTS.BLOGS, {
    params: {
      ...params,
    },
  })
  return response.data
}

export const getBlogDetails = async (id) => {
  const response = await axios.get(ENDPOINTS.BLOG_DETAILS(id), {})

  return response.data
}

export const updateBlog = async (id, body, signal) => {
  return await axios.patch(ENDPOINTS.BLOG_DETAILS(id), body, { signal })
}

export const updateStatusBlog = async (id, body, signal) => {
  const url = `${ENDPOINTS.BLOGS}/${id}/verify`
  return await axios.patch(url, body, { signal })
}

export const removeBlog = async (id, signal) => {
  const url = `${ENDPOINTS.BLOGS}/${id}`
  return await axios.delete(url, { signal })
}
