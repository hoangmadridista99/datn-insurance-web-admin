import { getAxios } from '@utils/axios'

export const updateOperator = async (id, body, signal) => {
  const axios = getAxios()
  return await axios.patch(`/v1/api/admin/users/${id}`, body, {
    signal,
  })
}
