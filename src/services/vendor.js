import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'
const axios = getAxios()

export const getVendors = async (params) => {
  const response = await axios.get(ENDPOINTS.VENDORS, { params: { ...params } })

  return response.data
}

export const updateVendor = async (id, params) => {
  return await axios.patch(ENDPOINTS.UPDATE_VENDOR_USER(id), params)
}
