const BaseUrl = process.env.NEXT_PUBLIC_BASE_URL

export const getInsuranceObjectives = async () => {
  const url = `${BaseUrl}/v1/api/client/insurance-objectives`
  return (await fetch(url)).json()
}
