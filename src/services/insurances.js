import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'

const axios = getAxios()

export const getInsurances = async (params) => {
  const response = await axios.get(ENDPOINTS.INSURANCES, {
    params: { ...params },
  })

  return response.data
}

export const createInsurance = async (body, signal) => {
  return await axios.post(ENDPOINTS.INSURANCES, body, { signal })
}

export const updateInsurance = async (id, body, signal) => {
  const url = `${ENDPOINTS.INSURANCES}/${id}`
  return await axios.patch(url, body, { signal })
}

export const updateStatus = async (id, body, signal) => {
  const url = `${ENDPOINTS.INSURANCES}/${id}/verify`
  return await axios.patch(url, body, { signal })
}

export const removeInsurance = async (id, signal) => {
  const url = `${ENDPOINTS.INSURANCES}/${id}`
  return await axios.delete(url, { signal })
}

export const getHospitalLinkedList = async () => {
  const response = await axios.get(ENDPOINTS.HOSPITAL_LINKED, {})

  return response.data
}

export const getInsuranceCategories = async () => {
  const response = await axios.get(ENDPOINTS.INSURANCE_CATEGORIES, {})

  return response.data
}

export const getInsuranceDetails = async (id) => {
  const response = await axios.get(ENDPOINTS.INSURANCE_DETAILS(id))

  return response.data
}

export const postCarCost = async (params) => {
  const response = await axios.post(
    ENDPOINTS.GET_INSURANCES_BY_CAR_COST,
    params
  )

  return response
}
