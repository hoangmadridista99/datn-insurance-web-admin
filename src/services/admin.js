import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'
const axios = getAxios()

export const createAdmin = async (body, signal) => {
  return await axios.post(ENDPOINTS.SUPPER_ADMIN, body, { signal })
}

export const getAdmins = async (params) => {
  const response = await axios.get(ENDPOINTS.SUPPER_ADMIN, {
    params: {
      ...params,
    },
  })
  return response.data
}

export const updateAdmin = async (id, body, signal) => {
  const url = `${ENDPOINTS.SUPPER_ADMIN}/${id}`
  return await axios.patch(url, body, { signal })
}

export const removeAdmin = async (id, signal) => {
  const url = `${ENDPOINTS.SUPPER_ADMIN}/${id}`
  return await axios.delete(url, { signal })
}
