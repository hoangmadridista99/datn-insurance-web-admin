import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'
const axios = getAxios()

export const getLifeHealthUnverifiedRatings = async (params) => {
  console.log('🚀 ~ params:', params)
  const response = await axios.get(ENDPOINTS.RATINGS.UNVERIFIED, {
    params: { ...params },
  })

  return response.data
}

// export const getRatingsByInsurance = async (id, page, signal) => {
//   const url = `${ENDPOINTS.RATINGS.INSURANCE}/${id}?page=${page}&limit=5`
//   const response = await axios.get(url, { signal })
//   return response.data
// }

export const updateLifeHealthUnverifiedRating = async (id, body) => {
  return await axios.patch(
    ENDPOINTS.RATINGS.UPDATE_LIFE_HEALTH_UNVERIFIED(id),
    body
  )
}

export const deleteLifeHealthUnverifiedRating = async (id) => {
  return await axios.delete(ENDPOINTS.RATINGS.DELETE_LIFE_HEALTH_UNVERIFIED(id))
}

// Car ratings
export const getCarUnverifiedRatings = async (params) => {
  const response = await axios.get(ENDPOINTS.RATINGS.CAR_UNVERIFIED, {
    params: { ...params },
  })

  return response.data
}

export const updateCarUnverifiedRating = async (id, body) => {
  return await axios.patch(ENDPOINTS.RATINGS.UPDATE_CAR_UNVERIFIED(id), body)
}

export const deleteCarUnverifiedRating = async (id) => {
  return await axios.delete(ENDPOINTS.RATINGS.DELETE_CAR_UNVERIFIED(id))
}
