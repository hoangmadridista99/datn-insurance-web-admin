import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'

const axios = getAxios()

export const createCarInsurance = async (body, signal) => {
  return await axios.post(ENDPOINTS.CAR_INSURANCES, body, { signal })
}

export const getCarInsurances = async (params, signal) => {
  const response = await axios.get(
    ENDPOINTS.CAR_INSURANCES,
    {
      params: { ...params },
    },
    signal
  )
  return response.data
}

export const getCarInsuranceDetails = async (id, signal) => {
  const url = `${ENDPOINTS.CAR_INSURANCES}/${id}`
  const response = await axios.get(url, { signal })
  return response.data
}

export const updateCarInsurance = async (id, body, signal) => {
  const url = `${ENDPOINTS.CAR_INSURANCES}/${id}`
  const response = await axios.patch(url, body, { signal })
  return response.data
}

export const deleteCarInsurance = async (id, signal) => {
  const url = `${ENDPOINTS.CAR_INSURANCES}/${id}`
  return await axios.delete(url, { signal })
}
