import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'

const axios = getAxios()

export const getFavoriteList = async (params) => {
  const response = await axios.get(ENDPOINTS.INSURANCES_FAVORITE, {
    params: { ...params },
  })

  return response.data
}

export const getFavoriteListItem = async (id, params) => {
  const response = await axios.get(ENDPOINTS.INSURANCE_FAVORITE_DETAILS(id), {
    params: {
      ...params,
    },
  })

  return response.data
}
