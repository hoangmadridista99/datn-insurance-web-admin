import { ENDPOINTS } from '@utils/constants/api'
import { getAxios } from '@utils/axios'

const axios = getAxios()

export const updateProfile = async (body) => {
  const response = await axios.patch(ENDPOINTS.UPDATE_PROFILE, body)

  return response.data
}

export const changePassword = async (body) => {
  return await axios.patch(ENDPOINTS.CHANGE_PASSWORD, body)
}
