import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'

const axios = getAxios()

export const createCarInsuranceSettingsPhysical = async (body) => {
  return await axios.post(ENDPOINTS.CAR_INSURANCE_SETTINGS.PHYSICAL, body)
}

export const updateCarInsuranceSettingsPhysical = async (id, body) => {
  const url = ENDPOINTS.CAR_INSURANCE_SETTINGS.UPDATE_PHYSICAL(id)
  return await axios.patch(url, body)
}

export const createCarInsuranceSettingsMandatory = async (body) => {
  return await axios.post(ENDPOINTS.CAR_INSURANCE_SETTINGS.MANDATORY, body)
}

export const getCarInsuranceSettingMandatorDetails = async (signal) => {
  const response = await axios.get(
    ENDPOINTS.CAR_INSURANCE_SETTINGS.MANDATOR_DETAILS,
    { signal }
  )
  return response.data
}
