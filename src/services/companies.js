import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'

export const getCompanies = async () => {
  try {
    const axios = getAxios()
    const response = await axios.get(ENDPOINTS.COMPANIES_CLIENT)
    return response.data
  } catch (_) {
    return []
  }
}
