import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'

const axios = getAxios()

export const getCategories = async (page, filterValues, signal) => {
  const url = `${ENDPOINTS.BLOG.CATEGORIES}?limit=10&page=${page}`
  const response = await axios.get(url, { params: filterValues }, { signal })

  return response.data
}

export const getCategoriesFromClient = async (signal) => {
  const response = await axios.get(ENDPOINTS.BLOG.CLIENT_CATEGORIES, { signal })

  return response.data
}

export const createCategory = async (params, signal) => {
  return await axios.post(ENDPOINTS.BLOG.CATEGORIES, params, { signal })
}

export const updateCategory = async (id, body, signal) => {
  const url = `${ENDPOINTS.BLOG.CATEGORIES}/${id}`
  return await axios.patch(url, body, { signal })
}

export const removeCategory = async (id, signal) => {
  const url = `${ENDPOINTS.BLOG.CATEGORIES}/${id}`
  return axios.delete(url, { signal })
}
