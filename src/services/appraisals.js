import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'

const axios = getAxios()

export const getAppraisals = async (params) => {
  const response = await axios.get(ENDPOINTS.APPRAISALS, {
    params: { ...params },
  })

  return response.data
}

export const getAppraisalDetails = async (recordId, signal) => {
  const url = `${ENDPOINTS.APPRAISALS}/${recordId}`
  const response = await axios.get(url, { signal })

  return response.data
}

export const patchAppraisalApproved = async (recordId, params, signal) => {
  const response = await axios.patch(
    ENDPOINTS.UPDATE_APPROVE_APPRAISAL(recordId),
    {
      ...params,
      signal,
    }
  )

  return response.data
}

export const patchAppraisalRejected = async (recordId, params, signal) => {
  const response = await axios.patch(
    ENDPOINTS.UPDATE_REJECT_APPRAISAL(recordId),
    {
      ...params,
      signal,
    }
  )

  return response
}
