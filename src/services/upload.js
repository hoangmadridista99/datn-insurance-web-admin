import { getAxios } from '@utils/axios'
import { ENDPOINTS } from '@utils/constants/api'

export const uploadBlogBanner = async (body, signal) => {
  const axios = getAxios()
  const response = await axios.post(ENDPOINTS.UPLOAD.BLOG_BANNER, body, {
    signal,
  })
  return response.data
}

export const uploadPdf = async (body) => {
  const axios = getAxios()
  const response = await axios.post(ENDPOINTS.UPLOAD.PDF, body)
  return response.data
}
