import { getFavoriteListItem } from '@services/favorite'
import {
  BulletSvg,
  CloseIconSvg,
  SortAscendingSvg,
  SortDescendingSvg,
} from '@utils/icons'
import { Empty, Modal } from 'antd'
import Heart from 'components/Icon/Heart'
import Image from 'next/image'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'next-i18next'
import { formatNumber } from '@helpers/formatCurrency'
import CustomizePagination from 'components/Pagination'
import { PAGINATION_INITIAL_VALUES, SORTING } from '@constants/common'
import { isEmpty } from 'lodash'

const FILTER_INITIAL_VALUES = {
  sort_created_at: SORTING.DESC,
}

const InterestDetailsModal = ({
  isModalOpen,
  handleCancel,
  interestItem,
  formatDate,
}) => {
  const { t } = useTranslation(['insurances'])

  const [interestedUsers, setInterestedUsers] = useState()
  const [pagination, setPagination] = useState(PAGINATION_INITIAL_VALUES)
  const [filterValues, setFilterValues] = useState(FILTER_INITIAL_VALUES)

  const handleGetFavoriteListItems = useCallback(async () => {
    try {
      if (!interestItem?.id) return

      const params = {
        ...filterValues,
        page: pagination.currentPage,
        limit: pagination.itemsPerPage,
      }

      const response = await getFavoriteListItem(interestItem?.id, params)

      if (!isEmpty(response)) {
        setInterestedUsers(!isEmpty(response.data) ? response.data : [])
        setPagination(
          !isEmpty(response.meta) ? response.meta : PAGINATION_INITIAL_VALUES
        )
      }
    } catch (error) {
      console.log('Error', error)
    }
  }, [
    filterValues,
    interestItem?.id,
    pagination.currentPage,
    pagination.itemsPerPage,
  ])

  const handleSortValues = () => {
    if (interestedUsers.length < 2) return

    switch (filterValues.sort_created_at) {
      case SORTING.ASC:
        return setFilterValues((prevState) => ({
          ...prevState,
          sort_created_at: SORTING.DESC,
        }))

      default:
        return setFilterValues((prevState) => ({
          ...prevState,
          sort_created_at: SORTING.ASC,
        }))
    }
  }

  const handleChangePage = (value) =>
    setPagination((prevState) => ({ ...prevState, currentPage: value }))

  useEffect(() => {
    handleGetFavoriteListItems()
  }, [handleGetFavoriteListItems])

  return (
    <Modal
      open={isModalOpen}
      footer={null}
      onCancel={handleCancel}
      width={820}
      closeIcon={<CloseIconSvg />}
      centered
    >
      <div className="px-2 py-3">
        <div className="mb-4 border-0 border-b border-solid border-ghost-white">
          <p className="mb-4 text-center text-xl font-medium text-arsenic">
            {t('insurances:modal:interest_list:title')}
          </p>
        </div>

        <div className="mb-6 flex items-end justify-between rounded-xl">
          <div className="flex items-center">
            <Image
              src={interestItem?.company?.logo}
              width={48}
              height={48}
              alt="Company"
            />
            <div className="ml-4 flex flex-col justify-between">
              <p className="text-base font-bold text-arsenic">
                {interestItem?.name}
              </p>
              <span className="text-sm text-nickel">
                {t(
                  `insurances:form:options:${interestItem?.insurance_category?.label}`
                )}
              </span>
            </div>
          </div>

          <div className="flex gap-2">
            <Heart />
            <p className="text-sm font-normal text-arsenic">
              {formatNumber(interestItem?.total_interest)}&nbsp;
              {t('insurances:modal:interest_list:interested_person')}
            </p>
          </div>
        </div>

        <div className="mb-2 flex items-center justify-between px-4 text-base font-bold text-metallic">
          <p className="w-8/10">
            {t('insurances:modal:interest_list:interested_by')}
          </p>

          <p className="item-center flex flex-1 justify-between">
            {t('insurances:modal:interest_list:time')}
            <button
              type="ghost"
              className="cursor-pointer border-none bg-transparent"
              onClick={handleSortValues}
              disabled={isEmpty(interestedUsers)}
            >
              {filterValues.sort_created_at === SORTING.DESC ? (
                <SortDescendingSvg />
              ) : (
                <SortAscendingSvg />
              )}
            </button>
          </p>
        </div>

        {!isEmpty(interestedUsers) && (
          <>
            <div className="mb-4 w-full rounded-xl border border-solid border-platinum p-4">
              {isEmpty(interestedUsers) && <Empty className="mt-20" />}

              <div className="no-scrollbar flex max-h-[50vh] min-h-[50vh] w-full flex-col gap-3 overflow-hidden overflow-y-auto">
                {interestedUsers?.map((interestedUser) => (
                  <div
                    key={interestedUser?.id}
                    className="flex items-center justify-between border-0 border-b border-solid border-ghost-white pb-3"
                  >
                    <div className="flex w-8/10 flex-col gap-1.5">
                      <p className="text-sm font-bold text-arsenic">
                        {interestedUser?.user?.first_name}
                      </p>

                      <p className="inline-flex text-sm font-normal text-nickel">
                        {interestedUser?.user?.phone}
                        <span className="mx-1.5 flex items-center">
                          <BulletSvg />
                        </span>
                        {interestedUser?.user?.email}
                      </p>
                    </div>

                    <p className="flex-1 text-sm font-normal text-nickel">
                      {formatDate({ date: interestedUser?.created_at })}
                    </p>
                  </div>
                ))}
              </div>
            </div>

            <CustomizePagination
              pagination={pagination}
              onChangePage={handleChangePage}
            />
          </>
        )}
      </div>
    </Modal>
  )
}

export default InterestDetailsModal
