import { Button, Modal } from 'antd'
import Image from 'next/image'
import React from 'react'
import RatingStar from './Star'
import dayjs from 'dayjs'
import { useTranslation } from 'next-i18next'
import { CloseIconSvg } from '@utils/icons'

const RatingDetailsModal = ({
  pendingRating,
  isPreview,
  isApproved,
  isRejected,
  handleCloseModalPreview,
  handleClickRejected,
  handleClickApproved,
}) => {
  const { t } = useTranslation(['insurances'])

  return (
    <Modal
      centered
      open={isPreview}
      onCancel={handleCloseModalPreview}
      destroyOnClose
      footer={null}
      width={'50%'}
      closeIcon={<CloseIconSvg />}
    >
      <div className="p-8">
        <p className=" mb-4 border-0 border-b border-solid border-ghost-white pb-4 text-center text-xl font-medium text-arsenic">
          Chi tiết đánh giá bảo hiểm
        </p>

        <div className="mb-4 flex items-center">
          <Image
            src={pendingRating.insurance.company.logo}
            width={64}
            height={64}
            alt="Company"
          />

          <div className="ml-6">
            <p className="mb-1 text-base font-bold text-arsenic">
              {pendingRating?.insurance?.name}
            </p>
            {pendingRating?.insurance?.label && (
              <span className="text-sm text-ultramarine-blue">
                {t(
                  `insurances:form:options:${pendingRating?.insurance?.insurance_type}`
                )}
              </span>
            )}
          </div>
        </div>

        <div className="mb-3 flex items-center">
          <p className="mr-4 text-sm text-spanish-gray">Đánh giá sao:</p>
          <RatingStar score={pendingRating?.score} />
        </div>

        <p className="text-sm text-nickel">{pendingRating?.comment}</p>

        <div className="mb-8 flex items-center justify-between">
          <div className="flex items-center text-spanish-gray">
            <p className="mr-1 text-sm">Tạo bởi:</p>
            <p className="text-sm font-bold text-nickel">{`${pendingRating.user.first_name} ${pendingRating.user.last_name}`}</p>
          </div>

          <p className="text-sm font-bold text-nickel">
            {dayjs(pendingRating.created_at).format('DD/MM/YYYY - HH/mm')}
          </p>
        </div>

        <div className="flex items-center justify-end gap-6">
          <Button
            className="border-nickel px-10 text-base font-bold text-nickel hover:border-ultramarine-blue"
            type="default"
            size="large"
            disabled={isApproved || isRejected}
            loading={isRejected}
            onClick={handleClickRejected}
          >
            Từ chối
          </Button>

          <Button
            className="px-10 text-base font-bold"
            size="large"
            type="primary"
            disabled={isApproved || isRejected}
            loading={isApproved}
            onClick={handleClickApproved}
          >
            Chấp nhận
          </Button>
        </div>
      </div>
    </Modal>
  )
}

export default RatingDetailsModal
