import React from 'react'

import StartIcon from '@components/Icon/Star'
import { convertRatingStar } from '@helpers/convertRatingStar'

const RatingStar = ({ score }) => {
  const { star, nonStar } = convertRatingStar(score)

  return (
    <div className="flex items-center">
      {[...Array(star)].map((_, i) => (
        <StartIcon key={`star-active-${i}`} isActive />
      ))}
      {[...Array(nonStar)].map((_, i) => (
        <StartIcon key={`star-non-active-${i}`} />
      ))}
    </div>
  )
}

export default RatingStar
