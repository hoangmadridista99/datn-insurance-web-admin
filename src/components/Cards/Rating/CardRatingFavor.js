import React, { useState } from 'react'
import Image from 'next/image'
import dayjs from 'dayjs'
import Heart from 'components/Icon/Heart'
import { Button } from 'antd'
import { useTranslation } from 'next-i18next'
import InterestDetailsModal from './InterestDetailsModal'

const CardRatingFavor = ({ interestItem }) => {
  const { t } = useTranslation(['insurances'])

  const [isModalVisible, setIsModalVisible] = useState(false)

  const formatDate = ({ date }) => {
    return dayjs(date).format('DD/MM/YYYY - HH:mm')
  }

  const handleOpenModal = () => setIsModalVisible(true)

  const handleCloseModal = () => setIsModalVisible(false)

  return (
    <>
      <div className="mb-4 flex items-center rounded-xl border border-solid border-platinum p-4 last:mb-0">
        <div className="flex w-3/12 items-center">
          <Image
            src={interestItem?.company?.logo}
            className="object-contain"
            width={48}
            height={48}
            alt="Company"
          />
          <div className="ml-4">
            <p className="mb-1 text-base font-bold text-arsenic">
              {interestItem?.name}
            </p>

            {interestItem?.insurance_category?.label && (
              <span className="text-sm font-normal text-nickel">
                {t(
                  `insurances:form:options:${interestItem?.insurance_category?.label}`
                )}
              </span>
            )}
          </div>
        </div>

        <div className="flex w-4/12 gap-2 pr-6">
          <Heart />
          <p className="text-sm font-normal text-arsenic">
            <span>{interestItem?.total_interest}&nbsp;</span>
            {interestItem?.total_interest < 2
              ? t('insurances:interested.fields.one_person_interested')
              : t(
                  'insurances:interested.fields.more_than_one_person_interested'
                )}
          </p>
        </div>

        <div className="w-3/12">
          <p className="mb-1 text-sm font-bold text-nickel">
            {formatDate({ date: interestItem?.user_latest?.updated_at })}
          </p>
          <div className="flex items-center">
            <p className="mr-1 text-xs text-spanish-gray">
              {t('insurances:interested.fields.interested_by')}
            </p>
            <p className="text-sm font-bold text-spanish-gray">{`${interestItem?.user_latest?.user?.first_name}`}</p>
          </div>
        </div>

        <div className="flex w-2/12 justify-end">
          <Button
            type="default"
            className="rounded-lg px-4 text-base text-nickel hover:text-azure"
            onClick={handleOpenModal}
            size="large"
          >
            {t('insurances:interested.button.see_more')}
          </Button>

          {isModalVisible && (
            <InterestDetailsModal
              isModalOpen={isModalVisible}
              handleCancel={handleCloseModal}
              interestItem={interestItem}
              formatDate={formatDate}
            />
          )}
        </div>
      </div>
    </>
  )
}

export default CardRatingFavor
