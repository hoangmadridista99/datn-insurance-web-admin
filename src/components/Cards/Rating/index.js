import React, { useState, useCallback } from 'react'
import Image from 'next/image'
import { Dropdown } from 'antd'
import dayjs from 'dayjs'

import RatingStar from './Star'

import {
  deleteLifeHealthUnverifiedRating,
  updateLifeHealthUnverifiedRating,
} from '@services/ratings'
import { handleError } from '@helpers/handleError'
import { useTranslation } from 'next-i18next'
import RatingDetailsModal from './RatingDetailsModal'
import ArrowDown from 'components/Icon/ArrowDown'

const CardRating = ({ onRefresh, ...pendingRating }) => {
  const { t } = useTranslation(['insurances'])

  const [isPreview, setIsPreview] = useState(false)
  const [isApproved, setIsApproved] = useState(false)
  const [isRejected, setIsRejected] = useState(false)

  const handleApproveRating = useCallback(async () => {
    try {
      setIsApproved(true)
      const body = {
        is_verified_by_admin: true,
      }

      await updateLifeHealthUnverifiedRating(pendingRating?.id, body)

      onRefresh()
      handleCloseModalPreview()
    } catch (error) {
      handleError(error)
    } finally {
      setIsApproved(false)
    }
  }, [onRefresh, pendingRating?.id])

  const handleRejectRating = useCallback(async () => {
    try {
      setIsRejected(true)

      await deleteLifeHealthUnverifiedRating(pendingRating?.id)

      onRefresh()
      handleCloseModalPreview()
    } catch (error) {
      handleError(error)
    } finally {
      setIsRejected(false)
    }
  }, [onRefresh, pendingRating?.id])

  const handleOpenModalPreview = () => setIsPreview(true)
  const handleCloseModalPreview = () => setIsPreview(false)

  // const handleClickApproved = () => setIsApproved(true)
  // const handleClickRejected = () => setIsRejected(true)

  const items = [
    {
      key: 'preview',
      label: (
        <p
          className="cursor-pointer text-base
    text-nickel transition duration-300 hover:text-ultramarine-blue"
          onClick={handleOpenModalPreview}
        >
          {t('insurances:card:preview')}
        </p>
      ),
    },
    {
      key: 'approved',
      label: (
        <p
          className="cursor-pointer rounded-none
    text-base text-nickel transition duration-300 hover:text-ultramarine-blue"
          onClick={handleApproveRating}
        >
          {t('insurances:preview:approve')}
        </p>
      ),
    },
    {
      key: 'rejected',
      label: (
        <p
          className="cursor-pointer text-base
    text-nickel transition duration-300 hover:text-ultramarine-blue"
          onClick={handleRejectRating}
        >
          {t('insurances:preview:reject')}
        </p>
      ),
    },
  ]

  const isDisableDropdown = isPreview || isApproved || isRejected

  return (
    <>
      <div className="mb-4 flex items-center rounded-xl border border-solid border-platinum p-4">
        <div className="flex w-3/12 items-center">
          <Image
            src={pendingRating?.insurance?.company?.logo}
            width={48}
            height={48}
            alt="Company"
          />

          <div className="flex-1 px-4">
            <p className="mb-1 text-base font-bold text-arsenic">
              {pendingRating?.insurance?.name}
            </p>
            {pendingRating?.insurance?.insurance_category?.label && (
              <span className="text-sm text-nickel">
                {t(
                  `insurances:form:options:${pendingRating?.insurance?.insurance_category?.label}`
                )}
              </span>
            )}
          </div>
        </div>

        <div className="w-4/12 pr-6">
          <p className="mb-2 truncate text-sm text-nickel">
            {pendingRating?.comment}
          </p>

          <RatingStar score={pendingRating?.score} />
        </div>

        <div className="w-3/12">
          <p className="mb-1 text-sm font-bold text-nickel">
            {dayjs(pendingRating?.created_at).format('DD/MM/YYYY')}
          </p>

          <div className="flex items-center text-spanish-gray">
            <p className="mr-1 text-xs">Tạo bởi:</p>
            <p className="text-sm font-bold">{`${pendingRating.user.first_name} ${pendingRating.user.last_name}`}</p>
          </div>
        </div>

        <div className="w-2/12">
          <Dropdown
            disabled={isDisableDropdown}
            placement="bottom"
            menu={{ items }}
            trigger={['click']}
          >
            <button className="group flex cursor-pointer items-center justify-center rounded-lg border border-nickel bg-transparent px-4 py-2 text-nickel transition hover:border-ultramarine-blue [&_span]:hover:text-ultramarine-blue [&_svg]:order-1 [&_svg]:hover:stroke-ultramarine-blue">
              <p className="mr-2 text-base group-hover:text-ultramarine-blue">
                {t('insurances:card:action')}
              </p>

              <ArrowDown />
            </button>
          </Dropdown>
        </div>
      </div>

      <RatingDetailsModal
        pendingRating={pendingRating}
        isPreview={isPreview}
        isApproved={isApproved}
        isRejected={isRejected}
        handleCloseModalPreview={handleCloseModalPreview}
        handleClickRejected={handleRejectRating}
        handleClickApproved={handleApproveRating}
      />
    </>
  )
}

export default CardRating
