import { useState } from 'react'
import { notification } from 'antd'
import { updateStatus, removeInsurance } from '@services/insurances'
import { useRouter } from 'next/router'
import { ROLES, STATUSES } from '@utils/constants/insurances'
import { useTranslation } from 'next-i18next'

export const useCardInsuranceLogic = ({
  onUpdateStatusSuccess,
  onRemoveSuccess,
  ...data
}) => {
  const { t } = useTranslation('insurances')
  const router = useRouter()

  const [reason, setReason] = useState()
  const [isLoading, setIsLoading] = useState(false)
  const [isPreview, setIsPreview] = useState(false)
  const [isModalReject, setIsModalReject] = useState(false)
  const [isModalRemove, setIsModalRemove] = useState(false)

  const openPreviewModal = () => setIsPreview(true)
  const closePreviewModal = () => setIsPreview(false)

  const handleOpenModalReject = () => setIsModalReject(true)
  const handleCloseModalReject = () => {
    setReason(null)
    setIsModalReject(false)
  }

  const handleOpenModalRemove = () => setIsModalRemove(true)
  const handleCloseModalRemove = () => setIsModalRemove(false)

  const handleUpdateInsuranceStatus = async (status) => {
    try {
      setIsLoading(true)
      const params = { status, reason: reason || null }
      await updateStatus(data.id, params)

      onUpdateStatusSuccess(data.id, params)
      notification.success({
        message: t('insurances:message.update_status_success'),
      })
    } catch (error) {
      console.log('Error', error)
      notification.error({
        message: t('insurances:message.update_status_failed'),
      })
    } finally {
      if (status === STATUSES.REJECTED && reason) {
        handleCloseModalReject()
      }
      setIsLoading(false)
    }
  }

  const handleRemoveInsurance = async () => {
    try {
      setIsLoading(true)
      await removeInsurance(data.id)

      onRemoveSuccess(data.id)
      notification.success({
        message: t('insurances:message.delete_success'),
      })
    } catch (error) {
      console.log('Error', error)
      notification.success({
        message: t('insurances:message.delete_failed'),
      })
    } finally {
      setIsLoading(false)
      setIsModalRemove(false)
    }
  }

  const handleClickUpdate = () => {
    router.push(`/insurances/update-insurance/${data?.id}`)
  }

  const handleChangeReason = (event) => {
    const {
      target: { value },
    } = event

    setReason(value)
  }

  // Validator
  const itemsUpdate = [
    {
      key: 'update',
      label: (
        <p
          className="cursor-pointer
    text-nickel transition duration-300 hover:text-azure"
          onClick={handleClickUpdate}
        >
          {t('insurances:card.edit')}
        </p>
      ),
    },
  ]

  const itemsDelete = [
    {
      key: 'delete',
      label: (
        <p
          className="cursor-pointer
    text-nickel transition duration-300 hover:text-azure"
          onClick={handleOpenModalRemove}
        >
          {t('insurances:card.delete')}
        </p>
      ),
    },
  ]

  const itemsApprove = [
    {
      key: 'approved',
      label: (
        <p
          className="cursor-pointer
    text-nickel transition duration-300 hover:text-azure"
          onClick={() => handleUpdateInsuranceStatus(STATUSES.APPROVED)}
        >
          {t('insurances:card.approve')}
        </p>
      ),
    },
    {
      key: 'rejected',
      label: (
        <p
          className="cursor-pointer
    text-nickel transition duration-300 hover:text-azure"
          onClick={handleOpenModalReject}
        >
          {t('insurances:card.reject')}
        </p>
      ),
    },
  ]

  const itemsWaitingForApproved = [
    {
      key: 'waitingForApproved',
      label: (
        <p
          className="cursor-pointer
    text-nickel transition duration-300 hover:text-azure"
          onClick={() => handleUpdateInsuranceStatus(STATUSES.PENDING)}
        >
          {t('insurances:card.pending')}
        </p>
      ),
    },
  ]

  const items = [
    ...(data.user.role === ROLES.ADMIN || data.user.role === ROLES.SUPER_ADMIN
      ? itemsUpdate
      : []),
    {
      key: 'preview',
      label: (
        <p
          className="cursor-pointer
    text-nickel transition duration-300 hover:text-azure"
          onClick={openPreviewModal}
        >
          {t('insurances:card.preview')}
        </p>
      ),
    },
    ...(data.user.role === ROLES.VENDOR && data.status === STATUSES.APPROVED
      ? itemsWaitingForApproved
      : []),
    ...(data.user.role === ROLES.ADMIN || data.user.role === ROLES.SUPER_ADMIN
      ? itemsDelete
      : []),
    ...(data.user.role === ROLES.VENDOR && data.status === STATUSES.PENDING
      ? itemsApprove
      : []),
  ]

  return {
    isLoading,
    isPreview,
    isModalReject,
    isModalRemove,
    items,
    reason,
    onCloseModalReject: handleCloseModalReject,
    onCloseModalRemove: handleCloseModalRemove,
    handleRemoveInsurance,
    onChangeReason: handleChangeReason,
    handleUpdateInsuranceStatus,
    handleOpenModalReject,
    openPreviewModal,
    closePreviewModal,
  }
}
