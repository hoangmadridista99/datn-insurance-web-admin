import React from 'react'
import { Button, Modal, Popover, Input, Dropdown } from 'antd'
import format from 'date-fns/format'
import { useTranslation } from 'next-i18next'
import Image from 'next/image'
import classNames from 'classnames'

import { useCardInsuranceLogic } from './useCardInsuranceLogic'

import { getStatus } from '@helpers/getStatus'
import { formatCurrency } from '@helpers/formatCurrency'
import {
  HEALTH_INSURANCE,
  IMAGE_FALLBACK,
  LIFE_INSURANCE,
  ROLES,
  STATUSES,
} from '@utils/constants/insurances'
import LifeModal from 'components/Preview/Life/preview/LifeModal'
import HealthModal from 'components/Preview/Health/preview/HealthModal'
import { CloseIconSvg } from '@utils/icons'
import ArrowDown from 'components/Icon/ArrowDown'

const { TextArea } = Input

const CardInsurance = (props) => {
  const { ...data } = props

  const { t } = useTranslation('insurances')
  const {
    isLoading,
    isPreview,
    isModalReject,
    isModalRemove,
    items,
    reason,
    onCloseModalReject,
    onCloseModalRemove,
    handleRemoveInsurance,
    onChangeReason,
    handleUpdateInsuranceStatus,
    handleOpenModalReject,
    closePreviewModal,
  } = useCardInsuranceLogic(props)

  return (
    <>
      <div className="mb-4 flex items-center rounded-xl border border-solid border-platinum p-4 hover:shadow-md">
        <div className="flex w-4/12 items-center">
          <Image
            src={data.company?.logo}
            fallback={IMAGE_FALLBACK}
            alt="company-logo"
            className="rounded-lg"
            preview="false"
            width={48}
            height={48}
          />

          <div className="mx-4">
            <p className="text-base font-bold text-arsenic">{data?.name}</p>
            <p className="text-sm text-nickel">
              {t(`insurances:form:options:${data?.insurance_category?.label}`)}
            </p>
          </div>
        </div>

        <div className="flex w-3/12 flex-col">
          <div className="mb-1 flex items-center">
            <p className="mr-1 font-bold text-ultramarine-blue">{`${formatCurrency(
              data.terms?.monthly_fee?.from ?? 0
            )} VNĐ`}</p>
            <span className="text-metallic">
              /{t('insurances:fields.month')}
            </span>
          </div>

          <div className="flex items-center">
            <span className="mr-0.5 text-sm font-bold text-nickel">
              {data.rating_scores}
            </span>
            <Image src="/svg/star.svg" width={20} height={20} alt="rating" />
            <Image
              src="/svg/dot.svg"
              width={4}
              height={4}
              alt="Dot"
              className="mx-2"
            />
            <span className="text-xs text-spanish-gray">{`${formatCurrency(
              data.total_rating
            )} ${t('insurances:fields.review_lower')}`}</span>
          </div>
        </div>

        <div
          className={classNames({
            'w-2/12': data.user.role === ROLES.VENDOR,
            'w-3/12': data.user.role === ROLES.ADMIN,
          })}
        >
          <p className="mb-1 font-bold text-nickel">
            {format(new Date(data?.created_at), 'dd/MM/yyyy')}
          </p>
          <p className="text-xs text-spanish-gray">
            {t('card.created')}
            <b className="font-bold text-nickel">
              {data?.user?.role === ROLES.ADMIN
                ? 'Admin'
                : `${data?.user?.first_name} ${data?.user?.last_name}`}
            </b>
          </p>
        </div>

        {data.user.role === ROLES.VENDOR &&
          data.status === STATUSES.REJECTED && (
            <Popover
              title={t('insurances:form.modal.reject.title')}
              content={
                <p className="max-w-md break-all text-sm text-nickel">
                  {data.reason}
                </p>
              }
              trigger="hover"
              rootClassName="mx-4"
            >
              <div className="flex w-2/12 items-center justify-center rounded-3xl bg-linen px-4 py-1">
                <p className="mr-2 whitespace-nowrap text-xs text-vermilion xl:text-base">
                  {getStatus(data.status)}
                </p>
                <Image
                  src="/svg/danger.svg"
                  width={20}
                  height={20}
                  alt="Warning"
                />
              </div>
            </Popover>
          )}

        {data.user.role === ROLES.VENDOR &&
          data.status !== STATUSES.REJECTED && (
            <div
              className={classNames(
                'flex w-2/12 items-center justify-center rounded-3xl px-4 py-1',
                {
                  'bg-cosmic-latte': data.status === STATUSES.PENDING,
                  'bg-chinese-white': data.status === STATUSES.APPROVED,
                }
              )}
            >
              <p
                className={classNames(
                  'whitespace-nowrap text-xs xl:text-base',
                  {
                    'text-royal-orange': data.status === STATUSES.PENDING,
                    'text-apple': data.status === STATUSES.APPROVED,
                  }
                )}
              >
                {getStatus(data.status)}
              </p>
            </div>
          )}

        <div
          className={classNames('flex w-2/12 justify-end', {
            'w-3/12': data.user.role !== ROLES.VENDOR,
          })}
          id="area"
        >
          <Dropdown
            disabled={isLoading}
            placement="bottom"
            menu={{ items }}
            trigger={['click']}
            getPopupContainer={() => document.getElementById('area')}
          >
            <button className="group flex cursor-pointer items-center justify-center rounded-lg border border-nickel bg-transparent px-4 py-2 text-nickel transition hover:border-ultramarine-blue [&_span]:hover:text-ultramarine-blue [&_svg]:order-1 [&_svg]:hover:stroke-ultramarine-blue">
              <p className="mr-2 text-base group-hover:text-ultramarine-blue">
                {t('insurances:card:action')}
              </p>

              <ArrowDown />
            </button>
          </Dropdown>
        </div>
      </div>

      {isModalReject && (
        <Modal
          open={isModalReject}
          centered
          footer={null}
          onCancel={onCloseModalReject}
          closeIcon={<CloseIconSvg />}
        >
          <div className="mx-auto flex w-4/5 flex-col items-center py-8">
            <p className="mb-2 text-2xl font-bold text-outer-space">
              {t('insurances:fields:refuse_approve')}
            </p>
            <p className="mb-4 text-nickel">
              {t('insurances:fields:refuse_approve_reason')}
            </p>
            <TextArea
              onChange={onChangeReason}
              placeholder={t('insurances:form:placeholder:input_reason')}
              className="h-20 w-full"
            />
            <div className="mt-6 flex w-full items-center">
              <Button
                className="mr-2 w-full"
                onClick={onCloseModalReject}
                disabled={isLoading}
              >
                {t('insurances:fields:cancel')}
              </Button>
              <Button
                className="ml-2 w-full"
                type="primary"
                onClick={() => handleUpdateInsuranceStatus(STATUSES.REJECTED)}
                loading={isLoading}
                disabled={isLoading || !reason}
              >
                {t('insurances:fields:accept')}
              </Button>
            </div>
          </div>
        </Modal>
      )}

      {isModalRemove && (
        <Modal
          open={isModalRemove}
          centered
          footer={null}
          onCancel={onCloseModalRemove}
          okButtonProps={{ loading: isLoading }}
          closeIcon={<CloseIconSvg />}
        >
          <div className="mx-auto flex w-4/5 flex-col items-center py-8">
            <p className="mb-2 text-2xl font-bold text-outer-space">
              {t('insurances:fields:remove_insurance')}
            </p>
            <p className="mb-4 text-nickel">
              {t('insurances:fields:remove_insurance_reason')}
            </p>
            <TextArea
              onChange={onChangeReason}
              placeholder={t('insurances:form:placeholder:input_reason')}
              className="h-20 w-full"
            />
            <div className="mt-6 flex w-full items-center">
              <Button
                className="mr-2 w-full"
                onClick={onCloseModalRemove}
                disabled={isLoading}
              >
                {t('insurances:fields:cancel')}
              </Button>
              <Button
                className="ml-2 w-full"
                type="primary"
                onClick={handleRemoveInsurance}
                loading={isLoading}
                disabled={isLoading || !reason}
              >
                {t('insurances:fields:accept')}
              </Button>
            </div>
          </div>
        </Modal>
      )}

      {props?.insurance_category.label === LIFE_INSURANCE && isPreview && (
        <LifeModal
          isOpen={isPreview}
          onClose={closePreviewModal}
          insuranceId={props.id}
          handleChangeStatus={handleUpdateInsuranceStatus}
          handleOpenModalReject={handleOpenModalReject}
        />
      )}

      {props?.insurance_category.label === HEALTH_INSURANCE && isPreview && (
        <HealthModal
          isOpen={isPreview}
          onClose={closePreviewModal}
          insuranceId={props.id}
          handleChangeStatus={handleUpdateInsuranceStatus}
          handleOpenModalReject={handleOpenModalReject}
        />
      )}
    </>
  )
}

export default CardInsurance
