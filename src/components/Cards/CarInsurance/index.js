import { Dropdown } from 'antd'
import ArrowDown from 'components/Icon/ArrowDown'
import Image from 'next/image'
import React, { useCallback, useMemo } from 'react'
import { useTranslation } from 'next-i18next'
import { formatCurrency } from '@helpers/formatCurrency'
import {
  CAR_INSURANCE_TYPE,
  PHYSICAL_CAR_TYPE,
} from '@utils/constants/insurances'
import dayjs from 'dayjs'
import { useRouter } from 'next/router'
import { ROUTERS } from '@utils/constants/routers'

const CardCarInsurance = ({ data, isMandatory, handleClickPreview }) => {
  const { t } = useTranslation(['car-insurances'])

  const router = useRouter()

  const handleClickUpdate = useCallback(
    () => router.push(`${ROUTERS.CAR_INSURANCES}/update/${data?.id}`),
    [data?.id, router]
  )

  const items = useMemo(
    () => [
      {
        key: 'update',
        label: (
          <p
            className="cursor-pointer text-base text-nickel transition duration-300 hover:text-azure"
            onClick={handleClickUpdate}
          >
            {t('car-insurances:button:update')}
          </p>
        ),
      },
      {
        key: 'preview',
        label: (
          <p
            className="cursor-pointer text-base text-nickel transition duration-300 hover:text-azure"
            onClick={() => handleClickPreview(data?.id)}
          >
            {t('car-insurances:button:preview')}
          </p>
        ),
      },
    ],
    [handleClickUpdate, t, handleClickPreview, data?.id]
  )

  return (
    <div className="mb-4 rounded-xl border border-solid border-platinum p-4 last:mb-0">
      <div className="flex items-center gap-1">
        <div className="flex w-3/12 items-center gap-4">
          <Image
            src={data?.company_logo_url}
            width={48}
            height={48}
            alt="logo"
            className="object-contain"
          />
          <p className="text-base font-bold text-arsenic">
            {data?.insurance_name}
          </p>
        </div>
        <div className="flex w-4/12 flex-col justify-center gap-2 text-arsenic">
          {data?.cost_of_purchasing && (
            <div className="flex items-baseline gap-1">
              <p className="w-1/2">{`${t('car-insurances:card:physical')} `}</p>
              <p className="font-bold text-ultramarine-blue">
                {`${formatCurrency(data.cost_of_purchasing?.value ?? 0)} ${
                  data.cost_of_purchasing?.type === PHYSICAL_CAR_TYPE.COST
                    ? 'VNĐ'
                    : `% ${t('car-insurances:card:car_price')}`
                }`}
              </p>
            </div>
          )}
          {(data?.insurance_type === CAR_INSURANCE_TYPE.BOTH ||
            data?.insurance_type === CAR_INSURANCE_TYPE.MANDATORY) && (
            <div className="flex items-baseline gap-1">
              <p className="mb-2 w-1/2">{`${t(
                'car-insurances:card:mandatory'
              )} `}</p>
              <p
                className={
                  isMandatory
                    ? 'font-bold text-ultramarine-blue'
                    : 'text-spanish-gray'
                }
              >
                {t(
                  `car-insurances:card:price_${
                    isMandatory ? 'updated' : 'not_updated'
                  }`
                )}
              </p>
            </div>
          )}
        </div>
        <div className="w-3/12">
          <p className="mb-2 font-bold text-nickel">
            {dayjs(data.updated_at).format('DD/MM/YYYY - HH:mm')}
          </p>
          <p className="text-spanish-gray">
            <span className="text-xs">
              {`${t('car-insurances:card:created_at')} `}
            </span>
            <span className="font-bold">{`${data?.user?.first_name} ${data?.user?.last_name}`}</span>
          </p>
        </div>
        <div className="flex w-2/12 justify-end">
          <Dropdown
            // disabled={isDisableDropdown}
            placement="bottom"
            menu={{ items }}
            trigger={['click']}
          >
            <button className="group flex cursor-pointer items-center justify-center rounded-lg border border-nickel bg-transparent px-4 py-2 text-nickel transition hover:border-ultramarine-blue [&_span]:hover:text-ultramarine-blue [&_svg]:order-1 [&_svg]:hover:stroke-ultramarine-blue">
              <p className="mr-2 text-base group-hover:text-ultramarine-blue">
                {t('car-insurances:button:action')}
              </p>

              <ArrowDown />
            </button>
          </Dropdown>
        </div>
      </div>
    </div>
  )
}

export default CardCarInsurance
