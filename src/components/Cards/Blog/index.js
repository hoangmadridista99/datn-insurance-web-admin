import React from 'react'
import { Card, Dropdown, Popover } from 'antd'
import dayjs from 'dayjs'
import classNames from 'classnames'
import Image from 'next/image'
import { useCardBlogLogic } from './useCardBlogLogic'
import { IMAGE_FALLBACK, ROLES, STATUSES } from '@utils/constants/insurances'
import DangerIconSvg from 'components/Icon/Danger'
import { useTranslation } from 'next-i18next'
import ArrowDown from 'components/Icon/ArrowDown'

function CardBlog(props) {
  const { t } = useTranslation(['blogs'])
  const { isLoading, data, locale } = props
  const { items } = useCardBlogLogic(props)

  const StatusReject = () => (
    <Popover
      title={t('blogs:card:reason-rejected')}
      content={
        <p className="max-w-md break-all text-sm text-nickel">{data.reason}</p>
      }
      rootClassName="mx-4"
      trigger="hover"
    >
      <div className="w-2/12">
        <p className="flex w-fit items-center gap-2 rounded-3xl bg-linen px-4 py-1 text-xs text-vermilion xl:text-base">
          {t(`blogs:status:${data.status}`)}
          <DangerIconSvg />
        </p>
      </div>
    </Popover>
  )
  const StatusWithoutReject = () => (
    <div className={classNames('w-2/12')}>
      <p
        className={classNames(
          'w-fit whitespace-nowrap rounded-3xl px-4 py-1 text-xs xl:text-base',
          {
            'text-royal-orange': data.status === STATUSES.PENDING,
            'text-apple': data.status === STATUSES.APPROVED,
            'bg-cosmic-latte': data.status === STATUSES.PENDING,
            'bg-chinese-white': data.status === STATUSES.APPROVED,
          }
        )}
      >
        {t(`blogs:status:${data.status}`)}
      </p>
    </div>
  )

  return (
    <Card
      loading={isLoading}
      className="mb-6 p-4 last:mb-0"
      bodyStyle={{ height: '100%', padding: 0 }}
    >
      <div className="flex h-full items-center">
        <div className="relative mr-6 flex h-20 w-2/12 items-center justify-center">
          <Image
            src={data.banner_image_url}
            fallback={IMAGE_FALLBACK}
            alt={data.title}
            className="w-2/3 rounded-lg object-cover"
            preview="false"
            fill
          />
        </div>

        <div
          className={`self-center pr-20 ${
            data.user.role === ROLES.VENDOR ? 'w-4/12' : 'w-5/12'
          }`}
        >
          <p className="line-clamp-2 mb-1.5 text-sm font-bold text-arsenic lg:whitespace-normal">
            {data.title}
          </p>
          <p className="inline-block rounded-full bg-lavender p-1 text-xs text-azure">
            {locale === 'vi'
              ? data?.blog_category?.vn_label
              : data?.blog_category?.en_label}
          </p>
        </div>

        <div
          className={`pr-1 ${
            data?.user?.role === ROLES.VENDOR ? 'w-2/12' : 'w-3/12'
          }`}
        >
          <p className="mb-1 text-xs font-bold text-nickel xl:text-sm">
            {dayjs(new Date(data.created_at)).format('DD/MM/YYYY')}
          </p>

          <p className="text-xs text-spanish-gray">
            {t('blogs:card:created-by')}
            <span className="text-sm font-bold">
              &nbsp;{`${data.user.first_name} ${data.user.last_name}`}
            </span>
          </p>
        </div>

        {data?.user?.role === ROLES.VENDOR &&
          (data?.status === STATUSES.REJECTED ? (
            <StatusReject />
          ) : (
            <StatusWithoutReject />
          ))}

        <div className="flex w-2/12 items-center justify-end">
          <Dropdown placement="bottom" menu={{ items }} trigger={['click']}>
            <button className="group flex cursor-pointer items-center justify-center rounded-lg border border-nickel bg-transparent px-4 py-2 text-nickel transition hover:border-ultramarine-blue [&_span]:hover:text-ultramarine-blue [&_svg]:order-1 [&_svg]:hover:stroke-ultramarine-blue">
              <p className="mr-2 text-base group-hover:text-ultramarine-blue">
                Action
              </p>

              <ArrowDown />
            </button>
          </Dropdown>
        </div>
      </div>
    </Card>
  )
}

export default CardBlog
