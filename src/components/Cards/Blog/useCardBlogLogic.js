import React, { useCallback, useMemo } from 'react'
import { notification } from 'antd'
import { updateStatusBlog } from '@services/blogs'
import { handleError } from '@helpers/handleError'
import { ROLES, STATUSES } from '@utils/constants/insurances'
import { useTranslation } from 'next-i18next'
import { KEY_MODAL } from '@utils/constants/blog'

export const useCardBlogLogic = (props) => {
  const { handleOpenModal, onUpdateSuccess, data } = props

  const { t } = useTranslation(['blogs'])

  const handleUpdateStatus = useCallback(
    async (status, signal) => {
      try {
        const body = { status }
        await updateStatusBlog(data.id, body, signal)
        onUpdateSuccess(data.id, body)
        notification.success({ message: t('blogs:responses:update-status') })
      } catch (error) {
        handleError(error)
      }
    },
    [data.id, onUpdateSuccess, t]
  )

  const itemsActionApprove = useMemo(
    () => [
      {
        key: 'approved',
        label: (
          <p
            className="cursor-pointer
text-nickel transition duration-300 hover:text-azure"
            onClick={() => handleUpdateStatus(STATUSES.APPROVED)}
          >
            {t('blogs:button:approved')}
          </p>
        ),
      },
      {
        key: 'rejected',
        label: (
          <p
            className="cursor-pointer
text-nickel transition duration-300 hover:text-azure"
            onClick={() => handleOpenModal(KEY_MODAL.REJECT, data)}
          >
            {t('blogs:button:rejected')}
          </p>
        ),
      },
    ],
    [data, handleOpenModal, handleUpdateStatus, t]
  )
  const itemDelete = useMemo(
    () => [
      {
        key: 'delete',
        label: (
          <p
            className="cursor-pointer
text-nickel transition duration-300 hover:text-azure"
            onClick={() => handleOpenModal(KEY_MODAL.REMOVE, data)}
          >
            {t('blogs:button:remove')}
          </p>
        ),
      },
    ],
    [data, handleOpenModal, t]
  )
  const itemUpdate = useMemo(
    () => [
      {
        key: 'update',
        label: (
          <p
            className="cursor-pointer
text-nickel transition duration-300 hover:text-azure"
            onClick={() => handleOpenModal(KEY_MODAL.UPDATE, data)}
          >
            {t('blogs:button:update')}
          </p>
        ),
      },
    ],
    [data, handleOpenModal, t]
  )
  const itemWaitingForApproved = useMemo(
    () => [
      {
        key: 'waitingForApproved',
        label: (
          <p
            className="cursor-pointer
text-nickel transition duration-300 hover:text-azure"
            onClick={() => handleUpdateStatus(STATUSES.PENDING)}
          >
            {t('blogs:button:waiting-for-approval')}
          </p>
        ),
      },
    ],
    [handleUpdateStatus, t]
  )
  const items = useMemo(
    () => [
      ...(data?.user?.role === ROLES.ADMIN ||
      data?.user?.role === ROLES.SUPER_ADMIN
        ? itemUpdate
        : []),
      {
        key: 'preview',
        label: (
          <p
            className="cursor-pointer
text-nickel transition duration-300 hover:text-azure"
            onClick={() => handleOpenModal(KEY_MODAL.PREVIEW, data)}
          >
            {t('blogs:button:preview')}
          </p>
        ),
      },
      ...(data?.user?.role === ROLES.VENDOR && data.status === STATUSES.APPROVED
        ? itemWaitingForApproved
        : []),
      ...(data?.user?.role === ROLES.ADMIN ||
      data?.user?.role === ROLES.SUPER_ADMIN
        ? itemDelete
        : []),
      ...(data?.status === STATUSES.PENDING ? itemsActionApprove : []),
    ],
    [
      data,
      handleOpenModal,
      itemDelete,
      itemUpdate,
      itemWaitingForApproved,
      itemsActionApprove,
      t,
    ]
  )

  return {
    items,
  }
}
