import React from 'react'
import { Button, Card } from 'antd'
import { useTranslation } from 'next-i18next'

function CardBlogCategory({
  isLoading,
  blogCategory,
  onClickUpdate,
  onClickDelete,
}) {
  const { t } = useTranslation(['blogs'])

  return (
    <Card loading={isLoading} className="mb-6 h-full last:mb-0">
      <div className="flex h-full items-center">
        <p className="mr-6 w-2/5 text-xs font-normal text-arsenic">
          {blogCategory.vn_label}
        </p>

        <div className="w-2/5 pr-20 text-xs font-normal text-arsenic">
          {blogCategory.en_label}
        </div>

        <div className="flex w-1/5 items-center gap-3">
          <Button
            type="default"
            size="large"
            className="w-24 px-4 text-base font-normal text-nickel hover:border-ultramarine-blue hover:text-ultramarine-blue"
            onClick={() => onClickUpdate(blogCategory)}
          >
            {t('blogs:button:update')}
          </Button>

          <Button
            type="default"
            size="large"
            className="w-24 px-4 text-base font-normal text-nickel hover:border-ultramarine-blue hover:text-ultramarine-blue"
            onClick={() => onClickDelete(blogCategory)}
          >
            {t('blogs:button:remove')}
          </Button>
        </div>
      </div>
    </Card>
  )
}

export default CardBlogCategory
