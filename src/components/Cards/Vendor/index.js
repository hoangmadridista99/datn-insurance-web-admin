import React from 'react'
import { Button, Modal, Input, Dropdown, Popover } from 'antd'
import { useTranslation } from 'next-i18next'
import Image from 'next/image'
import { useCardVendorLogic } from './useCardVendorLogic'
import { CloseIconSvg } from '@utils/icons'
import ArrowDown from 'components/Icon/ArrowDown'
import dayjs from 'dayjs'
import classNames from 'classnames'
import { IMAGE_FALLBACK, VENDOR_STATUSES } from '@utils/constants/insurances'
import { getStatus } from '@helpers/getStatus'
import VendorPreviewModal from 'components/Preview/Vendor'

const { TextArea } = Input

const CardVendor = (props) => {
  const { ...data } = props

  const { t } = useTranslation(['vendor', 'modal'])
  const {
    isDisableDropdown,
    isModalReject,
    isLoading,
    items,
    reason,
    onChangeReason,
    handleSubmitReject,
    onCloseModalReject,
    handleOpenModalReject,
    isPreview,
    closePreviewModal,
    handleUpdateStatus,
  } = useCardVendorLogic(props)

  return (
    <>
      <div className="mb-4 flex items-center rounded-xl border border-solid border-platinum p-4 hover:shadow-md">
        <div className="flex w-3/12 items-center">
          <Image
            src={data.company?.logo}
            fallback={IMAGE_FALLBACK}
            alt={data.company?.short_name}
            className="rounded-lg"
            preview="false"
            width={48}
            height={48}
          />

          <div className="mx-4">
            <p className="text-base font-bold text-arsenic">
              {data?.company.short_name}
            </p>
          </div>
        </div>

        <div className="flex w-2/12 flex-col">
          <p className="mb-1 text-sm font-bold text-nickel">{data?.fullName}</p>

          <div className="flex items-center pr-2 text-spanish-gray">
            <p className="mr-1 whitespace-nowrap text-xs">
              {t('vendor:preview.date_of_birth')}
            </p>
            <p className="text-sm font-bold">
              {dayjs(data?.date_of_birth).format('DD/MM/YYYY')}
            </p>
          </div>
        </div>

        <div className="flex w-3/12 flex-col pr-4">
          <div className="mb-1 flex items-center text-spanish-gray">
            <p className="mr-1 text-xs">{t('vendor:preview.phone')}</p>
            <p className="text-sm font-bold">{data?.phone}</p>
          </div>

          <div className="flex items-center text-spanish-gray">
            <p className="mr-1 text-xs">{t('vendor:preview.email')}</p>
            <p className="overflow-hidden text-ellipsis whitespace-nowrap text-sm font-bold">
              {data?.email}
            </p>
          </div>
        </div>

        <div className="w-2/12">
          {data?.vendor_status === VENDOR_STATUSES.REJECTED && (
            <Popover
              title={t('modal:reject.popover_title')}
              content={
                <p className="max-w-[422px] break-all text-sm text-nickel">
                  {data?.reason}
                </p>
              }
              trigger="hover"
              rootClassName="mx-4"
            >
              <div className="flex w-fit items-center justify-center whitespace-nowrap rounded-3xl bg-linen px-4 py-1">
                <p className="mr-2 text-xs text-vermilion xl:text-base">
                  {t(`vendor:preview.status.${getStatus(data?.vendor_status)}`)}
                </p>
                <Image
                  src="/svg/danger.svg"
                  width={20}
                  height={20}
                  alt="Warning"
                />
              </div>
            </Popover>
          )}

          {data?.vendor_status !== VENDOR_STATUSES.REJECTED && (
            <div
              className={classNames(
                'flex w-fit items-center justify-center whitespace-nowrap rounded-3xl px-4 py-1',
                {
                  'bg-cosmic-latte':
                    data?.vendor_status === VENDOR_STATUSES.PENDING,
                  'bg-chinese-white':
                    data?.vendor_status === VENDOR_STATUSES.ACCEPTED,
                }
              )}
            >
              <p
                className={classNames('text-xs xl:text-base', {
                  'text-royal-orange':
                    data?.vendor_status === VENDOR_STATUSES.PENDING,
                  'text-apple':
                    data?.vendor_status === VENDOR_STATUSES.ACCEPTED,
                })}
              >
                {t(`vendor:preview.status.${getStatus(data?.vendor_status)}`)}
              </p>
            </div>
          )}
        </div>

        <div className="flex w-2/12 justify-end" id="area">
          <Dropdown
            disabled={isDisableDropdown}
            placement="bottom"
            menu={{ items }}
            trigger={['click']}
            getPopupContainer={() => document.getElementById('area')}
          >
            <button className="group flex cursor-pointer items-center justify-center rounded-lg border border-nickel bg-transparent px-4 py-2 text-nickel transition hover:border-ultramarine-blue [&_span]:hover:text-ultramarine-blue [&_svg]:order-1 [&_svg]:hover:stroke-ultramarine-blue">
              <p className="mr-2 whitespace-nowrap text-base group-hover:text-ultramarine-blue">
                {t('vendor:button.action')}
              </p>

              <ArrowDown />
            </button>
          </Dropdown>
        </div>
      </div>

      {isModalReject && (
        <Modal
          open={isModalReject}
          centered
          footer={null}
          onCancel={onCloseModalReject}
          closeIcon={<CloseIconSvg />}
        >
          <div className="mx-auto flex w-4/5 flex-col items-center py-8">
            <p className="mb-2 text-2xl font-bold text-outer-space">
              {t('modal:reject:fields:refuse_title')}
            </p>
            <p className="mb-4 text-nickel">
              {t('modal:reject:fields:refuse_description')}
            </p>
            <TextArea
              onChange={onChangeReason}
              placeholder={t('modal:placeholder:reject_reason')}
              className="h-20 w-full"
            />
            <div className="mt-6 flex w-full items-center gap-4">
              <Button
                type="default"
                size="large"
                className="w-full px-6 text-base font-bold text-nickel"
                onClick={onCloseModalReject}
                disabled={isLoading.rejected}
              >
                {t('vendor:button:cancel')}
              </Button>
              <Button
                className="w-full px-6 text-base font-bold"
                type="primary"
                size="large"
                onClick={handleSubmitReject}
                loading={isLoading.accepted}
                disabled={isLoading.accepted || !reason}
              >
                {t('vendor:button:confirm')}
              </Button>
            </div>
          </div>
        </Modal>
      )}

      {isPreview && (
        <VendorPreviewModal
          visible={isPreview}
          closePreviewModal={closePreviewModal}
          isLoading={isLoading}
          vendorDetails={data}
          handleUpdateStatus={handleUpdateStatus}
          handleOpenModalReject={handleOpenModalReject}
        />
      )}
    </>
  )
}

export default CardVendor
