import { useState, useCallback, useMemo } from 'react'
import { notification } from 'antd'
import { VENDOR_STATUSES } from '@utils/constants/insurances'
import { updateVendor } from '@services/vendor'
import { useTranslation } from 'next-i18next'

export const useCardVendorLogic = (props) => {
  const { t } = useTranslation('vendor')
  const { handleGetVendors, ...data } = props

  const [isLoading, setIsLoading] = useState({
    accepted: false,
    rejected: false,
  })
  const [isModalReject, setIsModalReject] = useState(false)
  const [reason, setReason] = useState()
  const [isPreview, setIsPreview] = useState(false)

  const openPreviewModal = () => setIsPreview(true)
  const closePreviewModal = () => setIsPreview(false)

  const handleOpenModalReject = () => setIsModalReject(true)
  const handleCloseRejectModal = () => {
    setReason(undefined)
    setIsModalReject(false)
  }

  const handleUpdateStatus = useCallback(
    async (params) => {
      setIsLoading({
        accepted: params.vendor_status === VENDOR_STATUSES.ACCEPTED || false,
        rejected: params.vendor_status === VENDOR_STATUSES.REJECTED || false,
      })
      try {
        const response = await updateVendor(data.id, params)

        if (response.status === 204) {
          handleGetVendors()
          notification.success({
            message: t('vendor:message:update_vendor_status_success'),
          })
        }
      } catch (error) {
        console.log('Error', error)
        notification.error({
          message: t('vendor:message:update_vendor_status_failed'),
        })
      } finally {
        setIsLoading({
          accepted: false,
          rejected: false,
        })
      }
    },
    [data.id, handleGetVendors, t]
  )

  const handleSubmitReject = () => {
    if (!reason) {
      return notification.warning({
        message: t('vendor:message:reject_reason'),
      })
    }

    handleUpdateStatus({ vendor_status: VENDOR_STATUSES.REJECTED, reason })
    handleCloseRejectModal()
  }

  const handleChangeReason = (e) => setReason(e.target.value)

  const actionItems = [
    {
      key: 'preview',
      label: (
        <p
          className="cursor-pointer
    text-nickel transition duration-300 hover:text-azure"
          onClick={openPreviewModal}
        >
          {t('vendor:button:see_details')}
        </p>
      ),
    },
    ...(data.vendor_status === VENDOR_STATUSES.PENDING
      ? [
          {
            key: 'approved',
            label: (
              <p
                className="cursor-pointer
    text-nickel transition duration-300 hover:text-azure"
                onClick={() =>
                  handleUpdateStatus({
                    vendor_status: VENDOR_STATUSES.ACCEPTED,
                  })
                }
              >
                {t('vendor:button:approve')}
              </p>
            ),
          },
          {
            key: 'rejected',
            label: (
              <p
                className="cursor-pointer
    text-nickel transition duration-300 hover:text-azure"
                onClick={handleOpenModalReject}
              >
                {t('vendor:button:reject')}
              </p>
            ),
          },
        ]
      : []),
  ]

  const isDisableDropdown = useMemo(
    () => isLoading.accepted || isLoading.rejected,
    [isLoading]
  )

  return {
    isModalReject,
    isDisableDropdown,
    isLoading,
    items: actionItems,
    reason,
    onCloseModalReject: handleCloseRejectModal,
    handleSubmitReject,
    onChangeReason: handleChangeReason,
    handleUpdateStatus,
    handleOpenModalReject,
    isPreview,
    openPreviewModal,
    closePreviewModal,
  }
}
