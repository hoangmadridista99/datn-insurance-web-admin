import React, { useState } from 'react'
import Image from 'next/image'
import dayjs from 'dayjs'
import { Button, Popover } from 'antd'
import { useTranslation } from 'next-i18next'
import { APPRAISAL_STATUS, STATUSES } from '@utils/constants/insurances'
import { getAppraisalStatus } from '@helpers/getStatus'
import classNames from 'classnames'
import AppraisalDetailsModal from './AppraisalDetailsModal'

const CardAppraisal = ({ appraisal }) => {
  const { t } = useTranslation(['insurances'])

  const [isModalVisible, setIsModalVisible] = useState(false)
  const [cardStatus, setCardStatus] = useState(appraisal?.form_status)
  const [reasonRejected, setReasonRejected] = useState(null)

  const formatDate = (date) => {
    return dayjs(date).format('DD/MM/YYYY - HH:mm')
  }

  const handleOpenModal = () => setIsModalVisible(true)

  const handleCloseModal = () => setIsModalVisible(false)

  return (
    <>
      <div className="mb-4 flex items-center gap-6 rounded-xl border border-solid border-platinum p-4 last:mb-0">
        <div className="w-1/3">
          <p className="mb-1 text-base font-bold text-arsenic">
            {t('insurances:appraisal.general.car_insurance')}
          </p>

          <p className="text-sm font-normal text-nickel">
            ID: {appraisal.form_code}
          </p>
        </div>

        <div className="w-1/4">
          <p className="mb-1 text-sm font-bold text-nickel">
            {formatDate(appraisal?.created_at)}
          </p>
          <div className="flex items-center">
            <p className="mr-1 text-xs text-spanish-gray">
              {t('insurances:card.created')}
            </p>
            <p className="text-sm font-bold text-spanish-gray">{`${appraisal?.user?.first_name} ${appraisal?.user?.last_name}`}</p>
          </div>
        </div>

        <div className="w-1/4">
          {cardStatus === STATUSES.REJECTED && (
            <Popover
              title={t('insurances:form.modal.reject.title')}
              content={
                <p className="max-w-[422px] break-all text-sm text-nickel">
                  {reasonRejected || appraisal?.reason}
                </p>
              }
              trigger="hover"
              rootClassName="mx-4"
            >
              <div className="flex w-fit items-center justify-center whitespace-nowrap rounded-3xl bg-linen px-4 py-1">
                <p className="mr-2 text-xs text-vermilion xl:text-base">
                  {getAppraisalStatus(cardStatus)}
                </p>
                <Image
                  src="/svg/danger.svg"
                  width={20}
                  height={20}
                  alt="Warning"
                />
              </div>
            </Popover>
          )}

          {cardStatus !== STATUSES.REJECTED && (
            <div
              className={classNames(
                'flex w-fit items-center justify-center whitespace-nowrap rounded-3xl px-4 py-1',
                {
                  'bg-cosmic-latte': cardStatus === STATUSES.PENDING,
                  'bg-chinese-white': cardStatus === STATUSES.APPROVED,
                  'bg-lavender': cardStatus === APPRAISAL_STATUS.PAID,
                }
              )}
            >
              <p
                className={classNames('text-xs xl:text-base', {
                  'text-royal-orange': cardStatus === STATUSES.PENDING,
                  'text-apple': cardStatus === STATUSES.APPROVED,
                  'text-azure': cardStatus === APPRAISAL_STATUS.PAID,
                })}
              >
                {getAppraisalStatus(cardStatus)}
              </p>
            </div>
          )}
        </div>

        <div className="flex w-1/4 justify-end">
          <Button
            type="default"
            className="rounded-lg px-4 text-base text-nickel hover:text-azure"
            onClick={handleOpenModal}
            size="large"
          >
            {t('insurances:appraisal.general.see_detail')}
          </Button>

          {isModalVisible && (
            <AppraisalDetailsModal
              isModalOpen={isModalVisible}
              handleCancel={handleCloseModal}
              appraisalId={appraisal?.id}
              formatDate={formatDate}
              status={cardStatus}
              setCardStatus={setCardStatus}
              setReasonRejected={setReasonRejected}
              reasonRejected={reasonRejected}
            />
          )}
        </div>
      </div>
    </>
  )
}

export default CardAppraisal
