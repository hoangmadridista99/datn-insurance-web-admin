import {
  STATUSES,
  TARGET_USE_BUSINESS,
  TYPE_INVOICE,
} from '@utils/constants/insurances'
import { CancelSvg, CheckboxSvg } from '@utils/icons'
import Image from 'next/image'
import React from 'react'
import { useTranslation } from 'next-i18next'
import dayjs from 'dayjs'
import { Empty } from 'antd'
import { handleFormatterWithCommas } from '@utils/helper'

const AppraisalInformation = ({ status, appraisalDetails = {} }) => {
  const { t } = useTranslation(['insurances'])

  const renderCarValue = (type) => {
    switch (type) {
      case STATUSES.PENDING:
        return (
          <p className="w-1/2 text-right font-normal text-royal-orange">
            {t('insurances:appraisal.general.waiting_appraisal')}
          </p>
        )

      case STATUSES.APPROVED:
        return (
          <p className="w-1/2 text-right font-medium text-arsenic">
            {handleFormatterWithCommas(appraisalDetails?.appraisal?.car_cost) ||
              0}
            &nbsp;vnđ
          </p>
        )

      default:
        return (
          <p className="w-1/2 text-right font-normal text-spanish-gray">
            {t('insurances:appraisal.general.unable_appraisal')}
          </p>
        )
    }
  }

  const renderTypeOfBusiness = (value) => {
    if (value) {
      const type = TARGET_USE_BUSINESS.find((item) => item.value === value)

      return t(`insurances:appraisal.general.${type.label}`)
    }
  }

  const renderTypeOfInvoice = (value) => {
    if (value) {
      const type = TYPE_INVOICE.find((item) => item.value === value)

      return t(`insurances:appraisal.general.${type.label}`)
    }
  }

  const {
    is_deduct_one_million_vnd,
    is_complete_vehicle_damage,
    is_component_vehicle_damage,
    is_total_vehicle_theft,
    free_roadside_assistance,
    is_choosing_service_center,
    is_component_vehicle_theft,
    is_no_depreciation_cost,
    is_water_damage,
    purpose,
    car_license_plate,
    car_brand,
    car_model,
    car_version,
    year_of_production,
    seating_capacity,
    insurance_period_from,
    insurance_period_to,
    policyholder_full_name,
    policyholder_address,
    policyholder_phone_number,
    policyholder_email,
    bank_name,
    bank_branch,
    branch_address,
    staff_full_name,
    staff_phone_number,
    invoice_type,
    invoice_name,
    invoice_address,
    invoice_tax_id,
    invoice_email,
  } = appraisalDetails

  const insuranceBenefits = [
    {
      id: 'basic_benefits',
      title: 'basic_benefits',
      benefits: [
        {
          id: 1,
          value: is_deduct_one_million_vnd,
          label: 'is_deduct_one_million_vnd',
        },
        {
          id: 2,
          value: is_complete_vehicle_damage,
          label: 'is_complete_vehicle_damage',
        },
        {
          id: 3,
          value: is_component_vehicle_damage,
          label: 'is_component_vehicle_damage',
        },
        {
          id: 4,
          value: is_total_vehicle_theft,
          label: 'is_total_vehicle_theft',
        },
        {
          id: 5,
          value: free_roadside_assistance,
          label: 'free_roadside_assistance',
        },
      ],
    },
    {
      id: 'advanced_benefits',
      title: 'advanced_benefits',
      benefits: [
        {
          id: 6,
          value: is_choosing_service_center,
          label: 'is_choosing_service_center',
        },
        {
          id: 7,
          value: is_component_vehicle_theft,
          label: 'is_component_vehicle_theft',
        },
        {
          id: 8,
          value: is_no_depreciation_cost,
          label: 'is_no_depreciation_cost',
        },
        {
          id: 9,
          value: is_water_damage,
          label: 'is_water_damage',
        },
      ],
    },
    {
      id: 'additional_benefits',
      title: 'additional_benefits',
      benefits: [
        {
          id: 10,
          value: is_water_damage,
          label: 'is_water_damage',
        },
      ],
    },
  ]

  const vehicleInformation = [
    {
      id: 1,
      value: renderTypeOfBusiness(purpose),
      label: 'purpose_using',
    },
    {
      id: 2,
      value: car_license_plate,
      label: 'license_plates',
    },
    {
      id: 3,
      value: car_brand,
      label: 'brand',
    },
    {
      id: 4,
      value: car_model,
      label: 'vehicle_series',
    },
    {
      id: 5,
      value: car_version,
      label: 'vehicle_version',
    },
    {
      id: 6,
      value: year_of_production,
      label: 'year_of_manufacture',
    },
    {
      id: 7,
      value: seating_capacity,
      label: 'number_of_seats',
    },
    {
      id: 8,
      value: renderCarValue(status),
      label: 'vehicle_value',
    },
  ]

  const contractInformation = [
    {
      id: 1,
      value: (
        <>
          {dayjs(insurance_period_from).format('DD/MM/YYYY')}
          &nbsp;-&nbsp;
          {dayjs(insurance_period_to).format('DD/MM/YYYY')}
        </>
      ),
      label: 'insurance_period',
    },
    {
      id: 2,
      value: policyholder_full_name,
      label: 'contract_holder',
    },
    {
      id: 3,
      value: policyholder_address,
      label: 'address',
    },
    {
      id: 4,
      value: policyholder_phone_number,
      label: 'phone_number',
    },
    {
      id: 5,
      value: policyholder_email,
      label: 'email',
    },
  ]

  const bankBeneficiaryInformation = [
    {
      id: 1,
      value: bank_name,
      label: 'bank_name',
    },
    {
      id: 2,
      value: bank_branch,
      label: 'bank_branch_name',
    },
    {
      id: 3,
      value: branch_address,
      label: 'bank_branch_address',
    },
    {
      id: 4,
      value: staff_full_name,
      label: 'staff_name',
    },
    {
      id: 5,
      value: staff_phone_number,
      label: 'bank_phone',
    },
  ]

  const electronicInvoicesInformation = [
    {
      id: 1,
      value: renderTypeOfInvoice(invoice_type),
      label: 'invoice_type',
    },
    {
      id: 2,
      value: invoice_name,
      label: 'company_name',
    },
    {
      id: 3,
      value: invoice_address,
      label: 'company_address',
    },
    {
      id: 4,
      value: invoice_tax_id,
      label: 'tax_code',
    },
    {
      id: 5,
      value: invoice_email,
      label: 'company_mail',
    },
  ]

  return (
    <>
      {/* car information */}
      <div className="rounded-xl border border-solid border-platinum p-8">
        <p className="mb-6 text-2xl font-bold text-arsenic">
          {t('insurances:appraisal.general.vehicle_information')}
        </p>

        <div className="flex flex-col gap-3 [&>*:last-child]:border-0 [&>*:last-child]:pb-0">
          {vehicleInformation.map((vehicle) => (
            <div
              key={vehicle.id}
              className="flex items-center justify-between border-0 border-b border-solid border-ghost-white pb-3 text-base"
            >
              <p className=" w-1/2 font-normal text-nickel">
                {t(`insurances:appraisal.general.${vehicle.label}`)}
              </p>
              {vehicle.id === vehicleInformation.length ? (
                vehicle?.value
              ) : (
                <p className="w-1/2 text-right font-medium text-arsenic">
                  {vehicle?.value}
                </p>
              )}
            </div>
          ))}
        </div>
      </div>

      {/* insurance benefits */}
      <div className="rounded-xl border border-solid border-platinum p-8">
        <p className="mb-6 text-2xl font-bold text-arsenic">
          {t('insurances:appraisal.general.insurance_benefits')}
        </p>

        <div className="flex flex-col gap-4 rounded-lg bg-ghost-white p-6">
          {insuranceBenefits.map((data) => (
            <div key={data.id}>
              <div className="mb-3 border-0 border-b border-solid border-platinum pb-2 text-base font-bold text-nickel">
                {t(`insurances:appraisal.general.${data.title}`)}
              </div>

              <div className="flex flex-col items-start gap-3">
                {data.benefits.map((benefit) => (
                  <div
                    key={benefit.id}
                    className="flex items-center justify-start gap-4"
                  >
                    {benefit.value ? <CheckboxSvg /> : <CancelSvg />}
                    <p className="text-base font-normal text-nickel">
                      {t(`insurances:appraisal.benefits.${benefit.label}`)}
                    </p>
                  </div>
                ))}
              </div>
            </div>
          ))}
        </div>
      </div>

      {/* appraisal information */}
      <div className="rounded-xl border border-solid border-platinum p-8">
        <p className="mb-6 text-2xl font-bold text-arsenic">
          {t('insurances:appraisal.general.appraisal_information')}
        </p>

        <div className="mb-3 flex items-center justify-between border-0 border-b border-solid border-ghost-white pb-3 text-base">
          <p className=" w-1/2 font-normal text-nickel">
            {t('insurances:appraisal.general.deadline_date')}
          </p>
          <p className="w-1/2 text-right font-medium text-arsenic">
            {dayjs(appraisalDetails?.certificate_expiration_date).format(
              'DD/MM/YYYY'
            )}
          </p>
        </div>

        <div>
          <p className="mb-3 text-base font-normal text-nickel">
            {t('insurances:appraisal.general.appraisal_images')}
          </p>

          <div className="flex items-center justify-start gap-4 overflow-hidden overflow-x-auto">
            {appraisalDetails?.certificate_expiration_date ? (
              <>
                {appraisalDetails?.certificate_image_url && (
                  <Image
                    src={appraisalDetails?.certificate_image_url}
                    alt="appraisal-img"
                    width={164}
                    height={124}
                    className="rounded-4"
                  />
                )}
              </>
            ) : (
              <>
                {appraisalDetails?.vehicle_condition_image_urls && (
                  <>
                    {appraisalDetails?.vehicle_condition_image_urls?.map(
                      (url) => (
                        <Image
                          key={url}
                          src="/images/appraisal.png"
                          alt="appraisals-img"
                          width={164}
                          height={124}
                          className="rounded-4"
                        />
                      )
                    )}
                  </>
                )}

                {!appraisalDetails?.vehicle_condition_image_urls && (
                  <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                )}
              </>
            )}
          </div>
        </div>
      </div>

      {/* contract information */}
      <div className="rounded-xl border border-solid border-platinum p-8">
        <p className="mb-6 text-2xl font-bold text-arsenic">
          {t('insurances:appraisal.general.contract_information')}
        </p>

        <div className="flex flex-col gap-3 [&>*:last-child]:border-0 [&>*:last-child]:pb-0">
          {contractInformation.map((contract) => (
            <div
              key={contract.id}
              className="flex items-center justify-between border-0 border-b border-solid border-ghost-white pb-3 text-base"
            >
              <p className=" w-1/2 font-normal text-nickel">
                {t(`insurances:appraisal.general.${contract.label}`)}
              </p>
              <p className="w-1/2 text-right font-medium text-arsenic">
                {contract.value}
              </p>
            </div>
          ))}
          {/* <div className="flex items-center justify-between border-0 border-b border-solid border-ghost-white pb-3 text-base">
            <p className=" w-1/2 font-normal text-nickel">
              {t('insurances:appraisal.general.insurance_period')}
            </p>
            <p className="w-1/2 text-right font-medium text-arsenic">
              {dayjs(insurance_period_from).format('DD/MM/YYYY')}
              &nbsp;-&nbsp;
              {dayjs(insurance_period_to).format('DD/MM/YYYY')}
            </p>
          </div> */}

          {/* <div className="flex items-center justify-between border-0 border-b border-solid border-ghost-white pb-3 text-base">
            <p className=" w-1/2 font-normal text-nickel">
              {t('insurances:appraisal.general.contract_holder')}
            </p>
            <p className="w-1/2 text-right font-medium text-arsenic">
              {appraisalDetails?.policyholder_full_name}
            </p>
          </div> */}

          {/* <div className="flex items-center justify-between border-0 border-b border-solid border-ghost-white pb-3 text-base">
            <p className=" w-1/2 font-normal text-nickel">
              {t('insurances:appraisal.general.address')}
            </p>
            <p className="w-1/2 text-right font-medium text-arsenic">
              {appraisalDetails?.policyholder_address}
            </p>
          </div> */}

          {/* <div className="flex items-center justify-between border-0 border-b border-solid border-ghost-white pb-3 text-base">
            <p className=" w-1/2 font-normal text-nickel">
              {t('insurances:appraisal.general.phone_number')}
            </p>
            <p className="w-1/2 text-right font-medium text-arsenic">
              {appraisalDetails?.policyholder_phone_number}
            </p>
          </div> */}

          {/* <div className="flex items-center justify-between border-0 border-b border-solid border-ghost-white pb-3 text-base">
            <p className=" w-1/2 font-normal text-nickel">
              {t('insurances:appraisal.general.email')}
            </p>
            <p className="w-1/2 text-right font-medium text-arsenic">
              {appraisalDetails?.policyholder_email}
            </p>
          </div> */}
        </div>
      </div>

      {/* bank beneficiary vehicle information */}
      <div className="rounded-xl border border-solid border-platinum p-8">
        <p className="mb-6 text-2xl font-bold text-arsenic">
          {t('insurances:appraisal.general.bank_beneficiary')}
        </p>

        <div className="flex flex-col gap-3 [&>*:last-child]:border-0 [&>*:last-child]:pb-0">
          {bankBeneficiaryInformation.map((bank) => (
            <div
              key={bank.id}
              className="flex items-center justify-between border-0 border-b border-solid border-ghost-white pb-3 text-base"
            >
              <p className=" w-1/2 font-normal text-nickel">
                {t(`insurances:appraisal.general.${bank.label}`)}
              </p>
              <p className="w-1/2 text-right font-medium text-arsenic">
                {bank.value}
              </p>
            </div>
          ))}
        </div>
      </div>

      {/* electronic invoices information */}
      <div className="rounded-xl border border-solid border-platinum p-8">
        <p className="mb-6 text-2xl font-bold text-arsenic">
          {t('insurances:appraisal.general.electronic_invoices')}
        </p>

        <div className="flex flex-col gap-3 [&>*:last-child]:border-0 [&>*:last-child]:pb-0">
          {electronicInvoicesInformation.map((invoice) => (
            <div
              key={invoice.id}
              className="flex items-center justify-between border-0 border-b border-solid border-ghost-white pb-3 text-base"
            >
              <p className=" w-1/2 font-normal text-nickel">
                {t(`insurances:appraisal.general.${invoice.label}`)}
              </p>
              <p className="w-1/2 text-right font-medium text-arsenic">
                {invoice.value}
              </p>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}

export default AppraisalInformation
