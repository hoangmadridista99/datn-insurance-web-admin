import { Button, Form, InputNumber } from 'antd'
import React, { useState } from 'react'
import { useTranslation } from 'next-i18next'
import CustomCollapse from './CustomCollapse'
import { postCarCost } from '@services/insurances'
import {
  handleFormatter,
  handleFormatterWithCommas,
  handleParser,
} from '@utils/helper'
import { TagSvg } from '@utils/icons'
import { useInputNumber } from '@hooks/useInputNumber'

const Assessment = ({
  form,
  idSelected = null,
  setIdSelected,
  setCarCostId,
  status,
}) => {
  const { t } = useTranslation(['insurances'])
  const initialValues = {
    appraisal: null,
  }
  const { onKeyDown } = useInputNumber()

  const [companies, setCompanies] = useState([])
  const [isCalculate, setIsCalculate] = useState(false)
  const [carPrice, setCarPrice] = useState(0)

  const handleCalculate = async (fieldValues) => {
    try {
      const inputValue = fieldValues.appraisal
      if (isCalculate || !inputValue) return setIsCalculate(false)

      const params = { car_cost: inputValue }

      const response = await postCarCost(params)

      if (response.status === 201 && response.data) {
        const { appraisal_data: appraisalData, id } = response.data

        setCarPrice(inputValue)
        setIsCalculate(true)
        setCompanies(appraisalData)
        setCarCostId(id)
      }
    } catch (error) {
      console.log('Error', error)
    }
  }

  return (
    <Form
      form={form}
      layout="vertical"
      onFinish={handleCalculate}
      className="assessment-form"
      initialValues={initialValues}
    >
      <Form.Provider>
        <div className="mb-6 w-full">
          <div className="rounded-lg border border-solid border-platinum p-8">
            <div className="mb-6">
              <p className="mb-2 text-2xl font-bold text-arsenic">
                {t('insurances:appraisal.appraisal_car_value')}
              </p>
              {isCalculate && (
                <p className="text-base font-normal text-nickel">
                  {t('insurances:appraisal.general.cost_base_on_car_value')}
                </p>
              )}
            </div>
            <div className="flex w-full items-start justify-between gap-2">
              <div className="flex w-full items-start justify-between">
                {isCalculate ? (
                  <div className="flex items-center gap-2">
                    <TagSvg />
                    <p className="text-xl font-bold text-outer-space">
                      {handleFormatterWithCommas(carPrice)}
                    </p>
                  </div>
                ) : (
                  <>
                    <Form.Item
                      className="h-fit w-full"
                      name="appraisal"
                      rules={[
                        {
                          required: true,
                          message: t('insurances:form:validation:car_value'),
                        },
                      ]}
                    >
                      <InputNumber
                        className="w-full rounded-r-none border-r-0"
                        placeholder={t('insurances:form.placeholder.car_value')}
                        controls={false}
                        formatter={handleFormatter}
                        parser={handleParser}
                        size="large"
                        onKeyDown={(event) => onKeyDown(event, 'appraisal')}
                      />
                    </Form.Item>
                    <div className="mr-1 flex items-center rounded-r-md border border-solid border-light-silver bg-platinum py-1.75 px-3 text-base font-normal text-metallic">
                      VNĐ
                    </div>
                  </>
                )}
              </div>

              <Button
                type="default"
                htmlType="submit"
                size="large"
                className="rounded-lg border-0 bg-fresh-air px-10 text-base font-bold text-ultramarine-blue"
              >
                {isCalculate
                  ? t('insurances:appraisal.change_value')
                  : t('insurances:appraisal.calculate_insurance_cost')}
              </Button>
            </div>
          </div>
        </div>

        {isCalculate && companies.length > 1 && (
          <div className="mb-20 w-full rounded-lg border border-solid border-platinum p-8">
            <p className="mb-6 w-full text-2xl font-bold text-arsenic">
              {t('insurances:appraisal.insurers')}
            </p>

            {companies.map((company) => {
              return (
                <CustomCollapse
                  key={company.physical_setting_id}
                  company={company}
                  setIdSelected={setIdSelected}
                  idSelected={idSelected}
                  status={status}
                />
              )
            })}
          </div>
        )}
      </Form.Provider>
    </Form>
  )
}

export default Assessment
