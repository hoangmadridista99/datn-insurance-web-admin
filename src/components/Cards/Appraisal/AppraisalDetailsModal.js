import { CheckSvg, CloseIconSvg } from '@utils/icons'
import { Button, Form, Input, Modal, Tabs } from 'antd'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'next-i18next'
import { APPRAISAL_STATUS, STATUSES } from '@utils/constants/insurances'
import AppraisalInformation from './AppraisalInfomation'
import Assessment from './Assessment'
import CheckIconSvg from 'components/Icon/Check'
import CancelIconSvg from 'components/Icon/Cancel'
import ClockIconSvg from 'components/Icon/Clock'
import SuccessfulEvaluation from './SuccessfulEvaluation'
import {
  getAppraisalDetails,
  patchAppraisalApproved,
  patchAppraisalRejected,
} from '@services/appraisals'

const { useForm } = Form

const ACTIVE_STEP = {
  GENERAL: 'general',
  VALUATION: 'valuation',
}

const { TextArea } = Input

const AppraisalDetailsModal = ({
  isModalOpen,
  handleCancel,
  appraisalId,
  status,
  setCardStatus,
  setReasonRejected,
  reasonRejected,
}) => {
  const { t } = useTranslation(['insurances'])
  const [form] = useForm()

  const [appraisalDetails, setAppraisalDetails] = useState()
  const [activeStep, setActiveStep] = useState(ACTIVE_STEP.GENERAL)
  const [idSelected, setIdSelected] = useState([])
  const [carCostId, setCarCostId] = useState()
  const [isRejectModal, setIsRejectModal] = useState(false)

  const cancelText =
    activeStep === ACTIVE_STEP.GENERAL
      ? t('insurances:appraisal:button:reject')
      : t('insurances:appraisal:button:back')
  const okText =
    activeStep === ACTIVE_STEP.GENERAL
      ? t('insurances:appraisal:button:approve')
      : t('insurances:appraisal:button:submit')

  const handleOpenRejectModal = () => setIsRejectModal(true)
  const handleCloseRejectModal = () => setIsRejectModal(false)

  const fetchAppraisalDetails = useCallback(async () => {
    try {
      if (!appraisalId) return

      const response = await getAppraisalDetails(appraisalId)

      setAppraisalDetails(response)
    } catch (error) {
      console.log('Error', error)
    }
  }, [appraisalId])

  useEffect(() => {
    fetchAppraisalDetails()
  }, [fetchAppraisalDetails])

  const handleChangeTab = (activeKey) => setActiveStep(activeKey)

  const Status = ({ status }) => {
    const className = 'flex items-center gap-1.5 text-base'
    switch (status) {
      case APPRAISAL_STATUS.PENDING:
        return (
          <div className={className}>
            <ClockIconSvg />
            <p className="text-royal-orange">
              {t('insurances:appraisal.general.pending')}
            </p>
          </div>
        )
      case APPRAISAL_STATUS.APPROVED:
        return (
          <div className={className}>
            <CheckIconSvg />
            <p className="text-apple">
              {t('insurances:appraisal.general.approve')}
            </p>
          </div>
        )
      case APPRAISAL_STATUS.REJECTED:
        return (
          <div className={className}>
            <CancelIconSvg />
            <p className="text-vermilion">
              {t('insurances:appraisal.general.reject')}
            </p>
          </div>
        )

      default:
        return null
    }
  }

  const TabLabelGeneralByStatusPending = () => (
    <div className="flex items-center gap-2">
      <p className="flex h-6 w-6 items-center justify-center rounded-full bg-ultramarine-blue text-white">
        {activeStep === ACTIVE_STEP.VALUATION ? <CheckSvg /> : 1}
      </p>
      <p className="text-ultramarine-blue">
        {t('insurances:appraisal.general.check_information')}
      </p>
    </div>
  )

  const TabLabelValuationByStatusPending = () => (
    <div className="flex items-center gap-2">
      <p
        className={`flex h-6 w-6 items-center justify-center rounded-full border border-solid border-platinum ${
          activeStep === ACTIVE_STEP.VALUATION
            ? 'bg-ultramarine-blue text-white'
            : ''
        }`}
      >
        2
      </p>
      <p>{t('insurances:appraisal.general.valuation')}</p>
    </div>
  )

  const items = [
    {
      key: 'general',
      label:
        status === APPRAISAL_STATUS.APPROVED ? (
          t('insurances:appraisal.general.general_information')
        ) : (
          <TabLabelGeneralByStatusPending />
        ),
    },
    {
      key: 'valuation',
      label:
        status === APPRAISAL_STATUS.APPROVED ? (
          t('insurances:appraisal.general.vehicle_insurance_valuation')
        ) : (
          <TabLabelValuationByStatusPending />
        ),
      disabled: activeStep === 'general' && status === STATUSES.PENDING,
    },
  ]

  const handleRejectForm = async () => {
    try {
      const newParams = {
        form_status: 'rejected',
        reason: reasonRejected,
      }
      const response = await patchAppraisalRejected(appraisalId, newParams)

      if (response.status === 204) {
        handleCloseRejectModal()
        setCardStatus(STATUSES.REJECTED)
        setAppraisalDetails((prevState) => ({ ...prevState, ...newParams }))
      }
    } catch (error) {
      console.log('Error', error)
    }
  }

  const onCancel = () => {
    if (activeStep !== ACTIVE_STEP.GENERAL) {
      return handleChangeTab(ACTIVE_STEP.GENERAL)
    }

    if (activeStep === ACTIVE_STEP.GENERAL && status !== STATUSES.PENDING) {
      return handleCancel()
    }

    return handleOpenRejectModal()
  }

  const handleSubmitForm = async () => {
    if (activeStep === 'general') {
      return handleChangeTab(ACTIVE_STEP.VALUATION)
    }

    if (status !== STATUSES.PENDING) {
      return handleCancel()
    }

    try {
      const params = {
        appraisal_form_id: appraisalDetails.id,
        physical_settings_ids: idSelected,
      }

      const response = await patchAppraisalApproved(carCostId, params)

      setCardStatus(STATUSES.APPROVED)
      setActiveStep(ACTIVE_STEP.GENERAL)
      setAppraisalDetails(response)
    } catch (error) {
      console.log('Error', error)
    }
  }

  return (
    <Modal
      open={isModalOpen}
      okText={okText}
      onCancel={handleCancel}
      cancelText={cancelText}
      width={820}
      closeIcon={<CloseIconSvg />}
      centered
      className="appraisal-details-modal"
      footer={
        status === STATUSES.PENDING
          ? [
              <Button
                type="default"
                key="back"
                onClick={onCancel}
                className="text-base font-bold text-nickel"
              >
                {cancelText}
              </Button>,
              <Button
                key="submit"
                type="primary"
                onClick={handleSubmitForm}
                className="text-base font-bold disabled:text-nickel"
                disabled={
                  status === STATUSES.PENDING &&
                  idSelected.length < 1 &&
                  activeStep !== ACTIVE_STEP.GENERAL
                }
              >
                {okText}
              </Button>,
            ]
          : null
      }
    >
      <div className="w-full py-3">
        <div
          className={` ${
            status === APPRAISAL_STATUS.REJECTED
              ? 'border-0 border-b border-solid border-platinum pb-4'
              : ''
          }`}
        >
          <p className="mb-3 text-start text-8 font-bold leading-12 text-arsenic">
            {t('insurances:appraisal.general.car_insurance')}
          </p>
          <div className="flex items-center justify-between">
            <div className="flex flex-col items-start gap-1.5">
              <div className="flex items-center gap-2 text-base text-nickel">
                <span>{t('insurances:fields.status')}:</span>
                <Status status={status} />
              </div>

              {appraisalDetails?.reason && (
                <p className="text-sm text-spanish-gray">
                  {t('insurances:appraisal.general.reject_reason')}:&nbsp;
                  {appraisalDetails?.reason})
                </p>
              )}
            </div>
            <p className="text-nickel">
              {t('insurances:appraisal.general.code_number')}:&nbsp;
              <span className="font-bold">{appraisalDetails?.form_code}</span>
            </p>
          </div>
          {(status === APPRAISAL_STATUS.PENDING ||
            status === APPRAISAL_STATUS.APPROVED) && (
            <Tabs
              className={`mt-8 ${
                status === APPRAISAL_STATUS.PENDING
                  ? 'tab-underline-hidden'
                  : ''
              }`}
              activeKey={activeStep}
              items={items}
              onChange={handleChangeTab}
            />
          )}
        </div>

        <div className="no-scrollbar flex h-[65vh] w-full flex-col gap-6 overflow-hidden overflow-y-auto pt-8">
          {activeStep === ACTIVE_STEP.GENERAL && (
            <AppraisalInformation
              status={status}
              appraisalDetails={appraisalDetails}
            />
          )}

          {status === APPRAISAL_STATUS.PENDING &&
            activeStep === ACTIVE_STEP.VALUATION && (
              <Assessment
                form={form}
                setIdSelected={setIdSelected}
                idSelected={idSelected}
                setCarCostId={setCarCostId}
                status={status}
              />
            )}

          {status === APPRAISAL_STATUS.APPROVED &&
            activeStep === ACTIVE_STEP.VALUATION && (
              <SuccessfulEvaluation
                status={status}
                appraisal={appraisalDetails?.appraisal}
              />
            )}
        </div>
      </div>

      {isRejectModal && (
        <Modal
          open={isRejectModal}
          centered
          footer={null}
          onCancel={handleCloseRejectModal}
          closeIcon={<CloseIconSvg />}
        >
          <div className="mx-auto flex w-4/5 flex-col items-center py-8">
            <p className="mb-2 text-2xl font-bold text-outer-space">
              {t('insurances:fields:refuse_approve')}
            </p>
            <p className="mb-4 text-nickel">
              {t('insurances:fields:refuse_approve_reason')}
            </p>
            <TextArea
              onChange={(event) => setReasonRejected(event.target.value)}
              placeholder={t('insurances:form:placeholder:input_reason')}
              className="h-20 w-full"
            />
            <div className="mt-6 flex w-full items-center">
              <Button
                className="mr-2 w-full"
                onClick={() => {
                  handleCloseRejectModal()
                  setReasonRejected(null)
                }}
              >
                {t('insurances:fields:cancel')}
              </Button>
              <Button
                className="ml-2 w-full"
                type="primary"
                onClick={handleRejectForm}
                disabled={!reasonRejected}
              >
                {t('insurances:fields:accept')}
              </Button>
            </div>
          </div>
        </Modal>
      )}
    </Modal>
  )
}

export default AppraisalDetailsModal
