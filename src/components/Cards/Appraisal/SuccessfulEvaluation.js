import React from 'react'
import TagSvgIcon from 'components/Icon/Tag'
import CustomCollapse from './CustomCollapse'
import { handleFormatterWithCommas } from '@utils/helper'

const SuccessfulEvaluation = ({ appraisal, status }) => {
  return (
    <>
      <div className="mb-6 rounded-lg border border-solid border-platinum p-8">
        <p className="mb-2 text-2xl font-bold text-arsenic">Giá trị xe</p>
        <div className="flex items-center justify-between">
          <p className="text-base text-nickel">Giá trị xe sau khi thẩm định</p>
          <p className="flex items-center gap-2">
            <TagSvgIcon />
            <span className="text-xl font-bold text-outer-space">
              {handleFormatterWithCommas(appraisal?.car_cost)}&nbsp;VNĐ
            </span>
          </p>
        </div>
      </div>
      <div className="rounded-lg border border-solid border-platinum p-8">
        <p className="mb-6 text-2xl font-bold text-arsenic">Nhà bảo hiểm</p>
        {appraisal?.appraisal_data.map((company) => (
          <CustomCollapse
            key={company.physical_setting_id}
            company={company}
            status={status}
          />
        ))}
      </div>
    </>
  )
}

export default SuccessfulEvaluation
