import { Checkbox, Collapse } from 'antd'
import React, { useState } from 'react'
import { useTranslation } from 'next-i18next'
import Image from 'next/image'
import ArrowDown from 'components/Icon/ArrowDown'
import { handleFormatterWithCommas } from '@utils/helper'
import { STATUSES } from '@utils/constants/insurances'

const CustomCollapse = ({ company, setIdSelected, idSelected, status }) => {
  const { t } = useTranslation(['insurances'])
  const [checked, setChecked] = useState(null)

  const onChange = (event) => {
    event.preventDefault()
    event.stopPropagation()
    const {
      target: { checked },
    } = event

    if (!checked) {
      let cloneArray = [...idSelected]

      const isExist = cloneArray.some(
        (id) => id === company.physical_setting_id
      )

      if (isExist) {
        const index = cloneArray.indexOf(company.physical_setting_id)

        cloneArray.splice(index, 1)
        setIdSelected(cloneArray)
      } else {
        setIdSelected((prevState) => [
          ...prevState,
          company.physical_setting_id,
        ])
      }
    } else {
      setIdSelected((prevState) => [...prevState, company.physical_setting_id])
    }

    setChecked(checked)
  }

  const HeaderPanel = () => (
    <div className="order-0 flex items-center justify-between">
      <div className="flex items-center gap-4">
        {status === STATUSES.PENDING && (
          <div className="top-3.5 left-4 z-10 my-auto h-full py-4">
            <Checkbox onChange={onChange} checked={checked} />
          </div>
        )}

        <Image
          src={company?.company_logo_url || company?.insurance?.company?.logo}
          width={48}
          height={48}
          alt="Logo"
        />
        <p className="text-base font-bold text-arsenic">
          {company?.company_name || company?.insurance?.name}
        </p>
      </div>
      <p className="text-sm text-nickel">
        {t('insurances:appraisal.collapse.maximum')}&nbsp;
        <span className="font-bold text-azure">
          {handleFormatterWithCommas(company?.maximum_price)}&nbsp;VNĐ
        </span>
        &nbsp;/{t('insurances:fields.year')}
      </p>
    </div>
  )

  return (
    <div className="mb-4 flex w-full items-center justify-start gap-4">
      <Collapse
        expandIconPosition="end"
        className="collapse-auto-insurance mb-4 flex-1 bg-white last:mb-0"
        expandIcon={({ isActive }) => (
          <ArrowDown
            className={`stroke-arsenic transition duration-300 ${
              !isActive ? 'rotate-90' : ''
            }`}
          />
        )}
      >
        <Collapse.Panel key="default" header={<HeaderPanel />}>
          <div className="mb-6 flex items-center justify-between text-base font-bold text-nickel last:mb-0">
            <p>{t('insurances:appraisal.collapse.insurance_cost')}</p>
            <p className="text-outer-space">
              {handleFormatterWithCommas(company?.cost_of_purchasing)}
              &nbsp;VNĐ&nbsp;
              <span className="font-normal">/năm</span>
            </p>
          </div>
          <div className="mb-6 last:mb-0">
            <p className="mb-5 text-base font-bold text-nickel">
              {t('insurances:appraisal.collapse.advanced_benefits_cost')}
            </p>
            <div className="mb-4 flex items-center justify-between text-nickel last:mb-0">
              <p>
                {t('insurances:appraisal.benefits.is_choosing_service_center')}
              </p>
              <p className="text-base font-normal text-outer-space">
                +&nbsp;
                {handleFormatterWithCommas(company?.choosing_service_center)}
                &nbsp;VNĐ
              </p>
            </div>
            <div className="mb-4 flex items-center justify-between text-nickel last:mb-0">
              <p>
                {t('insurances:appraisal.benefits.is_component_vehicle_theft')}
              </p>
              <p className="text-base font-normal text-outer-space">
                +&nbsp;
                {handleFormatterWithCommas(company?.component_vehicle_theft)}
                &nbsp;VNĐ
              </p>
            </div>
            <div className="mb-4 flex items-center justify-between text-nickel last:mb-0">
              <p>
                {t('insurances:appraisal.benefits.is_no_depreciation_cost')}
              </p>
              <p className="text-base font-normal text-outer-space">
                +&nbsp;
                {handleFormatterWithCommas(company?.no_depreciation_cost)}
                &nbsp;VNĐ
              </p>
            </div>
            <div className="mb-4 flex items-center justify-between text-nickel last:mb-0">
              <p>{t('insurances:appraisal.benefits.is_water_damage')}</p>
              <p className="text-base font-normal text-outer-space">
                +&nbsp;{handleFormatterWithCommas(company?.water_damage)}
                &nbsp;VNĐ
              </p>
            </div>
          </div>
          <div className="mb-6 last:mb-0">
            <p className="mb-5 text-base font-bold text-nickel">
              {t('insurances:appraisal.collapse.additional_benefits_cost')}
            </p>
            <div className="mb-4 flex items-center justify-between text-nickel last:mb-0">
              <p>
                {t('insurances:appraisal.benefits.insured_for_each_person')}
              </p>
              <p className="text-base font-normal text-outer-space">
                +&nbsp;
                {handleFormatterWithCommas(company?.insured_for_each_person)}
                &nbsp;VNĐ
              </p>
            </div>
          </div>
        </Collapse.Panel>
      </Collapse>
    </div>
  )
}

export default CustomCollapse
