import React, { useState, useEffect, useCallback } from 'react'
import classNames from 'classnames'
import { Dropdown, notification } from 'antd'
import { removeAdmin, updateAdmin } from '@services/admin'
import { handleError } from '@helpers/handleError'
import ArrowDown from 'components/Icon/ArrowDown'
import { useTranslation } from 'next-i18next'

const CardUser = ({ onUpdate, onUpdateSuccess, handleGetData, ...data }) => {
  const { t } = useTranslation(['operators', 'insurances'])

  const [isRemove, setIsRemove] = useState(false)
  const [isActionBlock, setIsActionBlock] = useState(false)

  const handleRemove = useCallback(
    async (signal) => {
      await removeAdmin(data.id, signal)

      handleGetData()
    },
    [data.id, handleGetData]
  )

  const handleActionBlock = useCallback(
    async (signal) => {
      try {
        const body = { is_blocked: !data.is_blocked }
        await updateAdmin(data.id, body, signal)

        onUpdateSuccess(data.id, body)
        notification.success({
          message: data.is_blocked
            ? t('operators:messages:unlocked_success')
            : t('operators:messages:locked_success'),
        })
      } catch (error) {
        console.log('Error', error)
      }
    },
    [data.id, data.is_blocked, onUpdateSuccess, t]
  )

  useEffect(() => {
    const controller = new AbortController()

    if (isRemove) {
      handleRemove(controller.signal)
        .then(() =>
          notification.success({
            message: t('operators:messages:delete_success'),
          })
        )
        .catch((error) => handleError(error))
        .finally(() => setIsRemove(false))
    }

    return () => controller.abort()
  }, [t, isRemove, handleRemove])

  useEffect(() => {
    const controller = new AbortController()

    if (isActionBlock) {
      handleActionBlock(controller.signal)
        .catch((error) => handleError(error))
        .finally(() => setIsActionBlock(false))
    }

    return () => controller.abort()
  }, [handleActionBlock, isActionBlock])

  const handleClickUpdate = () => onUpdate(data)
  const handleClickActionBlock = () => setIsActionBlock(true)
  const handleClickRemove = () => setIsRemove(true)

  const items = [
    {
      key: 'update',
      label: (
        <p
          className="cursor-pointer
          text-base text-nickel transition duration-300 hover:text-azure"
          onClick={handleClickUpdate}
        >
          {t('operators:button:edit')}
        </p>
      ),
    },
    {
      key: data.is_blocked ? 'unlock' : 'lock',
      label: (
        <p
          className="cursor-pointer
          text-base text-nickel transition duration-300 hover:text-azure"
          onClick={handleClickActionBlock}
        >
          {data.is_blocked
            ? t('operators:button:unlock')
            : t('operators:button:lock')}
        </p>
      ),
    },
    {
      key: 'delete',
      label: (
        <p
          className="cursor-pointer
          text-base text-nickel transition duration-300 hover:text-azure"
          onClick={handleClickRemove}
        >
          {t('operators:button:delete')}
        </p>
      ),
    },
  ]

  return (
    <div className="mb-4 flex items-center rounded-lg border border-solid border-platinum py-6 px-4 last:mb-0">
      <p className="w-1/3 text-base font-bold text-arsenic">{`${data.first_name} ${data.last_name}`}</p>
      <p className="w-1/4 text-sm text-nickel">{data.account}</p>
      <div className="w-1/4">
        <span
          className={classNames(
            'inline-block rounded-2xl py-1 px-4 text-base',
            {
              'bg-lavender text-azure': !data.is_blocked,
              'bg-ghost-white text-metallic': data.is_blocked,
            }
          )}
        >
          {data.is_blocked
            ? t('operators:fields:locked')
            : t('operators:fields:unlocked')}
        </span>
      </div>
      <div className="flex w-1/4 justify-end">
        <Dropdown
          // disabled={isDisableDropdown}
          placement="bottom"
          menu={{ items }}
          trigger={['click']}
        >
          <button className="group flex cursor-pointer items-center justify-center rounded-lg border border-nickel bg-transparent px-4 py-2 text-nickel transition hover:border-ultramarine-blue [&_span]:hover:text-ultramarine-blue [&_svg]:order-1 [&_svg]:hover:stroke-ultramarine-blue">
            <p className="mr-2 text-base group-hover:text-ultramarine-blue">
              {t('insurances:card:action')}
            </p>

            <ArrowDown />
          </button>
        </Dropdown>
      </div>
    </div>
  )
}

export default CardUser
