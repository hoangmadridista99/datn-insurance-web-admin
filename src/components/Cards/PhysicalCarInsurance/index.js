import React from 'react'
import Image from 'next/image'
import { Button, Card } from 'antd'
import { useRouter } from 'next/router'
import { formatCurrency } from '@helpers/formatCurrency'
import dayjs from 'dayjs'
import { useTranslation } from 'next-i18next'
import { PHYSICAL_CAR_TYPE } from '@utils/constants/insurances'

const CardPhysicalCarInsurance = ({ isLoading, data }) => {
  const router = useRouter()
  const { t } = useTranslation(['insurance-price'])

  const handleClickUpdatePrice = () =>
    router.push(`${router.asPath}/physical-car/${data?.id}`)

  return (
    <Card
      loading={isLoading}
      className="mb-4 p-4 last:mb-0"
      bodyStyle={{ height: '100%', padding: 0 }}
    >
      <div className="flex items-center gap-1">
        <div className="flex w-3/12 items-center gap-4">
          <Image
            width={48}
            height={48}
            src={data?.company_logo_url}
            alt="logo"
            className="object-contain"
          />
          <p className="text-base font-bold text-arsenic">
            {data?.insurance_name}
          </p>
        </div>
        <div className="flex w-4/12 items-center">
          {!data?.physical_setting && (
            <p className="text-base text-metallic">
              {t('insurance-price:physical_car:card:no_physical')}
            </p>
          )}
          {data.physical_setting && (
            <p className="mb-1 text-base font-bold text-ultramarine-blue">
              {`${
                data.physical_setting.cost_of_purchasing.type ===
                PHYSICAL_CAR_TYPE.COST
                  ? formatCurrency(
                      data.physical_setting?.cost_of_purchasing?.value ?? 0
                    )
                  : data.physical_setting?.cost_of_purchasing.value
              } ${
                data.physical_setting.cost_of_purchasing.type ===
                PHYSICAL_CAR_TYPE.COST
                  ? 'VNĐ'
                  : '%'
              }`}
            </p>
          )}
        </div>
        <div className="w-3/12">
          <p className="mb-1 text-sm font-bold text-nickel">
            {dayjs(new Date(data.created_at)).format('DD/MM/YYYY - HH:mm')}
          </p>
          {data?.physical_setting?.user && (
            <p className="text-xs text-spanish-gray">
              {`${t('insurance-price:physical_car:card:update_by')}: `}
              <span className="text-sm font-bold">{`${data?.physical_setting?.user?.first_name} ${data?.physical_setting?.user?.last_name}`}</span>
            </p>
          )}
        </div>
        <div className="flex w-2/12 justify-end">
          <Button
            onClick={handleClickUpdatePrice}
            className="border-nickel text-nickel"
            size="large"
          >
            {t('insurance-price:physical_car:card:button')}
          </Button>
        </div>
      </div>
    </Card>
  )
}

export default CardPhysicalCarInsurance
