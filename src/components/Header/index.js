import React, { useState, useEffect } from 'react'
import Image from 'next/image'
import { useSession } from 'next-auth/react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useTranslation } from 'next-i18next'
import { signOut } from 'next-auth/react'
import ArrowDownBoldIconSvg from 'components/Icon/ArrowDownBold'
import InternetIconSvg from 'components/Icon/Internet'
import ProfileIconSvg from 'components/Icon/Profile'
import LogoutIconSvg from 'components/Icon/Logout'
import { USER_ROLE } from '@utils/constants/operator'

const Header = () => {
  const [isShowLanguages, setIsShowLanguages] = useState(false)
  const [isShowProfile, setIsShowProfile] = useState(false)
  const [locale, setLocale] = useState('vi')

  const session = useSession()
  const router = useRouter()
  const { t } = useTranslation(['common'])

  useEffect(() => {
    if (window) {
      const language = window.navigator.language

      if (language.includes('en')) {
        setLocale('en')
      }
    }
  }, [])
  useEffect(() => {
    const handleDisableShowLanguages = () => {
      setIsShowLanguages(false)
      setIsShowProfile(false)
    }

    router.events.on('routeChangeStart', handleDisableShowLanguages)

    setLocale(router.locale ?? 'vi')

    return () =>
      router.events.off('routeChangeStart', handleDisableShowLanguages)
  }, [router])

  const handleToggleLanguages = () => {
    setIsShowLanguages(!isShowLanguages)
    if (isShowProfile) {
      setIsShowProfile(false)
    }
  }
  const handleToggleProfile = () => {
    setIsShowProfile(!isShowProfile)
    if (isShowLanguages) {
      setIsShowLanguages(false)
    }
  }
  const handleClickBackground = () => {
    setIsShowLanguages(false)
    setIsShowProfile(false)
  }
  const handleClickLogout = () => signOut()

  return (
    <>
      <div
        onClick={handleClickBackground}
        className="aria-hidden:-z-1 fixed z-40 h-screen w-screen aria-hidden:hidden"
        aria-hidden={!isShowLanguages && !isShowProfile}
      />
      <header className="flex h-14 items-center justify-end border-2 border-solid border-cultured bg-white px-6 py-4">
        <div className="relative">
          <button
            className={`relative flex cursor-pointer items-center border-none bg-transparent ${
              isShowLanguages || isShowProfile ? 'z-50' : ''
            }`}
            type="button"
            onClick={handleToggleLanguages}
          >
            <InternetIconSvg />
            <span className="mx-2 text-nickel">{t(`common:${locale}`)}</span>
            <ArrowDownBoldIconSvg />
          </button>
          <div
            className="aria-hidden:opacity-200 top-7/5 absolute -z-10 w-full scale-95 rounded-lg bg-white py-3 text-center opacity-0 shadow blur-lg transition duration-500 aria-hidden:z-50 aria-hidden:opacity-100 aria-hidden:blur-none"
            aria-hidden={isShowLanguages}
          >
            <Link
              className="duration-400 block w-full px-3 py-1 text-nickel transition hover:bg-azure hover:text-cultured"
              href={router.asPath}
              locale="vi"
            >
              {t('common:vi')}
            </Link>
            <Link
              className="duration-400 block w-full px-3 py-1 text-nickel transition hover:bg-azure hover:text-cultured"
              href={router.asPath}
              locale="en"
            >
              {t('common:en')}
            </Link>
          </div>
        </div>
        <div className="mx-4 h-full w-px bg-platinum" />
        <div className="relative overflow-visible">
          <button
            className={`relative flex cursor-pointer items-center border-none bg-transparent ${
              isShowLanguages || isShowProfile ? 'z-50' : ''
            }`}
            type="button"
            onClick={handleToggleProfile}
          >
            <div className="relative h-6 w-6 rounded-full">
              <Image
                src={
                  session?.data?.user?.avatar_profile_url ??
                  '/svg/avatar-default.svg'
                }
                fill
                className="rounded-full"
                alt="Avatar"
              />
            </div>
            <p className="ml-1 text-sm text-arsenic">
              {session?.data?.user?.first_name && session?.data?.user?.last_name
                ? `${session?.data?.user?.first_name} ${session?.data?.user?.last_name}`
                : 'Admin'}
            </p>
            <ArrowDownBoldIconSvg />
          </button>
          <div
            className="aria-hidden:opacity-200 top-7/5 absolute right-0 -z-10 w-auto scale-95 rounded-lg bg-white p-4 opacity-0 shadow blur-lg transition duration-500 aria-hidden:z-50 aria-hidden:opacity-100 aria-hidden:blur-none"
            aria-hidden={isShowProfile}
          >
            <div className="mb-3 flex items-center border-0 border-b border-solid border-cultured pb-3 last:mb-0 last:pb-0">
              <div className="relative mr-2 h-10 w-10 rounded-full">
                <Image
                  src={
                    session?.data?.user?.avatar_profile_url ??
                    '/svg/avatar-default.svg'
                  }
                  fill
                  className="rounded-full"
                  alt="Avatar"
                />
              </div>
              <div className="flex flex-col justify-between">
                <p className="whitespace-nowrap text-sm text-nickel">
                  {session?.data?.user?.first_name &&
                  session?.data?.user?.last_name
                    ? `${session?.data?.user?.first_name} ${session?.data?.user?.last_name}`
                    : 'Vendor'}
                </p>
                <p className="text-xs font-medium text-arsenic">
                  {session?.data?.role === USER_ROLE.SUPER_ADMIN
                    ? 'Super Admin'
                    : 'Admin'}
                </p>
              </div>
            </div>
            <Link
              href="/settings"
              className="group inline-block rounded-md py-1 px-2 text-arsenic transition duration-200 hover:bg-azure hover:text-cultured"
            >
              <div className="flex items-center">
                <ProfileIconSvg className="stroke-arsenic group-hover:stroke-cultured" />
                <span className="ml-2 whitespace-nowrap">
                  {t('common:sidebar:settings')}
                </span>
              </div>
            </Link>
            <div className="my-3 border-0 border-b border-solid border-cultured" />
            <button
              className="group flex w-full cursor-pointer items-center rounded-md border-none bg-transparent py-1 px-2 text-arsenic transition duration-200 hover:bg-azure hover:text-cultured"
              type="button"
              onClick={handleClickLogout}
            >
              <LogoutIconSvg className="stroke-arsenic group-hover:stroke-cultured" />
              <span className="ml-2 whitespace-nowrap">
                {t('common:sidebar:logout')}
              </span>
            </button>
          </div>
        </div>
      </header>
    </>
  )
}

export default Header
