import React from 'react'
import Image from 'next/image'
import dayjs from 'dayjs'
import classNames from 'classnames'

const CommentBlog = ({ onClickUpdate, onClickRemove, data }) => {
  return (
    <div className="group mb-8 border-b border-b-ghost-white p-4 last:mb-0">
      <div className="mb-3 flex items-center justify-between">
        <div className="flex items-center">
          <div className="flex items-center">
            <Image
              src={data.user.avatar_profile_url ?? '/svg/avatar-default.svg'}
              width={20}
              height={20}
              alt="Avatar"
            />
            <span className="ml-2 text-sm font-bold text-arsenic">
              {`${data.user.first_name} ${data.user.last_name}`}
            </span>
          </div>
          <span className="mx-2">-</span>
          <span className="text-xs text-spanish-gray">
            {`${dayjs(new Date(data.created_at)).format('DD-MM-YYYY HH:mm')}`}
          </span>
        </div>
        <div className="flex items-center">
          <div
            className="mr-1 hidden h-6 w-6 cursor-pointer items-center justify-center rounded-full bg-spanish-gray-200 transition duration-500 group-hover:flex"
            onClick={onClickUpdate}
          >
            <Image
              src={data.is_hide ? '/svg/show.svg' : '/svg/hidden.svg'}
              width={15}
              height={15}
              alt="Remove Comment"
            />
          </div>
          <div
            className="ml-1 hidden h-6 w-6 cursor-pointer items-center justify-center rounded-full bg-spanish-gray-200 transition duration-500 group-hover:flex"
            onClick={onClickRemove}
          >
            <Image
              src="/svg/close-blog.svg"
              width={10}
              height={10}
              alt="Remove Comment"
            />
          </div>
        </div>
      </div>
      <p
        className={classNames('text-sm text-nickel', {
          'line-through': data.is_hide,
        })}
      >
        {data.comment}
      </p>
    </div>
  )
}

export default CommentBlog
