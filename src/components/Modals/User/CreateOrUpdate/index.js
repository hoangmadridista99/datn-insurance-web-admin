/* eslint-disable indent */
import React, { useCallback } from 'react'
import { Form, Modal, notification } from 'antd'
import { createAdmin, updateAdmin } from '@services/admin'
import { CloseIconSvg } from '@utils/icons'
import { useTranslation } from 'next-i18next'
import FormUser from 'components/Forms/User'
const { useForm } = Form

const CreateOrUpdateUserModal = ({
  initialValues,
  handleGetData,
  onClose,
  isOpenModal,
}) => {
  const { t } = useTranslation(['operators'])
  const [form] = useForm()

  const handleCreate = useCallback(async () => {
    try {
      const fieldValues = await form.validateFields()

      if (initialValues) {
        await updateAdmin(initialValues.id, fieldValues)
        notification.success({
          message: t('operators:messages:edit_success'),
        })
      } else {
        await createAdmin(fieldValues)
        notification.success({
          message: t('operators:messages:create_success'),
        })
      }

      form.resetFields()
      handleGetData()
      onClose()
    } catch (error) {
      if (initialValues) {
        return notification.error({
          message: t('operators:messages:edit_failed'),
        })
      }

      console.log('Error')
      return notification.error({
        message: t('operators:messages:create_failed'),
      })
    }
  }, [form, handleGetData, initialValues, onClose, t])

  return (
    <Modal
      width="60%"
      open={isOpenModal}
      centered
      destroyOnClose
      onCancel={onClose}
      closeIcon={<CloseIconSvg />}
      onOk={handleCreate}
      okText={
        !initialValues
          ? t('operators:button:create_account')
          : t('operators:button:save_changes')
      }
      cancelText={t('operators:button:cancel')}
    >
      <FormUser form={form} initialValues={initialValues} />
    </Modal>
  )
}

export default CreateOrUpdateUserModal
