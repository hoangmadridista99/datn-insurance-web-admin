import React, { useState, useCallback } from 'react'
import { Modal, Button, Input, notification } from 'antd'
import { STATUSES } from '@utils/constants/insurances'
import { updateStatusBlog } from '@services/blogs'
import { handleError } from '@helpers/handleError'
import { useTranslation } from 'next-i18next'

const { TextArea } = Input

const ReasonForRefusalBlogModal = ({
  isOpen,
  blogId,
  onClose,
  onUpdateSuccess,
}) => {
  const [isLoading, setIsLoading] = useState(false)
  const [reason, setReason] = useState()

  const { t } = useTranslation(['blogs'])

  const handleClickSubmitReject = useCallback(async () => {
    try {
      if (blogId) {
        setIsLoading(true)
        const body = {
          status: STATUSES.REJECTED,
          reason,
        }

        await updateStatusBlog(blogId, body)

        notification.success({ message: t('blogs:responses:update-status') })
        onUpdateSuccess(blogId, body)
        handleCloseModal()
      }
    } catch (error) {
      handleError(error)
    } finally {
      setIsLoading(false)
    }
  }, [blogId, handleCloseModal, onUpdateSuccess, reason, t])

  const handleChangeReason = (event) => {
    const {
      target: { value },
    } = event

    setReason(value)
  }

  const handleCloseModal = useCallback(() => {
    setReason(undefined)
    onClose()
  }, [onClose])

  return (
    <Modal
      open={isOpen}
      centered
      footer={null}
      onCancel={handleCloseModal}
      destroyOnClose
    >
      <div className="mx-auto flex w-4/5 flex-col items-center py-8">
        <p className="mb-2 text-2xl font-bold text-outer-space">
          {t('blogs:modals:rejected:title')}
        </p>
        <p className="mb-4 text-nickel">{t('blogs:modals:rejected:desc')}</p>
        <TextArea
          onChange={handleChangeReason}
          placeholder={t('blogs:placeholder:reason')}
          className="h-20 w-full"
        />
        <div className="mt-6 flex w-full items-center">
          <Button
            className="mr-2 w-full"
            onClick={handleCloseModal}
            disabled={isLoading}
          >
            {t('blogs:modals:rejected:button:cancel')}
          </Button>
          <Button
            className="ml-2 w-full"
            type="primary"
            onClick={handleClickSubmitReject}
            loading={isLoading}
            disabled={isLoading || !reason}
          >
            {t('blogs:modals:rejected:button:ok')}
          </Button>
        </div>
      </div>
    </Modal>
  )
}

export default ReasonForRefusalBlogModal
