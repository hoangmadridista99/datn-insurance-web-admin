import React from 'react'
import { Spin, Pagination, Modal, Avatar } from 'antd'
import CommentBlog from '@components/CommentBlog'
import { CloseIconSvg } from '@utils/icons'
import Image from 'next/image'
import dayjs from 'dayjs'
import { UserOutlined } from '@ant-design/icons'
import { useBlogPreviewModalLogic } from '@hooks/useBlogPreviewModalLogic'
import { isEmpty } from 'lodash'

const PreviewBlogModal = ({ isOpen, blog, onClose }) => {
  const {
    blogDetails,
    comments,
    pagination,
    handleCloseModal,
    handleClickUpdateComment,
    handleClickRemoveComment,
    handleClickPageComments,
  } = useBlogPreviewModalLogic({ blog, onClose })

  return (
    <Modal
      centered
      open={isOpen}
      onCancel={handleCloseModal}
      footer={false}
      width="70%"
      className="card-blog"
      closeIcon={<CloseIconSvg />}
      destroyOnClose
    >
      <div className="no-scrollbar max-h-[70vh] min-h-[40vh] overflow-hidden overflow-y-scroll">
        {blogDetails ? (
          <div className="">
            <div className="mb-3 flex items-center gap-2">
              {blogDetails.user.avatar_profile_url ? (
                <Image
                  src={blogDetails.user.avatar_profile_url}
                  fill
                  alt="Avatar"
                  className="static h-5 w-5 rounded-full"
                />
              ) : (
                <Avatar size={20} icon={<UserOutlined />} />
              )}
              <div className="flex items-center gap-1 text-xs text-nickel">
                <p className="text-sm">
                  {blogDetails.user.last_name}&nbsp;
                  {blogDetails.user.first_name}
                </p>
                <p>|</p>
                <p>
                  {dayjs(new Date(blogDetails.created_at)).format(
                    'DD MMMM YYYY'
                  )}
                </p>
              </div>
            </div>
            <p className="description mb-2 text-8 font-bold leading-12 text-arsenic">
              {blogDetails?.title}
            </p>
            <div className="mb-6">
              <p className="w-fit rounded-lg bg-lavender p-1 text-azure">
                {blogDetails?.blog_category?.vn_label}
              </p>
            </div>
            <div
              className="content mb-8"
              dangerouslySetInnerHTML={{
                __html: decodeURI(blogDetails?.content),
              }}
            />

            {comments && !isEmpty(comments) && (
              <div className="rounded-xl bg-pale-silver p-5">
                {comments.map((item) => (
                  <CommentBlog
                    key={item.id}
                    onClickUpdate={() =>
                      handleClickUpdateComment(item.id, !item.is_hide)
                    }
                    onClickRemove={() => handleClickRemoveComment(item.id)}
                    data={item}
                  />
                ))}

                <Pagination
                  current={pagination.currentPage}
                  total={pagination.totalPages}
                  pageSize={pagination.itemsPerPage}
                  showSizeChanger={false}
                  hideOnSinglePage
                  className="mt-4 text-right"
                  onChange={handleClickPageComments}
                />
              </div>
            )}
          </div>
        ) : (
          <div className="flex max-h-[70vh] min-h-[40vh] w-full items-center justify-center">
            <Spin size="large" />
          </div>
        )}
      </div>
    </Modal>
  )
}

export default PreviewBlogModal
