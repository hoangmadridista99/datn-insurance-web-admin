import React, { useState } from 'react'
import { Form, Modal, notification } from 'antd'
import FormBlog from 'components/Forms/Blog'
import { handleError } from '@helpers/handleError'
import { createBlog } from '@services/blogs'
import { useTranslation } from 'next-i18next'

const { useForm } = Form

const CreateBlogModal = ({ categories, isOpen, onClose, onRefreshing }) => {
  const [form] = useForm()
  const { t } = useTranslation(['blogs'])

  const [isLoading, setIsLoading] = useState(false)

  const handleClickSubmitForm = () => form.submit()

  const handleCloseModal = () => {
    form.resetFields()
    onClose()
  }

  const handleFinishForm = async (body) => {
    try {
      setIsLoading(true)
      await createBlog(body)

      notification.success({ message: t('blogs:responses:create') })
      onRefreshing()
      handleCloseModal()
    } catch (error) {
      handleError(error)
    } finally {
      setIsLoading(false)
    }
  }

  return (
    <Modal
      centered
      width="70%"
      destroyOnClose
      open={isOpen}
      closable={false}
      onCancel={handleCloseModal}
      onOk={handleClickSubmitForm}
      okText={t('blogs:modals:create-button-ok')}
      confirmLoading={isLoading}
      cancelButtonProps={{ disabled: isLoading }}
    >
      <div className="no-scrollbar h-[80vh] overflow-hidden overflow-y-scroll">
        <FormBlog
          form={form}
          categories={categories}
          onFinishForm={handleFinishForm}
          isDisable={isLoading}
        />
      </div>
    </Modal>
  )
}

export default CreateBlogModal
