import React, { useState, useEffect, useCallback } from 'react'
import { Form, Modal, Spin, notification } from 'antd'
import { getBlogDetails, updateBlog } from '@services/blogs'
import { handleError } from '@helpers/handleError'
import FormBlog from 'components/Forms/Blog'
import { useTranslation } from 'next-i18next'

const { useForm } = Form

const UpdateBlogModal = ({
  categories,
  blogId,
  onClose,
  isOpen,
  onUpdateSuccess,
}) => {
  const [form] = useForm()
  const { t } = useTranslation(['blogs'])

  const [isLoaded, setIsLoaded] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [blogDetails, setBlogDetails] = useState(null)

  const handleGetBlogDetails = useCallback(async () => {
    try {
      if (blogId) {
        const response = await getBlogDetails(blogId)
        setBlogDetails(response)
      }
    } catch (error) {
      handleError(error)
    } finally {
      setIsLoaded(true)
    }
  }, [blogId])

  const handleClickSubmitForm = () => form.submit()

  const handleCloseModal = useCallback(() => {
    form.resetFields()
    setBlogDetails(null)
    onClose()
  }, [form, onClose])

  const handleFinishForm = useCallback(
    async (body) => {
      try {
        if (blogId) {
          setIsLoading(true)
          await updateBlog(blogId, body)

          notification.success({ message: t('blogs:responses:update') })
          onUpdateSuccess(blogId, body)
          handleCloseModal()
        }
      } catch (error) {
        handleError(error)
      } finally {
        setIsLoading(false)
      }
    },
    [blogId, handleCloseModal, onUpdateSuccess, t]
  )

  useEffect(() => {
    handleGetBlogDetails()
  }, [handleGetBlogDetails])

  return (
    <Modal
      centered
      width="70%"
      destroyOnClose
      open={isOpen}
      onCancel={handleCloseModal}
      closable={false}
      onOk={handleClickSubmitForm}
      okText={t('blogs:modals:update-button-ok')}
      confirmLoading={isLoading}
      cancelButtonProps={{ disabled: isLoading }}
    >
      <div className="no-scrollbar h-[80vh] overflow-hidden overflow-y-scroll">
        {blogDetails ? (
          <FormBlog
            blogDetails={blogDetails}
            categories={categories}
            onFinishForm={handleFinishForm}
            form={form}
            isDisable={!isLoaded || isLoading}
          />
        ) : (
          <div className="flex h-full w-full items-center justify-center">
            <Spin />
          </div>
        )}
      </div>
    </Modal>
  )
}

export default UpdateBlogModal
