import React, { useCallback, useState } from 'react'
import { Modal, notification } from 'antd'
import { removeBlog } from '@services/blogs'
import { handleError } from '@helpers/handleError'
import { useTranslation } from 'next-i18next'

const RemoveBlogModal = ({ blog, onClose, onRefreshing }) => {
  const [isLoading, setIsLoading] = useState(false)

  const { t } = useTranslation(['blogs'])

  const handleRemoveBlog = useCallback(async () => {
    try {
      if (blog) {
        setIsLoading(true)

        await removeBlog(blog.id)

        notification.success({ message: t('blogs:responses:remove') })
        onRefreshing()
        onClose()
      }
    } catch (error) {
      handleError(error)
    } finally {
      setIsLoading(false)
    }
  }, [blog, onClose, onRefreshing, t])

  return (
    <Modal
      okText={t('blogs:modals:remove:button:ok')}
      cancelText={t('blogs:modals:remove:button:cancel')}
      open={!!blog}
      centered
      title={`${t('blogs:modals:remove:title')} ${blog?.title}`}
      onCancel={onClose}
      onOk={handleRemoveBlog}
      confirmLoading={isLoading}
    >
      {t('blogs:modals:remove:content')}
    </Modal>
  )
}

export default RemoveBlogModal
