import { CloseIconSvg } from '@utils/icons'
import { Form, Modal } from 'antd'
import React from 'react'
import { useTranslation } from 'next-i18next'
import FormBlogCategory from 'components/Forms/BlogCategory'
const { useForm } = Form

const CreateOrUpdateBlogCategoryModal = ({
  isModalVisible,
  onCancel,
  onSubmit,
  initialValues,
}) => {
  const { t } = useTranslation(['blogs'])
  const [form] = useForm()

  return (
    <Modal
      open={isModalVisible}
      onOk={async () => {
        await form.validateFields()
        onSubmit(form.getFieldsValue())
      }}
      onCancel={() => {
        form.resetFields()
        onCancel()
      }}
      closeIcon={<CloseIconSvg />}
      cancelText={t('blogs:modals.categories.button.cancel')}
      okText={t('blogs:modals.categories.button.save')}
      destroyOnClose
      centered
    >
      <FormBlogCategory form={form} initialValues={initialValues} />
    </Modal>
  )
}

export default CreateOrUpdateBlogCategoryModal
