import React from 'react'

const Loading = () => (
  <div className="loading fixed top-0 left-0 z-50 h-screen w-screen bg-black-0.1 text-center text-white">
    <svg width="205" height="250" viewBox="0 0 50 50">
      <polygon
        strokeWidth="1"
        stroke="#fff"
        fill="none"
        points="20,1 40,40 1,40"
      ></polygon>
      <text fill="#fff" x="5" y="47">
        Loading
      </text>
    </svg>
  </div>
)

export default Loading
