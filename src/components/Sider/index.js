import React, { useEffect, useState } from 'react'
import { Layout, Menu } from 'antd'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/react'
import { useTranslation } from 'next-i18next'
import { ROUTERS, routers } from '@utils/constants/routers'
import { ROLES } from '@utils/constants/insurances'
import Link from 'next/link'
import { ChevronDown } from '@utils/icons'

const { Sider } = Layout

const KEY_DROPDOWN = {
  INSURANCES: 'life-health-insurances',
  BLOGS: 'blogs',
  CAR_INSURANCES: 'car-insurances',
}

const Sidebar = () => {
  const router = useRouter()
  const { t } = useTranslation('common')
  const session = useSession()

  const [openKeys, setOpenKeys] = useState([])

  const handleMenuItems = (routers) => {
    return routers
      .filter((item) => {
        if (session.data?.role === ROLES.SUPER_ADMIN) return true

        return item.link !== '/operators'
      })
      .map((item) => {
        let result = {
          key: item.key,
        }

        if (!item.link && item.children) {
          result = {
            ...result,
            label: (
              <button className="flex cursor-pointer items-start gap-2 whitespace-pre-wrap border-none bg-transparent py-2 text-start text-base transition duration-300">
                <p className="mt-0.5">
                  {item.children.some((childrenItem) =>
                    childrenItem.key.includes(router.pathname)
                  )
                    ? item.activeIcon
                    : item.icon}
                </p>
                {t(item.i18n)}
              </button>
            ),
            children: item.children.map((child) => ({
              key: child.key,
              label: (
                <p
                  onClick={() => handleClickMenu(child.link)}
                  className={`sub-menu font-normal hover:bg-none hover:text-azure ${
                    router.asPath === child.link ? 'text-azure' : 'text-white'
                  }`}
                >
                  <span key={child.link} className="text-sm hover:bg-none">
                    &bull; {t(child.i18n)}
                  </span>
                </p>
              ),
            })),
          }
        }

        if (item.link && !item.children) {
          result = {
            ...result,
            label: (
              <button
                onClick={() => {
                  handleClickMenu(item.link)
                  const timer = setTimeout(() => setOpenKeys([]), 500)
                  return () => clearTimeout(timer)
                }}
                className={`flex w-full cursor-pointer items-center gap-2 border-none bg-transparent py-2 text-start text-white transition duration-200 ${
                  item.link === router.pathname
                    ? 'hover:text-inherit'
                    : 'hover:text-azure'
                }`}
              >
                <p>{item.icon}</p>
                <p className="text-base">{t(item.i18n)}</p>
              </button>
            ),
          }
        }

        if (!item.link && !item.children) {
          result = {
            ...result,
            label: (
              <div
                className="w-full cursor-default border-none bg-transparent pt-2 text-start transition duration-200"
                disabled
              >
                <p className="text-xs text-spanish-gray">{t(item.i18n)}</p>
              </div>
            ),
          }
        }

        return result
      })
  }

  useEffect(() => {
    const route = router.route

    switch (route) {
      case ROUTERS.INSURANCES:
      case ROUTERS.CREATE_INSURANCE:
      case ROUTERS.RATING_INSURANCES:
      case ROUTERS.FAVORITE:
        setOpenKeys([KEY_DROPDOWN.INSURANCES])
        break
      case ROUTERS.BLOGS:
        setOpenKeys([KEY_DROPDOWN.BLOGS])
        break
      case ROUTERS.CAR_INSURANCES:
      case ROUTERS.CREATE_CAR_INSURANCE:
        setOpenKeys([KEY_DROPDOWN.CAR_INSURANCES])
        break
      default:
        return
    }
  }, [router])

  const handleClickMenu = (link) => {
    if (link === router.pathname || link === router.asPath)
      return window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })

    router.push(link)
  }

  const onOpenChange = (keys) => {
    const rootSubmenuKeys = [
      'life-health-insurances',
      'blogs',
      'car-insurances',
    ]
    const latestOpenKey = keys.find((key) => openKeys.indexOf(key) === -1)

    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys)
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : [])
    }
  }

  return (
    <>
      {session.data && (
        <Sider width={300}>
          <div className="no-scrollbar fixed top-0 left-0 flex h-full w-75 flex-col overflow-hidden overflow-y-auto bg-maastricht-blue pb-10">
            <div className="mb-4 w-full p-6">
              <Link href="/">
                <Image
                  width={125}
                  height={32}
                  src="/images/logo.png"
                  alt="Sosanh24"
                />
              </Link>
            </div>

            <Menu
              selectedKeys={router.asPath}
              theme="dark"
              mode="inline"
              expandIcon={({ isOpen }) => (
                <ChevronDown
                  className={`mt-2 self-start transition duration-1000 ${
                    isOpen ? 'rotate-0' : '-rotate-90'
                  }`}
                />
              )}
              items={handleMenuItems(routers)}
              openKeys={openKeys}
              onOpenChange={onOpenChange}
            />
          </div>
        </Sider>
      )}
    </>
  )
}

export default Sidebar
