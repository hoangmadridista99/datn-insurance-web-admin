/* eslint-disable indent */
import React from 'react'
import { Form, Input } from 'antd'
import { useTranslation } from 'next-i18next'

const FormUser = ({ form, initialValues }) => {
  const { t } = useTranslation(['operators'])

  return (
    <div className="py-3 px-2">
      <h1 className="text-center text-2xl font-medium text-arsenic">
        {initialValues
          ? t('operators:fields:edit_info')
          : t('operators:fields:create_account')}
      </h1>
      <Form form={form} initialValues={initialValues}>
        <div className="my-4 border-x-0 border-b-0 border-t border-solid border-ghost-white pt-4">
          <div className="mb-6 flex items-center last:mb-0">
            <div className="mr-3 w-1/2">
              <p className="mb-2 font-bold text-arsenic">
                {t('operators:fields:name')}
              </p>
              <Form.Item
                className="mb-0"
                name="first_name"
                rules={[
                  {
                    required: true,
                    message: t('operators:messages:name_required'),
                  },
                ]}
              >
                <Input
                  size="large"
                  className="w-full"
                  placeholder={t('operators:placeholders:your_name')}
                />
              </Form.Item>
            </div>
            <div className="ml-3 w-1/2">
              <p className="mb-2 font-bold text-arsenic">
                {t('operators:fields:surname')}
              </p>
              <Form.Item
                className="mb-0"
                name="last_name"
                rules={[
                  {
                    required: true,
                    message: t('operators:messages:surname_required'),
                  },
                ]}
              >
                <Input
                  size="large"
                  className="w-full"
                  placeholder={t('operators:placeholders:your_surname')}
                />
              </Form.Item>
            </div>
          </div>

          {!initialValues && (
            <div className="mb-6 last:mb-0">
              <p className="mb-2 font-bold text-arsenic">
                {t('operators:fields:username')}
              </p>
              <Form.Item
                className="mb-0"
                name="account"
                rules={[
                  {
                    required: true,
                    message: t('operators:messages:username_required'),
                  },
                  {
                    min: 4,
                    message: t('operators:messages:username_at_least'),
                  },
                ]}
              >
                <Input
                  size="large"
                  className="w-full"
                  placeholder={t('operators:placeholders:username')}
                />
              </Form.Item>
            </div>
          )}
          <div className="mb-6 last:mb-0">
            <p className="mb-2 font-bold text-arsenic">
              {t('operators:fields:password')}
            </p>
            <Form.Item
              className="mb-0"
              name="password"
              rules={[
                {
                  required: !initialValues ? true : false,
                  message: t('operators:messages:password_required'),
                },
                {
                  min: 8,
                  message: t('operators:messages:password_at_least_required'),
                },
                {
                  max: 30,
                  message: t('operators:messages:password_maximum_required'),
                },
              ]}
            >
              <Input
                size="large"
                className="w-full"
                placeholder={t('operators:placeholders:password')}
                type="password"
              />
            </Form.Item>
          </div>
          <div className="mb-6 last:mb-0">
            <p className="mb-2 font-bold text-arsenic">
              {t('operators:fields:re_password')}
            </p>
            <Form.Item
              dependencies={['password']}
              className="mb-0"
              name="repeatPassword"
              rules={[
                {
                  required: !initialValues ? true : false,
                  message: t('operators:messages:re_password_required'),
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('password') === value)
                      return Promise.resolve()

                    return Promise.reject(
                      new Error(t('operators:messages:password_incorrect'))
                    )
                  },
                }),
              ]}
            >
              <Input
                size="large"
                className="w-full"
                placeholder={t('operators:placeholders:re_password')}
                type="password"
              />
            </Form.Item>
          </div>
        </div>
      </Form>
    </div>
  )
}

export default FormUser
