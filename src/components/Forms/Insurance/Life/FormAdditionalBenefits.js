import React from 'react'
import { Form, Input } from 'antd'
import { useTranslation } from 'next-i18next'
import { LIFE_INSURANCE_FIELDS } from '@utils/constants/insurances'
import FormFieldHaveLevel from '../FormFieldHaveLevel'

const { TextArea } = Input

const FormAdditionalBenefits = () => {
  const { t } = useTranslation(['insurances'])

  return (
    <>
      {LIFE_INSURANCE_FIELDS.additional_benefits.map((item) => (
        <FormFieldHaveLevel
          key={item}
          title={t(`insurances:form:additional_benefits:${item}`)}
          name={['additional_benefits', item]}
          fieldDetail={
            item !== 'flexible_and_diverse'
              ? t(`insurances:fields:${item}`)
              : undefined
          }
        >
          <Form.Item name={['additional_benefits', item, 'text']}>
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </FormFieldHaveLevel>
      ))}
    </>
  )
}

export default FormAdditionalBenefits
