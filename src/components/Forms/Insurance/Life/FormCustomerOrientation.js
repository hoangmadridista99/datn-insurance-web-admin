import React from 'react'
import { useTranslation } from 'next-i18next'
import FormFieldHaveLevel from '../FormFieldHaveLevel'
import { Form, Input } from 'antd'
import { LIFE_INSURANCE_FIELDS } from '@utils/constants/insurances'

const { TextArea } = Input

const FormCustomerOrientation = () => {
  const { t } = useTranslation(['insurances'])

  return (
    <>
      {LIFE_INSURANCE_FIELDS.customer_orientation.map((item) => (
        <FormFieldHaveLevel
          key={item}
          title={t(`insurances:form:customer_orientation:${item}`)}
          name={['customer_orientation', item]}
          fieldDetail={t(`insurances:fields:${item}`)}
        >
          <Form.Item name={['customer_orientation', item, 'text']}>
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </FormFieldHaveLevel>
      ))}
    </>
  )
}

export default FormCustomerOrientation
