import React from 'react'
import { Form, Input } from 'antd'
import { useTranslation } from 'next-i18next'

const { TextArea } = Input

const FormMainBenefits = () => {
  const { t } = useTranslation(['insurances'])

  return (
    <Form.Item
      name="key_benefits"
      rules={[
        {
          required: true,
          message: t('insurances:form:validation:benefit'),
        },
      ]}
      className="m-0"
    >
      <TextArea
        placeholder={t('insurances:form:key_benefits:description')}
        autoSize={{ minRows: 5, maxRows: 7 }}
        size="large"
      />
    </Form.Item>
  )
}

export default FormMainBenefits
