import { Popover } from 'antd'
import Image from 'next/image'
import React, { memo } from 'react'

const FormInsuranceField = ({
  children,
  title,
  fieldDetail,
  isAlignCenter = false,
  isHiddenFieldDetail = true,
}) => (
  <div
    className={`mb-6 flex w-full  justify-between last:mb-0 ${
      isAlignCenter ? 'items-center' : 'items-start'
    }`}
  >
    <div
      className={`flex w-3/10 items-center ${isAlignCenter ? 'mb-6' : 'mb-0'}`}
    >
      {isHiddenFieldDetail && (
        <>
          {fieldDetail && (
            <Popover
              trigger={['hover']}
              placement="right"
              title={title}
              content={
                <p className="w-3/10 whitespace-pre-wrap text-sm text-nickel">
                  {fieldDetail}
                </p>
              }
            >
              <Image
                width={24}
                height={24}
                src="/svg/question.svg"
                alt="information"
              />
            </Popover>
          )}

          {!fieldDetail && (
            <Image
              width={24}
              height={24}
              src="/svg/question.svg"
              alt="information"
            />
          )}
        </>
      )}

      <p className="ml-2 text-base font-bold text-arsenic">{title}</p>
    </div>
    <div className="w-13/20">{children}</div>
  </div>
)

export default memo(FormInsuranceField)
