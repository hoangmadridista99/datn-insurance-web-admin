import React, { memo, useMemo } from 'react'
import Image from 'next/image'
import classNames from 'classnames'
import { Collapse, Popover } from 'antd'
import { useTranslation } from 'next-i18next'

import {
  LIFE_INSURANCE_FIELDS,
  HEALTH_INSURANCE,
  HEALTH_INSURANCE_FIELDS,
  LIFE_INSURANCE,
  CAR_INSURANCE,
  CAR_INSURANCE_FIELDS,
} from '@utils/constants/insurances'

const FormInsuranceLayout = ({ children, ...props }) => {
  const {
    active,
    title,
    activeKey,
    insurancesType,
    hidden = false,
    customTrans = undefined,
    destroyInactivePanel = false,
  } = props

  const fieldList = useMemo(() => {
    switch (insurancesType) {
      case HEALTH_INSURANCE:
        return HEALTH_INSURANCE_FIELDS
      case LIFE_INSURANCE:
        return LIFE_INSURANCE_FIELDS
      case CAR_INSURANCE:
        return CAR_INSURANCE_FIELDS
      default:
        return null
    }
  }, [insurancesType])

  const { t } = useTranslation([customTrans ?? 'insurances'])

  const ContentPopover = () => (
    <>
      {fieldList &&
        fieldList[activeKey].map((item) => {
          const isActive = active.indexOf(item) !== -1

          return (
            <div key={item} className="mb-2.5 flex items-center last:mb-0">
              <Image
                width={24}
                height={24}
                alt="Check"
                src={`/svg/${isActive ? 'check-success' : 'info-circle'}.svg`}
              />
              <span
                className={classNames('ml-2.5', {
                  'text-salem': isActive,
                  'text-royal-orange': !isActive,
                })}
              >
                {t(
                  `${customTrans ?? 'insurances'}:form${
                    activeKey !== 'benefits' ? `:${activeKey}` : ''
                  }:${item}`
                )}
              </span>
            </div>
          )
        })}
    </>
  )

  const Header = () => (
    <div className="flex items-center">
      <p className="text-8 font-bold leading-12 text-arsenic">{title}</p>
      <Popover placement="right" content={<ContentPopover />}>
        {!hidden && (
          <div
            className={`ant-popover-open ml-4 flex items-center justify-center rounded py-1 px-3 ${
              active.length === fieldList[activeKey].length
                ? 'bg-green-100'
                : 'bg-antique-white'
            }`}
          >
            <p
              className={`mb-0 mr-2 ${
                active.length === fieldList[activeKey].length
                  ? 'text-green-700'
                  : 'text-royal-orange'
              }`}
            >{`${active.length}/${fieldList[activeKey].length}`}</p>
            <Image
              width={24}
              height={24}
              alt="Popover"
              src={`/svg/${
                active.length === fieldList[activeKey].length
                  ? 'check-success'
                  : 'info-circle'
              }.svg`}
            />
          </div>
        )}
      </Popover>
    </div>
  )

  const Icon = ({ isActive }) => (
    <Image
      width={24}
      height={24}
      alt="Arrow Down"
      src="/svg/arrow-down.svg"
      className={classNames('transition duration-200', {
        'rotate-90': isActive,
      })}
    />
  )

  return (
    <Collapse
      defaultActiveKey={`form-${activeKey}`}
      size="large"
      expandIconPosition="end"
      expandIcon={Icon}
      className="form-insurance mb-10 overflow-hidden last:m-0"
      destroyInactivePanel={destroyInactivePanel}
    >
      <Collapse.Panel
        className="bg-white"
        key={`form-${activeKey}`}
        header={<Header />}
      >
        {children}
      </Collapse.Panel>
    </Collapse>
  )
}

export default memo(FormInsuranceLayout)
