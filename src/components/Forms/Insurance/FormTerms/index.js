import React, { memo, useState } from 'react'
import { Checkbox, Input, Select, Form, InputNumber } from 'antd'
import { useTranslation } from 'next-i18next'

import FormFieldHaveLevel from '../FormFieldHaveLevel'

import {
  LIST_TYPE,
  LIST_PAY,
  LIST_PERSON,
  LIST_PROFESSION,
  HEALTH_INSURANCE,
  CUSTOMER_SEGMENT,
  LIFE_INSURANCE,
} from '@utils/constants/insurances'
import { formatCurrency } from '@helpers/formatCurrency'
import { useInputNumber } from '@hooks/useInputNumber'

const { TextArea } = Input

const FormInsuranceTerms = ({ insurancesType }) => {
  const { t } = useTranslation(['insurances'])
  const { onKeyDown } = useInputNumber()

  const [selectedValues, setSelectedValues] = useState({
    deadlineForDeal: 'year',
  })

  const handleOptions = (list) =>
    list.map((item) => ({
      value: item,
      label: t(`insurances:form:options:${item}`),
    }))

  const handleFormatter = (value) => {
    if (isNaN(value)) return undefined

    return value ? formatCurrency(value) : ''
  }

  const handleParser = (value) => {
    const formatValue = parseInt(value.replace(/\./g, ''))

    if (isNaN(formatValue)) return

    return parseInt(value.replace(/\./g, ''))
  }

  return (
    <>
      {/* age_eligibility */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:age_eligibility')}
        fieldDetail={t('insurances:fields:age_eligibility')}
        name={['terms', 'age_eligibility']}
      >
        <div className="flex flex-col justify-start">
          <div className="flex items-center">
            <p className="w-1/5 pr-2 text-right text-base font-bold text-arsenic">
              {t('insurances:form:from')}
            </p>
            <div className="flex w-full items-center">
              <Form.Item
                className="mx-2 mb-0 w-full"
                name={['terms', 'age_eligibility', 'from']}
                // validateStatus={
                //   errors['terms.age_eligibility_from'] ? 'error' : undefined
                // }
              >
                <InputNumber
                  size="large"
                  controls={false}
                  min={0}
                  max={100}
                  placeholder={t('insurances:form:placeholder:old')}
                  className="w-full"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      {t('insurances:form:addOnAfter:old')}
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'terms.age_eligibility_from')
                  }
                />
              </Form.Item>

              <p className="text-base font-bold text-arsenic">
                {t('insurances:form:to')}
              </p>

              <Form.Item
                className="mx-2 mb-0 w-full"
                name={['terms', 'age_eligibility', 'to']}
                // validateStatus={
                //   errors['terms.age_eligibility_to'] ? 'error' : undefined
                // }
              >
                <InputNumber
                  size="large"
                  controls={false}
                  min={0}
                  max={100}
                  placeholder={t('insurances:form:placeholder:old')}
                  className="w-full"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      {t('insurances:form:addOnAfter:old')}
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'terms.age_eligibility_to')
                  }
                />
              </Form.Item>
            </div>
          </div>

          {/* {(errors['terms.age_eligibility_from'] ||
            errors['terms.age_eligibility_to']) && (
            <p className="pl-9/50 text-sm text-vermilion">{message}</p>
          )} */}
        </div>

        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'age_eligibility', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* deadline_for_deal */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:deadline_for_deal')}
        fieldDetail={t('insurances:fields:deadline_for_deal')}
        name={['terms', 'deadline_for_deal']}
      >
        <Form.Item name={['terms', 'deadline_for_deal', 'type']}>
          <Select
            size="large"
            placeholder={t('insurances:form:placeholder:select')}
            className="w-full"
            options={handleOptions(LIST_TYPE)}
            onChange={(value) =>
              setSelectedValues({ ...selectedValues, deadlineForDeal: value })
            }
          />
        </Form.Item>
        <div className="mt-4 flex items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:from')}
          </p>
          <div className="flex w-full items-center">
            <Form.Item
              className="mx-2 mb-0 w-full"
              name={['terms', 'deadline_for_deal', 'from']}
            >
              <InputNumber
                controls={false}
                placeholder={t('insurances:form:placeholder:time')}
                className="w-full"
                size="large"
                addonAfter={
                  <span className="text-base font-normal text-metallic">
                    {t(
                      `insurances:form:addOnAfter:${selectedValues.deadlineForDeal}`
                    )}
                  </span>
                }
                onKeyDown={(event) =>
                  onKeyDown(event, 'terms.deadline_for_deal_from')
                }
              />
            </Form.Item>
            <p className="text-base font-bold  text-arsenic">
              {t('insurances:form:to')}
            </p>
            <Form.Item
              className="mx-2 mb-0 w-full"
              name={['terms', 'deadline_for_deal', 'to']}
            >
              <InputNumber
                controls={false}
                placeholder={t('insurances:form:placeholder:time')}
                className="w-full"
                size="large"
                addonAfter={
                  <span className="text-base font-normal text-metallic">
                    {t(
                      `insurances:form:addOnAfter:${selectedValues.deadlineForDeal}`
                    )}
                  </span>
                }
                onKeyDown={(event) =>
                  onKeyDown(event, 'terms.deadline_for_deal_to')
                }
              />
            </Form.Item>
          </div>
        </div>
        <div className="mt-4 flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:value')}
          </p>
          <Form.Item
            name={['terms', 'deadline_for_deal', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t(
                `insurances:form:placeholder:${selectedValues.deadlineForDeal}`
              )}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">
                  {t(
                    `insurances:form:addOnAfter:${selectedValues.deadlineForDeal}`
                  )}
                </span>
              }
              onKeyDown={(event) => onKeyDown(event, 'terms.deadline_for_deal')}
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'deadline_for_deal', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              size="large"
              autoSize={{ minRows: 5, maxRows: 7 }}
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* insurance_minimum_fee */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:insurance_minimum_fee')}
        fieldDetail={t('insurances:fields:insurance_minimum_fee')}
        name={['terms', 'insurance_minimum_fee']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['terms', 'insurance_minimum_fee', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) =>
                onKeyDown(event, 'terms.insurance_minimum_fee')
              }
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'insurance_minimum_fee', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* insured_person */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:insured_person')}
        fieldDetail={t('insurances:fields:insured_person')}
        name={['terms', 'insured_person']}
      >
        <div className="flex w-full items-center">
          <Form.Item
            name={['terms', 'insured_person', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <Checkbox.Group className="flex flex-col">
              {handleOptions(LIST_PERSON).map((item) => (
                <Checkbox
                  key={item.value}
                  className="mx-0 mb-6 text-base text-arsenic last:mb-0"
                  value={item.value}
                >
                  {item.label}
                </Checkbox>
              ))}
            </Checkbox.Group>
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'insured_person', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* profession */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:profession')}
        fieldDetail={t('insurances:fields:profession')}
        name={['terms', 'profession']}
      >
        <Form.Item
          name={['terms', 'profession', 'text']}
          className="mx-2 mb-0 w-full"
        >
          <Checkbox.Group className="flex flex-col">
            {handleOptions(LIST_PROFESSION).map((item) => (
              <Checkbox
                key={item.value}
                className="mx-0 mb-6 text-base text-arsenic last:mb-0"
                value={item.value}
              >
                {item.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </Form.Item>
      </FormFieldHaveLevel>

      {/* deadline_for_payment */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:deadline_for_payment')}
        fieldDetail={t('insurances:fields:deadline_for_payment')}
        name={['terms', 'deadline_for_payment']}
      >
        <Form.Item
          name={['terms', 'deadline_for_payment', 'value']}
          className="mx-2 mb-0 w-full"
        >
          <Checkbox.Group className="flex flex-col">
            {handleOptions(LIST_PAY).map((item) => (
              <Checkbox
                key={item.value}
                className="mx-0 mb-6 text-base text-arsenic last:mb-0"
                value={item.value}
              >
                {item.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </Form.Item>
        <Form.Item
          name={['terms', 'deadline_for_payment', 'description']}
          className="mx-2 mt-4 w-full"
        >
          <TextArea
            placeholder={t('insurances:form:placeholder:describe')}
            autoSize={{ minRows: 5, maxRows: 7 }}
            size="large"
          />
        </Form.Item>
      </FormFieldHaveLevel>

      {/* insured_customer */}
      {insurancesType === HEALTH_INSURANCE && (
        <FormFieldHaveLevel
          title={t('insurances:form:terms:customer_segment')}
          fieldDetail={t('insurances:fields:customer_segment')}
          name={['terms', 'customer_segment']}
        >
          <Form.Item
            name={['terms', 'customer_segment', 'text']}
            className="mx-2 mb-0 w-full"
          >
            <Checkbox.Group className="flex flex-col">
              {handleOptions(CUSTOMER_SEGMENT).map((item) => (
                <Checkbox
                  key={item.value}
                  className="mx-0 mb-6 text-base text-arsenic last:mb-0"
                  value={item.value}
                >
                  {item.label}
                </Checkbox>
              ))}
            </Checkbox.Group>
          </Form.Item>
        </FormFieldHaveLevel>
      )}

      {/* age_of_contract_termination */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:age_of_contract_termination')}
        fieldDetail={t('insurances:fields:age_of_contract_termination')}
        name={['terms', 'age_of_contract_termination']}
      >
        <Form.Item name={['terms', 'age_of_contract_termination', 'type']}>
          <Select
            className="w-full"
            options={handleOptions(LIST_TYPE)}
            placeholder={t('insurances:form:placeholder:select')}
            size="large"
          />
        </Form.Item>
        <div className="mt-4 flex items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:from')}
          </p>
          <div className="flex w-full items-center">
            <Form.Item
              className="mx-2 mb-0 w-full"
              name={['terms', 'age_of_contract_termination', 'from']}
            >
              <InputNumber
                controls={false}
                min={0}
                max={100}
                placeholder={t('insurances:form:placeholder:year')}
                className="w-full"
                size="large"
                addonAfter={
                  <span className="text-base font-normal text-metallic">
                    {t('insurances:form:addOnAfter:old')}
                  </span>
                }
                onKeyDown={(event) =>
                  onKeyDown(event, 'terms.age_of_contract_termination_from')
                }
              />
            </Form.Item>
            <p className="text-base font-bold  text-arsenic">
              {t('insurances:form:to')}
            </p>
            <Form.Item
              className="mx-2 mb-0 w-full"
              name={['terms', 'age_of_contract_termination', 'to']}
            >
              <InputNumber
                controls={false}
                min={0}
                max={100}
                placeholder={t('insurances:form:placeholder:year')}
                className="w-full"
                size="large"
                addonAfter={
                  <span className="text-base font-normal text-metallic">
                    {t('insurances:form:addOnAfter:old')}
                  </span>
                }
                onKeyDown={(event) =>
                  onKeyDown(event, 'terms.age_of_contract_termination_to')
                }
              />
            </Form.Item>
          </div>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'age_of_contract_termination', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* termination_conditions */}
      {insurancesType === LIFE_INSURANCE && (
        <FormFieldHaveLevel
          title={t('insurances:form:terms:termination_conditions')}
          fieldDetail={t('insurances:fields:termination_conditions')}
          name={['terms', 'termination_conditions']}
        >
          <Form.Item name={['terms', 'termination_conditions', 'text']}>
            <Input
              placeholder={t('insurances:form:placeholder:condition')}
              className="w-full"
              size="large"
            />
          </Form.Item>
        </FormFieldHaveLevel>
      )}

      {/* total_sum_insured */}
      <FormFieldHaveLevel
        fieldDetail={t('insurances:fields:total_sum_insured')}
        title={t('insurances:form:terms:total_sum_insured')}
        name={['terms', 'total_sum_insured']}
      >
        <div className="flex items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:from')}
          </p>
          <div className="flex w-full items-center">
            <Form.Item
              className="mx-2 mb-0 w-full"
              name={['terms', 'total_sum_insured', 'from']}
            >
              <InputNumber
                className="w-full"
                controls={false}
                placeholder={t('insurances:form:placeholder:money')}
                formatter={handleFormatter}
                parser={handleParser}
                size="large"
                addonAfter={
                  <span className="text-base font-normal text-metallic">
                    VNĐ
                  </span>
                }
                onKeyDown={(event) =>
                  onKeyDown(event, 'terms.total_sum_insured_from')
                }
              />
            </Form.Item>
            <p className="text-base font-bold  text-arsenic">
              {t('insurances:form:to')}
            </p>
            <Form.Item
              className="mx-2 mb-0 w-full"
              name={['terms', 'total_sum_insured', 'to']}
            >
              <InputNumber
                className="w-full"
                controls={false}
                placeholder={t('insurances:form:placeholder:money')}
                formatter={handleFormatter}
                parser={handleParser}
                size="large"
                addonAfter={
                  <span className="text-base font-normal text-metallic">
                    VNĐ
                  </span>
                }
                onKeyDown={(event) =>
                  onKeyDown(event, 'terms.total_sum_insured_to')
                }
              />
            </Form.Item>
          </div>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'total_sum_insured', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* monthly_fee */}
      <FormFieldHaveLevel
        title={t('insurances:form:terms:monthly_fee')}
        fieldDetail={t('insurances:fields:monthly_fee')}
        name={['terms', 'monthly_fee']}
      >
        <div className="flex items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:from')}
          </p>
          <div className="flex w-full items-center">
            <Form.Item
              className="mx-2 mb-0"
              name={['terms', 'monthly_fee', 'from']}
            >
              <InputNumber
                className="w-full"
                controls={false}
                placeholder={t('insurances:form:placeholder:money')}
                formatter={handleFormatter}
                parser={handleParser}
                size="large"
                addonAfter={
                  <span className="text-base font-normal text-metallic">
                    VNĐ
                  </span>
                }
                onKeyDown={(event) =>
                  onKeyDown(event, 'terms.monthly_fee_from')
                }
              />
            </Form.Item>
            <p className="text-base font-bold  text-arsenic">
              {t('insurances:form:to')}
            </p>
            <Form.Item
              className="mx-2 mb-0"
              name={['terms', 'monthly_fee', 'to']}
            >
              <InputNumber
                className="w-full"
                controls={false}
                placeholder={t('insurances:form:placeholder:money')}
                formatter={handleFormatter}
                parser={handleParser}
                size="large"
                addonAfter={
                  <span className="text-base font-normal text-metallic">
                    VNĐ
                  </span>
                }
                onKeyDown={(event) => onKeyDown(event, 'terms.monthly_fee_to')}
              />
            </Form.Item>
          </div>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['terms', 'monthly_fee', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>
    </>
  )
}

export default memo(FormInsuranceTerms)
