import React, { memo } from 'react'
import { Form, Select, Checkbox, Input } from 'antd'
import { useTranslation } from 'next-i18next'
import FormField from '../FormField'
import { LIFE_INSURANCE } from '@utils/constants/insurances'

const { TextArea } = Input

const FormInsuranceDetails = ({
  companies,
  // insurances,
  objectives,
  insurancesType,
}) => {
  const { t } = useTranslation(['insurances'])

  return (
    <>
      {/* company_id */}
      <FormField
        title={t('insurances:form:details:company_id')}
        isAlignCenter={true}
      >
        <Form.Item
          name="company_id"
          rules={[
            {
              required: true,
              message: t('insurances:form:validation:company_id'),
            },
          ]}
        >
          <Select
            placeholder={t('insurances:form:placeholder:select')}
            style={{ width: '100%' }}
            options={companies}
            size="large"
          />
        </Form.Item>
      </FormField>

      {/* name */}
      <FormField title={t('insurances:form:details:name')} isAlignCenter={true}>
        <Form.Item
          name="name"
          rules={[
            { required: true, message: t('insurances:form:validation:name') },
          ]}
        >
          <Input
            placeholder={t('insurances:form:placeholder:name')}
            size="large"
          />
        </Form.Item>
      </FormField>

      {/* description_insurance */}
      <FormField title={t('insurances:form:details:description_insurance')}>
        <Form.Item
          name="description_insurance"
          rules={[
            {
              required: true,
              message: t('insurances:form:validation:description_insurance'),
            },
          ]}
        >
          <TextArea
            size="large"
            placeholder={t('insurances:form:placeholder:describe')}
            autoSize={{ minRows: 5, maxRows: 7 }}
          />
        </Form.Item>
      </FormField>

      {/* objective_of_insurance */}
      {insurancesType === LIFE_INSURANCE && (
        <FormField title={t('insurances:form:details:objective_of_insurance')}>
          <Form.Item
            name="objective_of_insurance"
            rules={[
              {
                required: true,
                message: t('insurances:form:validation:objective_of_insurance'),
              },
            ]}
          >
            <Checkbox.Group className="flex flex-col gap-6">
              {objectives.map((item) => (
                <Checkbox
                  className="m-0 text-base text-arsenic"
                  key={item.value}
                  value={item.value}
                >
                  {item.label}
                </Checkbox>
              ))}
            </Checkbox.Group>
          </Form.Item>
        </FormField>
      )}
    </>
  )
}

export default memo(FormInsuranceDetails)
