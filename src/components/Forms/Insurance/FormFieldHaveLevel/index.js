import React, { memo } from 'react'
import { Form, Radio } from 'antd'
import Image from 'next/image'

import FormField from '../FormField'

import { FORM_LEVEL } from '@utils/constants/insurances'

const FormInsuranceFieldHaveLevel = ({
  title,
  name,
  fieldDetail,
  children,
}) => {
  return (
    <FormField fieldDetail={fieldDetail} title={title}>
      <Form.Item name={[...(Array.isArray(name) ? name : [name]), 'level']}>
        <Radio.Group className="mx-2 flex items-center justify-between xl:justify-start">
          {FORM_LEVEL.map((item) => (
            <div
              key={item.value}
              className="flex items-center first:ml-0 xl:mx-10"
            >
              <Radio size="small" value={item.value} />
              <Image
                width={18}
                height={18}
                src={`/svg/${item.icon}`}
                alt={item.alt}
              />
            </div>
          ))}
        </Radio.Group>
      </Form.Item>
      {children}
    </FormField>
  )
}

export default memo(FormInsuranceFieldHaveLevel)
