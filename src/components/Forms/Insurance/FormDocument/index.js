/* eslint-disable indent */
import React, { memo } from 'react'
import { Button, Upload, Form } from 'antd'
import { useTranslation } from 'next-i18next'

import FormField from '../FormField'

import { uploadPdf } from '@services/upload'
import { handleError } from '@helpers/handleError'

const { useFormInstance } = Form

const FormInsuranceDocument = () => {
  const { t } = useTranslation(['insurances'])
  const form = useFormInstance()

  const actionUpload = async (file) => {
    try {
      const body = new FormData()
      body.append('file', file)
      return await uploadPdf(body)
    } catch (error) {
      handleError(error)
    }
  }

  return (
    <>
      <FormField
        title={t('insurances:form:document:benefits_illustration_table')}
      >
        <Form.Item name="benefits_illustration_table">
          <Upload
            defaultFileList={form.getFieldValue('benefits_illustration_table')}
            maxCount={1}
            accept=".pdf"
            action={actionUpload}
            method="GET"
          >
            <Button type="primary"> Upload file</Button>
          </Upload>
        </Form.Item>
      </FormField>
      <FormField title={t('insurances:form:document:documentation_url')}>
        <Form.Item name="documentation_url">
          <Upload
            fileList={form.getFieldValue('documentation_url')}
            maxCount={1}
            accept=".pdf"
            action={actionUpload}
            method="GET"
          >
            <Button type="primary">Upload file</Button>
          </Upload>
        </Form.Item>
      </FormField>
    </>
  )
}

export default memo(FormInsuranceDocument)
