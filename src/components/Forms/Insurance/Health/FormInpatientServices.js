import React, { memo } from 'react'
import { Input, Select, Form, InputNumber, Checkbox } from 'antd'
import { useTranslation } from 'next-i18next'
import FormFieldHaveLevel from '../FormFieldHaveLevel'
import { HOSPITALIZATION_LIST, ROOM_TYPES } from '@utils/constants/insurances'
import { formatCurrency } from '@helpers/formatCurrency'
import { useInputNumber } from '@hooks/useInputNumber'

const { TextArea } = Input

const FormInpatientServices = () => {
  const { t } = useTranslation(['insurances'])
  const { onKeyDown } = useInputNumber()

  const handleOptions = (list) =>
    list.map((item) => ({
      value: item.value,
      label: t(`insurances:form:options:${item.label}`),
    }))

  const handleFormatter = (value) => {
    if (isNaN(value)) return undefined

    return value ? formatCurrency(value) : ''
  }

  const handleParser = (value) => {
    const formatValue = parseInt(value.replace(/\./g, ''))

    if (isNaN(formatValue)) return

    return parseInt(value.replace(/\./g, ''))
  }

  return (
    <>
      {/* room_type */}
      <FormFieldHaveLevel
        title={t('insurances:form:inpatient:room_type')}
        fieldDetail={t('insurances:fields:room_type')}
        name={['inpatient', 'room_type']}
      >
        <div className="flex w-full items-center">
          <Form.Item
            name={['inpatient', 'room_type', 'values']}
            className="mx-2 mb-0 w-full"
          >
            <Checkbox.Group className="flex flex-col">
              {handleOptions(ROOM_TYPES).map((item) => (
                <Checkbox
                  key={item.value}
                  className="mx-0 mb-6 text-base text-arsenic last:mb-0"
                  value={item.value}
                >
                  {item.label}
                </Checkbox>
              ))}
            </Checkbox.Group>
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['inpatient', 'room_type', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* for_cancer */}
      <FormFieldHaveLevel
        title={t('insurances:form:inpatient:for_cancer')}
        fieldDetail={t('insurances:fields:for_cancer')}
        name={['inpatient', 'for_cancer']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['inpatient', 'for_cancer', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) => onKeyDown(event, 'inpatient.for_cancer')}
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['inpatient', 'for_cancer', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* for_illnesses */}
      <FormFieldHaveLevel
        title={t('insurances:form:inpatient:for_illnesses')}
        fieldDetail={t('insurances:fields:for_illnesses')}
        name={['inpatient', 'for_illnesses']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['inpatient', 'for_illnesses', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) => onKeyDown(event, 'inpatient.for_illnesses')}
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['inpatient', 'for_illnesses', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* for_accidents */}
      <FormFieldHaveLevel
        title={t('insurances:form:inpatient:for_accidents')}
        fieldDetail={t('insurances:fields:for_accidents')}
        name={['inpatient', 'for_accidents']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['inpatient', 'for_accidents', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) => onKeyDown(event, 'inpatient.for_accidents')}
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['inpatient', 'for_accidents', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* for_surgical */}
      <FormFieldHaveLevel
        title={t('insurances:form:inpatient:for_surgical')}
        fieldDetail={t('insurances:fields:for_surgical')}
        name={['inpatient', 'for_surgical']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['inpatient', 'for_surgical', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) => onKeyDown(event, 'inpatient.for_surgical')}
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['inpatient', 'for_surgical', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* for_hospitalization */}
      <FormFieldHaveLevel
        title={t('insurances:form:inpatient:for_hospitalization')}
        fieldDetail={t('insurances:fields:for_hospitalization')}
        name={['inpatient', 'for_hospitalization']}
      >
        <Form.Item
          name={['inpatient', 'for_hospitalization', 'type']}
          className="mx-2 w-full"
        >
          <Select
            placeholder="Select option"
            className="w-full"
            options={handleOptions(HOSPITALIZATION_LIST)}
            size="large"
          />
        </Form.Item>

        <div className="mb-6 flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['inpatient', 'for_hospitalization', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) =>
                onKeyDown(event, 'inpatient.for_hospitalization')
              }
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* for_intensive_care */}
      <FormFieldHaveLevel
        title={t('insurances:form:inpatient:for_intensive_care')}
        fieldDetail={t('insurances:fields:for_intensive_care')}
        name={['inpatient', 'for_intensive_care']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['inpatient', 'for_intensive_care', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) =>
                onKeyDown(event, 'inpatient.for_intensive_care')
              }
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['inpatient', 'for_intensive_care', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* for_administrative */}
      <FormFieldHaveLevel
        title={t('insurances:form:inpatient:for_administrative')}
        fieldDetail={t('insurances:fields:for_administrative')}
        name={['inpatient', 'for_administrative']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['inpatient', 'for_administrative', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) =>
                onKeyDown(event, 'inpatient.for_administrative')
              }
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['inpatient', 'for_administrative', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* for_organ_transplant */}
      <FormFieldHaveLevel
        title={t('insurances:form:inpatient:for_organ_transplant')}
        fieldDetail={t('insurances:fields:for_organ_transplant')}
        name={['inpatient', 'for_organ_transplant']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['inpatient', 'for_organ_transplant', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) =>
                onKeyDown(event, 'inpatient.for_organ_transplant')
              }
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['inpatient', 'for_organ_transplant', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>
    </>
  )
}

export default memo(FormInpatientServices)
