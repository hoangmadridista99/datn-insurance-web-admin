import React from 'react'
import { useTranslation } from 'next-i18next'
import FormFieldHaveLevel from '../FormFieldHaveLevel'
import { Form, InputNumber, Input, Checkbox } from 'antd'
import { INSURANCE_SCOPE_LIST } from '@utils/constants/insurances'
import { useInputNumber } from '@hooks/useInputNumber'

const { TextArea } = Input

const FormCustomerOrientation = () => {
  const { t } = useTranslation(['insurances'])
  const { onKeyDown } = useInputNumber()

  const handleOptions = (list) =>
    list.map((item) => ({
      value: item,
      label: t(`insurances:form:options:${item}`),
    }))

  return (
    <>
      {/* insurance_scope */}
      <FormFieldHaveLevel
        title={t('insurances:form:health_customer_orientation:insurance_scope')}
        fieldDetail={t('insurances:fields:insurance_scope')}
        name={['customer_orientation', 'insurance_scope']}
      >
        <Form.Item
          name={['customer_orientation', 'insurance_scope', 'values']}
          className="mx-2 mb-0 w-full"
        >
          <Checkbox.Group className="flex flex-col">
            {handleOptions(INSURANCE_SCOPE_LIST).map((item) => (
              <Checkbox
                key={item.value}
                className="mx-0 mb-6 text-base text-arsenic last:mb-0"
                value={item.value}
              >
                {item.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </Form.Item>
      </FormFieldHaveLevel>

      {/* waiting_period */}
      <FormFieldHaveLevel
        title={t('insurances:form:health_customer_orientation:waiting_period')}
        fieldDetail={t('insurances:fields:waiting_period')}
        name={['customer_orientation', 'waiting_period']}
      >
        <div className="flex items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:from')}
          </p>
          <div className=" flex w-full items-center">
            <Form.Item
              className="mx-2 mb-0 w-full"
              name={['customer_orientation', 'waiting_period', 'from']}
            >
              <InputNumber
                controls={false}
                min={0}
                max={100}
                placeholder={t('insurances:form:placeholder:year')}
                className="w-full"
                size="large"
                addonAfter={
                  <span className="text-base font-normal text-metallic">
                    {t('insurances:form:addOnAfter:year')}
                  </span>
                }
                onKeyDown={(event) =>
                  onKeyDown(event, 'customer_orientation.waiting_period_from')
                }
              />
            </Form.Item>
            <p className="text-base font-bold  text-arsenic">
              {t('insurances:form:to')}
            </p>
            <Form.Item
              className="mx-2 mb-0 w-full"
              name={['customer_orientation', 'waiting_period', 'to']}
            >
              <InputNumber
                controls={false}
                min={0}
                max={100}
                placeholder={t('insurances:form:placeholder:year')}
                className="w-full"
                size="large"
                addonAfter={
                  <span className="text-base font-normal text-metallic">
                    {t('insurances:form:addOnAfter:year')}
                  </span>
                }
                onKeyDown={(event) =>
                  onKeyDown(event, 'customer_orientation.waiting_period_to')
                }
              />
            </Form.Item>
          </div>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['customer_orientation', 'waiting_period', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* compensation_process */}
      <FormFieldHaveLevel
        title={t(
          'insurances:form:health_customer_orientation:compensation_process'
        )}
        fieldDetail={t('insurances:fields:compensation_process')}
        name={['customer_orientation', 'compensation_process']}
      >
        <Form.Item
          name={['customer_orientation', 'compensation_process', 'description']}
          className="mx-2 mt-4 w-full"
        >
          <TextArea
            placeholder={t('insurances:form:placeholder:detail')}
            autoSize={{ minRows: 5, maxRows: 7 }}
          />
        </Form.Item>
      </FormFieldHaveLevel>

      {/* reception_and_processing_time */}
      <FormFieldHaveLevel
        title={t(
          'insurances:form:health_customer_orientation:reception_and_processing_time'
        )}
        fieldDetail={t('insurances:fields:reception_and_processing_time')}
        name={['customer_orientation', 'reception_and_processing_time']}
      >
        <Form.Item
          name={[
            'customer_orientation',
            'reception_and_processing_time',
            'text',
          ]}
          className="mx-2 mt-4 w-full"
        >
          <TextArea
            placeholder={t('insurances:form:placeholder:detail')}
            autoSize={{ minRows: 5, maxRows: 7 }}
          />
        </Form.Item>
      </FormFieldHaveLevel>
    </>
  )
}

export default FormCustomerOrientation
