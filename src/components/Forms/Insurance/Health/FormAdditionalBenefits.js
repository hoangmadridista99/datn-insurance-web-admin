import React from 'react'
import { Form, Checkbox, InputNumber, Input } from 'antd'
import { useTranslation } from 'next-i18next'
import FormField from '../FormField'
import { DENTAL, OBSTETRIC } from '@utils/constants/insurances'
import FormFieldHaveLevel from '../FormFieldHaveLevel'
import { formatCurrency } from '@helpers/formatCurrency'
import { useInputNumber } from '@hooks/useInputNumber'

const { TextArea } = Input

const FormAdditionalBenefits = ({
  handleChangeCheckbox,
  section,
  benefitSelected,
}) => {
  const { t } = useTranslation(['insurances'])
  const { onKeyDown } = useInputNumber()

  const handleFormatter = (value) => {
    if (isNaN(value)) return undefined

    return value ? formatCurrency(value) : ''
  }

  const handleParser = (value) => {
    const formatValue = parseInt(value.replace(/\./g, ''))

    if (isNaN(formatValue)) return

    return parseInt(value.replace(/\./g, ''))
  }

  switch (section) {
    case DENTAL:
      return (
        <>
          {/* examination_and_diagnosis */}
          <FormFieldHaveLevel
            title={t('insurances:form:dental:examination_and_diagnosis')}
            fieldDetail={t('insurances:fields:examination_and_diagnosis')}
            name={['dental', 'examination_and_diagnosis']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['dental', 'examination_and_diagnosis', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'dental.examination_and_diagnosis')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['dental', 'examination_and_diagnosis', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>

          {/* gingivitis */}
          <FormFieldHaveLevel
            title={t('insurances:form:dental:gingivitis')}
            fieldDetail={t('insurances:fields:gingivitis')}
            name={['dental', 'gingivitis']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['dental', 'gingivitis', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) => onKeyDown(event, 'dental.gingivitis')}
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['dental', 'gingivitis', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>

          {/* xray_and_diagnostic_imaging */}
          <FormFieldHaveLevel
            title={t('insurances:form:dental:xray_and_diagnostic_imaging')}
            fieldDetail={t('insurances:fields:xray_and_diagnostic_imaging')}
            name={['dental', 'xray_and_diagnostic_imaging']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['dental', 'xray_and_diagnostic_imaging', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'dental.xray_and_diagnostic_imaging')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['dental', 'xray_and_diagnostic_imaging', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>

          {/* filling_teeth_basic */}
          <FormFieldHaveLevel
            title={t('insurances:form:dental:filling_teeth_basic')}
            fieldDetail={t('insurances:fields:filling_teeth_basic')}
            name={['dental', 'filling_teeth_basic']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['dental', 'filling_teeth_basic', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'dental.filling_teeth_basic')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['dental', 'filling_teeth_basic', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>

          {/* root_canal_treatment */}
          <FormFieldHaveLevel
            title={t('insurances:form:dental:root_canal_treatment')}
            fieldDetail={t('insurances:fields:root_canal_treatment')}
            name={['dental', 'root_canal_treatment']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['dental', 'root_canal_treatment', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'dental.root_canal_treatment')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['dental', 'root_canal_treatment', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>

          {/* dental_pathology */}
          <FormFieldHaveLevel
            title={t('insurances:form:dental:dental_pathology')}
            fieldDetail={t('insurances:fields:dental_pathology')}
            name={['dental', 'dental_pathology']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['dental', 'dental_pathology', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'dental.dental_pathology')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['dental', 'dental_pathology', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>

          {/* dental_calculus */}
          <FormFieldHaveLevel
            title={t('insurances:form:dental:dental_calculus')}
            fieldDetail={t('insurances:fields:dental_calculus')}
            name={['dental', 'dental_calculus']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['dental', 'dental_calculus', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'dental.dental_calculus')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['dental', 'dental_calculus', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>
        </>
      )

    case OBSTETRIC:
      return (
        <>
          {/* give_birth_normally */}
          <FormFieldHaveLevel
            title={t('insurances:form:obstetric:give_birth_normally')}
            fieldDetail={t('insurances:fields:give_birth_normally')}
            name={['obstetric', 'give_birth_normally']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['obstetric', 'give_birth_normally', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'obstetric.give_birth_normally')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['obstetric', 'give_birth_normally', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>

          {/* caesarean_section */}
          <FormFieldHaveLevel
            title={t('insurances:form:obstetric:caesarean_section')}
            fieldDetail={t('insurances:fields:caesarean_section')}
            name={['obstetric', 'caesarean_section']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['obstetric', 'caesarean_section', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'obstetric.caesarean_section')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['obstetric', 'caesarean_section', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>

          {/* obstetric_complication */}
          <FormFieldHaveLevel
            title={t('insurances:form:obstetric:obstetric_complication')}
            fieldDetail={t('insurances:fields:obstetric_complication')}
            name={['obstetric', 'obstetric_complication']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['obstetric', 'obstetric_complication', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'obstetric.obstetric_complication')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['obstetric', 'obstetric_complication', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>

          {/* give_birth_abnormality */}
          <FormFieldHaveLevel
            title={t('insurances:form:obstetric:give_birth_abnormality')}
            fieldDetail={t('insurances:fields:give_birth_abnormality')}
            name={['obstetric', 'give_birth_abnormality']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['obstetric', 'give_birth_abnormality', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'obstetric.give_birth_abnormality')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['obstetric', 'give_birth_abnormality', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>

          {/* after_give_birth_fee */}
          <FormFieldHaveLevel
            title={t('insurances:form:obstetric:after_give_birth_fee')}
            fieldDetail={t('insurances:fields:after_give_birth_fee')}
            name={['obstetric', 'after_give_birth_fee']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['obstetric', 'after_give_birth_fee', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'obstetric.after_give_birth_fee')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['obstetric', 'after_give_birth_fee', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>

          {/* before_discharged */}
          <FormFieldHaveLevel
            title={t('insurances:form:obstetric:before_discharged')}
            fieldDetail={t('insurances:fields:before_discharged')}
            name={['obstetric', 'before_discharged']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['obstetric', 'before_discharged', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'obstetric.before_discharged')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['obstetric', 'before_discharged', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>

          {/* postpartum_childcare_cost */}
          <FormFieldHaveLevel
            title={t('insurances:form:obstetric:postpartum_childcare_cost')}
            fieldDetail={t('insurances:fields:postpartum_childcare_cost')}
            name={['obstetric', 'postpartum_childcare_cost']}
          >
            <div className="flex w-full items-center">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:money')}
              </p>
              <Form.Item
                name={['obstetric', 'postpartum_childcare_cost', 'value']}
                className="mx-2 mb-0 w-full"
              >
                <InputNumber
                  className="w-full"
                  controls={false}
                  placeholder={t('insurances:form:placeholder:money')}
                  formatter={handleFormatter}
                  parser={handleParser}
                  size="large"
                  addonAfter={
                    <span className="text-base font-normal text-metallic">
                      VNĐ
                    </span>
                  }
                  onKeyDown={(event) =>
                    onKeyDown(event, 'obstetric.postpartum_childcare_cost')
                  }
                />
              </Form.Item>
            </div>
            <div className="mt-4 flex">
              <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
                {t('insurances:form:description')}
              </p>
              <Form.Item
                name={['obstetric', 'postpartum_childcare_cost', 'description']}
                className="mx-2 w-full"
              >
                <TextArea
                  placeholder={t('insurances:form:placeholder:detail')}
                  autoSize={{ minRows: 5, maxRows: 7 }}
                  size="large"
                />
              </Form.Item>
            </div>
          </FormFieldHaveLevel>
        </>
      )

    default:
      return (
        <FormField
          title={t(
            'insurances:form:health_additional_benefits:existed_services'
          )}
        >
          <Form.Item>
            <Checkbox.Group
              value={benefitSelected}
              className="w-full"
              onChange={handleChangeCheckbox}
            >
              <Checkbox value={DENTAL} className="text-base text-arsenic">
                {t('insurances:form:health_additional_benefits:dental')}
              </Checkbox>

              <Checkbox value={OBSTETRIC} className="text-base text-arsenic">
                {t('insurances:form:health_additional_benefits:obstetric')}
              </Checkbox>
            </Checkbox.Group>
          </Form.Item>
        </FormField>
      )
  }
}

export default FormAdditionalBenefits
