import React, { memo } from 'react'
import { Input, Form, InputNumber } from 'antd'
import { useTranslation } from 'next-i18next'
import FormFieldHaveLevel from '../FormFieldHaveLevel'
import { formatCurrency } from '@helpers/formatCurrency'
import { useInputNumber } from '@hooks/useInputNumber'

const { TextArea } = Input

const FormOutpatientServices = () => {
  const { t } = useTranslation(['insurances'])
  const { onKeyDown } = useInputNumber()

  const handleFormatter = (value) => {
    if (isNaN(value)) return undefined

    return value ? formatCurrency(value) : ''
  }

  const handleParser = (value) => {
    const formatValue = parseInt(value.replace(/\./g, ''))

    if (isNaN(formatValue)) return

    return parseInt(value.replace(/\./g, ''))
  }

  return (
    <>
      {/* examination_and_treatment */}
      <FormFieldHaveLevel
        title={t('insurances:form:outpatient:examination_and_treatment')}
        fieldDetail={t('insurances:fields:examination_and_treatment')}
        name={['outpatient', 'examination_and_treatment']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['outpatient', 'examination_and_treatment', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) =>
                onKeyDown(event, 'outpatient.examination_and_treatment')
              }
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['outpatient', 'examination_and_treatment', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* testing_and_diagnosis */}
      <FormFieldHaveLevel
        title={t('insurances:form:outpatient:testing_and_diagnosis')}
        fieldDetail={t('insurances:fields:testing_and_diagnosis')}
        name={['outpatient', 'testing_and_diagnosis']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['outpatient', 'testing_and_diagnosis', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) =>
                onKeyDown(event, 'outpatient.testing_and_diagnosis')
              }
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['outpatient', 'testing_and_diagnosis', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* home_care */}
      <FormFieldHaveLevel
        title={t('insurances:form:outpatient:home_care')}
        fieldDetail={t('insurances:fields:home_care')}
        name={['outpatient', 'home_care']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['outpatient', 'home_care', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) => onKeyDown(event, 'outpatient.home_care')}
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['outpatient', 'home_care', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* due_to_accident */}
      <FormFieldHaveLevel
        title={t('insurances:form:outpatient:due_to_accident')}
        fieldDetail={t('insurances:fields:due_to_accident')}
        name={['outpatient', 'due_to_accident']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['outpatient', 'due_to_accident', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) =>
                onKeyDown(event, 'outpatient.due_to_accident')
              }
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['outpatient', 'due_to_accident', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* due_to_illness */}
      <FormFieldHaveLevel
        title={t('insurances:form:outpatient:due_to_illness')}
        fieldDetail={t('insurances:fields:due_to_illness')}
        name={['outpatient', 'due_to_illness']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['outpatient', 'due_to_illness', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) =>
                onKeyDown(event, 'outpatient.due_to_illness')
              }
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['outpatient', 'due_to_illness', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* due_to_cancer */}
      <FormFieldHaveLevel
        title={t('insurances:form:outpatient:due_to_cancer')}
        fieldDetail={t('insurances:fields:due_to_cancer')}
        name={['outpatient', 'due_to_cancer']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['outpatient', 'due_to_cancer', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) =>
                onKeyDown(event, 'outpatient.due_to_cancer')
              }
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['outpatient', 'due_to_cancer', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>

      {/* restore_functionality */}
      <FormFieldHaveLevel
        title={t('insurances:form:outpatient:restore_functionality')}
        fieldDetail={t('insurances:fields:restore_functionality')}
        name={['outpatient', 'restore_functionality']}
      >
        <div className="flex w-full items-center">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:money')}
          </p>
          <Form.Item
            name={['outpatient', 'restore_functionality', 'value']}
            className="mx-2 mb-0 w-full"
          >
            <InputNumber
              className="w-full"
              controls={false}
              placeholder={t('insurances:form:placeholder:money')}
              formatter={handleFormatter}
              parser={handleParser}
              size="large"
              addonAfter={
                <span className="text-base font-normal text-metallic">VNĐ</span>
              }
              onKeyDown={(event) =>
                onKeyDown(event, 'outpatient.restore_functionality')
              }
            />
          </Form.Item>
        </div>
        <div className="mt-4 flex">
          <p className="w-1/5 pr-2 text-right text-base font-bold  text-arsenic">
            {t('insurances:form:description')}
          </p>
          <Form.Item
            name={['outpatient', 'restore_functionality', 'description']}
            className="mx-2 w-full"
          >
            <TextArea
              placeholder={t('insurances:form:placeholder:detail')}
              autoSize={{ minRows: 5, maxRows: 7 }}
              size="large"
            />
          </Form.Item>
        </div>
      </FormFieldHaveLevel>
    </>
  )
}

export default memo(FormOutpatientServices)
