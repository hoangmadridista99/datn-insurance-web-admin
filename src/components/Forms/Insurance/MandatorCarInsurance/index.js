import React from 'react'
import FormLayout from 'components/Forms/Insurance/FormLayout'
import ItemCompulsoryAutoInsurance from './Item'
import { useTranslation } from 'next-i18next'

const FormMandatorCarInsurance = () => {
  const { t } = useTranslation(['car-insurances'])

  return (
    <>
      <FormLayout
        title={t('car-insurances:form:mandatory:title:no_business')}
        hidden
      >
        <ItemCompulsoryAutoInsurance
          field="non_business_less_than"
          title={t('car-insurances:form:mandatory:key:less_than_6_seats')}
        />
        <ItemCompulsoryAutoInsurance
          field="non_business_more_than_or_equal"
          title={t('car-insurances:form:mandatory:key:6_to_11_seats')}
        />
        <ItemCompulsoryAutoInsurance
          field="non_business_passenger_cargo_car"
          title={t('car-insurances:form:mandatory:key:both_people_and_goods')}
        />
      </FormLayout>
      <FormLayout
        title={t('car-insurances:form:mandatory:title:business')}
        hidden
      >
        <ItemCompulsoryAutoInsurance
          field="business_less_than"
          title={t('car-insurances:form:mandatory:key:less_than_6_seats')}
        />
        <ItemCompulsoryAutoInsurance
          field="business_six_seats"
          title={t('car-insurances:form:mandatory:key:6_seats')}
        />
        <ItemCompulsoryAutoInsurance
          field="business_seven_seats"
          title={t('car-insurances:form:mandatory:key:7_seats')}
        />
        <ItemCompulsoryAutoInsurance
          field="business_eight_seats"
          title={t('car-insurances:form:mandatory:key:8_seats')}
        />
        <ItemCompulsoryAutoInsurance
          field="business_nine_seats"
          title={t('car-insurances:form:mandatory:key:9_seats')}
        />
        <ItemCompulsoryAutoInsurance
          field="business_ten_seats"
          title={t('car-insurances:form:mandatory:key:10_seats')}
        />
        <ItemCompulsoryAutoInsurance
          field="business_eleven_seats"
          title={t('car-insurances:form:mandatory:key:11_seats')}
        />
        <ItemCompulsoryAutoInsurance
          field="business_passenger_cargo_car"
          title={t('car-insurances:form:mandatory:key:both_people_and_goods')}
        />
      </FormLayout>
    </>
  )
}

export default FormMandatorCarInsurance
