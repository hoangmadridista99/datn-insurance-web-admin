import React from 'react'
import { Form, InputNumber } from 'antd'
import { useTranslation } from 'next-i18next'
import { handleFormatter, handleParser } from '@utils/helper'

const FormItemMandatorCarInsurance = ({ field, title }) => {
  const { t } = useTranslation(['car-insurances'])

  return (
    <>
      <p className="mb-4 text-base font-bold text-arsenic">{title}</p>
      <Form.Item
        name={[field]}
        className="mb-10 last:mb-0"
        rules={[
          {
            required: true,
            message: t('car-insurances:form:validate:physical_cost'),
          },
        ]}
      >
        <InputNumber
          size="large"
          controls={false}
          parser={handleParser}
          formatter={handleFormatter}
          placeholder={t('car-insurances:form:placeholders:default_cost')}
          className="w-full"
          addonAfter={
            <span className="text-base font-normal text-metallic">VNĐ</span>
          }
        />
      </Form.Item>
    </>
  )
}

export default FormItemMandatorCarInsurance
