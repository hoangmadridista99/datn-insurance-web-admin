import React, { useCallback, useMemo, useState, useRef, useEffect } from 'react'
import { Form, message, notification } from 'antd'
import {
  CAR_INSURANCE_FIELDS,
  CAR_INSURANCE_TYPE,
} from '@utils/constants/insurances'
import FormCarInsuranceDetails from './Details'
import FormCarInsurancePhysical from './Physical'
import { handleError } from '@helpers/handleError'
import { useTranslation } from 'next-i18next'

const CarInsuranceForm = ({
  form,
  handleFinishForm,
  isLoading,
  setIsLoading,
  carInsuranceDetails,
}) => {
  const { t } = useTranslation(['car-insurances'])
  const inputFileRef = useRef(null)
  let debounceId = null

  const [fields, setFields] = useState([])
  const [activeFields, setActiveFields] = useState(
    carInsuranceDetails ? CAR_INSURANCE_FIELDS : []
  )
  const [file, setFile] = useState(null)
  const [fileUrl, setFileUrl] = useState(
    carInsuranceDetails?.company_logo_url ?? null
  )
  const [insuranceType, setInsuranceType] = useState([])
  const [isUploadError, setIsUploadError] = useState(false)

  useEffect(() => {
    if (carInsuranceDetails) {
      const insurance_type =
        carInsuranceDetails?.insurance_type === CAR_INSURANCE_TYPE.BOTH
          ? [CAR_INSURANCE_TYPE.MANDATORY, CAR_INSURANCE_TYPE.PHYSICAL]
          : [carInsuranceDetails.insurance_type]
      setInsuranceType(insurance_type)
    }
  }, [carInsuranceDetails])

  const handleActiveField = useCallback(
    (list) => {
      let listFieldsActive = list.reduce((result, field) => {
        const isField = fields.find(
          (item) => item.name[0] === field && item.value
        )

        if (!isField) return result

        return [...result, field]
      }, [])

      if (fileUrl && !listFieldsActive.includes('company_logo')) {
        listFieldsActive = [...listFieldsActive, 'company_logo']
      }

      if (!fileUrl && listFieldsActive.includes('company_logo')) {
        listFieldsActive = listFieldsActive.filter(
          (item) => item !== 'company_logo'
        )
      }

      return listFieldsActive
    },
    [fields, fileUrl]
  )

  const isPhysical = useMemo(
    () => insuranceType.includes(CAR_INSURANCE_TYPE.PHYSICAL),
    [insuranceType]
  )

  const handleClickChooseFile = () => inputFileRef?.current?.click()

  const handleChangeInputFile = (event) => {
    const { files } = event.target

    if (files && files.length > 0) {
      const isLessThan15MB = files[0].size / 1024 / 1024 <= 15

      if (!isLessThan15MB) {
        setIsUploadError(true)
        return message.error(t('settings:profile:form:avatar:error'))
      }

      setIsUploadError(false)
      setFile(files[0])
      return setFileUrl(URL.createObjectURL(files[0]))
    }
  }

  const handleFieldsChange = (_, allFields) => {
    clearTimeout(debounceId)

    debounceId = setTimeout(() => {
      setActiveFields(null)
      setFields(allFields)
    }, 500)
  }

  const onFinishForm = async (values) => {
    try {
      if (!file && !fileUrl) {
        notification.warning({
          message: t('car-insurances:form:validate:company_logo'),
        })
        return
      }

      setIsLoading(true)
      const { insurance_type, ...data } = values
      const insuranceType =
        insurance_type.length >= 2 ? CAR_INSURANCE_TYPE.BOTH : insurance_type[0]
      const dataBody = { ...data, insurance_type: insuranceType }
      const body = new FormData()
      if (file) {
        body.append('file', file)
      }
      Object.entries(dataBody).forEach(([key, value]) => {
        if (typeof value === 'object') {
          body.append(key, JSON.stringify(value))
          return
        }

        body.append(key, value)
      })
      await handleFinishForm(body)
    } catch (error) {
      handleError(error)
    } finally {
      setIsLoading(false)
      form.resetFields()
      setFile(null)
      setFileUrl(null)
      setInsuranceType([])
      setFields([])
      setActiveFields(null)
    }
  }

  return (
    <Form
      form={form}
      scrollToFirstError
      onFieldsChange={handleFieldsChange}
      onFinish={onFinishForm}
      className="mb-10"
      disabled={isLoading}
    >
      <FormCarInsuranceDetails
        fileUrl={fileUrl}
        inputFileRef={inputFileRef}
        handleChangeInputFile={handleChangeInputFile}
        handleClickChooseFile={handleClickChooseFile}
        handleActiveField={handleActiveField}
        activeFields={activeFields}
        setInsuranceType={setInsuranceType}
        isUploadError={isUploadError}
      />
      {isPhysical && <FormCarInsurancePhysical form={form} />}
    </Form>
  )
}

export default CarInsuranceForm
