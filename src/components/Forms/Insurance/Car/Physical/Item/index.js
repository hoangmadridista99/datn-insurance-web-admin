import React, { useState } from 'react'
import { Form, Radio, InputNumber } from 'antd'
import { PHYSICAL_CAR_TYPE } from '@utils/constants/insurances'
import { useTranslation } from 'next-i18next'
import { handleFormatter, handleParser } from '@utils/helper'
import { useInputNumber } from '@hooks/useInputNumber'

const FormItemCarInsurancePhysical = ({ form, title, field, placeholder }) => {
  const [type, setType] = useState(PHYSICAL_CAR_TYPE.COST)

  const { t } = useTranslation(['car-insurances'])
  const { onKeyDown, setErrors } = useInputNumber()

  const handleChangeRadioType = async (event) => {
    const {
      target: { value },
    } = event

    setType(value)
    setErrors((preState) => ({ ...preState, carInsuranceValue: false }))

    form.setFieldValue([field, 'value'], null)
    await form.validateFields([field])
    form.resetFields([[field, 'value']])
  }

  return (
    <div className="mb-10 last:mb-0">
      {title && <p className="mb-4 text-base font-bold text-nickel">{title}</p>}
      <Form.Item initialValue={type} name={[field, 'type']} className="mb-5">
        <Radio.Group onChange={handleChangeRadioType} className="flex gap-10">
          <Radio className="text-arsenic" value={PHYSICAL_CAR_TYPE.COST}>
            {t('car-insurances:form:options:cost')}
          </Radio>
          <Radio className="text-arsenic" value={PHYSICAL_CAR_TYPE.PERCENTAGE}>
            {t('car-insurances:form:options:percentage')}
          </Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        name={[field, 'value']}
        className="mb-0"
        rules={[
          {
            required: true,
            message: t(`car-insurances:form:validate:physical_${type}`),
          },
          {
            type: 'number',
            max: type === PHYSICAL_CAR_TYPE.PERCENTAGE && 100,
            message: t('car-insurances:form:validate:percentage_over'),
          },
        ]}
      >
        <InputNumber
          size="large"
          controls={false}
          placeholder={
            placeholder ?? t(`car-insurances:form:placeholders:default_${type}`)
          }
          className="w-full"
          formatter={handleFormatter}
          parser={handleParser}
          addonAfter={
            <span className="w-14 text-base font-normal text-metallic">
              {type === PHYSICAL_CAR_TYPE.COST ? 'VNĐ' : '%'}
            </span>
          }
          onKeyDown={(event) => onKeyDown(event, 'carInsuranceValue')}
        />
      </Form.Item>
    </div>
  )
}

export default FormItemCarInsurancePhysical
