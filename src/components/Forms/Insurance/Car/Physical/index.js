import React from 'react'
import FormLayout from '../../FormLayout'
import { useTranslation } from 'next-i18next'
import FormItemCarInsurancePhysical from './Item'

const FormCarInsurancePhysical = ({ form }) => {
  const { t } = useTranslation(['car-insurances'])

  return (
    <FormLayout
      title={t('car-insurances:form:physical:title:default')}
      hidden
      destroyInactivePanel={true}
    >
      <div className="mb-6 border-0 border-b border-solid border-platinum pb-3">
        <p className="mb-2 text-xl font-bold text-arsenic">
          {t('car-insurances:form:physical:title:price_insurance')}
        </p>
        <span className="text-base text-nickel">
          {t('car-insurances:form:physical:description:price_insurance')}
        </span>
      </div>
      <FormItemCarInsurancePhysical
        form={form}
        field="cost_of_purchasing"
        placeholder={t('car-insurances:form:placeholders:cost_insurance')}
      />
      <div className="mb-6 border-0 border-b border-solid border-platinum pb-3">
        <p className="mb-2 text-xl font-bold text-arsenic">
          {t('car-insurances:form:physical:title:cost_advanced_benefits')}
        </p>
      </div>
      <FormItemCarInsurancePhysical
        form={form}
        field="choosing_service_center"
        title={t('car-insurances:form:physical:key:choosing_service_center')}
      />
      <FormItemCarInsurancePhysical
        form={form}
        field="component_vehicle_theft"
        title={t('car-insurances:form:physical:key:component_vehicle_theft')}
      />
      <FormItemCarInsurancePhysical
        form={form}
        field="no_depreciation_cost"
        title={t('car-insurances:form:physical:key:no_depreciation_cost')}
      />
      <FormItemCarInsurancePhysical
        form={form}
        field="water_damage"
        title={t('car-insurances:form:physical:key:water_damage')}
      />
      <div className="mb-6 border-0 border-b border-solid border-platinum pb-3">
        <p className="mb-2 text-xl font-bold text-arsenic">
          {t('car-insurances:form:physical:title:cost_additional_benefits')}
        </p>
      </div>
      <FormItemCarInsurancePhysical
        form={form}
        field="insured_for_each_person"
        title={t('car-insurances:form:physical:key:insured_for_each_person')}
      />
    </FormLayout>
  )
}

export default FormCarInsurancePhysical
