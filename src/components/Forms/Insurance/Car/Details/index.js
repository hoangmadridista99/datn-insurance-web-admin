import React, { useMemo } from 'react'
import FormLayout from '../../FormLayout'
import {
  CAR_INSURANCE,
  CAR_INSURANCE_FIELDS,
  CAR_INSURANCE_TYPE,
} from '@utils/constants/insurances'
import FormField from '../../FormField'
import { Checkbox, Form, Input } from 'antd'
import { useTranslation } from 'next-i18next'
import ImageIconSvg from 'components/Icon/Image'
import EditUnderlineIconSvg from 'components/Icon/EditUnderline'
import Image from 'next/image'
import { NICKEL, VERMILION } from '@utils/constants/color'

const { TextArea } = Input

const FormCarInsuranceDetails = ({
  fileUrl,
  inputFileRef,
  handleChangeInputFile,
  handleClickChooseFile,
  handleActiveField,
  activeFields,
  setInsuranceType,
  isUploadError,
}) => {
  const { t } = useTranslation(['car-insurances'])

  const optionsInsuranceType = useMemo(
    () =>
      Object.values(CAR_INSURANCE_TYPE)
        .filter((key) => key !== CAR_INSURANCE_TYPE.BOTH)
        .map((key) => ({
          value: key,
          label: t(`car-insurances:form:options:${key}`),
        })),
    [t]
  )

  return (
    <FormLayout
      activeKey="details"
      title={t('car-insurances:form:details:title')}
      active={
        activeFields?.details ?? handleActiveField(CAR_INSURANCE_FIELDS.details)
      }
      insurancesType={CAR_INSURANCE}
      customTrans="car-insurances"
    >
      <FormField
        title={t('car-insurances:form:details:company_name')}
        isHiddenFieldDetail={false}
      >
        <Form.Item
          name="company_name"
          rules={[
            {
              required: true,
              message: t('car-insurances:form:validate:company_name'),
            },
          ]}
        >
          <Input
            placeholder={t('car-insurances:form:placeholders:company_name')}
            size="large"
          />
        </Form.Item>
      </FormField>
      <FormField
        title={t('car-insurances:form:details:company_logo')}
        isHiddenFieldDetail={false}
      >
        <input
          onChange={handleChangeInputFile}
          type="file"
          className="hidden"
          ref={inputFileRef}
          accept=".jpeg, .jpg, .png"
        />
        <div
          onClick={handleClickChooseFile}
          className="flex cursor-pointer items-center gap-4"
        >
          {fileUrl ? (
            <Image
              width={88}
              height={88}
              src={fileUrl}
              alt="image"
              className="rounded-md object-cover"
            />
          ) : (
            <div className="flex items-center justify-center rounded-md border border-dashed border-chinese-silver p-7">
              <ImageIconSvg />
            </div>
          )}
          <div className="text-sm text-nickel">
            <p className="mb-1">
              {t('car-insurances:button:upload:max_image_size')}
            </p>
            <p className="mb-4">
              {t('car-insurances:button:upload:suggested_size')}
            </p>

            <div className="flex items-center gap-4">
              <button
                type="button"
                className={`${
                  isUploadError ? 'border-vermilion ' : 'border-nickel'
                } flex cursor-pointer items-center gap-2 rounded border border-solid bg-transparent px-4 py-1`}
              >
                <EditUnderlineIconSvg
                  stroke={isUploadError ? VERMILION : NICKEL}
                />
                <span
                  className={`${
                    isUploadError ? 'text-vermilion' : 'text-nickel'
                  } ml-2 font-bold `}
                >
                  {t('car-insurances:button:upload:title')}
                </span>
              </button>

              {isUploadError && (
                <p className="text-vermilion">
                  <span>{t('settings:profile:form:avatar:error')}</span>
                </p>
              )}
            </div>
          </div>
        </div>
      </FormField>
      <FormField
        title={t('car-insurances:form:details:insurance_name')}
        isHiddenFieldDetail={false}
      >
        <Form.Item
          name="insurance_name"
          rules={[
            {
              required: true,
              message: t('car-insurances:form:validate:insurance_name'),
            },
          ]}
        >
          <Input
            placeholder={t('car-insurances:form:placeholders:insurance_name')}
            size="large"
          />
        </Form.Item>
      </FormField>
      <FormField
        title={t('car-insurances:form:details:insurance_description')}
        isHiddenFieldDetail={false}
      >
        <Form.Item
          name="insurance_description"
          rules={[
            {
              required: true,
              message: t('car-insurances:form:validate:insurance_description'),
            },
          ]}
        >
          <TextArea
            placeholder={t(
              'car-insurances:form:placeholders:insurance_description'
            )}
            size="large"
            rows={4}
          />
        </Form.Item>
      </FormField>
      <FormField
        title={t('car-insurances:form:details:insurance_type')}
        isHiddenFieldDetail={false}
      >
        <Form.Item
          name="insurance_type"
          rules={[
            {
              required: true,
              message: t('car-insurances:form:validate:insurance_type'),
            },
          ]}
        >
          <Checkbox.Group
            onChange={(values) => setInsuranceType(values)}
            className="flex flex-col gap-6"
          >
            {optionsInsuranceType.map((item) => (
              <Checkbox
                className="m-0 text-base text-arsenic"
                key={item.value}
                value={item.value}
              >
                {item.label}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </Form.Item>
      </FormField>
    </FormLayout>
  )
}

export default FormCarInsuranceDetails
