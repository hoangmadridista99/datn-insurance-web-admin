import React, { useState, useRef, useCallback, useEffect } from 'react'
import Image from 'next/image'
import { Button, Input, Form, Select, notification } from 'antd'
import { uploadBlogBanner } from '@services/upload'

import Editor from '@components/Editor'
import { handleError } from '@helpers/handleError'
import { useTranslation } from 'next-i18next'

const { TextArea } = Input

const FormBlog = (props) => {
  const { t } = useTranslation(['blogs'])
  const inputUploadFileRef = useRef(null)
  const { form, categories, onFinishForm, blogDetails, isDisable } = props

  const [fileUpload, setFileUpload] = useState(null)
  const [banner, setBanner] = useState()
  const [content, setContent] = useState()

  const handleUploadBannerBlog = useCallback(async (file, signal) => {
    const body = new FormData()
    body.append('file', file)

    const result = await uploadBlogBanner(body, signal)

    setBanner(result.image_url)
  }, [])

  useEffect(() => {
    if (blogDetails) {
      setBanner(blogDetails?.banner_image_url)
      setContent(blogDetails?.content)
    }
  }, [blogDetails])
  useEffect(() => {
    const controller = new AbortController()

    if (fileUpload) {
      handleUploadBannerBlog(fileUpload, controller.signal)
        .catch((error) => handleError(error))
        .finally(() => setFileUpload(null))
    }

    return () => controller.abort()
  }, [handleUploadBannerBlog, fileUpload])

  const handleFinishForm = (values) => {
    if (!banner) {
      notification.warning({ message: t('blogs:form:errors:banner') })

      return
    }

    if (!content) {
      notification.warning({ message: t('blogs:form:errors:content') })

      return
    }

    return onFinishForm({ ...values, banner_image_url: banner, content })
  }

  const handleClickUploadBannerBlog = () => {
    if (inputUploadFileRef && inputUploadFileRef.current) {
      inputUploadFileRef.current.click()
    }
  }
  const handleClickRemoveBanner = () => setBanner(undefined)

  const handleChangeUpload = (e) => {
    const value = e.target.files[0]

    setFileUpload(value)
  }
  const handleChangeContent = (value) => setContent(value)

  return (
    <Form
      className="w-full flex-col justify-between rounded-2xl bg-white p-6 shadow-md-0.08"
      form={form}
      onFinish={handleFinishForm}
      initialValues={blogDetails}
      disabled={isDisable}
    >
      <div className="mb-6 last:mb-0">
        <label
          className="mb-2 block text-sm font-bold text-gray-700"
          htmlFor="banner_image_url"
        >
          Ảnh bìa
        </label>
        {!banner && (
          <>
            <input
              className="hidden"
              id="banner_image_url"
              type="file"
              ref={inputUploadFileRef}
              onChange={handleChangeUpload}
              accept="image/gif,image/jpeg,image/jpg,image/png,image/svg"
            />
            <div className="flex h-37 w-full flex-col items-center justify-center rounded-lg border border-dashed shadow-inset">
              <div className="mb-4 flex items-center justify-center">
                <Image
                  src="/svg/image-default.svg"
                  width={48}
                  height={48}
                  alt="Form Blog Image Default"
                />
                <div className="ml-4 flex h-full flex-col justify-between">
                  <p className="text-sm font-normal text-arsenic">
                    {t('blogs:form:upload:instruct')}
                  </p>
                  <span className="text-xs font-normal text-nickel">
                    {t('blogs:form:upload:rules')}
                  </span>
                </div>
              </div>
              <Button
                id="banner"
                type="primary"
                onClick={handleClickUploadBannerBlog}
                disabled={!!fileUpload}
              >
                {t('blogs:form:button:upload')}
              </Button>
            </div>
          </>
        )}
        {banner && (
          <div className="relative h-100 w-full">
            <Image src={banner} fill alt="Banner" className="object-cover" />
            <Button
              className="absolute  top-4 right-4 z-10 flex h-8 w-8 items-center justify-center rounded-full bg-black-0.5"
              onClick={handleClickRemoveBanner}
            >
              <Image
                src="/svg/close-blog.svg"
                width={10}
                height={10}
                alt="Remove"
              />
            </Button>
          </div>
        )}
      </div>
      <div className="mb-6 last:mb-0">
        <label
          className="mb-4 block text-base font-bold text-arsenic"
          htmlFor="blog_category_id"
        >
          {t('blogs:form:category')}
        </label>
        <Form.Item
          name="blog_category_id"
          rules={[{ required: true, message: t('blogs:form:errors:category') }]}
        >
          <Select
            size="large"
            options={categories}
            placeholder={t('blogs:placeholder:choose')}
          />
        </Form.Item>
      </div>
      <div className="mb-6 last:mb-0">
        <label
          className="mb-4 block text-base font-bold text-arsenic"
          htmlFor="title"
        >
          {t('blogs:form:title')}
        </label>
        <Form.Item
          name="title"
          rules={[{ required: true, message: t('blogs:form:errors:title') }]}
        >
          <Input
            className="w-full rounded-md border border-chinese-silver p-3 placeholder:text-chinese-silver focus:outline-none"
            id="title"
            type="text"
            placeholder={t('blogs:placeholder:title')}
          />
        </Form.Item>
      </div>
      <div className="mb-6 last:mb-0">
        <label
          className="mb-4 block text-base font-bold text-arsenic"
          htmlFor="description"
        >
          {t('blogs:form:description')}
        </label>
        <Form.Item
          className="mb-6"
          name="description"
          rules={[
            { required: true, message: t('blogs:form:errors:description') },
          ]}
        >
          <TextArea
            className="w-full rounded-md border border-chinese-silver p-3 placeholder:text-chinese-silver focus:outline-none"
            id="description"
            type="text"
            placeholder={t('blogs:placeholder:description')}
          />
        </Form.Item>
      </div>
      <div className="mb-6 last:mb-0">
        <label
          className="mb-4 block text-base font-bold text-arsenic"
          htmlFor="content"
        >
          {t('blogs:form:content')}
        </label>
        <Editor
          value={content ? decodeURI(content) : undefined}
          onChange={handleChangeContent}
        />
      </div>
    </Form>
  )
}

export default FormBlog
