import { Form, Input, Switch } from 'antd'
import React from 'react'
import { useTranslation } from 'next-i18next'

const FormBlogCategory = ({ form, initialValues }) => {
  const { t } = useTranslation(['blogs'])

  return (
    <>
      <p className="border-0 border-b border-solid border-ghost-white pb-4 text-center text-2xl font-medium text-arsenic">
        {initialValues
          ? t('blogs:modals.categories:form_update_title')
          : t('blogs:modals.categories:form_create_title')}
      </p>

      <Form form={form} initialValues={initialValues} layout="vertical">
        <div className="my-4">
          <div className="flex flex-col items-start gap-2">
            <p className="text-base font-bold text-arsenic">
              {t('blogs:vietnamese_title')}
            </p>

            <Form.Item
              name="vn_label"
              className="w-full"
              rules={[
                { required: true, message: t('blogs:form.errors.enter') },
              ]}
            >
              <Input placeholder={t('blogs:placeholder.enter')} size="large" />
            </Form.Item>
          </div>

          <div className="flex flex-col items-start gap-2">
            <p className="text-base font-bold text-arsenic">
              {t('blogs:english_title')}
            </p>
            <Form.Item
              name="en_label"
              className="w-full"
              rules={[
                { required: true, message: t('blogs:form.errors.enter') },
              ]}
            >
              <Input placeholder={t('blogs:placeholder.enter')} size="large" />
            </Form.Item>
          </div>

          {initialValues && (
            <div className="flex items-center gap-4">
              <p className="text-base font-bold text-arsenic">
                {t('blogs:modals.categories.button.hide_label')}
              </p>

              <Form.Item name="is_hide" className="mb-0">
                <Switch defaultChecked={initialValues?.is_hide} />
              </Form.Item>
            </div>
          )}
        </div>
      </Form>
    </>
  )
}

export default FormBlogCategory
