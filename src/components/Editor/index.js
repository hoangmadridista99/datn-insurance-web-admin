import React, { useState } from 'react'
import { Editor } from '@tinymce/tinymce-react'

import { handleError } from '@helpers/handleError'
import { uploadBlogBanner } from '@services/upload'
import LoadingIconSvg from 'components/Icon/Loading'

const EditorComponent = ({ onChange, value }) => {
  const [isLoaded, setIsLoaded] = useState(false)

  const handleUploadImage = (blobInfo) =>
    new Promise((resolve) => {
      const body = new FormData()
      body.append('file', blobInfo.blob(), blobInfo.filename())

      const reader = new FileReader()

      uploadBlogBanner(body)
        .then((result) => resolve(result.image_url))
        .catch((error) => handleError(error))

      reader.readAsDataURL(blobInfo.blob())
    })

  return (
    <>
      {!isLoaded && <LoadingIconSvg />}
      <Editor
        apiKey="5fbchhduklga0b7i65mfqrdhtatmm6qrgwt3rvbybo0rt3vz"
        onEditorChange={(value) => onChange(encodeURI(value))}
        init={{
          height: 500,
          menubar: false,
          plugins: ['fullscreen', 'link', 'image', 'advlist', 'lists'],
          toolbar: [
            'fullscreen | undo redo | blocks | fontsize | bold italic underline backcolor | numlist bullist outdent indent | link image | alignleft aligncenter alignright alignjustify ',
          ],
          content_style: 'body { font-size:14px }',
          file_picker_types: 'image',
          automatic_uploads: true,
          images_upload_handler: handleUploadImage,
        }}
        onLoadContent={() => setIsLoaded(true)}
        value={value}
      />
    </>
  )
}

export default EditorComponent
