import React from 'react'
import { useTranslation } from 'next-i18next'
import { formatCurrency } from '@helpers/formatCurrency'

const PreviewMandatoryCarInsurance = ({ carInsuranceSettingMandatory }) => {
  const { t } = useTranslation(['car-insurances'])

  return (
    <>
      <p className="mb-4 text-base font-bold text-arsenic">
        {t('car-insurances:form:mandatory:title:no_business')}
      </p>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:mandatory:key:less_than_6_seats')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceSettingMandatory?.non_business_less_than ?? 0
          )} VNĐ`}
        </p>
      </div>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:mandatory:key:6_to_11_seats')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceSettingMandatory?.non_business_more_than_or_equal ?? 0
          )} VNĐ`}
        </p>
      </div>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:mandatory:key:both_people_and_goods')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceSettingMandatory?.non_business_passenger_cargo_car ?? 0
          )} VNĐ`}
        </p>
      </div>
      <p className="mb-4 text-base font-bold text-arsenic">
        {t('car-insurances:form:mandatory:title:business')}
      </p>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:mandatory:key:less_than_6_seats')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceSettingMandatory?.business_less_than ?? 0
          )} VNĐ`}
        </p>
      </div>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:mandatory:key:6_seats')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceSettingMandatory?.business_six_seats ?? 0
          )} VNĐ`}
        </p>
      </div>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:mandatory:key:7_seats')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceSettingMandatory?.business_seven_seats ?? 0
          )} VNĐ`}
        </p>
      </div>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:mandatory:key:8_seats')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceSettingMandatory?.business_eight_seats ?? 0
          )} VNĐ`}
        </p>
      </div>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:mandatory:key:9_seats')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceSettingMandatory?.business_nine_seats ?? 0
          )} VNĐ`}
        </p>
      </div>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:mandatory:key:10_seats')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceSettingMandatory?.business_ten_seats ?? 0
          )} VNĐ`}
        </p>
      </div>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:mandatory:key:11_seats')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceSettingMandatory?.business_eleven_seats ?? 0
          )} VNĐ`}
        </p>
      </div>
      <div className="flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:mandatory:key:both_people_and_goods')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceSettingMandatory?.business_passenger_cargo_car ?? 0
          )} VNĐ`}
        </p>
      </div>
    </>
  )
}

export default PreviewMandatoryCarInsurance
