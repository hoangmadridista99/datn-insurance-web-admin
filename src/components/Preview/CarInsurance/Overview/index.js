import React from 'react'
import { useTranslation } from 'next-i18next'
import { CAR_INSURANCE_TYPE } from '@utils/constants/insurances'

const PreviewOverviewCarInsurance = ({ carInsuranceDetails }) => {
  const { t } = useTranslation(['car-insurances'])

  return (
    <>
      <div className="mb-6">
        <h2 className="mb-4 text-xl font-medium text-arsenic">
          {`${t('car-insurances:preview:information')} ${
            carInsuranceDetails?.insurance_name
          }`}
        </h2>
        <p className="text-base font-normal text-nickel">
          {carInsuranceDetails?.insurance_description}
        </p>
      </div>
      <h3 className="mb-4 text-xl font-medium text-arsenic">
        {t('car-insurances:preview:insurance_type')}
      </h3>
      <ul className="list-inside text-base text-nickel">
        {carInsuranceDetails?.insurance_type === CAR_INSURANCE_TYPE.BOTH ? (
          <>
            <li className="mb-3">{t('car-insurances:card:physical')}</li>
            <li>{t('car-insurances:card:mandatory')}</li>
          </>
        ) : (
          <li>
            {t(`car-insurances:card:${carInsuranceDetails?.insurance_type}`)}
          </li>
        )}
      </ul>
    </>
  )
}

export default PreviewOverviewCarInsurance
