import React from 'react'
import { useTranslation } from 'next-i18next'
import { formatCurrency } from '@helpers/formatCurrency'
import { PHYSICAL_CAR_TYPE } from '@utils/constants/insurances'

const PreviewPhysicalCarInsurance = ({ carInsuranceDetails }) => {
  const { t } = useTranslation(['car-insurances'])

  return (
    <>
      <div className="mb-6 flex items-center justify-between">
        <p className="text-base font-bold text-arsenic">
          {t('car-insurances:form:physical:title:price_insurance')}
        </p>
        <p className="text-base font-bold text-ultramarine-blue">
          {`${formatCurrency(
            carInsuranceDetails?.cost_of_purchasing?.value ?? 0
          )} ${
            carInsuranceDetails?.cost_of_purchasing?.type ===
            PHYSICAL_CAR_TYPE.COST
              ? 'VNĐ'
              : `% ${t('car-insurances:card:car_price')}`
          }`}
        </p>
      </div>
      <p className="mb-4 text-base font-bold text-arsenic">
        {t('car-insurances:form:physical:title:cost_advanced_benefits')}
      </p>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:physical:key:choosing_service_center')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceDetails?.choosing_service_center?.value ?? 0
          )} ${
            carInsuranceDetails?.choosing_service_center?.type ===
            PHYSICAL_CAR_TYPE.COST
              ? 'VNĐ'
              : `% ${t('car-insurances:card:car_price')}`
          }`}
        </p>
      </div>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:physical:key:component_vehicle_theft')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceDetails?.component_vehicle_theft?.value ?? 0
          )} ${
            carInsuranceDetails?.component_vehicle_theft?.type ===
            PHYSICAL_CAR_TYPE.COST
              ? 'VNĐ'
              : `% ${t('car-insurances:card:car_price')}`
          }`}
        </p>
      </div>
      <div className="mb-4 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:physical:key:no_depreciation_cost')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceDetails?.no_depreciation_cost?.value ?? 0
          )} ${
            carInsuranceDetails?.no_depreciation_cost?.type ===
            PHYSICAL_CAR_TYPE.COST
              ? 'VNĐ'
              : `% ${t('car-insurances:card:car_price')}`
          }`}
        </p>
      </div>
      <div className="mb-6 flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:physical:key:water_damage')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceDetails?.water_damage?.value ?? 0
          )} ${
            carInsuranceDetails?.water_damage?.type === PHYSICAL_CAR_TYPE.COST
              ? 'VNĐ'
              : `% ${t('car-insurances:card:car_price')}`
          }`}
        </p>
      </div>
      <p className="mb-4 text-base font-bold text-arsenic">
        {t('car-insurances:form:physical:title:cost_additional_benefits')}
      </p>
      <div className="flex items-center justify-between">
        <p className="text-base text-nickel">
          {t('car-insurances:form:physical:key:insured_for_each_person')}
        </p>
        <p className="text-base text-arsenic">
          {`+ ${formatCurrency(
            carInsuranceDetails?.insured_for_each_person?.value ?? 0
          )} ${
            carInsuranceDetails?.insured_for_each_person?.type ===
            PHYSICAL_CAR_TYPE.COST
              ? 'VNĐ'
              : `% ${t('car-insurances:card:car_price')}`
          }`}
        </p>
      </div>
    </>
  )
}

export default PreviewPhysicalCarInsurance
