import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { Modal, Spin, Tabs } from 'antd'
import Image from 'next/image'
import PreviewOverviewCarInsurance from './Overview'
import PreviewPhysicalCarInsurance from './Physical'
import PreviewMandatoryCarInsurance from './Mandatory'
import { useTranslation } from 'next-i18next'
import { handleError } from '@helpers/handleError'
import { getCarInsuranceDetails } from '@services/car-insurances'
import { getCarInsuranceSettingMandatorDetails } from '@services/car-insurance-settings'
import { CAR_INSURANCE_TYPE } from '@utils/constants/insurances'
import { CloseIconSvg } from '@utils/icons'

const PreviewCarInsurance = ({ carInsuranceId, handleClickClosePreview }) => {
  const [carInsuranceDetails, setCarInsuranceDetails] = useState(null)
  const [carInsuranceSettingMandatory, setCarInsuranceSettingMandatory] =
    useState(null)

  const { t } = useTranslation(['car-insurances'])

  const handleGetCarInsuranceDetails = useCallback(
    async (signal) => {
      try {
        if (carInsuranceId) {
          const resCarInsuranceDetails = await getCarInsuranceDetails(
            carInsuranceId,
            signal
          )
          setCarInsuranceDetails(resCarInsuranceDetails)
          if (!carInsuranceSettingMandatory) {
            const resCarInsuranceMandatory =
              await getCarInsuranceSettingMandatorDetails(signal)
            setCarInsuranceSettingMandatory(resCarInsuranceMandatory)
          }
        }
      } catch (error) {
        handleError(error)
      }
    },
    [carInsuranceId, carInsuranceSettingMandatory]
  )

  useEffect(() => {
    const controller = new AbortController()

    handleGetCarInsuranceDetails(controller.signal)

    return () => controller.abort()
  }, [handleGetCarInsuranceDetails])

  const handleClickCancel = () => {
    setCarInsuranceDetails(null)
    handleClickClosePreview()
  }

  const itemPhysical = useMemo(
    () =>
      carInsuranceDetails?.insurance_type === CAR_INSURANCE_TYPE.BOTH ||
      carInsuranceDetails?.insurance_type === CAR_INSURANCE_TYPE.PHYSICAL
        ? [
            {
              key: 'physical',
              label: t('car-insurances:preview:tabs:physical'),
              children: (
                <PreviewPhysicalCarInsurance
                  carInsuranceDetails={carInsuranceDetails}
                />
              ),
            },
          ]
        : [],
    [carInsuranceDetails, t]
  )
  const itemMandatory = useMemo(
    () =>
      (carInsuranceDetails?.insurance_type === CAR_INSURANCE_TYPE.BOTH ||
        carInsuranceDetails?.insurance_type === CAR_INSURANCE_TYPE.MANDATORY) &&
      carInsuranceSettingMandatory
        ? [
            {
              key: 'mandatory',
              label: t('car-insurances:preview:tabs:mandatory'),
              children: (
                <PreviewMandatoryCarInsurance
                  carInsuranceSettingMandatory={carInsuranceSettingMandatory}
                />
              ),
            },
          ]
        : [],
    [carInsuranceDetails, t, carInsuranceSettingMandatory]
  )
  const items = useMemo(() => {
    if (carInsuranceDetails)
      return [
        {
          key: 'overview',
          label: t('car-insurances:preview:tabs:overview'),
          children: (
            <PreviewOverviewCarInsurance
              carInsuranceDetails={carInsuranceDetails}
            />
          ),
        },
        ...itemPhysical,
        ...itemMandatory,
      ]

    return null
  }, [carInsuranceDetails, itemMandatory, itemPhysical, t])

  const Header = () =>
    !carInsuranceDetails ? null : (
      <div className="mb-8 flex items-center gap-4 border-0 border-b border-solid border-platinum pb-4">
        <Image
          src={carInsuranceDetails?.company_logo_url}
          width={64}
          height={64}
          alt="logo"
        />
        <p className="text-2xl font-bold text-arsenic">
          {carInsuranceDetails?.insurance_name}
        </p>
      </div>
    )

  return (
    <Modal
      open={!!carInsuranceId}
      width="70%"
      onCancel={handleClickCancel}
      centered
      footer={null}
      closeIcon={<CloseIconSvg />}
    >
      <div className="no-scrollbar h-[70vh] overflow-hidden overflow-y-scroll">
        {!carInsuranceDetails && (
          <div className="flex h-full items-center justify-center">
            <Spin />
          </div>
        )}

        {carInsuranceDetails && (
          <div className="py-5 px-4">
            <Header />
            <Tabs
              defaultActiveKey="overview"
              popupClassName="test"
              centered
              size="large"
              items={items}
              className="car_insurance_preview"
            />
          </div>
        )}
      </div>
    </Modal>
  )
}

export default PreviewCarInsurance
