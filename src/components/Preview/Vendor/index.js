const { Modal, Button } = require('antd')
import { VENDOR_STATUSES } from '@utils/constants/insurances'
import { CloseIconSvg } from '@utils/icons'
import dayjs from 'dayjs'
import { useTranslation } from 'next-i18next'
import Image from 'next/image'

const VendorPreviewModal = ({
  visible,
  closePreviewModal,
  isLoading,
  vendorDetails,
  handleUpdateStatus,
  handleOpenModalReject,
}) => {
  const { t } = useTranslation('vendor')

  const customizeData = [
    {
      id: 1,
      label: t('vendor:preview:full_name'),
      value: vendorDetails?.fullName,
    },
    {
      id: 2,
      label: t('vendor:preview:date_of_birth'),
      value: dayjs(vendorDetails?.date_of_birth).format('DD/MM/YYYY'),
    },
    {
      id: 3,
      label: t('vendor:preview:phone'),
      value: vendorDetails?.phone,
    },
    {
      id: 4,
      label: t('vendor:preview:email'),
      value: vendorDetails?.email,
    },
    {
      id: 5,
      label: t('vendor:preview:address'),
      value: vendorDetails?.address,
    },
  ]

  return (
    <Modal
      open={visible}
      centered
      onCancel={closePreviewModal}
      okButtonProps={{ loading: visible }}
      closeIcon={<CloseIconSvg />}
      footer={null}
      width={550}
    >
      <div className="px-2 py-3">
        <div className="mb-4 flex items-center justify-start gap-1.5 border-0 border-b border-solid border-ghost-white pb-3">
          <Image
            src={vendorDetails?.company?.logo}
            width={64}
            height={64}
            alt="Company"
          />

          <div className="flex-1">
            <p className="text-base font-bold text-arsenic">
              {vendorDetails?.company?.short_name}
            </p>
          </div>
        </div>

        <div className="mb-8 flex flex-col items-start gap-2">
          {customizeData.map((item) => (
            <div
              key={item.id}
              className="flex w-full items-center justify-start text-base text-nickel"
            >
              <p className="w-1/3 font-bold">{item.label}</p>
              <p className="flex-1 overflow-hidden text-ellipsis whitespace-nowrap font-normal">
                {item.value}
              </p>
            </div>
          ))}
        </div>

        {vendorDetails.vendor_status === VENDOR_STATUSES.PENDING && (
          <div className="flex items-center justify-end gap-6">
            <Button
              className={`px-10 text-base font-bold ${
                (!isLoading.rejected || !isLoading.accepted) &&
                'border-nickel text-nickel hover:border-ultramarine-blue hover:text-ultramarine-blue'
              }`}
              type="default"
              size="large"
              disabled={isLoading.rejected || isLoading.accepted}
              loading={isLoading.rejected}
              onClick={handleOpenModalReject}
            >
              {t('vendor:button:reject')}
            </Button>

            <Button
              className="px-10 text-base font-bold"
              size="large"
              type="primary"
              disabled={isLoading.accepted || isLoading.rejected}
              loading={isLoading.accepted}
              onClick={() =>
                handleUpdateStatus({ vendor_status: VENDOR_STATUSES.ACCEPTED })
              }
            >
              {t('vendor:button:approve')}
            </Button>
          </div>
        )}
      </div>
    </Modal>
  )
}

export default VendorPreviewModal
