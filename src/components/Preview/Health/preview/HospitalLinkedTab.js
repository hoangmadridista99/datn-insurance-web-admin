import { Table } from 'antd'
import React from 'react'
import { useTranslation } from 'next-i18next'
import { ROLES, STATUSES } from '@utils/constants/insurances'

const HospitalLinkedTab = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances'])

  const columns = [
    {
      title: t('insurances:form:linkedList:hospital_name'),
      dataIndex: 'name',
    },
    {
      title: t('insurances:form:linkedList:hospital_address'),
      dataIndex: 'address',
    },
    {
      title: t('insurances:form:linkedList:hospital_province'),
      dataIndex: 'province',
    },
    {
      title: t('insurances:form:linkedList:hospital_district'),
      dataIndex: 'district',
    },
  ]
  return (
    <div
      className={`no-scrollbar ${
        insuranceDetails?.status === STATUSES.PENDING ? 'h-[39vh]' : 'h-[47vh]'
      }  overflow-hidden overflow-y-scroll`}
    >
      <div className="mb-6 font-normal text-nickel">
        <Table
          rowKey={'id'}
          size="middle"
          bordered
          dataSource={
            insuranceDetails?.user?.role === ROLES.VENDOR &&
            Array.isArray(insuranceDetails.hospitals_pending)
              ? [
                  ...insuranceDetails.hospitals,
                  ...insuranceDetails.hospitals_pending,
                ]
              : insuranceDetails?.hospitals
          }
          columns={columns}
          pagination={{
            pageSize: 10,
          }}
        />
      </div>
    </div>
  )
}

export default HospitalLinkedTab
