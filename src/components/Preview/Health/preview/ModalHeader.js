import { HOSPITALIZATION_LIST, ROOM_TYPES } from '@utils/constants/insurances'
import { formatCurrency } from '@helpers/formatCurrency'
import { formatPrices, getIconByLevel } from '@utils/helper'
import Image from 'next/image'
import React from 'react'
import { useTranslation } from 'next-i18next'

const ModalHeader = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances'])

  const convertRoomTypeDataToText = (type) => {
    switch (type) {
      case ROOM_TYPES[0]?.value:
        return <>{t(`insurances:form:options:${ROOM_TYPES[0].label}`)}</>

      case ROOM_TYPES[1]?.value:
        return <>{t(`insurances:form:options:${ROOM_TYPES[1].label}`)}</>

      case ROOM_TYPES[2]?.value:
        return <>{t(`insurances:form:options:${ROOM_TYPES[2].label}`)}</>

      default:
        return <>{t('insurances:fields:no_support')}</>
    }
  }

  const convertHospitalizationDataToText = (type) => {
    switch (type) {
      case HOSPITALIZATION_LIST[0]?.value:
        return (
          <>
            Tối đa&nbsp;
            {formatCurrency(
              insuranceDetails?.inpatient?.for_hospitalization?.value
            )}
            &nbsp;VNĐ/&nbsp;
            {t('insurances:fields:day')}
          </>
        )

      case HOSPITALIZATION_LIST[1]?.value:
        return (
          <>
            Tối đa&nbsp;
            {formatCurrency(
              insuranceDetails?.inpatient?.for_hospitalization?.value
            )}
            &nbsp;VNĐ/&nbsp;{t('insurances:fields:year')}
          </>
        )

      default:
        return <>{t('insurances:fields:no_support')}</>
    }
  }

  return (
    <>
      <div className="mb-4 flex items-end justify-between">
        <div className="inline-flex w-1/2">
          <Image
            src={insuranceDetails?.company.logo}
            width={64}
            height={64}
            alt={insuranceDetails?.company.short_name}
            className="object-none"
          />

          <div className="ml-4 flex flex-col justify-between">
            <h1 className="text-2xl text-arsenic">{insuranceDetails?.name}</h1>

            <p className="text-base font-medium text-nickel">
              {insuranceDetails?.company.long_name}
            </p>
          </div>
        </div>

        <div className="inline-flex whitespace-nowrap text-right">
          {(insuranceDetails?.terms.monthly_fee.from ||
            insuranceDetails?.terms.monthly_fee.to) &&
            formatPrices(
              insuranceDetails?.terms.monthly_fee.from,
              insuranceDetails?.terms.monthly_fee.to,
              t('insurances:fields:month')
            )}
        </div>
      </div>

      <div className="text-sm font-normal text-nickel">
        <p className="mb-3 flex items-center">
          {getIconByLevel(insuranceDetails?.terms.age_eligibility?.level)}
          <span className="ml-2 mr-1">
            {t('insurances:form:inpatient:room_type')}:
          </span>
          <span>
            {convertRoomTypeDataToText(
              insuranceDetails?.inpatient?.room_type?.type
            )}
          </span>
        </p>

        <p className="mb-3 flex items-center">
          {getIconByLevel(insuranceDetails?.terms.age_eligibility?.level)}
          <span className="ml-2 mr-1">
            {t('insurances:form:inpatient:for_hospitalization')}:
          </span>
          <span>
            {convertHospitalizationDataToText(
              insuranceDetails?.inpatient?.for_hospitalization?.type
            )}
          </span>
        </p>

        <p className="mb-3 flex items-center">
          {getIconByLevel(insuranceDetails?.inpatient.for_cancer?.level)}
          <span className="ml-2 mr-1">
            {t('insurances:form:inpatient:for_cancer')}:
          </span>
          <span>
            Tối đa&nbsp;
            {formatCurrency(insuranceDetails?.inpatient?.for_cancer?.value)}
            &nbsp;VNĐ/&nbsp;buổi trị liệu
          </span>
        </p>

        <p className="mb-3 flex items-center">
          {insuranceDetails?.dental ? (
            <Image
              width={18}
              height={18}
              src="/svg/check-success.svg"
              alt="Check Success"
            />
          ) : (
            <Image width={18} height={18} src="/svg/close.svg" alt="close" />
          )}

          <span className="ml-2 mr-1">
            {t('insurances:form:health_additional_benefits:dental')}
            :&nbsp;
            {insuranceDetails?.dental
              ? t('insurances:fields:yes')
              : t('insurances:fields:no')}
          </span>
        </p>

        <p className="mb-3 flex items-center">
          {insuranceDetails?.obstetric ? (
            <Image
              width={18}
              height={18}
              src="/svg/check-success.svg"
              alt="Check Success"
            />
          ) : (
            <Image width={18} height={18} src="/svg/close.svg" alt="close" />
          )}

          <span className="ml-2 mr-1">
            {t('insurances:form:health_additional_benefits:obstetric')}
            :&nbsp;
            {insuranceDetails?.obstetric
              ? t('insurances:fields:yes')
              : t('insurances:fields:no')}
          </span>
        </p>
      </div>
    </>
  )
}

export default ModalHeader
