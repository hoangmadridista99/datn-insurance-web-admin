/* eslint-disable indent */
import React, { useState, useEffect, useCallback } from 'react'
import { Modal, Card, Tabs, Button } from 'antd'
import { useTranslation } from 'next-i18next'
import { getInsuranceDetails } from '@services/insurances'
import OverviewTab from './OverviewTab'
import InpatientTab from './InpatientTab'
import OutpatientTab from './OutpatientTab'
import AdditionalBenefitTab from './AdditionalBenefitTab'
import HospitalLinkedTab from './HospitalLinkedTab'
import { CloseIconSvg } from '@utils/icons'
import ModalHeader from './ModalHeader'
import { STATUSES } from '@utils/constants/insurances'

const HealthModal = ({
  isOpen,
  insuranceId,
  onClose,
  handleChangeStatus,
  handleOpenModalReject,
}) => {
  const { t } = useTranslation(['insurances'])

  const [insuranceDetails, setInsuranceDetails] = useState(null)

  const fetchInsuranceDetails = useCallback(async () => {
    try {
      const response = await getInsuranceDetails(insuranceId)

      setInsuranceDetails(response)
    } catch (error) {
      console.log('Error', error)
    }
  }, [insuranceId])

  useEffect(() => {
    fetchInsuranceDetails()
  }, [fetchInsuranceDetails])

  const tabList = [
    {
      key: 1,
      label: t('insurances:form:overview:title'),
      children: <OverviewTab insuranceDetails={insuranceDetails} />,
    },
    {
      key: 2,
      label: t('insurances:form:inpatient:title'),
      children: <InpatientTab insuranceDetails={insuranceDetails} />,
    },
    {
      key: 3,
      label: t('insurances:form:outpatient:title'),
      children: <OutpatientTab insuranceDetails={insuranceDetails} />,
    },
    {
      key: 4,
      label: t('insurances:form:health_additional_benefits:title'),
      children: <AdditionalBenefitTab insuranceDetails={insuranceDetails} />,
    },
    {
      key: 5,
      label: t('insurances:form:linkedList:title'),
      children: <HospitalLinkedTab insuranceDetails={insuranceDetails} />,
    },
  ]

  return (
    <Modal
      centered
      open={isOpen}
      width="70%"
      destroyOnClose
      onCancel={onClose}
      title={<ModalHeader insuranceDetails={insuranceDetails} />}
      closeIcon={<CloseIconSvg />}
      footer={
        insuranceDetails?.status === STATUSES.PENDING
          ? [
              <Button
                key="reject"
                type="default"
                onClick={() => {
                  handleOpenModalReject()
                  onClose()
                }}
              >
                Reject
              </Button>,
              <Button
                key="approve"
                type="primary"
                onClick={() => {
                  handleChangeStatus(STATUSES.APPROVED)
                  onClose()
                }}
              >
                Approve
              </Button>,
            ]
          : null
      }
    >
      <div
        className={
          insuranceDetails?.status === STATUSES.PENDING
            ? 'h-[50vh]'
            : 'h-[60vh]'
        }
      >
        <Card bordered={false} className="border-none shadow-none">
          <Tabs
            centered
            defaultActiveKey={1}
            items={tabList}
            className="text-base text-spanish-gray"
          />
        </Card>
      </div>
    </Modal>
  )
}

export default HealthModal
