import { formatCurrency } from '@helpers/formatCurrency'
import React from 'react'
import { useTranslation } from 'next-i18next'
import { Empty } from 'antd'
import { getIconByLevel } from '@utils/helper'
import { STATUSES } from '@utils/constants/insurances'

const AdditionalBenefitTab = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances'])

  const renderField = (determinedValue, description) => {
    return (
      <>
        {determinedValue && (
          <p>
            {formatCurrency(determinedValue)}
            <span>&nbsp;</span>
            VNĐ
          </p>
        )}

        {description && <p>{description}</p>}

        {!determinedValue && !description && (
          <>{t('insurances:fields:no_support')}</>
        )}
      </>
    )
  }

  return (
    <div
      className={`no-scrollbar ${
        insuranceDetails?.status === STATUSES.PENDING ? 'h-[39vh]' : 'h-[47vh]'
      }  overflow-hidden overflow-y-scroll`}
    >
      {/* dental */}
      {insuranceDetails?.dental && (
        <div className="mb-6 font-normal text-nickel">
          <p className="mb-4 text-xl font-medium text-arsenic">
            {t('insurances:form:health_additional_benefits:dental')}
          </p>
          {/* examination_and_diagnosis */}
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5">
              {t('insurances:form:dental:examination_and_diagnosis')}
            </p>

            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.dental.examination_and_diagnosis?.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.dental?.examination_and_diagnosis?.value,
                  insuranceDetails?.dental?.examination_and_diagnosis
                    ?.description
                )}
              </div>
            </div>
          </div>
          {/* gingivitis */}
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5">{t('insurances:form:dental:gingivitis')}</p>

            <div className="flex flex-1 items-center">
              {getIconByLevel(insuranceDetails?.dental.gingivitis.level)}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.dental?.gingivitis?.value,
                  insuranceDetails?.dental?.gingivitis?.description
                )}
              </div>
            </div>
          </div>
          {/* xray_and_diagnostic_imaging */}
          <div className="mb-6 flex items-center justify-between last:mb-0">
            <p className="w-2/5">
              {t('insurances:form:dental:xray_and_diagnostic_imaging')}
            </p>

            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.dental.xray_and_diagnostic_imaging.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.dental?.xray_and_diagnostic_imaging?.value,
                  insuranceDetails?.dental?.xray_and_diagnostic_imaging
                    ?.description
                )}
              </div>
            </div>
          </div>
          {/* filling_teeth_basic */}
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5">
              {t('insurances:form:dental:filling_teeth_basic')}
            </p>

            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.dental?.filling_teeth_basic?.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.dental?.filling_teeth_basic?.value,
                  insuranceDetails?.dental?.filling_teeth_basic?.description
                )}
              </div>
            </div>
          </div>
          {/* root_canal_treatment */}
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5">
              {t('insurances:form:dental:root_canal_treatment')}
            </p>
            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.dental?.root_canal_treatment?.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.dental?.root_canal_treatment?.value,
                  insuranceDetails?.dental?.root_canal_treatment?.description
                )}
              </div>
            </div>
          </div>
          {/* dental_pathology */}
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5 pr-2">
              {t('insurances:form:dental:dental_pathology')}
            </p>

            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.dental?.dental_pathology?.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.dental?.dental_pathology?.value,
                  insuranceDetails?.dental?.dental_pathology?.description
                )}
              </div>
            </div>
          </div>
          {/* dental_calculus */}
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5">
              {t('insurances:form:dental:dental_calculus')}
            </p>
            <div className="flex flex-1 items-center">
              {getIconByLevel(insuranceDetails?.dental?.dental_calculus?.level)}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.dental?.dental_calculus?.value,
                  insuranceDetails?.dental?.dental_calculus?.description
                )}
              </div>
            </div>
          </div>
        </div>
      )}

      {/* obstetric */}
      {insuranceDetails?.obstetric && (
        <div className="mb-6 font-normal text-nickel">
          <p className="mb-4 text-xl font-medium text-arsenic">
            {t('insurances:form:health_additional_benefits:obstetric')}
          </p>
          {/* give_birth_normally */}
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5">
              {t('insurances:form:obstetric:give_birth_normally')}
            </p>

            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.obstetric.give_birth_normally?.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.obstetric?.give_birth_normally?.value,
                  insuranceDetails?.obstetric?.give_birth_normally?.description
                )}
              </div>
            </div>
          </div>
          {/* caesarean_section */}
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5">
              {t('insurances:form:obstetric:caesarean_section')}
            </p>

            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.obstetric.caesarean_section.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.obstetric?.caesarean_section?.value,
                  insuranceDetails?.obstetric?.caesarean_section?.description
                )}
              </div>
            </div>
          </div>
          {/* obstetric_complication */}
          <div className="mb-6 flex items-center justify-between last:mb-0">
            <p className="w-2/5">
              {t('insurances:form:obstetric:obstetric_complication')}
            </p>

            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.obstetric.obstetric_complication.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.obstetric?.obstetric_complication?.value,
                  insuranceDetails?.obstetric?.obstetric_complication
                    ?.description
                )}
              </div>
            </div>
          </div>
          {/* give_birth_abnormality */}
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5 pr-4">
              {t('insurances:form:obstetric:give_birth_abnormality')}
            </p>

            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.obstetric?.give_birth_abnormality?.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.obstetric?.give_birth_abnormality?.value,
                  insuranceDetails?.obstetric?.give_birth_abnormality
                    ?.description
                )}
              </div>
            </div>
          </div>
          {/* after_give_birth_fee */}
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5">
              {t('insurances:form:obstetric:after_give_birth_fee')}
            </p>
            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.obstetric?.after_give_birth_fee?.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.obstetric?.after_give_birth_fee?.value,
                  insuranceDetails?.obstetric?.after_give_birth_fee?.description
                )}
              </div>
            </div>
          </div>
          {/* before_discharged */}
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5 pr-4">
              {t('insurances:form:obstetric:before_discharged')}
            </p>

            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.obstetric?.before_discharged?.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.obstetric?.before_discharged?.value,
                  insuranceDetails?.obstetric?.before_discharged?.description
                )}
              </div>
            </div>
          </div>
          {/* postpartum_childcare_cost */}
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5">
              {t('insurances:form:obstetric:postpartum_childcare_cost')}
            </p>
            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.obstetric?.postpartum_childcare_cost?.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.obstetric?.postpartum_childcare_cost?.value,
                  insuranceDetails?.obstetric?.postpartum_childcare_cost
                    ?.description
                )}
              </div>
            </div>
          </div>
        </div>
      )}

      {!insuranceDetails?.dental && !insuranceDetails?.dental && (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
      )}
    </div>
  )
}

export default AdditionalBenefitTab
