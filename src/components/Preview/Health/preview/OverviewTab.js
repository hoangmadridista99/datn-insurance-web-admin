import {
  CUSTOMER_SEGMENT_I18N,
  INSURANCE_SCOPES_I18N,
  OPTIONS_TYPE,
  PAY_I18N_OPTIONS,
  PERSON_I18N_OPTIONS,
  PROFESSION_I18N_OPTIONS,
  STATUSES,
} from '@utils/constants/insurances'
import { formatCurrency } from '@helpers/formatCurrency'
import React from 'react'
import { useTranslation } from 'next-i18next'
import { getIconByLevel } from '@utils/helper'

const OverviewTab = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances'])

  const renderTextValue = (
    fromValue = null,
    toValue = null,
    determinedValue = null
  ) => {
    if (fromValue && toValue) {
      return <span>{`Từ ${fromValue} đến ${toValue}`}</span>
    }

    if (fromValue) {
      return <span>{`Từ ${fromValue}`}</span>
    }

    if (toValue) {
      return <span>{`Đến ${toValue}`}</span>
    }

    if (determinedValue) {
      return <span>{`Trong vòng ${determinedValue}`}</span>
    }
  }

  const renderUnitValue = (unit) => {
    return OPTIONS_TYPE.map((element) => {
      if (element.value === unit) {
        return <>{t(`insurances:fields:${unit}`)}</>
      }
    })
  }

  const renderField = (
    fromValue,
    toValue,
    determinedValue,
    type,
    description,
    isFormatType = true
  ) => {
    return (
      <>
        {(determinedValue || fromValue || toValue) && (
          <>
            {determinedValue && (
              <p>
                {renderTextValue(null, null, determinedValue)}
                <span>&nbsp;</span>
                {renderUnitValue(type)}
              </p>
            )}

            <p>
              {renderTextValue(fromValue, toValue)}
              <span>&nbsp;</span>
              {isFormatType ? renderUnitValue(type) : <>{type}</>}
            </p>
          </>
        )}

        {description && <p>{description}</p>}

        {!determinedValue && !fromValue && !toValue && !description && (
          <>{t('insurances:fields:no_support')}</>
        )}
      </>
    )
  }

  return (
    <div
      className={`no-scrollbar ${
        insuranceDetails?.status === STATUSES.PENDING ? 'h-[39vh]' : 'h-[47vh]'
      }  overflow-hidden overflow-y-scroll`}
    >
      <div className="mb-6 text-nickel">
        <p className="mb-4 text-xl font-medium text-arsenic">{`Thông tin về ${insuranceDetails?.name}`}</p>
        <p className="text-base font-normal">
          {insuranceDetails?.description_insurance}
        </p>
      </div>
      <div className="mb-6 font-normal">
        <p className="mb-4 text-xl font-medium text-arsenic">
          {t('insurances:form:details:title')}
        </p>

        <div className="mb-3 flex items-center text-nickel">
          <p className="w-2/5">{t('insurances:form:details:company_id')}</p>
          <p className="flex-1">{insuranceDetails?.company.long_name}</p>
        </div>

        <div className="mb-3 flex items-center text-nickel">
          <p className="w-2/5">{t('insurances:form:details:insurance_type')}</p>
          <p className="flex-1">
            {t(
              `insurances:form:options:${insuranceDetails?.insurance_category?.label}`
            )}
          </p>
        </div>

        <div className="mb-3 flex items-center text-nickel">
          <p className="w-2/5">{t('insurances:form:details:name')}</p>
          <p className="flex-1">{insuranceDetails?.name}</p>
        </div>
      </div>

      {/* terms */}
      <div className="mb-6 font-normal text-nickel">
        <p className="mb-4 text-xl font-medium text-arsenic">
          {t('insurances:form:terms:title')}
        </p>
        {/* age_eligibility */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">{t('insurances:form:terms:age_eligibility')}</p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms.age_eligibility?.level)}

            <div className="ml-4">
              {(insuranceDetails?.terms.age_eligibility.from ||
                insuranceDetails?.terms.age_eligibility.to) && (
                <p>
                  {`Từ ${
                    insuranceDetails?.terms.age_eligibility.from &&
                    insuranceDetails?.terms.age_eligibility.to
                      ? `${insuranceDetails?.terms.age_eligibility.from} đến ${insuranceDetails?.terms.age_eligibility.to}`
                      : insuranceDetails?.terms.deadline_for_deal.from ||
                        insuranceDetails?.terms.deadline_for_deal.to
                  } tuổi`}
                </p>
              )}

              {insuranceDetails?.terms.age_eligibility.description && (
                <p>{insuranceDetails?.terms.age_eligibility.description}</p>
              )}

              {!insuranceDetails?.terms.age_eligibility.from &&
                !insuranceDetails?.terms.age_eligibility.to &&
                !insuranceDetails?.terms.age_eligibility.description && (
                  <>{t('insurances:fields:no_support')}</>
                )}
            </div>
          </div>
        </div>
        {/* deadline_for_deal */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">
            {t('insurances:form:terms:deadline_for_deal')}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms.deadline_for_deal.level)}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.terms?.deadline_for_deal?.from,
                insuranceDetails?.terms?.deadline_for_deal?.to,
                insuranceDetails?.terms?.deadline_for_deal?.value,
                insuranceDetails?.terms?.deadline_for_deal?.type,
                insuranceDetails?.terms?.deadline_for_deal?.description
              )}
            </div>
          </div>
        </div>
        {/* insurance_minimum_fee */}
        <div className="mb-6 flex items-center justify-between last:mb-0">
          <p className="w-2/5">
            {t('insurances:form:terms:insurance_minimum_fee')}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.terms?.insurance_minimum_fee?.level
            )}

            <div className="ml-4">
              {insuranceDetails?.terms.insurance_minimum_fee.value && (
                <p>
                  {`${t('insurances:fields:amount_of_money')}: ${formatCurrency(
                    insuranceDetails?.terms?.insurance_minimum_fee?.value
                  )} VNĐ`}
                </p>
              )}

              {insuranceDetails?.terms?.insurance_minimum_fee?.description && (
                <p>
                  {insuranceDetails?.terms?.insurance_minimum_fee?.description}
                </p>
              )}

              {!insuranceDetails?.terms.insurance_minimum_fee.value &&
                !insuranceDetails?.terms?.insurance_minimum_fee
                  ?.description && <>{t('insurances:fields:no_support')}</>}
            </div>
          </div>
        </div>
        {/* insured_person */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">{t('insurances:form:terms:insured_person')}</p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms.insured_person.level)}

            <div className="ml-4">
              {insuranceDetails?.terms.insured_person.value && (
                <p>
                  {insuranceDetails?.terms.insured_person.value.map(
                    (element) => {
                      const find = PERSON_I18N_OPTIONS(t).find(
                        (item) => item.value === element
                      )

                      if (!find) return null

                      return (
                        <span className="commas" key={element}>
                          {find.label}
                        </span>
                      )
                    }
                  )}
                </p>
              )}

              {insuranceDetails?.terms?.insured_person?.description && (
                <p>{insuranceDetails?.terms?.insured_person?.description}</p>
              )}

              {!insuranceDetails?.terms?.insured_person?.value &&
                !insuranceDetails?.terms?.insured_person?.description && (
                  <>{t('insurances:fields:no_support')}</>
                )}
            </div>
          </div>
        </div>
        {/* profession */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">{t('insurances:form:terms:profession')}</p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms.profession.level)}

            <div className="ml-4">
              {insuranceDetails?.terms.profession.text && (
                <p>
                  {insuranceDetails?.terms.profession.text.map((element) => {
                    const find = PROFESSION_I18N_OPTIONS(t).find(
                      (item) => item.value === element
                    )

                    if (!find) return null

                    return (
                      <span className="commas" key={element}>
                        {find.label}
                      </span>
                    )
                  })}
                </p>
              )}

              {!insuranceDetails?.terms.profession.text && (
                <>{t('insurances:fields:no_support')}</>
              )}
            </div>
          </div>
        </div>
        {/* deadline_for_payment */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5 ">
            {t('insurances:form:terms:deadline_for_payment')}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms.deadline_for_payment.level)}

            <div className="ml-4">
              {insuranceDetails?.terms.deadline_for_payment.value && (
                <p>
                  {insuranceDetails?.terms.deadline_for_payment.value.map(
                    (element) => {
                      const find = PAY_I18N_OPTIONS(t).find(
                        (item) => item.value === element
                      )

                      if (!find) return null

                      return (
                        <span className="commas" key={element}>
                          {find.label}
                        </span>
                      )
                    }
                  )}
                </p>
              )}

              {insuranceDetails?.terms?.deadline_for_payment?.description && (
                <p className="text-nickel">
                  {insuranceDetails?.terms?.deadline_for_payment?.description}
                </p>
              )}

              {!insuranceDetails?.terms?.deadline_for_payment?.value &&
                !insuranceDetails?.terms?.deadline_for_payment?.description && (
                  <>{t('insurances:fields:no_support')}</>
                )}
            </div>
          </div>
        </div>
        {/* age_of_contract_termination */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5 ">
            {t('insurances:form:terms:age_of_contract_termination')}
          </p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.terms.age_of_contract_termination.level
            )}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.terms.age_of_contract_termination.from,
                insuranceDetails?.terms.age_of_contract_termination.to,
                null,
                insuranceDetails?.terms.age_of_contract_termination.type,
                insuranceDetails?.terms.age_of_contract_termination.description
              )}
            </div>
          </div>
        </div>
        {/* customer_segment */}
        {insuranceDetails?.terms?.customer_segment && (
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5">
              {t('insurances:form:terms:customer_segment')}
            </p>
            <div className="flex flex-1 items-center">
              {getIconByLevel(insuranceDetails?.terms.customer_segment?.level)}

              <div className="ml-4">
                {insuranceDetails?.terms.customer_segment.text && (
                  <p>
                    {insuranceDetails?.terms.customer_segment.text.map(
                      (element) => {
                        const find = CUSTOMER_SEGMENT_I18N(t).find(
                          (item) => item.value === element
                        )

                        if (!find) return null

                        return (
                          <span className="commas" key={element}>
                            {find.label}
                          </span>
                        )
                      }
                    )}
                  </p>
                )}

                {!insuranceDetails?.terms?.customer_segment?.text && (
                  <>{t('insurances:fields:no_support')}</>
                )}
              </div>
            </div>
          </div>
        )}
        {/* total_sum_insured */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">
            {t('insurances:form:terms:total_sum_insured')}
          </p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms.total_sum_insured.level)}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.terms?.total_sum_insured?.from
                  ? formatCurrency(
                      insuranceDetails?.terms?.total_sum_insured?.from
                    )
                  : insuranceDetails?.terms?.total_sum_insured?.from,
                insuranceDetails?.terms?.total_sum_insured?.to
                  ? formatCurrency(
                      insuranceDetails?.terms?.total_sum_insured?.to
                    )
                  : insuranceDetails?.terms?.total_sum_insured?.to,
                null,
                'VNĐ',
                insuranceDetails?.terms.total_sum_insured.description,
                false
              )}
            </div>
          </div>
        </div>
        {/* monthly_fee */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">{t('insurances:form:terms:monthly_fee')}</p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms.monthly_fee.level)}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.terms.monthly_fee.from
                  ? formatCurrency(insuranceDetails?.terms.monthly_fee.from)
                  : insuranceDetails?.terms.monthly_fee.from,
                insuranceDetails?.terms.monthly_fee.to
                  ? formatCurrency(insuranceDetails?.terms.monthly_fee.to)
                  : insuranceDetails?.terms.monthly_fee.to,
                null,
                'VNĐ',
                insuranceDetails?.terms.monthly_fee.description,
                false
              )}
            </div>
          </div>
        </div>
      </div>
      {/* health_customer_orientation */}
      <div className="mb-6 font-normal text-nickel">
        <p className="mb-4 text-xl font-medium text-arsenic">
          {t('insurances:form:health_customer_orientation:title')}
        </p>
        {/* insurance_scope */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">
            {t('insurances:form:health_customer_orientation:insurance_scope')}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.customer_orientation.insurance_scope?.level
            )}

            <div className="ml-4">
              {insuranceDetails?.customer_orientation.insurance_scope
                ?.values && (
                <p>
                  {insuranceDetails?.customer_orientation.insurance_scope.values.map(
                    (element) => {
                      const find = INSURANCE_SCOPES_I18N(t).find(
                        (item) => item.value === element
                      )

                      if (!find) return null

                      return (
                        <span className="commas" key={element}>
                          {find.label}
                        </span>
                      )
                    }
                  )}
                </p>
              )}

              {!insuranceDetails?.customer_orientation.insurance_scope
                ?.values && <>{t('insurances:fields:no_support')}</>}
            </div>
          </div>
        </div>
        {/* waiting_period */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">
            {t('insurances:form:health_customer_orientation:waiting_period')}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.customer_orientation.waiting_period.level
            )}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.customer_orientation?.waiting_period?.from,
                insuranceDetails?.customer_orientation?.waiting_period?.to,
                null,
                'ngày',
                insuranceDetails?.customer_orientation?.waiting_period
                  ?.description,
                false
              )}
            </div>
          </div>
        </div>
        {/* compensation_process */}
        <div className="mb-6 flex items-center justify-between last:mb-0">
          <p className="w-2/5">
            {t(
              'insurances:form:health_customer_orientation:compensation_process'
            )}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.customer_orientation.compensation_process.level
            )}

            <div className="ml-4">
              {insuranceDetails?.customer_orientation.compensation_process
                .description && (
                <p>
                  {
                    insuranceDetails?.customer_orientation.compensation_process
                      .description
                  }
                </p>
              )}

              {!insuranceDetails?.customer_orientation.compensation_process
                .description && <>{t('insurances:fields:no_support')}</>}
            </div>
          </div>
        </div>
        {/* reception_and_processing_time */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">
            {t(
              'insurances:form:health_customer_orientation:reception_and_processing_time'
            )}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.customer_orientation
                .reception_and_processing_time.level
            )}

            <div className="ml-4">
              {insuranceDetails?.customer_orientation
                ?.reception_and_processing_time?.text && (
                <p>
                  {
                    insuranceDetails?.customer_orientation
                      ?.reception_and_processing_time?.text
                  }
                </p>
              )}

              {!insuranceDetails?.customer_orientation
                ?.reception_and_processing_time?.text && (
                <>{t('insurances:fields:no_support')}</>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default OverviewTab
