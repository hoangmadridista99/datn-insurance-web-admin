import { formatCurrency } from '@helpers/formatCurrency'
import React from 'react'
import { useTranslation } from 'next-i18next'
import { getIconByLevel } from '@utils/helper'
import { STATUSES } from '@utils/constants/insurances'

const OutpatientTab = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances'])

  const renderField = (determinedValue, description) => {
    return (
      <>
        {determinedValue && (
          <p>
            {formatCurrency(determinedValue)}
            <span>&nbsp;</span>
            VNĐ
          </p>
        )}

        {description && <p>{description}</p>}

        {!determinedValue && !description && (
          <>{t('insurances:fields:no_support')}</>
        )}
      </>
    )
  }

  return (
    <div
      className={`no-scrollbar ${
        insuranceDetails?.status === STATUSES.PENDING ? 'h-[39vh]' : 'h-[47vh]'
      }  overflow-hidden overflow-y-scroll`}
    >
      <div className="mb-6 font-normal text-nickel">
        {/* examination_and_treatment */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">
            {t('insurances:form:outpatient:examination_and_treatment')}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.outpatient.examination_and_treatment?.level
            )}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.outpatient?.examination_and_treatment?.value,
                insuranceDetails?.outpatient?.examination_and_treatment
                  ?.description
              )}
            </div>
          </div>
        </div>
        {/* testing_and_diagnosis */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">
            {t('insurances:form:outpatient:testing_and_diagnosis')}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.outpatient.testing_and_diagnosis.level
            )}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.outpatient?.testing_and_diagnosis?.value,
                insuranceDetails?.outpatient?.testing_and_diagnosis?.description
              )}
            </div>
          </div>
        </div>
        {/* home_care */}
        <div className="mb-6 flex items-center justify-between last:mb-0">
          <p className="w-2/5">{t('insurances:form:outpatient:home_care')}</p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.outpatient.home_care.level)}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.outpatient?.home_care?.value,
                insuranceDetails?.outpatient?.home_care?.description
              )}
            </div>
          </div>
        </div>
        {/* due_to_accident */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">
            {t('insurances:form:outpatient:due_to_accident')}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.outpatient.due_to_accident.level)}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.outpatient?.due_to_accident?.value,
                insuranceDetails?.outpatient?.due_to_accident?.description
              )}
            </div>
          </div>
        </div>
        {/* due_to_illness */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">
            {t('insurances:form:outpatient:due_to_illness')}
          </p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.outpatient.due_to_illness.level)}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.outpatient?.due_to_illness?.value,
                insuranceDetails?.outpatient?.due_to_illness?.description
              )}
            </div>
          </div>
        </div>
        {/* due_to_cancer */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5 ">
            {t('insurances:form:outpatient:due_to_cancer')}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.outpatient.due_to_cancer.level)}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.outpatient?.due_to_cancer?.value,
                insuranceDetails?.outpatient?.due_to_cancer?.description
              )}
            </div>
          </div>
        </div>
        {/* restore_functionality */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5 ">
            {t('insurances:form:outpatient:restore_functionality')}
          </p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.outpatient?.restore_functionality?.level
            )}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.outpatient?.restore_functionality?.value,
                insuranceDetails?.outpatient?.restore_functionality?.description
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default OutpatientTab
