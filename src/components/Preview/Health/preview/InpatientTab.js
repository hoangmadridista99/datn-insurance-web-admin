import {
  HOSPITALIZATION_LIST,
  ROOM_TYPES,
  STATUSES,
} from '@utils/constants/insurances'
import { formatCurrency } from '@helpers/formatCurrency'
import React from 'react'
import { useTranslation } from 'next-i18next'
import { getIconByLevel } from '@utils/helper'

const InpatientTab = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances'])

  const renderUnitValue = (data, unit) => {
    return data.map((element) => {
      if (element.value === unit) {
        return <>VNĐ/&nbsp;{t(`insurances:fields:${element.label}`)}</>
      }
    })
  }

  const renderField = (
    determinedValue,
    description,
    type = null,
    data = []
  ) => {
    return (
      <>
        {determinedValue && (
          <p>
            {formatCurrency(determinedValue)}
            <span>&nbsp;</span>
            {type && data.length > 0 ? renderUnitValue(data, type) : 'VNĐ'}
          </p>
        )}

        {description && <p>{description}</p>}

        {!determinedValue && !description && (
          <>{t('insurances:fields:no_support')}</>
        )}
      </>
    )
  }

  return (
    <div
      className={`no-scrollbar ${
        insuranceDetails?.status === STATUSES.PENDING ? 'h-[39vh]' : 'h-[47vh]'
      }  overflow-hidden overflow-y-scroll`}
    >
      <div className="mb-6 font-normal text-nickel">
        {/* room_type */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">{t('insurances:form:inpatient:room_type')}</p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.inpatient.room_type?.level)}

            <div className="ml-4">
              {insuranceDetails?.inpatient.room_type.values && (
                <p>
                  {insuranceDetails?.inpatient.room_type.values.map(
                    (element) => {
                      const find = ROOM_TYPES.find(
                        (item) => item.value === element
                      )

                      if (!find) return null

                      return (
                        <span className="commas" key={element}>
                          {t(
                            `insurances:form:options:${find.label}`
                          ).toLowerCase()}
                        </span>
                      )
                    }
                  )}
                </p>
              )}

              {insuranceDetails?.inpatient.room_type.description && (
                <p>{insuranceDetails?.inpatient.room_type.description}</p>
              )}

              {!insuranceDetails?.inpatient.room_type.values &&
                !insuranceDetails?.inpatient.room_type.description && (
                  <>{t('insurances:fields:no_support')}</>
                )}
            </div>
          </div>
        </div>
        {/* for_cancer */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">{t('insurances:form:inpatient:for_cancer')}</p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.inpatient.for_cancer.level)}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.inpatient?.for_cancer?.value,
                insuranceDetails?.inpatient?.for_cancer?.description
              )}
            </div>
          </div>
        </div>
        {/* for_illnesses */}
        <div className="mb-6 flex items-center justify-between last:mb-0">
          <p className="w-2/5">
            {t('insurances:form:inpatient:for_illnesses')}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.inpatient.for_illnesses.level)}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.inpatient?.for_illnesses?.value,
                insuranceDetails?.inpatient?.for_illnesses?.description
              )}
            </div>
          </div>
        </div>
        {/* for_accidents */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">
            {t('insurances:form:inpatient:for_accidents')}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.inpatient.for_accidents.level)}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.inpatient?.for_accidents?.value,
                insuranceDetails?.inpatient?.for_accidents?.description
              )}
            </div>
          </div>
        </div>
        {/* for_surgical */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">{t('insurances:form:inpatient:for_surgical')}</p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.inpatient.for_surgical.level)}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.inpatient?.for_surgical?.value,
                insuranceDetails?.inpatient?.for_surgical?.description
              )}
            </div>
          </div>
        </div>
        {/* for_hospitalization */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5 ">
            {t('insurances:form:inpatient:for_hospitalization')}
          </p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.inpatient.for_hospitalization.level
            )}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.inpatient?.for_hospitalization?.value,
                insuranceDetails?.inpatient?.for_hospitalization?.description,
                insuranceDetails?.inpatient?.for_hospitalization?.type,
                HOSPITALIZATION_LIST
              )}
            </div>
          </div>
        </div>
        {/* for_intensive_care */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5 ">
            {t('insurances:form:inpatient:for_intensive_care')}
          </p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.inpatient?.for_intensive_care?.level
            )}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.inpatient?.for_intensive_care?.value,
                insuranceDetails?.inpatient?.for_intensive_care?.description
              )}
            </div>
          </div>
        </div>
        {/* for_administrative */}
        {insuranceDetails?.inpatient?.for_administrative && (
          <div className="mb-6 flex items-center justify-between">
            <p className="w-2/5">
              {t('insurances:form:inpatient:for_administrative')}
            </p>
            <div className="flex flex-1 items-center">
              {getIconByLevel(
                insuranceDetails?.inpatient.for_administrative?.level
              )}

              <div className="ml-4">
                {renderField(
                  insuranceDetails?.inpatient?.for_administrative?.value,
                  insuranceDetails?.inpatient?.for_administrative?.description
                )}
              </div>
            </div>
          </div>
        )}
        {/* for_organ_transplant */}
        <div className="mb-6 flex items-center justify-between">
          <p className="w-2/5">
            {t('insurances:form:inpatient:for_organ_transplant')}
          </p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.inpatient.for_organ_transplant.level
            )}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.inpatient?.for_organ_transplant?.value,
                insuranceDetails?.inpatient?.for_organ_transplant?.description
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default InpatientTab
