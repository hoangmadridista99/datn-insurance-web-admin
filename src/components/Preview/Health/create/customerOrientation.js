import {
  HEALTH_INSURANCE,
  HEALTH_INSURANCE_FIELDS,
} from '@utils/constants/insurances'
import FormLayout from 'components/Forms/Insurance/FormLayout'
import FormCustomerOrientation from 'components/Forms/Insurance/Health/FormCustomerOrientation'
import React from 'react'
import { useTranslation } from 'next-i18next'

const CustomerOrientation = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances'])

  return (
    <FormLayout
      activeKey="health_customer_orientation"
      title={t('insurances:form:health_customer_orientation:title')}
      active={
        activeFields?.health_customer_orientation ??
        handleActiveField(
          HEALTH_INSURANCE_FIELDS.health_customer_orientation,
          'customer_orientation'
        )
      }
      insurancesType={HEALTH_INSURANCE}
    >
      <FormCustomerOrientation />
    </FormLayout>
  )
}

export default CustomerOrientation
