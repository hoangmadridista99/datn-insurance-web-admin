import { Form, Tabs } from 'antd'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import Overview from './overview'
import InpatientServices from './inpatientServices'
import OutpatientServices from './outpatientServices'
import AdditionalBenefits from './additionalBenefits'
import LinkedList from './hostpitalLinkedList'
import CustomerOrientation from './customerOrientation'
import { useTranslation } from 'next-i18next'
import {
  HEALTH_DEFAULT_TABS,
  HEALTH_FORM_INITIAL_VALUES,
  HEALTH_INSURANCE_FIELDS,
  NONE,
} from '@utils/constants/insurances'

const HealthInsuranceForm = ({
  form,
  companies,
  objectives,
  onFinishForm,
  benefitSelected,
  setBenefitSelected,
  hospitals,
  setHospitals,
  initialValues = null,
}) => {
  let debounceId = null
  const { t } = useTranslation(['insurances'])

  const [fields, setFields] = useState([])
  const [activeFields, setActiveFields] = useState(null)
  const [activeKey, setActiveKey] = useState(1)

  const handleGetActiveFieldsByInitialValues = useCallback((obj) => {
    let result = []
    const keys = Object.keys(obj)

    for (const key of keys) {
      const item = obj[key]

      if (item?.level && item?.level !== NONE) {
        result = [...result, key]
      }
    }

    return result
  }, [])

  useEffect(() => {
    if (initialValues) {
      const listKeys = Object.keys(HEALTH_DEFAULT_TABS).reduce(
        (result, key) => {
          if (!initialValues[key]) return result

          return {
            ...result,
            [key]: handleGetActiveFieldsByInitialValues(initialValues[key]),
          }
        },
        {}
      )

      setActiveFields({
        ...listKeys,
        details: HEALTH_INSURANCE_FIELDS.details,
        health_customer_orientation: listKeys?.customer_orientation,
      })
    }
  }, [initialValues, handleGetActiveFieldsByInitialValues])

  const handleActiveField = useCallback(
    (list, child) => {
      if (child) {
        return list.reduce((result, field) => {
          const isField = fields.find(
            (item) =>
              item.name.length >= 3 &&
              item.name[0] === child &&
              item.name[1] === field &&
              item.name[2] === 'level' &&
              item.value !== NONE
          )

          if (!isField) return result

          return [...result, field]
        }, [])
      }

      return list.reduce((result, field) => {
        const isField = fields.find(
          (item) =>
            item.name[0] === field && item.errors.length < 1 && item.value
        )
        if (!isField) return result
        return [...result, field]
      }, [])
    },
    [fields]
  )

  const handleFieldsChange = (_, allFields) => {
    clearTimeout(debounceId)

    debounceId = setTimeout(() => {
      setActiveFields(null)
      setFields(allFields)
    }, 500)
  }

  const tabList = [
    {
      key: 1,
      label: t('insurances:form:overview:title'),
      children: (
        <Overview
          companies={companies}
          activeFields={activeFields}
          handleActiveField={handleActiveField}
          objectives={objectives}
        />
      ),
    },
    {
      key: 2,
      label: t('insurances:form:inpatient:title'),
      children: (
        <InpatientServices
          activeFields={activeFields}
          handleActiveField={handleActiveField}
        />
      ),
    },
    {
      key: 3,
      label: t('insurances:form:outpatient:title'),
      children: (
        <OutpatientServices
          activeFields={activeFields}
          handleActiveField={handleActiveField}
        />
      ),
    },
    {
      key: 4,
      label: t('insurances:form:health_additional_benefits:title'),
      children: (
        <AdditionalBenefits
          activeFields={activeFields}
          handleActiveField={handleActiveField}
          benefitSelected={benefitSelected}
          setBenefitSelected={setBenefitSelected}
        />
      ),
    },
    {
      key: 5,
      label: t('insurances:form:linkedList:title'),
      children: (
        <LinkedList hospitals={hospitals} setHospitals={setHospitals} />
      ),
    },
    {
      key: 6,
      label: t('insurances:form:health_customer_orientation:title'),
      children: (
        <CustomerOrientation
          activeFields={activeFields}
          handleActiveField={handleActiveField}
        />
      ),
    },
  ]

  const position = useMemo(
    () => ({
      left: <p className="w-6"></p>,
    }),
    []
  )

  const handleChangeTab = (key) => setActiveKey(key)

  return (
    <Form
      form={form}
      scrollToFirstError
      onFieldsChange={handleFieldsChange}
      initialValues={{
        ...HEALTH_FORM_INITIAL_VALUES,
        ...initialValues,
        dental: initialValues?.dental
          ? initialValues?.dental
          : HEALTH_FORM_INITIAL_VALUES?.dental,
        obstetric: initialValues?.obstetric
          ? initialValues?.obstetric
          : HEALTH_FORM_INITIAL_VALUES?.obstetric,
      }}
      onFinish={onFinishForm}
      className="mb-10"
      onFinishFailed={({ values }) => {
        if (
          !values.name ||
          !values.company_id ||
          !values.description_insurance ||
          !values.objective_of_insurance
        ) {
          return setActiveKey(1)
        }

        if (!values.key_benefits) {
          return setActiveKey(2)
        }
      }}
    >
      <Tabs
        type="card"
        defaultActiveKey={1}
        items={tabList}
        activeKey={activeKey}
        className="insurance-form text-spanish-gray"
        tabBarExtraContent={position}
        onChange={handleChangeTab}
      />
    </Form>
  )
}

export default HealthInsuranceForm
