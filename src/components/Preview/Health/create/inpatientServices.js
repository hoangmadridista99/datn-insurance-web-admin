import {
  HEALTH_INSURANCE,
  HEALTH_INSURANCE_FIELDS,
} from '@utils/constants/insurances'
import FormLayout from 'components/Forms/Insurance/FormLayout'
import FormInpatientServices from 'components/Forms/Insurance/Health/FormInpatientServices'
import React from 'react'
import { useTranslation } from 'next-i18next'

const InpatientServices = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances'])

  return (
    <FormLayout
      activeKey="inpatient"
      title={t('insurances:form:inpatient:title')}
      active={
        activeFields?.inpatient ??
        handleActiveField(HEALTH_INSURANCE_FIELDS.inpatient, 'inpatient')
      }
      insurancesType={HEALTH_INSURANCE}
    >
      <FormInpatientServices />
    </FormLayout>
  )
}

export default InpatientServices
