import {
  HEALTH_INSURANCE,
  HEALTH_INSURANCE_FIELDS,
} from '@utils/constants/insurances'
import FormLayout from 'components/Forms/Insurance/FormLayout'
import FormOutpatientServices from 'components/Forms/Insurance/Health/FormOutpatientServices'
import React from 'react'
import { useTranslation } from 'next-i18next'

const OutpatientServices = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances'])

  return (
    <FormLayout
      activeKey="outpatient"
      title={t('insurances:form:outpatient:title')}
      active={
        activeFields?.outpatient ??
        handleActiveField(HEALTH_INSURANCE_FIELDS.outpatient, 'outpatient')
      }
      insurancesType={HEALTH_INSURANCE}
    >
      <FormOutpatientServices />
    </FormLayout>
  )
}

export default OutpatientServices
