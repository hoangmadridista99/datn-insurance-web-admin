import {
  HEALTH_INSURANCE,
  HEALTH_INSURANCE_FIELDS,
} from '@utils/constants/insurances'
import FormDetails from 'components/Forms/Insurance/FormDetails'
import FormLayout from 'components/Forms/Insurance/FormLayout'
import FormTerms from 'components/Forms/Insurance/FormTerms'
import React from 'react'
import { useTranslation } from 'next-i18next'

const Overview = ({
  activeFields,
  handleActiveField,
  companies,
  objectives,
}) => {
  const { t } = useTranslation(['insurances'])

  return (
    <>
      <FormLayout
        activeKey="details"
        title={t('insurances:form:details:title')}
        active={
          activeFields?.details ??
          handleActiveField(HEALTH_INSURANCE_FIELDS.details)
        }
        insurancesType={HEALTH_INSURANCE}
      >
        <FormDetails
          companies={companies}
          objectives={[...objectives].map((item) => ({
            value: item.id,
            label: item.label,
          }))}
          insurancesType={HEALTH_INSURANCE}
        />
      </FormLayout>

      <FormLayout
        activeKey="terms"
        title={t('insurances:form:terms:title')}
        active={
          activeFields?.terms ??
          handleActiveField(HEALTH_INSURANCE_FIELDS.terms, 'terms')
        }
        insurancesType={HEALTH_INSURANCE}
      >
        <FormTerms insurancesType={HEALTH_INSURANCE} />
      </FormLayout>
    </>
  )
}

export default Overview
