import { SearchOutlined } from '@ant-design/icons'
import { NICKEL } from '@utils/constants/color'
import { getHospitalLinkedList } from '@services/insurances'
import { CloseIconSvg } from '@utils/icons'
import { Input, Modal, Table } from 'antd'
import React, { useCallback, useEffect, useState } from 'react'
import { useTranslation } from 'next-i18next'

const HospitalSavedModal = ({ isOpen, handleSubmit, onCancel }) => {
  const { t } = useTranslation(['insurances'])

  const [defaultHospitals, setDefaultHospitals] = useState(null)
  const [selectedRecords, setSelectedRecords] = useState(null)

  const getHospitals = useCallback(async () => {
    try {
      const response = await getHospitalLinkedList()

      if (response.length > 0) {
        setDefaultHospitals(response)
      }
    } catch (error) {
      console.log('Error', error)
    }
  }, [])

  useEffect(() => {
    getHospitals()
  }, [getHospitals])

  const columns = [
    {
      title: t('insurances:form:linkedList:hospital_name'),
      dataIndex: 'name',
    },
    {
      title: t('insurances:form:linkedList:hospital_address'),
      dataIndex: 'address',
    },
    {
      title: t('insurances:form:linkedList:hospital_province'),
      dataIndex: 'province',
    },
    {
      title: t('insurances:form:linkedList:hospital_district'),
      dataIndex: 'district',
    },
    // {
    //   title: 'Action',
    //   render: (_, record) => {
    //     // console.log('🚀 ~ record:', record)

    //     return (
    //       <div className="inline-flex gap-2">
    //         <Button
    //           type="ghost"
    //           icon={<EditIconSvg />}
    //           onClick={() => handleOpenModal('isEdit')}
    //         ></Button>
    //         <Button
    //           type="ghost"
    //           icon={<DeleteIconSvg />}
    //           onClick={() => handleOpenModal('isDelete')}
    //         ></Button>
    //       </div>
    //     )
    //   },
    // },
  ]

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setSelectedRecords(selectedRows)
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === 'Disabled User',
      // Column configuration not to be checked
      name: record.name,
    }),
  }

  return (
    <Modal
      centered
      okText={t('insurances:form:linkedList:modal:add_selected')}
      cancelText={t('insurances:form:linkedList:modal:cancel_text')}
      open={isOpen}
      onCancel={onCancel}
      onOk={() => {
        handleSubmit(selectedRecords)
      }}
      width={1000}
      closeIcon={<CloseIconSvg />}
      okButtonProps={{
        style: {},
        disabled: selectedRecords < 1,
      }}
    >
      <div className="py-4 px-3">
        <div className="mb-5 inline-flex w-full items-center justify-between border-0 border-b border-solid border-cultured py-5">
          <p className="text-2xl font-medium text-arsenic">
            {t('insurances:form:linkedList:modal:saved:title')}
          </p>

          <Input
            placeholder={t(
              'insurances:form:linkedList:modal:saved:filter_placeholder'
            )}
            size="large"
            prefix={<SearchOutlined style={{ fontSize: 24, color: NICKEL }} />}
            className="w-1/3"
          />
        </div>

        <Table
          rowKey={'id'}
          size="middle"
          bordered
          dataSource={defaultHospitals}
          columns={columns}
          rowSelection={{
            type: 'checkbox',
            ...rowSelection,
          }}
          scroll={{ y: 350 }}
          pagination={{
            pageSize: 5,
          }}
        />
      </div>
    </Modal>
  )
}

export default HospitalSavedModal
