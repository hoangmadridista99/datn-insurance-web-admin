import { DeleteIconSvg } from '@utils/icons'
import { Button, Table } from 'antd'
import React, { useState } from 'react'
import { useTranslation } from 'next-i18next'
import DeleteModal from './deleteModal'
import HospitalSavedModal from './hospitalSavedModal'
import CreateModal from './createModal'

const LinkedList = ({ hospitals, setHospitals }) => {
  const { t } = useTranslation(['insurances'])

  const [statuses, setStatuses] = useState({
    isDelete: false,
    isEdit: false,
    isFromSaved: false,
    isCreate: false,
  })
  const [selectedRecord, setSelectedRecord] = useState(null)

  // handle Delete modal
  const handleOpenModal = (status) =>
    setStatuses({ ...statuses, [status]: true })

  const handleCloseModal = (status) =>
    setStatuses({ ...statuses, [status]: false })

  const handleDeleteRecord = () => {
    const newHospitals = [...hospitals]

    hospitals.map((hospital, index) => {
      if (
        hospital.id === selectedRecord.id ||
        hospital.name === selectedRecord.name
      ) {
        newHospitals.splice(index, 1)
      }
    })

    setHospitals(newHospitals)
    handleCloseModal('isDelete')
  }

  const handleCreateLinkedHospital = (submitData) => {
    setHospitals([...hospitals, submitData])

    handleCloseModal('isCreate')
  }

  const handleChooseHospitalsFromSaved = (records) => {
    if (!records || records.length < 1) return

    if (hospitals && hospitals.length > 0) {
      hospitals.map((hospital) => {
        records.map((record, index) => {
          if (hospital.id === record.id) {
            records.splice(index, 1)
          }
        })
      })
    }

    setHospitals([...hospitals, ...records])
    handleCloseModal('isFromSaved')
  }

  const columns = [
    {
      title: t('insurances:form:linkedList:hospital_name'),
      dataIndex: 'name',
    },
    {
      title: t('insurances:form:linkedList:hospital_address'),
      dataIndex: 'address',
    },
    {
      title: t('insurances:form:linkedList:hospital_province'),
      dataIndex: 'province',
    },
    {
      title: t('insurances:form:linkedList:hospital_district'),
      dataIndex: 'district',
    },
    {
      title: 'Action',
      render: (_, record) => {
        return (
          <div className="inline-flex gap-2">
            <Button
              type="ghost"
              icon={<DeleteIconSvg />}
              onClick={() => {
                handleOpenModal('isDelete')
                setSelectedRecord(record)
              }}
            />
          </div>
        )
      },
    },
  ]

  return (
    <>
      {statuses.isDelete && (
        <DeleteModal
          isOpen={statuses.isDelete}
          handleSubmit={handleDeleteRecord}
          onCancel={() => handleCloseModal('isDelete')}
        />
      )}

      {statuses.isCreate && (
        <CreateModal
          isOpen={statuses.isCreate}
          handleSubmit={handleCreateLinkedHospital}
          onCancel={() => handleCloseModal('isCreate')}
        />
      )}

      {statuses.isFromSaved && (
        <HospitalSavedModal
          isOpen={statuses.isFromSaved}
          handleSubmit={handleChooseHospitalsFromSaved}
          onCancel={() => handleCloseModal('isFromSaved')}
        />
      )}

      <div className="h-full px-12 pt-10">
        <div className="mb-10 flex items-center justify-between">
          <p className="text-8 font-bold leading-12 text-arsenic">
            {t('insurances:form:linkedList:title')}
          </p>

          <div className="inline-flex gap-4">
            <Button
              onClick={() => handleOpenModal('isFromSaved')}
              className="h-full px-6 py-2 text-base font-bold text-nickel"
            >
              {t('insurances:form:linkedList:saved_list')}
            </Button>

            <Button
              onClick={() => handleOpenModal('isCreate')}
              className="h-full px-6 py-2 text-base font-bold text-nickel"
            >
              {t('insurances:form:linkedList:create')}
            </Button>
          </div>
        </div>

        <Table
          rowKey={'id'}
          size="middle"
          bordered
          dataSource={hospitals}
          columns={columns}
          scroll={{ y: 350 }}
          pagination={{
            pageSize: 10,
          }}
        />
      </div>
    </>
  )
}

export default LinkedList
