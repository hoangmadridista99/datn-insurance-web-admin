import { QuestionCircleOutlined } from '@ant-design/icons'
import { CloseIconSvg } from '@utils/icons'
import { Modal } from 'antd'
import React from 'react'
import { useTranslation } from 'next-i18next'

const DeleteModal = ({ isOpen, handleSubmit, onCancel }) => {
  const { t } = useTranslation(['insurances'])

  return (
    <Modal
      centered
      open={isOpen}
      onOk={handleSubmit}
      onCancel={onCancel}
      closeIcon={<CloseIconSvg />}
    >
      <div className="inline-flex items-center gap-4 px-10 pt-10 pb-8">
        <QuestionCircleOutlined className="text-5xl text-tart-orange" />

        <p className="text-base text-arsenic">
          {t('insurances:form:linkedList:modal:delete_content')}
        </p>
      </div>
    </Modal>
  )
}

export default DeleteModal
