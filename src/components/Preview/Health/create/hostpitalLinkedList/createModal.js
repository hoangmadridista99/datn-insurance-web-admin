import { CloseIconSvg } from '@utils/icons'
import { Form, Input, Modal } from 'antd'
import React from 'react'
import { useTranslation } from 'next-i18next'

const CreateModal = ({ isOpen, handleSubmit, onCancel }) => {
  const { t } = useTranslation(['insurances'])
  const [form] = Form.useForm()

  return (
    <Modal
      centered
      okText={t('insurances:form:linkedList:modal:create_text')}
      cancelText={t('insurances:form:linkedList:modal:cancel_text')}
      open={isOpen}
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then((values) => {
            form.resetFields()
            handleSubmit(values)
          })
          .catch((info) => {
            console.log('Validate Failed:', info)
          })
      }}
      width={800}
      closeIcon={<CloseIconSvg />}
    >
      <div className="p-3">
        <div className="mb-8 inline-flex w-full justify-center">
          <p className="text-2xl font-medium text-arsenic">
            {t('insurances:form:linkedList:modal:create:title')}
          </p>
        </div>

        <Form form={form} layout="vertical" name="create_hospital">
          <Form.Item
            name="name"
            label={t('insurances:form:linkedList:modal:create:name')}
            rules={[
              {
                required: true,
                message: 'Field can not empty!',
              },
            ]}
          >
            <Input
              size="large"
              placeholder={t('insurances:form:linkedList:modal:create:name')}
            />
          </Form.Item>

          <Form.Item
            name="address"
            label={t('insurances:form:linkedList:modal:create:address')}
            rules={[
              {
                required: true,
                message: 'Field can not empty!',
              },
            ]}
          >
            <Input
              size="large"
              placeholder={t('insurances:form:linkedList:modal:create:address')}
            />
          </Form.Item>

          <div className="inline-flex w-full items-center justify-between">
            <Form.Item
              name="province"
              label={t('insurances:form:linkedList:modal:create:province')}
              className="mr-6 w-1/2"
              rules={[
                {
                  required: true,
                  message: 'Field can not empty!',
                },
              ]}
            >
              <Input
                size="large"
                placeholder={t(
                  'insurances:form:linkedList:modal:create:province'
                )}
              />
            </Form.Item>

            <Form.Item
              label={t('insurances:form:linkedList:modal:create:district')}
              name="district"
              className="w-1/2"
              rules={[
                {
                  required: true,
                  message: 'Field can not empty!',
                },
              ]}
            >
              <Input
                size="large"
                placeholder={t(
                  'insurances:form:linkedList:modal:create:district'
                )}
              />
            </Form.Item>
          </div>
        </Form>
      </div>
    </Modal>
  )
}

export default CreateModal
