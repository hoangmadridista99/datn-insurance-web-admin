import {
  DENTAL,
  HEALTH_INSURANCE,
  HEALTH_INSURANCE_FIELDS,
  OBSTETRIC,
} from '@utils/constants/insurances'
import FormLayout from 'components/Forms/Insurance/FormLayout'
import FormAdditionalBenefits from 'components/Forms/Insurance/Health/FormAdditionalBenefits'
import React from 'react'
import { useTranslation } from 'next-i18next'

const AdditionalBenefits = ({
  activeFields,
  handleActiveField,
  benefitSelected,
  setBenefitSelected,
}) => {
  const { t } = useTranslation(['insurances'])

  const handleChangeCheckbox = (checkedValues) =>
    setBenefitSelected(checkedValues)

  const handleCheckExistElement = (value) =>
    benefitSelected.some((element) => element === value)

  return (
    <>
      <FormLayout
        activeKey="additional_benefits"
        title={t('insurances:form:health_additional_benefits:title')}
        active={
          activeFields?.additional_benefits ??
          handleActiveField(
            HEALTH_INSURANCE_FIELDS.additional_benefits,
            'additional_benefits'
          )
        }
        insurancesType={HEALTH_INSURANCE}
        hidden={true}
      >
        <FormAdditionalBenefits
          handleChangeCheckbox={handleChangeCheckbox}
          benefitSelected={benefitSelected}
        />
      </FormLayout>

      {handleCheckExistElement(DENTAL) && (
        <FormLayout
          activeKey="dental"
          title={t('insurances:form:health_additional_benefits:dental')}
          active={
            activeFields?.dental ??
            handleActiveField(HEALTH_INSURANCE_FIELDS.dental, 'dental')
          }
          insurancesType={HEALTH_INSURANCE}
        >
          <FormAdditionalBenefits section={DENTAL} />
        </FormLayout>
      )}

      {handleCheckExistElement(OBSTETRIC) && (
        <FormLayout
          activeKey="obstetric"
          title={t('insurances:form:health_additional_benefits:obstetric')}
          active={
            activeFields?.obstetric ??
            handleActiveField(HEALTH_INSURANCE_FIELDS.obstetric, 'obstetric')
          }
          insurancesType={HEALTH_INSURANCE}
        >
          <FormAdditionalBenefits section={OBSTETRIC} />
        </FormLayout>
      )}
    </>
  )
}

export default AdditionalBenefits
