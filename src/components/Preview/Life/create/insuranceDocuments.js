import {
  LIFE_INSURANCE,
  LIFE_INSURANCE_FIELDS,
} from '@utils/constants/insurances'
import FormLayout from 'components/Forms/Insurance/FormLayout'
import React from 'react'
import { useTranslation } from 'next-i18next'
import FormDocument from 'components/Forms/Insurance/FormDocument'

const InsuranceDocuments = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances'])

  return (
    <FormLayout
      activeKey="document"
      title={t('insurances:form:document:title')}
      active={
        activeFields?.document ??
        handleActiveField(LIFE_INSURANCE_FIELDS.document)
      }
      insurancesType={LIFE_INSURANCE}
    >
      <FormDocument />
    </FormLayout>
  )
}

export default InsuranceDocuments
