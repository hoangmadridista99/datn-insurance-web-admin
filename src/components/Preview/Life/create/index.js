import { Form, Tabs } from 'antd'
import React, { useCallback, useEffect, useMemo, useState } from 'react'
import Overview from './overview'
import CustomerOrientation from './customerOrientation'
import { useTranslation } from 'next-i18next'
import {
  LIFE_DEFAULT_TABS,
  LIFE_FORM_INITIAL_VALUES,
  LIFE_INSURANCE_FIELDS,
  NONE,
} from '@utils/constants/insurances'
import MainBenefits from './mainBenefits'
import AdditionalBenefits from './additionalBenefits'
import InsuranceDocuments from './insuranceDocuments'

const LifeInsuranceForm = ({
  form,
  companies,
  objectives,
  onFinishForm,
  benefitSelected,
  setBenefitSelected,
  initialValues = null,
}) => {
  let debounceId = null
  const { t } = useTranslation(['insurances'])

  const [fields, setFields] = useState([])
  const [activeFields, setActiveFields] = useState(null)
  const [activeKey, setActiveKey] = useState(1)

  const handleGetActiveFieldsByInitialValues = useCallback((obj) => {
    let result = []
    const keys = Object.keys(obj)

    for (const key of keys) {
      const item = obj[key]

      if (item?.level && item?.level !== NONE) {
        result = [...result, key]
      }
    }

    return result
  }, [])

  useEffect(() => {
    if (initialValues) {
      const listKeys = Object.keys(LIFE_DEFAULT_TABS).reduce((result, key) => {
        if (!initialValues[key]) return result

        return {
          ...result,
          [key]: handleGetActiveFieldsByInitialValues(initialValues[key]),
        }
      }, {})

      setActiveFields({
        ...listKeys,
        details: LIFE_INSURANCE_FIELDS.details,
        benefits: LIFE_INSURANCE_FIELDS.benefits,
        health_customer_orientation: listKeys?.customer_orientation,
      })
    }
  }, [initialValues, handleGetActiveFieldsByInitialValues])

  const handleActiveField = useCallback(
    (list, child) => {
      if (child) {
        return list.reduce((result, field) => {
          const isField = fields.find(
            (item) =>
              item.name.length >= 3 &&
              item.name[0] === child &&
              item.name[1] === field &&
              item.name[2] === 'level' &&
              item.value !== NONE
          )

          if (!isField) return result

          return [...result, field]
        }, [])
      }

      return list.reduce((result, field) => {
        const isField = fields.find(
          (item) =>
            item.name[0] === field && item.errors.length < 1 && item.value
        )
        if (!isField) return result
        return [...result, field]
      }, [])
    },
    [fields]
  )

  const handleFieldsChange = (_, allFields) => {
    clearTimeout(debounceId)

    debounceId = setTimeout(() => {
      setActiveFields(null)
      setFields(allFields)
    }, 500)
  }

  const tabList = [
    {
      key: 1,
      label: t('insurances:form:overview:title'),
      children: (
        <Overview
          companies={companies}
          activeFields={activeFields}
          handleActiveField={handleActiveField}
          objectives={objectives}
        />
      ),
      forceRender: true,
    },
    {
      key: 2,
      label: t('insurances:form:key_benefits:title'),
      children: (
        <MainBenefits
          activeFields={activeFields}
          handleActiveField={handleActiveField}
        />
      ),
      forceRender: true,
    },
    {
      key: 3,
      label: t('insurances:form:additional_benefits:title'),
      children: (
        <AdditionalBenefits
          activeFields={activeFields}
          handleActiveField={handleActiveField}
          benefitSelected={benefitSelected}
          setBenefitSelected={setBenefitSelected}
        />
      ),
    },
    {
      key: 4,
      label: t('insurances:form:customer_orientation:title'),
      children: (
        <CustomerOrientation
          activeFields={activeFields}
          handleActiveField={handleActiveField}
        />
      ),
    },
    {
      key: 5,
      label: t('insurances:form:document:title'),
      children: (
        <InsuranceDocuments
          activeFields={activeFields}
          handleActiveField={handleActiveField}
        />
      ),
    },
  ]

  const position = useMemo(
    () => ({
      left: <p className="w-6"></p>,
    }),
    []
  )

  const handleChangeTab = (key) => setActiveKey(key)

  return (
    <Form
      form={form}
      scrollToFirstError
      onFieldsChange={handleFieldsChange}
      initialValues={{
        ...LIFE_FORM_INITIAL_VALUES,
        ...initialValues,
      }}
      onFinish={onFinishForm}
      className="mb-10"
      onFinishFailed={({ values }) => {
        if (
          !values.name ||
          !values.company_id ||
          !values.description_insurance ||
          !values.objective_of_insurance ||
          values.objective_of_insurance.length < 1
        ) {
          return setActiveKey(1)
        }

        if (!values.key_benefits) {
          return setActiveKey(2)
        }
      }}
    >
      <Tabs
        type="card"
        defaultActiveKey={1}
        activeKey={activeKey}
        items={tabList}
        className="insurance-form text-spanish-gray"
        tabBarExtraContent={position}
        onChange={handleChangeTab}
      />
    </Form>
  )
}

export default LifeInsuranceForm
