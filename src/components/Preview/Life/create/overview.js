import {
  LIFE_INSURANCE_FIELDS,
  LIFE_INSURANCE,
} from '@utils/constants/insurances'
import FormDetails from 'components/Forms/Insurance/FormDetails'
import FormLayout from 'components/Forms/Insurance/FormLayout'
import FormTerms from 'components/Forms/Insurance/FormTerms'
import React from 'react'
import { useTranslation } from 'next-i18next'

const Overview = ({
  activeFields,
  handleActiveField,
  companies,
  objectives,
}) => {
  const { t } = useTranslation(['insurances'])

  return (
    <>
      <FormLayout
        activeKey="details"
        title={t('insurances:form:details:title')}
        active={
          activeFields?.details ??
          handleActiveField(LIFE_INSURANCE_FIELDS.details)
        }
        insurancesType={LIFE_INSURANCE}
      >
        <FormDetails
          companies={companies}
          objectives={objectives.map((item) => ({
            value: item.id,
            label: item.label,
          }))}
          insurancesType={LIFE_INSURANCE}
        />
      </FormLayout>

      <FormLayout
        activeKey="terms"
        title={t('insurances:form:terms:title')}
        active={
          activeFields?.terms ??
          handleActiveField(LIFE_INSURANCE_FIELDS.terms, 'terms')
        }
        insurancesType={LIFE_INSURANCE}
      >
        <FormTerms insurancesType={LIFE_INSURANCE} />
      </FormLayout>
    </>
  )
}

export default Overview
