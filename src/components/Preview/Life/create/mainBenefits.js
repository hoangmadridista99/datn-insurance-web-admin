import {
  LIFE_INSURANCE,
  LIFE_INSURANCE_FIELDS,
} from '@utils/constants/insurances'
import FormLayout from 'components/Forms/Insurance/FormLayout'
import React from 'react'
import { useTranslation } from 'next-i18next'
import FormMainBenefits from 'components/Forms/Insurance/Life/FormMainBenefits'

const MainBenefits = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances'])

  return (
    <FormLayout
      activeKey="benefits"
      title={t('insurances:form:key_benefits:title')}
      active={
        activeFields?.benefits ??
        handleActiveField(LIFE_INSURANCE_FIELDS.benefits)
      }
      insurancesType={LIFE_INSURANCE}
    >
      <FormMainBenefits />
    </FormLayout>
  )
}

export default MainBenefits
