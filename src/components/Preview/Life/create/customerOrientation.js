import {
  LIFE_INSURANCE,
  LIFE_INSURANCE_FIELDS,
} from '@utils/constants/insurances'
import FormLayout from 'components/Forms/Insurance/FormLayout'
import React from 'react'
import { useTranslation } from 'next-i18next'
import FormCustomerOrientation from 'components/Forms/Insurance/Life/FormCustomerOrientation'

const CustomerOrientation = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances'])

  return (
    <FormLayout
      activeKey="customer_orientation"
      title={t('insurances:form:customer_orientation:title')}
      active={
        activeFields?.customer_orientation ??
        handleActiveField(
          LIFE_INSURANCE_FIELDS.customer_orientation,
          'customer_orientation'
        )
      }
      insurancesType={LIFE_INSURANCE}
    >
      <FormCustomerOrientation />
    </FormLayout>
  )
}

export default CustomerOrientation
