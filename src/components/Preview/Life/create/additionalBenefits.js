import {
  LIFE_INSURANCE,
  LIFE_INSURANCE_FIELDS,
} from '@utils/constants/insurances'
import FormLayout from 'components/Forms/Insurance/FormLayout'
import React from 'react'
import { useTranslation } from 'next-i18next'
import FormAdditionalBenefits from 'components/Forms/Insurance/Life/FormAdditionalBenefits'

const AdditionalBenefits = ({ activeFields, handleActiveField }) => {
  const { t } = useTranslation(['insurances'])

  return (
    <FormLayout
      activeKey="additional_benefits"
      title={t('insurances:form:additional_benefits:title')}
      active={
        activeFields?.additional_benefits ??
        handleActiveField(
          LIFE_INSURANCE_FIELDS.additional_benefits,
          'additional_benefits'
        )
      }
      insurancesType={LIFE_INSURANCE}
    >
      <FormAdditionalBenefits />
    </FormLayout>
  )
}

export default AdditionalBenefits
