import React from 'react'
import { useTranslation } from 'next-i18next'
import { STATUSES } from '@utils/constants/insurances'

const MainTab = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances'])

  return (
    <div
      className={`no-scrollbar ${
        insuranceDetails?.status === STATUSES.PENDING ? 'h-[39vh]' : 'h-[47vh]'
      }  overflow-hidden overflow-y-scroll`}
    >
      <div className="mb-6">
        <p className="mb-4 text-xl font-medium text-arsenic">{`Thông tin về ${insuranceDetails?.name}`}</p>
        <p className="text-base text-nickel">
          {insuranceDetails?.description_insurance}
        </p>
      </div>

      <div className="mb-6 last:mb-0">
        <p className="mb-4 text-xl font-medium text-arsenic">
          {t('insurances:form:details:title')}
        </p>

        <div className="mb-3 flex items-center text-base font-normal text-nickel last:mb-0">
          <p className="w-2/5">{t('insurances:form:details:company_id')}</p>
          <p className="w-3/5">{insuranceDetails?.company.long_name}</p>
        </div>

        <div className="mb-3 flex items-center text-base font-normal text-nickel last:mb-0">
          <p className="w-2/5">{t('insurances:form:details:insurance_type')}</p>
          <p className="w-3/5">
            {t(
              `insurances:form:options:${insuranceDetails?.insurance_category?.label}`
            )}
          </p>
        </div>

        <div className="mb-3 flex items-center text-base font-normal text-nickel last:mb-0">
          <p className="w-2/5">{t('insurances:form:details:name')}</p>
          <p className="w-3/5 ">{insuranceDetails?.name}</p>
        </div>

        <div className="mb-3 flex items-center text-base font-normal text-nickel last:mb-0">
          <p className="w-2/5">
            {t('insurances:form:details:objective_of_insurance')}
          </p>
          <p className="w-3/5">
            {insuranceDetails?.objective_of_insurance?.map((item, index) => (
              <span className="commas" key={item.id}>
                {index < 1 ? item.label : item.label.toLowerCase()}
              </span>
            ))}
          </p>
        </div>
      </div>

      <div className="mb-6 last:mb-0">
        <h2 className="mb-4 text-xl font-medium text-arsenic">
          {t('insurances:form:key_benefits:title')}
        </h2>
        <p className="text-base text-nickel">
          {insuranceDetails?.key_benefits}
        </p>
      </div>
    </div>
  )
}

export default MainTab
