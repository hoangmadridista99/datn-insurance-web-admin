import { formatPrices, getIconByLevel } from '@utils/helper'
import Image from 'next/image'
import React from 'react'
import { useTranslation } from 'next-i18next'

const ModalHeader = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances'])

  return (
    <>
      <div className="mb-4 flex items-end justify-between">
        <div className="inline-flex w-1/2">
          <Image
            src={insuranceDetails?.company?.logo}
            width={64}
            height={64}
            alt={insuranceDetails?.company?.short_name}
            className="object-none"
          />

          <div className="ml-4 flex flex-col justify-between">
            <h1 className="text-2xl text-arsenic">{insuranceDetails?.name}</h1>

            <p className="text-base font-medium text-nickel">
              {insuranceDetails?.company?.long_name}
            </p>
          </div>
        </div>

        <div className="inline-flex whitespace-nowrap text-right">
          {(insuranceDetails?.terms?.monthly_fee?.from ||
            insuranceDetails?.terms?.monthly_fee?.to) &&
            formatPrices(
              insuranceDetails?.terms?.monthly_fee?.from,
              insuranceDetails?.terms?.monthly_fee?.to,
              t('insurances:fields:month')
            )}
        </div>
      </div>

      <div className="text-sm font-normal text-nickel">
        <p className="mb-3 flex items-center">
          {getIconByLevel(
            insuranceDetails?.customer_orientation?.acceptance_rate?.level
          )}
          <span className="ml-2 mr-1">
            {t('insurances:preview:acceptance_rate')}:
          </span>
          <span>
            {insuranceDetails?.customer_orientation?.acceptance_rate?.text}
            {!insuranceDetails?.customer_orientation?.acceptance_rate?.text && (
              <>{t('insurances:fields:no_support')}</>
            )}
          </span>
        </p>

        <p className="mb-3 flex items-center">
          {getIconByLevel(
            insuranceDetails?.customer_orientation
              ?.reception_and_processing_time?.level
          )}
          <span className="ml-2 mr-1">
            {t('insurances:preview:reception_and_processing_time')}:
          </span>
          <span>
            {
              insuranceDetails?.customer_orientation
                ?.reception_and_processing_time?.text
            }
            {!insuranceDetails?.customer_orientation
              ?.reception_and_processing_time?.text && (
              <>{t('insurances:fields:no_support')}</>
            )}
          </span>
        </p>

        <p className="mb-3 flex items-center">
          {getIconByLevel(
            insuranceDetails?.customer_orientation?.completion_time_deal?.level
          )}
          <span className="ml-2 mr-1">
            {t('insurances:preview:completion_time_deal')}:
          </span>
          <span>
            {insuranceDetails?.customer_orientation?.completion_time_deal?.text}
            {!insuranceDetails?.customer_orientation?.completion_time_deal
              ?.text && <>{t('insurances:fields:no_support')}</>}
          </span>
        </p>

        <p className="mb-3 flex items-center">
          {getIconByLevel(insuranceDetails?.terms?.deadline_for_payment?.level)}
          <span className="ml-2 mr-1">
            {t('insurances:form:terms:deadline_for_payment')}:
          </span>
          <span>
            {insuranceDetails?.terms?.deadline_for_payment?.text}
            {!insuranceDetails?.terms?.deadline_for_payment?.text && (
              <>{t('insurances:fields:no_support')}</>
            )}
          </span>
        </p>
      </div>
    </>
  )
}

export default ModalHeader
