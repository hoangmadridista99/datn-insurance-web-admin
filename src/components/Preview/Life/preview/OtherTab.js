import {
  LIFE_INSURANCE_FIELDS,
  OPTIONS_TYPE,
  PAY_I18N_OPTIONS,
  PERSON_I18N_OPTIONS,
  PROFESSION_I18N_OPTIONS,
  STATUSES,
} from '@utils/constants/insurances'
import { convertUrl } from '@helpers/convertUrl'
import { formatCurrency } from '@helpers/formatCurrency'
import { getIconByLevel } from '@utils/helper'
import { Image } from 'antd'
import Link from 'next/link'
import React from 'react'
import { useTranslation } from 'next-i18next'

const OtherTab = ({ insuranceDetails }) => {
  const { t } = useTranslation(['insurances'])

  const renderUnitValue = (unit) => {
    return OPTIONS_TYPE.map((element) => {
      if (element.value === unit) {
        return <>{t(`insurances:fields:${unit}`)}</>
      }
    })
  }

  const renderTextValue = (
    fromValue = null,
    toValue = null,
    determinedValue = null,
    type = null
  ) => {
    if (fromValue && toValue) {
      return (
        <span>
          {`Từ ${fromValue} đến ${toValue}`}&nbsp;{renderUnitValue(type)}
        </span>
      )
    }

    if (fromValue) {
      return (
        <span>
          {`Từ ${fromValue}`}&nbsp;{renderUnitValue(type)}
        </span>
      )
    }

    if (toValue) {
      return (
        <span>
          {`Đến ${toValue}`}&nbsp;{renderUnitValue(type)}
        </span>
      )
    }

    if (determinedValue) {
      return <span>&nbsp;{`hoặc ${determinedValue}`}</span>
    }
  }

  const renderField = (
    fromValue,
    toValue,
    determinedValue,
    type,
    description,
    isFormatType = true
  ) => {
    return (
      <>
        {(determinedValue || fromValue || toValue) && (
          <p>
            <span>
              {renderTextValue(fromValue, toValue)}
              {isFormatType ? renderUnitValue(type) : <>{type}</>}
            </span>

            {determinedValue && (
              <span>
                {renderTextValue(null, null, determinedValue)}
                <span>&nbsp;</span>
                {renderUnitValue(type)}
              </span>
            )}
          </p>
        )}

        {description && <p>{description}</p>}

        {!determinedValue && !fromValue && !toValue && !description && (
          <>{t('insurances:fields:no_support')}</>
        )}
      </>
    )
  }

  return (
    <div
      className={`no-scrollbar ${
        insuranceDetails?.status === STATUSES.PENDING ? 'h-[39vh]' : 'h-[47vh]'
      }  overflow-hidden overflow-y-scroll`}
    >
      {/* terms */}
      <div className="mb-6 font-normal text-nickel">
        <p className="mb-4 text-xl font-medium text-arsenic">
          {t('insurances:form:terms:title')}
        </p>
        {/* age_eligibility */}
        <div className="mb-6 flex items-center font-normal">
          <p className="w-2/5">{t('insurances:form:terms:age_eligibility')}</p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms?.age_eligibility?.level)}

            <div className="ml-4">
              {(insuranceDetails?.terms.age_eligibility.from ||
                insuranceDetails?.terms.age_eligibility.to) && (
                <p>
                  {renderTextValue(
                    insuranceDetails?.terms.age_eligibility.from,
                    insuranceDetails?.terms.age_eligibility.to,
                    null,
                    'old'
                  )}
                </p>
              )}

              {insuranceDetails?.terms.age_eligibility.description && (
                <p>{insuranceDetails?.terms.age_eligibility.description}</p>
              )}

              {!insuranceDetails?.terms.age_eligibility.from &&
                !insuranceDetails?.terms.age_eligibility.to &&
                !insuranceDetails?.terms.age_eligibility.description && (
                  <>{t('insurances:fields:no_support')}</>
                )}
            </div>
          </div>
        </div>
        {/* deadline_for_deal */}
        <div className="mb-6 flex items-center font-normal">
          <p className="w-2/5">
            {t('insurances:form:terms:deadline_for_deal')}
          </p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms.deadline_for_deal.level)}
            <div className="ml-4">
              {renderField(
                insuranceDetails?.terms?.deadline_for_deal?.from,
                insuranceDetails?.terms?.deadline_for_deal?.to,
                insuranceDetails?.terms?.deadline_for_deal?.value,
                insuranceDetails?.terms?.deadline_for_deal?.type,
                insuranceDetails?.terms?.deadline_for_deal?.description
              )}
            </div>
          </div>
        </div>
        {/* insurance_minimum_fee */}
        <div className="mb-6 flex items-center font-normal">
          <p className="w-2/5">
            {t('insurances:form:terms:insurance_minimum_fee')}
          </p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.terms?.insurance_minimum_fee?.level
            )}

            <div className="ml-4">
              {insuranceDetails?.terms?.insurance_minimum_fee?.value && (
                <p>
                  {`${formatCurrency(
                    insuranceDetails?.terms?.insurance_minimum_fee?.value
                  )} VNĐ`}
                </p>
              )}

              {insuranceDetails?.terms?.insurance_minimum_fee?.description && (
                <p>
                  {insuranceDetails?.terms?.insurance_minimum_fee?.description}
                </p>
              )}

              {!insuranceDetails?.terms?.insurance_minimum_fee?.value &&
                !insuranceDetails?.terms?.insurance_minimum_fee
                  ?.description && <>{t('insurances:fields:no_support')}</>}
            </div>
          </div>
        </div>
        {/* insured_person */}
        <div className="mb-6 flex items-center font-normal">
          <p className="w-2/5">{t('insurances:form:terms:insured_person')}</p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms?.insured_person?.level)}
            <div className="ml-4">
              {insuranceDetails?.terms?.insured_person?.value && (
                <p>
                  {insuranceDetails?.terms?.insured_person?.value.map(
                    (element, index) => {
                      const find = PERSON_I18N_OPTIONS(t).find(
                        (item) => item.value === element
                      )

                      if (!find) return null

                      return (
                        <span className="commas" key={element}>
                          {index < 1 ? find.label : find.label.toLowerCase()}
                        </span>
                      )
                    }
                  )}
                </p>
              )}

              {insuranceDetails?.terms?.insured_person?.description && (
                <p>{insuranceDetails?.terms?.insured_person?.description}</p>
              )}

              {!insuranceDetails?.terms?.insured_person?.value &&
                !insuranceDetails?.terms?.insured_person?.description && (
                  <>{t('insurances:fields:no_support')}</>
                )}
            </div>
          </div>
        </div>
        {/* profession */}
        <div className="mb-6 flex items-center font-normal">
          <p className="w-2/5">{t('insurances:form:terms:profession')}</p>

          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms?.profession?.level)}

            <div className="ml-4">
              {insuranceDetails?.terms?.profession?.text && (
                <p>
                  {insuranceDetails?.terms?.profession?.text.map(
                    (element, index) => {
                      const find = PROFESSION_I18N_OPTIONS(t).find(
                        (item) => item.value === element
                      )

                      if (!find) return null

                      return (
                        <span className="commas" key={element}>
                          {index < 1 ? find.label : find.label.toLowerCase()}
                        </span>
                      )
                    }
                  )}
                </p>
              )}

              {!insuranceDetails?.terms?.profession?.text && (
                <>{t('insurances:fields:no_support')}</>
              )}
            </div>
          </div>
        </div>
        {/* deadline_for_payment */}
        <div className="mb-6 flex items-center font-normal">
          <p className="w-2/5">
            {t('insurances:form:terms:deadline_for_payment')}
          </p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.terms?.deadline_for_payment?.level
            )}
            <div className="ml-4">
              {insuranceDetails?.terms.deadline_for_payment.value && (
                <p>
                  {insuranceDetails?.terms.deadline_for_payment.value.map(
                    (element, index) => {
                      const find = PAY_I18N_OPTIONS(t).find(
                        (item) => item.value === element
                      )

                      if (!find) return null

                      return (
                        <span className="commas" key={element}>
                          {index < 1 ? find.label : find.label.toLowerCase()}
                        </span>
                      )
                    }
                  )}
                </p>
              )}

              {insuranceDetails?.terms?.deadline_for_payment?.description && (
                <p className="text-nickel">
                  {insuranceDetails?.terms?.deadline_for_payment?.description}
                </p>
              )}

              {!insuranceDetails?.terms?.deadline_for_payment?.value &&
                !insuranceDetails?.terms?.deadline_for_payment?.description && (
                  <>{t('insurances:fields:no_support')}</>
                )}
            </div>
          </div>
        </div>
        {/* age_of_contract_termination */}
        <div className="mb-6 flex items-center font-normal">
          <p className="w-2/5">
            {t('insurances:form:terms:age_of_contract_termination')}
          </p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.terms.age_of_contract_termination.level
            )}
            <div className="ml-4">
              {renderField(
                insuranceDetails?.terms?.age_of_contract_termination?.from,
                insuranceDetails?.terms?.age_of_contract_termination?.to,
                insuranceDetails?.terms?.age_of_contract_termination?.value,
                insuranceDetails?.terms?.age_of_contract_termination?.type,
                insuranceDetails?.terms?.age_of_contract_termination
                  ?.description
              )}
            </div>
          </div>
        </div>
        {/* termination_conditions */}
        <div className="mb-6 flex items-center font-normal">
          <p className="w-2/5">
            {t('insurances:form:terms:termination_conditions')}
          </p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(
              insuranceDetails?.terms.termination_conditions?.level
            )}
            <div className="ml-4">
              {insuranceDetails?.terms?.termination_conditions?.text && (
                <p>{insuranceDetails?.terms?.termination_conditions?.text}</p>
              )}

              {!insuranceDetails?.terms?.termination_conditions?.text && (
                <>{t('insurances:fields:no_support')}</>
              )}
            </div>
          </div>
        </div>
        {/* total_sum_insured */}
        <div className="mb-6 flex items-center font-normal">
          <p className="w-2/5">
            {t('insurances:form:terms:total_sum_insured')}
          </p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms.total_sum_insured.level)}
            <div className="ml-4">
              {renderField(
                insuranceDetails?.terms?.total_sum_insured?.from
                  ? formatCurrency(
                      insuranceDetails?.terms?.total_sum_insured?.from
                    )
                  : insuranceDetails?.terms?.total_sum_insured?.from,
                insuranceDetails?.terms?.total_sum_insured?.to
                  ? formatCurrency(
                      insuranceDetails?.terms?.total_sum_insured?.to
                    )
                  : insuranceDetails?.terms?.total_sum_insured?.to,
                null,
                'VNĐ',
                insuranceDetails?.terms.total_sum_insured.description,
                false
              )}
            </div>
          </div>
        </div>
        {/* monthly_fee */}
        <div className="mb-6 flex items-center font-normal">
          <p className="w-2/5">{t('insurances:form:terms:monthly_fee')}</p>
          <div className="flex flex-1 items-center">
            {getIconByLevel(insuranceDetails?.terms.monthly_fee.level)}

            <div className="ml-4">
              {renderField(
                insuranceDetails?.terms.monthly_fee.from
                  ? formatCurrency(insuranceDetails?.terms.monthly_fee.from)
                  : insuranceDetails?.terms.monthly_fee.from,
                insuranceDetails?.terms.monthly_fee.to
                  ? formatCurrency(insuranceDetails?.terms.monthly_fee.to)
                  : insuranceDetails?.terms.monthly_fee.to,
                null,
                'VNĐ',
                insuranceDetails?.terms.monthly_fee.description,
                false
              )}
            </div>
          </div>
        </div>
      </div>
      {/* additional_benefits */}
      {insuranceDetails?.additional_benefits && (
        <div className="mb-6 last:mb-0">
          <p className="mb-4 text-xl font-medium text-arsenic">
            {t('insurances:form:additional_benefits:title')}
          </p>
          {LIFE_INSURANCE_FIELDS.additional_benefits
            .reduce((result, key) => {
              const item = insuranceDetails?.additional_benefits[key]

              return [...result, { ...item, key }]
            }, [])
            .map((item, index) => (
              <div
                key={`additional_benefits_${index}`}
                className="mb-6 flex items-center font-normal"
              >
                <p className="w-2/5">
                  {t(`insurances:create:additional_benefits:${item.key}`)}
                </p>

                <div className="flex flex-1 items-center">
                  {getIconByLevel(item.level)}
                  <div className="ml-4">
                    {item.text && <p>{item.text}</p>}
                    {!item.text && <>{t('insurances:fields:no_support')}</>}
                  </div>
                </div>
              </div>
            ))}
        </div>
      )}
      {/* customer_orientation */}
      <div className="mb-6 last:mb-0">
        <p className="mb-4 text-xl font-medium text-arsenic">
          {t('insurances:form:customer_orientation:title')}
        </p>

        {LIFE_INSURANCE_FIELDS.customer_orientation
          .reduce((result, key) => {
            const item = insuranceDetails?.customer_orientation[key]

            return [...result, { ...item, key }]
          }, [])
          .map((item, index) => (
            <div
              key={`customer_orientation-${index}`}
              className="mb-6 flex items-center font-normal"
            >
              <p className="w-2/5">
                {t(`insurances:create:customer_orientation:${item.key}`)}
              </p>
              <div className="flex flex-1 items-center">
                {getIconByLevel(item.level)}

                <div className="ml-4">
                  {item.text && <p>{item.text}</p>}
                  {!item.text && <>{t('insurances:fields:no_support')}</>}
                </div>
              </div>
            </div>
          ))}
      </div>
      {/* benefits_illustration_table */}
      <div className="mb-6 last:mb-0">
        <p className="mb-4 text-xl font-medium text-arsenic">
          {t('insurances:form:document:title')}
        </p>

        {insuranceDetails?.benefits_illustration_table && (
          <Link
            className="mb-4 flex items-center last:mb-0"
            href={
              convertUrl(insuranceDetails?.benefits_illustration_table).link
            }
          >
            <Image
              src="/svg/document.svg"
              width={24}
              height={24}
              alt="Document"
            />
            <span className="ml-3 text-nickel">
              {t('insurances:form:document:benefits_illustration_table')}
            </span>
          </Link>
        )}
        {insuranceDetails?.documentation_url && (
          <Link
            className="mb-4 flex items-center last:mb-0"
            href={convertUrl(insuranceDetails?.documentation_url).link}
          >
            <Image
              src="/svg/document.svg"
              width={24}
              height={24}
              alt="Document"
            />
            <span className="ml-3 text-nickel">
              {t('insurances:form:document:documentation_url')}
            </span>
          </Link>
        )}

        {!insuranceDetails?.benefits_illustration_table &&
          !insuranceDetails?.documentation_url && (
            <>{t('insurances:fields:no_support')}</>
          )}
      </div>
    </div>
  )
}

export default OtherTab
