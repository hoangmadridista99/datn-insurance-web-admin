/* eslint-disable indent */
import React, { useState, useEffect, useCallback } from 'react'
import { Modal, Card, Tabs, Button } from 'antd'
import { getInsuranceDetails } from '@services/insurances'
import MainTab from './MainTab'
import OtherTab from './OtherTab'
import { CloseIconSvg } from '@utils/icons'
import ModalHeader from './ModalHeader'
import { useTranslation } from 'next-i18next'
import { STATUSES } from '@utils/constants/insurances'

const LifeModal = ({
  isOpen,
  insuranceId,
  onClose,
  handleChangeStatus,
  handleOpenModalReject,
}) => {
  const { t } = useTranslation(['insurances'])

  const [insuranceDetails, setInsuranceDetails] = useState(null)

  const fetchInsuranceDetails = useCallback(async () => {
    try {
      const response = await getInsuranceDetails(insuranceId)

      setInsuranceDetails(response)
    } catch (error) {
      console.log('Error', error)
    }
  }, [insuranceId])

  const tabList = [
    {
      key: 'main',
      label: t('insurances:form:key_benefits:title'),
      children: <MainTab insuranceDetails={insuranceDetails} />,
    },
    {
      key: 'other',
      label: t('insurances:fields:other_information'),
      children: <OtherTab insuranceDetails={insuranceDetails} />,
    },
  ]

  useEffect(() => {
    fetchInsuranceDetails()
  }, [fetchInsuranceDetails])

  return (
    <Modal
      centered
      open={isOpen}
      width="70%"
      destroyOnClose
      onCancel={onClose}
      title={<ModalHeader insuranceDetails={insuranceDetails} />}
      closeIcon={<CloseIconSvg />}
      footer={
        insuranceDetails?.status === STATUSES.PENDING
          ? [
              <Button
                key="reject"
                type="default"
                onClick={() => {
                  handleOpenModalReject()
                  onClose()
                }}
              >
                {t('insurances:preview:reject')}
              </Button>,
              <Button
                key="approve"
                type="primary"
                onClick={() => {
                  handleChangeStatus(STATUSES.APPROVED)
                  onClose()
                }}
              >
                {t('insurances:preview:approve')}
              </Button>,
            ]
          : null
      }
    >
      <div
        className={
          insuranceDetails?.status === STATUSES.PENDING
            ? 'h-[50vh]'
            : 'h-[60vh]'
        }
      >
        <Card bordered={false} className="border-none shadow-none">
          <Tabs
            centered
            accessKey="main"
            items={tabList}
            className="text-base text-spanish-gray"
          />
        </Card>
      </div>
    </Modal>
  )
}

export default LifeModal
