import { Pagination } from 'antd'
import React from 'react'
import { useTranslation } from 'next-i18next'

const CustomizePagination = ({ pagination, onChangePage }) => {
  const { t } = useTranslation(['common'])

  return (
    <div className="flex flex-1 items-center justify-between text-base">
      <p className="text-spanish-gray">
        {`${(pagination.currentPage - 1) * pagination.itemsPerPage + 1} - ${
          pagination.currentPage * pagination.itemsPerPage >
          pagination.totalItems
            ? pagination.totalItems
            : pagination.currentPage * pagination.itemsPerPage
        } ${t('common:pagination.of')}`}
        <span className="font-bold text-arsenic">
          &nbsp;{pagination?.totalItems ?? 0}
        </span>
        {` ${t('common:pagination.search_results')}`}
      </p>

      <Pagination
        current={pagination.currentPage}
        total={pagination?.totalItems ?? 1}
        pageSize={pagination.itemsPerPage}
        showSizeChanger={false}
        hideOnSinglePage
        className="text-right text-base"
        onChange={onChangePage}
      />
    </div>
  )
}

export default CustomizePagination
