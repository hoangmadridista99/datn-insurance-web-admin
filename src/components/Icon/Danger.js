import React from 'react'

const DangerIconSvg = (props) => (
  <svg
    width={20}
    height={20}
    viewBox="0 0 20 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M10 18.333c4.584 0 8.334-3.75 8.334-8.333S14.584 1.667 10 1.667c-4.583 0-8.333 3.75-8.333 8.333s3.75 8.333 8.333 8.333zM10 6.667v4.166"
      stroke="#DE3618"
      strokeWidth={1.5}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M9.995 13.334h.008"
      stroke="#DE3618"
      strokeWidth={2}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export default DangerIconSvg
