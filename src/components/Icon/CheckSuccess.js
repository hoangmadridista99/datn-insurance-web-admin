import React from 'react'

const CheckSuccessIconSvg = (props) => (
  <svg
    width={98}
    height={98}
    viewBox="0 0 98 98"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g clipPath="url(#clip0_1733_9740)">
      <path
        d="M4 49C4 24.19 24.19 4 49 4s45 20.19 45 45-20.19 45-45 45S4 73.81 4 49z"
        fill="#008DFF"
        stroke="#E3F1FE"
        strokeWidth={8}
      />
      <path
        d="M66.102 41.222L46.187 61.103a3.058 3.058 0 01-2.165.897c-.785 0-1.569-.3-2.166-.897l-9.957-9.94a3.05 3.05 0 010-4.325 3.063 3.063 0 014.332 0l7.79 7.778L61.77 36.897a3.063 3.063 0 014.332 0 3.052 3.052 0 010 4.325z"
        fill="#fff"
      />
    </g>
    <defs>
      <clipPath id="clip0_1733_9740">
        <path fill="#fff" d="M0 0H98V98H0z" />
      </clipPath>
    </defs>
  </svg>
)

export default CheckSuccessIconSvg
