import React from 'react'

const CheckIconSvg = (props) => (
  <svg
    width={18}
    height={18}
    viewBox="0 0 18 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g clipPath="url(#clip0_5515_46807)">
      <path
        d="M9 17.303A8.303 8.303 0 109 .697a8.303 8.303 0 000 16.606z"
        fill="#50B83C"
      />
      <path
        d="M9 18c-4.962 0-9-4.037-9-9 0-4.962 4.038-9 9-9 4.963 0 9 4.038 9 9 0 4.963-4.037 9-9 9zM9 1.394C4.806 1.394 1.394 4.806 1.394 9S4.806 16.606 9 16.606 16.606 13.193 16.606 9c0-4.194-3.412-7.606-7.606-7.606z"
        fill="#50B83C"
      />
      <path
        d="M13.13 5.87a.698.698 0 00-.984.048L7.893 10.6 5.84 8.512a.696.696 0 10-.994.977l2.572 2.614a.696.696 0 001.012-.02l4.749-5.228a.698.698 0 00-.048-.985z"
        fill="#fff"
      />
    </g>
    <defs>
      <clipPath id="clip0_5515_46807">
        <path fill="#fff" d="M0 0H18V18H0z" />
      </clipPath>
    </defs>
  </svg>
)

export default CheckIconSvg
