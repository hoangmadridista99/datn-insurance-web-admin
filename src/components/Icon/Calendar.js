import React from 'react'

const CalendarIconSvg = (props) => (
  <svg
    width={25}
    height={24}
    viewBox="0 0 25 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M8.6 2v3M16.6 2v3M4.1 9.09h17M21.6 8.5V17c0 3-1.5 5-5 5h-8c-3.5 0-5-2-5-5V8.5c0-3 1.5-5 5-5h8c3.5 0 5 2 5 5z"
      stroke="#6C6F7B"
      strokeWidth={1.5}
      strokeMiterlimit={10}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M16.295 13.7h.009M16.295 16.7h.009M12.596 13.7h.009M12.596 16.7h.009M8.894 13.7h.01M8.894 16.7h.01"
      stroke="#6C6F7B"
      strokeWidth={2}
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export default CalendarIconSvg
