import React from 'react'

export default function Heart() {
  return (
    <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M1.99261 10.3537C1.04656 7.38563 2.16462 3.72602 5.23212 2.71746C6.86621 2.19877 8.87299 2.63101 9.99105 4.21589C11.0518 2.57338 13.1446 2.19877 14.75 2.71746C17.8461 3.72602 18.9355 7.38563 18.0182 10.3537C16.5561 15.0507 11.4245 17.5 9.99105 17.5C8.58631 17.4712 3.51203 15.0795 1.99261 10.3537Z" stroke="#8F929D" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
    </svg>

  )
}
