import React from 'react'

const CancelIconSvg = (props) => (
  <svg
    width={18}
    height={18}
    viewBox="0 0 18 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g clipPath="url(#clip0_5515_46844)">
      <path
        d="M9 17.307A8.307 8.307 0 109 .693a8.307 8.307 0 000 16.614z"
        fill="#DE3618"
      />
      <path
        d="M9 18a8.978 8.978 0 01-6.367-2.633c-3.51-3.51-3.51-9.223 0-12.734 3.51-3.51 9.224-3.51 12.734 0 3.51 3.511 3.51 9.223 0 12.734A8.98 8.98 0 019 18zM3.619 3.62c-2.966 2.966-2.966 7.794 0 10.761 2.968 2.966 7.796 2.967 10.762 0s2.966-7.795 0-10.762C11.413.653 6.586.653 3.619 3.62z"
        fill="#DE3618"
      />
      <path
        d="M6.579 12.118a.697.697 0 01-.493-1.19l4.93-4.932a.697.697 0 11.987.986l-4.931 4.932a.697.697 0 01-.494.204z"
        fill="#fff"
      />
      <path
        d="M11.51 12.118a.696.696 0 01-.493-.204L6.085 6.982a.697.697 0 11.987-.986l4.93 4.931a.697.697 0 01-.492 1.19z"
        fill="#fff"
      />
    </g>
    <defs>
      <clipPath id="clip0_5515_46844">
        <path fill="#fff" d="M0 0H18V18H0z" />
      </clipPath>
    </defs>
  </svg>
)

export default CancelIconSvg
