import React from 'react'

const TagSvgIcon = (props) => (
  <svg
    width={28}
    height={28}
    viewBox="0 0 28 28"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g clipPath="url(#clip0_5515_47337)" fill="#3E4857">
      <path d="M21.84 1.49h-8.597c-.544 0-1.066.217-1.451.602L.602 13.28a2.054 2.054 0 000 2.904l8.596 8.597a2.053 2.053 0 002.904 0l11.19-11.188c.385-.386.601-.909.601-1.454V3.543A2.053 2.053 0 0021.84 1.49zm-3.593 7.187a1.54 1.54 0 11.001-3.081 1.54 1.54 0 01-.001 3.08z" />
      <path d="M25.946 3.543V13.1c0 .474-.189.928-.524 1.263L14.05 25.734l.174.174a2.053 2.053 0 002.904 0l10.268-10.266A2.05 2.05 0 0028 14.191V5.597a2.053 2.053 0 00-2.054-2.054z" />
    </g>
    <defs>
      <clipPath id="clip0_5515_47337">
        <path fill="#fff" d="M0 0H28V28H0z" />
      </clipPath>
    </defs>
  </svg>
)

export default TagSvgIcon
