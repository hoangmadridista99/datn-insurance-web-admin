import React, { useState, useEffect, useRef } from 'react'
import { Form, Input, Spin, message, notification } from 'antd'
import Image from 'next/image'
import { useSession } from 'next-auth/react'
import { useTranslation } from 'next-i18next'
import CameraIconSvg from 'components/Icon/Camera'
import EditUnderlineIconSvg from 'components/Icon/EditUnderline'
import { updateProfile } from '@services/user'
import { NICKEL, VERMILION } from '@utils/constants/color'

const { useForm } = Form

const SettingProfile = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [fileAvatar, setFileAvatar] = useState()
  const [isUploadError, setIsUploadError] = useState(false)

  const [form] = useForm()
  const session = useSession()
  const { t } = useTranslation(['settings'])

  const inputFileRef = useRef(null)

  useEffect(() => {
    if (session.status === 'authenticated') {
      setIsLoading(false)
    }
  }, [session])

  const handleClickUploadImage = () => inputFileRef?.current?.click()

  const handleChangeUploadImage = (event) => {
    const { files } = event.target

    if (files && files.length > 0) {
      const isLessThan15MB = files[0].size / 1024 / 1024 <= 15

      if (!isLessThan15MB) {
        setIsUploadError(true)
        return message.error(t('settings:profile:form:avatar:error'))
      }

      setIsUploadError(false)
      return setFileAvatar(files[0])
    }
  }

  const handleFinishForm = async (values) => {
    try {
      const isNoFieldUpdate =
        Object.values(values).every((item) => !item?.trim()) && !fileAvatar
      if (isNoFieldUpdate) return
      setIsLoading(true)
      const data = {
        ...values,
        file: fileAvatar,
      }

      const body = new FormData()
      let updateSession = Object.keys(data).reduce(
        (result, key) => {
          if (!data[key]) return result
          body.append(key, data[key])
          if (key === 'file') return result
          return { ...result, [key]: data[key] }
        },
        { ...session.data.user }
      )
      const response = await updateProfile(body)
      if (response?.avatar_profile_url) {
        updateSession = {
          ...updateSession,
          avatar_profile_url: response.avatar_profile_url,
        }
      }
      await session.update(updateSession)
      console.log('🚀 ~ updateSession:', updateSession)
      form.resetFields()
      setFileAvatar(undefined)
      notification.success({ message: t('settings:responses:profile') })
    } catch (error) {
      console.log('Error:', error)
    } finally {
      setIsUploadError(false)
      setIsLoading(false)
    }
  }

  return (
    <div className="border border-t-0 border-platinum bg-white py-10 px-6">
      {!session.data?.user && (
        <div className="flex w-full justify-center">
          <Spin size="large" />
        </div>
      )}
      {session.data?.user && (
        <Form
          form={form}
          disabled={isLoading}
          onFinish={handleFinishForm}
          initialValues={{
            first_name: session.data.user?.first_name,
            last_name: session.data.user?.last_name,
            account: session.data.user?.account,
          }}
        >
          <div className="mb-6 last:mb-0">
            <p className="mb-2 text-base font-bold text-arsenic">
              {t('settings:profile:form:avatar:label')}
            </p>
            <div className="flex items-center">
              <input
                type="file"
                onChange={handleChangeUploadImage}
                ref={inputFileRef}
                className="hidden"
                disabled={isLoading}
                accept="image/jpg, image/png, image/jpeg"
              />
              <div className="flex h-22 items-center">
                <div
                  onClick={handleClickUploadImage}
                  className="relative mr-4 h-full w-22 cursor-pointer rounded-full border-2 border-arsenic"
                >
                  {fileAvatar || session.data.user?.avatar_profile_url ? (
                    <Image
                      src={
                        fileAvatar
                          ? URL.createObjectURL(fileAvatar)
                          : session.data.user?.avatar_profile_url
                      }
                      fill
                      alt="Avatar"
                      className="rounded-full object-cover"
                    />
                  ) : (
                    <Image
                      src={'/svg/avatar-default.svg'}
                      fill
                      alt="Avatar"
                      className="rounded-full object-cover"
                    />
                  )}
                  <CameraIconSvg className="absolute bottom-0 right-0 z-10" />
                </div>
                <div className="flex h-full flex-col justify-between">
                  <div className="text-sm text-nickel">
                    <p className="mb-1">
                      {t('settings:profile:form:avatar:description1')}
                    </p>
                    <p>{t('settings:profile:form:avatar:description2')}</p>
                  </div>

                  <div className="flex items-center gap-4">
                    <button
                      type="button"
                      onClick={handleClickUploadImage}
                      className={`flex cursor-pointer items-center self-start rounded border border-solid bg-transparent px-4 py-1 ${
                        isUploadError ? 'border-vermilion ' : 'border-nickel'
                      }`}
                    >
                      <EditUnderlineIconSvg
                        stroke={isUploadError ? VERMILION : NICKEL}
                      />
                      <span
                        className={`${
                          isUploadError ? 'text-vermilion' : 'text-nickel'
                        } ml-2 font-bold `}
                      >
                        {t('settings:button:upload')}
                      </span>
                    </button>

                    {isUploadError && (
                      <p className="text-vermilion">
                        <span>{t('settings:profile:form:avatar:error')}</span>
                      </p>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="mb-6 last:mb-0">
            <div className="flex items-center gap-2">
              <div className="w-1/2">
                <p className="mb-2 text-base font-bold text-arsenic">
                  {t('settings:profile:form:firstName')}
                </p>
                <Form.Item name="first_name" className="mb-0">
                  <Input size="large" />
                </Form.Item>
              </div>
              <div className="w-1/2">
                <p className="mb-2 text-base font-bold text-arsenic">
                  {t('settings:profile:form:lastName')}
                </p>
                <Form.Item name="last_name" className="mb-0">
                  <Input size="large" />
                </Form.Item>
              </div>
            </div>
          </div>
          <div className="mb-6 last:mb-0">
            <p className="mb-2 text-base font-bold text-arsenic">Account</p>
            <Form.Item name="account" className="mb-0">
              <Input size="large" disabled />
            </Form.Item>
          </div>
          <button
            type="submit"
            className="cursor-pointer rounded-lg border-none bg-ultramarine-blue px-10 py-3 font-bold text-cultured"
            disabled={isLoading}
          >
            {isLoading ? <Spin /> : t('settings:button:saved')}
          </button>
        </Form>
      )}
    </div>
  )
}

export default SettingProfile
