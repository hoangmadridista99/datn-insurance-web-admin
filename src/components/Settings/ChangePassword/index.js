import React, { useState } from 'react'
import { Form, Input, Spin, notification } from 'antd'
import { useTranslation } from 'next-i18next'
import { changePassword } from '@services/user'

const { useForm } = Form

const ChangePassword = () => {
  const [isLoading, setIsLoading] = useState(false)
  const [form] = useForm()
  const { t } = useTranslation(['settings'])

  const handleFinishForm = async (values) => {
    try {
      setIsLoading(true)
      await changePassword(values)
      form.resetFields()
      notification.success({ message: t('settings:responses:changePassword') })
    } catch (error) {
      console.log('🚀 ~ file: index.js:20 ~ handleFinishForm ~ error:', error)
    } finally {
      setIsLoading(false)
    }
  }

  const getRules = (keyMessageRequired) => [
    {
      required: true,
      message: t(`settings:changePassword:rules:${keyMessageRequired}`),
    },
    {
      min: 8,
      message: t('settings:changePassword:rules:min'),
    },
    {
      max: 30,
      message: t('settings:changePassword:rules:max'),
    },
  ]

  return (
    <div className="border-platinum border border-t-0 bg-white p-8">
      <Form disabled={isLoading} form={form} onFinish={handleFinishForm}>
        <div className="mb-6 last:mb-0">
          <p className="mb-2 text-base font-bold text-arsenic">
            {t('settings:changePassword:form:currentPassword')}
          </p>
          <Form.Item
            name="current_password"
            rules={getRules('currentPassword')}
          >
            <Input.Password size="large" />
          </Form.Item>
        </div>
        <div className="mb-6 last:mb-0">
          <p className="mb-2 text-base font-bold text-arsenic">
            {t('settings:changePassword:form:newPassword')}
          </p>
          <Form.Item name="new_password" rules={getRules('newPassword')}>
            <Input.Password size="large" />
          </Form.Item>
        </div>
        <div className="mb-6 last:mb-0">
          <p className="mb-2 text-base font-bold text-arsenic">
            {t('settings:changePassword:form:confirmationPassword')}
          </p>
          <Form.Item
            name="confirmation_password"
            dependencies={['new_password']}
            rules={[
              ...getRules('confirmationPassword'),
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('new_password') === value) {
                    return Promise.resolve()
                  }
                  if (value && value.length >= 8 && value.length <= 30)
                    return Promise.reject(
                      new Error(t('settings:changePassword:rules:notMatch'))
                    )
                  return Promise.reject()
                },
              }),
            ]}
          >
            <Input.Password size="large" />
          </Form.Item>
        </div>
        <button
          type="submit"
          className="rounded-lg border-none bg-ultramarine-blue px-10 py-3 font-bold text-cultured"
          disabled={isLoading}
        >
          {isLoading ? <Spin /> : t('settings:button:saved')}
        </button>
      </Form>
    </div>
  )
}

export default ChangePassword
