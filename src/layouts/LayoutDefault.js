import { useEffect, useState } from 'react'

import Loading from 'components/Loading'

export default function DefaultLayout({ children }) {
  const [isLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    setIsLoaded(true)
  }, [])

  return (
    <>
      {!isLoaded && <Loading />}
      {children}
    </>
  )
}
