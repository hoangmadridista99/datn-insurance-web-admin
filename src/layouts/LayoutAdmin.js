import React, { useState, useEffect } from 'react'
import { Layout, Spin } from 'antd'
import { useRouter } from 'next/router'
import Header from 'components/Header'

const { Content } = Layout

const LayoutAdmin = ({ children }) => {
  const [isLoading, setIsLoading] = useState(false)

  const router = useRouter()

  useEffect(() => {
    const handleChangeRouterStart = () => {
      setIsLoading(true)
    }

    router.events.on('routeChangeStart', handleChangeRouterStart)

    return () => router.events.off('routeChangeStart', handleChangeRouterStart)
  }, [router])

  useEffect(() => {
    const handleChangeRouterComplete = () => setIsLoading(false)

    router.events.on('routeChangeComplete', handleChangeRouterComplete)

    return () =>
      router.events.off('routeChangeComplete', handleChangeRouterComplete)
  }, [router])

  return (
    <Layout>
      <Header />
      <Content className="h-[calc(100vh-60px)] bg-white">
        {isLoading ? (
          <div className="flex h-full items-center justify-center">
            <Spin size="large" />
          </div>
        ) : (
          children
        )}
      </Content>
    </Layout>
  )
}

export default LayoutAdmin
