import React from 'react'

const Index = ({ children }) => (
  <div className="flex h-screen w-screen items-center justify-center">
    {children}
  </div>
)

export default Index
