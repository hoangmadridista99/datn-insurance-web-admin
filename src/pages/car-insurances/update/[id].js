import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import React, { useCallback, useEffect, useState } from 'react'
import { Form, Button, Spin, notification } from 'antd'
import CarInsuranceForm from 'components/Forms/Insurance/Car'
import { useTranslation } from 'next-i18next'
import Head from 'next/head'
import {
  getCarInsuranceDetails,
  updateCarInsurance,
} from '@services/car-insurances'
import { handleError } from '@helpers/handleError'
import { CAR_INSURANCE_TYPE } from '@utils/constants/insurances'
import { useRouter } from 'next/router'
import { ROUTERS } from '@utils/constants/routers'

const { useForm } = Form

const UpdateCarInsurance = ({ id }) => {
  const [isLoading, setIsLoading] = useState(false)

  const [carInsuranceDetails, setCarInsuranceDetails] = useState(null)

  const [form] = useForm()
  const { t } = useTranslation(['car-insurances'])
  const router = useRouter()

  const handleGetCarInsuranceDetails = useCallback(
    async (signal) => {
      try {
        const response = await getCarInsuranceDetails(id, signal)
        setCarInsuranceDetails(response)
        const insurance_type =
          response?.insurance_type === CAR_INSURANCE_TYPE.BOTH
            ? [CAR_INSURANCE_TYPE.MANDATORY, CAR_INSURANCE_TYPE.PHYSICAL]
            : [response.insurance_type]
        form.setFieldsValue({ ...response, insurance_type })
      } catch (error) {
        handleError(error)
      }
    },
    [form, id]
  )

  useEffect(() => {
    const controller = new AbortController()

    handleGetCarInsuranceDetails(controller.signal)

    return () => controller.abort()
  }, [handleGetCarInsuranceDetails])

  const handleClickSubmitForm = () => form.submit()

  const handleFinishForm = async (values) => {
    await updateCarInsurance(id, values)
    form.resetFields()
    notification.success({ message: t('car-insurances:success:update') })
    router.push(ROUTERS.CAR_INSURANCES)
  }

  return (
    <>
      <Head>
        <title>{t('car-insurances:head:update')}</title>
      </Head>
      <div className="h-full overflow-hidden overflow-y-auto p-6">
        <div className="mb-10 flex items-center justify-between">
          <h2 className="text-8 font-bold text-arsenic">
            {t('car-insurances:button:update_car')}
          </h2>
          <Button
            htmlType="submit"
            size="large"
            type="primary"
            onClick={handleClickSubmitForm}
            loading={isLoading}
            disabled={isLoading}
          >
            {t('car-insurances:button:save')}
          </Button>
        </div>
        {!carInsuranceDetails && (
          <div className="flex h-1/2 w-full items-center justify-center">
            <Spin />
          </div>
        )}
        {carInsuranceDetails && (
          <CarInsuranceForm
            form={form}
            handleFinishForm={handleFinishForm}
            isLoading={isLoading}
            setIsLoading={setIsLoading}
            carInsuranceDetails={carInsuranceDetails}
          />
        )}
      </div>
    </>
  )
}

export const getServerSideProps = async ({ locale, query }) => {
  const { id } = query

  if (!id || typeof id !== 'string')
    return {
      notFound: true,
    }

  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'car-insurances'])),
      id,
    },
  }
}

export default UpdateCarInsurance
