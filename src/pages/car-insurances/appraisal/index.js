import React from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import Head from 'next/head'
import { useTranslation } from 'next-i18next'
import { Button, Empty, Input, Select } from 'antd'
import { isEmpty } from 'lodash'
import CardAppraisal from 'components/Cards/Appraisal'
import { APPRAISAL_STATUS } from '@utils/constants/insurances'
import { SearchOutlined } from '@ant-design/icons'
import CustomizePagination from 'components/Pagination'
import { useCarInsuranceAppraisalLogic } from '@hooks/useCarInsuranceAppraisalLogic'

const AppraisalList = () => {
  const { t } = useTranslation(['vendor', 'common', 'insurances'])
  const {
    isLoading,
    insuranceCode,
    filterValues,
    appraisalList,
    pagination,
    handleChangeInsuranceCode,
    handleOptions,
    handleSelectValue,
    handleResetFilter,
    handleChangePage,
  } = useCarInsuranceAppraisalLogic()

  return (
    <>
      <Head>
        <title>Appraisal List - Sosanh24</title>
      </Head>

      <div className="no-scrollbar flex h-full w-full flex-col justify-between overflow-hidden overflow-y-auto bg-white px-6 py-10">
        <div className="min-h-100">
          {/* default filters */}
          <div className="mb-8 block overflow-hidden">
            <div className="flex items-center justify-start gap-4">
              <div className="flex w-full items-center justify-between gap-14">
                <Input
                  name="insurance_name"
                  size="large"
                  placeholder={t('insurances:filter.name')}
                  prefix={<SearchOutlined className="text-nickel" />}
                  className="w-3/5 text-base"
                  onChange={handleChangeInsuranceCode}
                  value={insuranceCode}
                  disabled={isLoading}
                />

                <Select
                  className="w-full text-nickel placeholder:text-nickel"
                  placeholder={t('insurances:form:placeholder:select')}
                  size="large"
                  allowClear={true}
                  options={handleOptions(Object.values(APPRAISAL_STATUS))}
                  onChange={(value) => handleSelectValue(value, 'form_status')}
                  value={filterValues?.insurance_category_label}
                  disabled={isLoading}
                />
              </div>

              <Button
                type="primary"
                size="large"
                className="rounded-lg border-0 px-6 text-base"
                disabled={
                  isLoading ||
                  (!filterValues?.insurance_name &&
                    !filterValues?.insurance_category_label)
                }
                onClick={handleResetFilter}
              >
                Reset
              </Button>
            </div>
          </div>

          <div className="mb-4 flex items-center gap-6 px-4 text-base font-bold text-metallic">
            <p className="w-1/3 text-start">
              {t('insurances:fields:insurance_name')}
            </p>
            <p className="w-1/4 text-start">{t('insurances:fields:time')}</p>
            <p className="w-1/4 text-start">{t('insurances:fields:status')}</p>
            <p className="w-1/4"></p>
          </div>

          {(isLoading || isEmpty(appraisalList)) && <Empty className="mt-20" />}

          {!isLoading && !isEmpty(appraisalList) && (
            <>
              <div className="[&>*:last-child]:mb-10">
                {appraisalList.map((appraisal) => (
                  <CardAppraisal key={appraisal.id} appraisal={appraisal} />
                ))}
              </div>

              <CustomizePagination
                pagination={pagination}
                onChangePage={handleChangePage}
              />
            </>
          )}
        </div>
      </div>
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'favorite',
        'insurances',
      ])),
    },
  }
}

export default AppraisalList
