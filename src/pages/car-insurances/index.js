import { Empty } from 'antd'
import CardCarInsurance from 'components/Cards/CarInsurance'
import PreviewCarInsurance from 'components/Preview/CarInsurance'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import Head from 'next/head'
import React from 'react'
import { useTranslation } from 'next-i18next'
import { isEmpty } from 'lodash'
import CustomizePagination from 'components/Pagination'
import { useCarInsurancesLogic } from '@hooks/useCarInsurancesLogic'

const CarInsurances = () => {
  const { t } = useTranslation(['car-insurances'])

  const {
    isLoading,
    carInsurances,
    isMandatory,
    pagination,
    previewCarInsuranceId,
    handleClickPreview,
    handleChangePage,
    handleClickClosePreview,
  } = useCarInsurancesLogic()

  return (
    <>
      <Head>
        <title>{t('car-insurances:head:list')}</title>
      </Head>

      <div className="no-scrollbar flex flex-col justify-between overflow-hidden overflow-y-auto px-6 py-10">
        <div className="min-h-100">
          <div className="mb-20">
            <p className="text-8 font-bold text-arsenic">
              {t('car-insurances:list:title')}
            </p>
          </div>

          <div className="mb-4 flex items-center gap-1 px-4 text-base font-bold text-metallic">
            <p className="w-1/4">{t('car-insurances:list:header:title')}</p>
            <p className="w-1/3">{t('car-insurances:list:header:type')}</p>
            <p className="w-1/4">{t('car-insurances:list:header:lasted')}</p>
            <p className="w-1/6" />
          </div>

          {(isLoading || isEmpty(carInsurances)) && <Empty className="mt-20" />}

          {!isLoading && !isEmpty(carInsurances) && (
            <>
              <div className="[&>*:last-child]:mb-10">
                {carInsurances.map((item) => (
                  <CardCarInsurance
                    key={item.id}
                    data={item}
                    isMandatory={isMandatory}
                    handleClickPreview={handleClickPreview}
                  />
                ))}
              </div>

              <CustomizePagination
                pagination={pagination}
                onChangePage={handleChangePage}
              />
            </>
          )}
        </div>
      </div>

      {carInsurances && (
        <PreviewCarInsurance
          carInsuranceId={previewCarInsuranceId}
          handleClickClosePreview={handleClickClosePreview}
        />
      )}
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'car-insurances'])),
    },
  }
}

export default CarInsurances
