import React from 'react'
import { SearchOutlined } from '@ant-design/icons'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { Button, Empty, Input, Select } from 'antd'
import Head from 'next/head'
import { isEmpty } from 'lodash'
import { useTranslation } from 'next-i18next'
import CardCarRating from 'components/Cards/CarRating'
import CustomizePagination from 'components/Pagination'
import { useCarInsuranceRatingLogic } from '@hooks/useCarInsuranceRatingLogic'

const RATING_STAR = [1, 2, 3, 4, 5]

export const InsuranceCarRating = () => {
  const { t } = useTranslation([
    'vendor',
    'common',
    'car-insurances',
    'insurances',
  ])
  const {
    isLoading,
    insuranceName,
    filterValues,
    pendingRatings,
    pagination,
    handleChangeInsuranceName,
    handleOptions,
    handleChangeInsuranceType,
    handleResetFilter,
    handleChangePage,
  } = useCarInsuranceRatingLogic()

  return (
    <>
      <Head>
        <title>Insurance Ratings - Sosanh24</title>
      </Head>

      <div className="no-scrollbar overflow-hidden overflow-y-auto px-6 py-10">
        {/* filter */}
        <div className="mb-8 flex items-center justify-between gap-4">
          <div className="flex w-2/3 items-center gap-4">
            <Input
              name="insurance_name"
              size="large"
              placeholder={t('car-insurances:form:placeholders:insurance_name')}
              prefix={<SearchOutlined className="text-nickel" />}
              className="w-full text-nickel placeholder:text-nickel"
              onChange={handleChangeInsuranceName}
              value={insuranceName}
            />

            <Select
              className="w-full text-nickel placeholder:text-nickel"
              placeholder={t('car-insurances:form:placeholders:rating_star')}
              size="large"
              allowClear={true}
              options={handleOptions(RATING_STAR)}
              onChange={handleChangeInsuranceType}
              value={filterValues?.rating_score}
            />
          </div>

          <Button
            type="primary"
            size="large"
            className="mr-0 rounded-lg py-2 px-6 text-base"
            onClick={handleResetFilter}
            disabled={
              isLoading ||
              (!filterValues.rating_score && !filterValues.insurance_name)
            }
          >
            Reset
          </Button>
        </div>

        {/* table header */}
        <div className="mb-4 flex items-center px-6 text-base font-bold text-metallic">
          <p className="w-3/12">{t('car-insurances:rating:insurance_name')}</p>
          <p className="w-4/12 pr-6">{t('car-insurances:rating:review')}</p>
          <p className="w-3/12">{t('car-insurances:rating:created_at')}</p>
          <p className="w-2/12" />
        </div>

        {/* table body */}
        {(isLoading || isEmpty(pendingRatings)) && <Empty className="mt-20" />}

        {!isLoading && !isEmpty(pendingRatings) && (
          <>
            <div className="[&>*:last-child]:mb-10">
              {pendingRatings.map((pendingRating) => (
                <CardCarRating
                  key={pendingRating?.id}
                  onRefresh={handleResetFilter}
                  {...pendingRating}
                />
              ))}
            </div>

            <CustomizePagination
              pagination={pagination}
              onChangePage={handleChangePage}
            />
          </>
        )}
      </div>
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'favorite',
        'car-insurances',
        'insurances',
      ])),
    },
  }
}

export default InsuranceCarRating
