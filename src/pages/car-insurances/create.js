import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import React, { useState } from 'react'
import { Form, Button, notification } from 'antd'
import CarInsuranceForm from 'components/Forms/Insurance/Car'
import { useTranslation } from 'next-i18next'
import Head from 'next/head'
import { createCarInsurance } from '@services/car-insurances'
import { useRouter } from 'next/router'

const { useForm } = Form

const CreateCarInsurance = () => {
  const { t } = useTranslation(['car-insurances', 'insurances', 'settings'])
  const router = useRouter()
  const [form] = useForm()

  const [isLoading, setIsLoading] = useState(false)

  const handleClickSubmitForm = () => form.submit()

  const handleFinishForm = async (values) => {
    try {
      await createCarInsurance(values)
      notification.success({ message: t('car-insurances:success:create') })
      router.push('/car-insurances')
    } catch (error) {
      console.log('Error', error)
    }
  }

  return (
    <>
      <Head>
        <title>{t('car-insurances:head:create')}</title>
      </Head>
      <div className="h-full overflow-hidden overflow-y-auto p-6">
        <div className="mb-10 flex items-center justify-between">
          <h2 className="text-8 font-bold text-arsenic">
            {t('car-insurances:button:create')}
          </h2>
          <Button
            htmlType="submit"
            size="large"
            type="primary"
            onClick={handleClickSubmitForm}
            loading={isLoading}
            disabled={isLoading}
          >
            {t('car-insurances:button:save')}
          </Button>
        </div>
        <CarInsuranceForm
          form={form}
          handleFinishForm={handleFinishForm}
          isLoading={isLoading}
          setIsLoading={setIsLoading}
        />
      </div>
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'car-insurances',
        'insurances',
        'settings',
      ])),
    },
  }
}

export default CreateCarInsurance
