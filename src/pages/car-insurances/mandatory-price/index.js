import { Button, Form, Modal, Result } from 'antd'
import FormMandatorCarInsurance from 'components/Forms/Insurance/MandatorCarInsurance'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import Head from 'next/head'
import React, { useState, useEffect, useCallback } from 'react'
import { useTranslation } from 'next-i18next'
import {
  createCarInsuranceSettingsMandatory,
  getCarInsuranceSettingMandatorDetails,
} from '@services/car-insurance-settings'
import { handleError } from '@helpers/handleError'
import CheckSuccessIconSvg from 'components/Icon/CheckSuccess'

const { useForm } = Form

const UpdateMandatorCarInsurance = () => {
  const [isLoading, setIsLoading] = useState(false)
  const [isModalSuccess, setIsModalSuccess] = useState(false)

  const { t } = useTranslation(['car-insurances'])
  const [form] = useForm()

  const handleGetCarInsuranceSettingsMandatoryDetails = useCallback(
    async (signal) => {
      const response = await getCarInsuranceSettingMandatorDetails(signal)
      form.setFieldsValue(response)
    },
    [form]
  )

  useEffect(() => {
    const controller = new AbortController()

    handleGetCarInsuranceSettingsMandatoryDetails()

    return () => controller.abort()
  }, [handleGetCarInsuranceSettingsMandatoryDetails])

  const handleFinishFormCompulsoryCar = useCallback(
    async (values) => {
      try {
        setIsLoading(true)
        await createCarInsuranceSettingsMandatory(values)
        form.setFieldsValue(values)
        setIsModalSuccess(true)
      } catch (error) {
        handleError(error)
      } finally {
        setIsLoading(false)
      }
    },
    [form]
  )

  const handleClickCloseModalSuccess = () => setIsModalSuccess(false)

  return (
    <>
      <Head>
        <title>{t('car-insurances:head:update-mandatory')}</title>
      </Head>
      <div className="no-scrollbar h-full overflow-hidden overflow-y-scroll p-6">
        <Form
          disabled={isLoading}
          form={form}
          onFinish={handleFinishFormCompulsoryCar}
        >
          <div className="mb-10 flex items-center justify-between">
            <h2 className="text-8 font-bold text-arsenic">
              {t('car-insurances:update-mandatory')}
            </h2>
            <Button
              size="large"
              type="primary"
              htmlType="submit"
              isLoading={isLoading}
              disabled={isLoading}
            >
              {t('car-insurances:button:save')}
            </Button>
          </div>
          <FormMandatorCarInsurance />
        </Form>
      </div>
      <Modal open={isModalSuccess} centered closable={false} footer={null}>
        <Result
          status="success"
          title={t('car-insurances:modal:title')}
          icon={
            <div className="flex w-full justify-center">
              <CheckSuccessIconSvg />
            </div>
          }
          extra={
            <button
              className="cursor-pointer rounded-lg border-none bg-ultramarine-blue px-8 py-2 font-bold text-cultured"
              type="button"
              onClick={handleClickCloseModalSuccess}
              key="form-insurance-button"
            >
              {t('car-insurances:modal:button')}
            </button>
          }
        />
      </Modal>
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'car-insurances'])),
    },
  }
}

export default UpdateMandatorCarInsurance
