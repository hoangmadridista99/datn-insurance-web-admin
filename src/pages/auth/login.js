import React, { useContext, useState } from 'react'
import { useRouter } from 'next/router'
import { getServerSession } from 'next-auth/next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { Button, Form, Input, notification } from 'antd'
import { useTranslation } from 'next-i18next'
import Head from 'next/head'
import AuthLayout from '@layouts/LayoutAuth'
import { authOptions } from 'pages/api/auth/[...nextauth]'
import { LockOutlined, UserOutlined } from '@ant-design/icons'
import { DataContext } from 'src/store/GlobalState'
import { signIn } from 'next-auth/react'

function Login() {
  const router = useRouter()
  const { t } = useTranslation(['auth', 'common'])
  const { dispatch } = useContext(DataContext)
  const [isDisabled, setIsDisabled] = useState(true)

  const handleLogin = async (params) => {
    try {
      dispatch({ type: 'LOADING', payload: { loading: true } })
      const response = await signIn('credentials', {
        account: params.username,
        password: params.password,
        redirect: false,
      })

      if (response.ok) {
        notification.success({
          message: t('common:notification.login_success'),
        })

        return router.push('/dashboard')
      }

      notification.error({
        message: t(`common:error.${response.error}`),
      })
    } catch (error) {
      console.log('Error:', error)
    } finally {
      dispatch({ type: 'LOADING', payload: { loading: false } })
    }
  }

  const onValuesChange = (changedValues, allValues) => {
    if (
      allValues.username != undefined &&
      allValues.password != undefined &&
      allValues.username != '' &&
      allValues.password != ''
    ) {
      setIsDisabled(false)
    } else {
      setIsDisabled(true)
    }
  }

  return (
    <>
      <Head>
        <title>Login - Sosanh24</title>
      </Head>

      <AuthLayout>
        <div className="flex w-1/2 flex-col justify-center rounded-lg border border-solid border-platinum py-12 shadow-md 2xl:w-1/3">
          <p className="mb-10 whitespace-nowrap px-4 text-center text-2xl font-medium text-arsenic">
            {t('title')}
          </p>

          <div className="mx-auto w-1/2">
            <Form
              name="login-form"
              labelCol={{
                span: 8,
              }}
              layout="vertical"
              onFinish={handleLogin}
              onValuesChange={onValuesChange}
              autoComplete="off"
            >
              <div className="mb-5 flex flex-col">
                <div>
                  <p className="mb-2 text-base font-normal text-arsenic">
                    {t('auth:account')}
                  </p>

                  <Form.Item
                    name="username"
                    rules={[
                      {
                        required: true,
                        message: t('error.username.empty'),
                      },
                      {
                        min: 4,
                        message: t('auth:error.username.short'),
                      },
                      {
                        max: 20,
                        message: t('auth:error.username.long'),
                      },
                      {
                        pattern: new RegExp(/^[a-zA-Z0-9]*$/),
                        message: t('auth:error.username.regex'),
                      },
                    ]}
                  >
                    <Input
                      prefix={<UserOutlined className="site-form-item-icon" />}
                      placeholder={t('auth:account')}
                      size="large"
                    />
                  </Form.Item>
                </div>

                <div>
                  <p className="mb-2 text-base font-normal text-arsenic">
                    {t('auth:pass')}
                  </p>

                  <Form.Item
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: t('error.password.empty'),
                      },
                      {
                        min: 8,
                        message: t('error.password.short'),
                      },
                      {
                        max: 30,
                        message: t('error.password.long'),
                      },
                    ]}
                  >
                    <Input.Password
                      prefix={<LockOutlined className="site-form-item-icon" />}
                      type="password"
                      placeholder={t('auth:pass')}
                      size="large"
                    />
                  </Form.Item>
                </div>
              </div>

              <div className="flex flex-row-reverse items-center justify-between gap-3">
                <Button
                  className="w-full text-base font-normal"
                  type="primary"
                  htmlType="submit"
                  disabled={isDisabled}
                  size="large"
                >
                  {t('auth:submit')}
                </Button>

                <Button
                  className="w-full text-base font-normal text-arsenic"
                  onClick={() => router.push('/')}
                  size="large"
                >
                  {t('auth:back')}
                </Button>
              </div>
            </Form>
          </div>
        </div>
      </AuthLayout>
    </>
  )
}

export const getServerSideProps = async ({ req, res, locale }) => {
  const session = await getServerSession(req, res, authOptions)

  if (session)
    return {
      redirect: {
        destination: '/dashboard',
        permanent: true,
      },
    }

  return {
    props: { ...(await serverSideTranslations(locale, ['auth', 'common'])) },
  }
}

export default Login
