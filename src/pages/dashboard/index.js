import React from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'

import Head from 'next/head'

function Dashboard() {
  const { t } = useTranslation('common')

  return (
    <>
      <Head>
        <title>Dashboard - Sosanh24</title>
      </Head>
      <h1>{t('common:sidebar.dashboard')}</h1>
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: { ...(await serverSideTranslations(locale, 'common')) },
  }
}

export default Dashboard
