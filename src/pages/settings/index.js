import React from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import SettingProfile from 'components/Settings/Profile'
import ChangePassword from 'components/Settings/ChangePassword'
import { useTranslation } from 'next-i18next'
import { Tabs } from 'antd'

const Settings = () => {
  const { t } = useTranslation(['settings'])

  const tabs = [
    {
      key: 'profile',
      label: t('settings:profile:title'),
      children: <SettingProfile />,
    },
    {
      key: 'password',
      label: t('settings:changePassword:title'),
      children: <ChangePassword />,
    },
  ]
  return (
    <>
      <div className="m-6 mb-10">
        <h1 className="mb-2 text-8 font-bold text-arsenic">
          {t('settings:title')}
        </h1>
        <p className="text-sm text-nickel">{t('settings:description')}</p>
      </div>

      <Tabs
        defaultActiveKey="profile"
        items={tabs}
        animated
        type="card"
        className="tabs-setting [&>div]:mb-0"
      />
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['settings', 'common'])),
    },
  }
}

export default Settings
