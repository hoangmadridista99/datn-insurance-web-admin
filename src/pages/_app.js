import React from 'react'
import { SessionProvider } from 'next-auth/react'
import { QueryClient, QueryClientProvider } from 'react-query'
import { appWithTranslation } from 'next-i18next'
import SiderComponent from '@components/Sider'
import { DataProvider } from 'src/store/GlobalState'
import { ConfigProvider } from 'antd'
import LayoutAdmin from '@layouts/LayoutAdmin'
import '../styles/globals.css'

function App({ Component, pageProps: { session, ...pageProps } }) {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        // ✅ globally default to 20 seconds
        staleTime: 1000 * 20,
      },
    },
  })

  return (
    <ConfigProvider>
      <DataProvider>
        <SessionProvider session={session}>
          <QueryClientProvider client={queryClient}>
            <div className="relative inline-flex w-full">
              <SiderComponent />
              <div className="h-full w-[calc(100vw-300px)] flex-1">
                <LayoutAdmin>
                  <Component {...pageProps} />
                </LayoutAdmin>
              </div>
            </div>
          </QueryClientProvider>
        </SessionProvider>
      </DataProvider>
    </ConfigProvider>
  )
}

export default appWithTranslation(App)
