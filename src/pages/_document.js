import React from 'react'
import { Html, Head, Main, NextScript } from 'next/document'

import i18nextConfig from '../../next-i18next.config'

export default function Document(props) {
  const { __NEXT_DATA__ } = props

  const currentLocale = __NEXT_DATA__.locale ?? i18nextConfig.i18n.defaultLocale

  return (
    <Html lang={currentLocale}>
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
