import React, { useCallback, useEffect, useState } from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import {
  INSURANCE_TYPES_DEFAULT,
  HEALTH_INSURANCE,
  LIFE_INSURANCE,
  DENTAL,
  OBSTETRIC,
} from '@utils/constants/insurances'
import { getInsuranceObjectives } from '@services/insurance-objectives'
import { getCompanies } from '@services/companies'
import { Button, Form, Select, Spin } from 'antd'
import Head from 'next/head'
import { useTranslation } from 'next-i18next'
import {
  getInsuranceCategories,
  getInsuranceDetails,
  updateInsurance,
} from '@services/insurances'
import { notification } from 'antd'
import { useRouter } from 'next/router'
import HealthInsuranceForm from '../../../components/Preview/Health/create'
import LifeInsuranceForm from 'components/Preview/Life/create'

function UpdateInsurance({ companies, objectives }) {
  const [form] = Form.useForm()
  const { t } = useTranslation(['insurances'])
  const router = useRouter()

  const [isLoaded, setIsLoaded] = useState(false)
  const [insuranceCategories, setInsuranceCategories] = useState(null)
  const [benefitSelected, setBenefitSelected] = useState([])
  const [hospitals, setHospitals] = useState([])
  const [insuranceDetails, setInsuranceDetails] = useState(null)

  const handleRenderComponent = () => {
    switch (insuranceDetails?.insurance_category?.label) {
      case LIFE_INSURANCE:
        return (
          <LifeInsuranceForm
            form={form}
            companies={companies}
            objectives={objectives}
            onFinishForm={onFinishForm}
            insurances={INSURANCE_TYPES_DEFAULT}
            benefitSelected={benefitSelected}
            setBenefitSelected={setBenefitSelected}
            initialValues={insuranceDetails}
          />
        )

      case HEALTH_INSURANCE:
        return (
          <HealthInsuranceForm
            form={form}
            companies={companies}
            objectives={objectives}
            onFinishForm={onFinishForm}
            benefitSelected={benefitSelected}
            setBenefitSelected={setBenefitSelected}
            hospitals={hospitals}
            setHospitals={setHospitals}
            initialValues={insuranceDetails}
          />
        )

      default:
        return null
    }
  }

  const handleOptions = (list) => {
    if (!list || list.length < 1) return

    return list.map((item) => ({
      value: item.label,
      label: t(`insurances:form:options:${item.label}`),
    }))
  }

  const fetchInsuranceDetails = useCallback(async () => {
    try {
      const response = await getInsuranceDetails(router?.query?.id)

      const customizeInsuranceObjectives = response?.objective_of_insurance.map(
        (objective) => objective?.id
      )

      setInsuranceDetails({
        ...response,
        objective_of_insurance: customizeInsuranceObjectives,
      })
      setHospitals(response?.hospitals)

      setBenefitSelected((prevState) => [
        ...prevState,
        response?.dental && typeof response?.dental === 'object'
          ? DENTAL
          : null,
        response?.obstetric && typeof response?.obstetric === 'object'
          ? OBSTETRIC
          : null,
      ])
    } catch (error) {
      console.log('Error', error)
    }
  }, [router?.query?.id])

  const fetchInsuranceCategories = useCallback(async () => {
    try {
      const response = await getInsuranceCategories()

      setInsuranceCategories(response)
    } catch (error) {
      console.log('Error', error)
    }
  }, [])

  const handleCustomizeFormValues = useCallback(
    (data) => {
      const {
        objective_of_insurance: objective,
        benefits_illustration_table: document_benefits,
        documentation_url: document,
        ...dataValues
      } = data

      const objective_of_insurance = [...(objective ?? [])].map((id) => {
        const findObjective = objectives.find((item) => item.id === id)

        return findObjective
      })

      let documentation_url = null
      let benefits_illustration_table = null

      if (document_benefits) {
        benefits_illustration_table = Array.isArray(document_benefits)
          ? document_benefits[0].url
          : document_benefits.file.xhr.responseURL
      }

      if (document) {
        documentation_url = Array.isArray(document)
          ? document[0].url
          : document.file.xhr.responseURL
      }

      const body = {
        ...dataValues,
        objective_of_insurance,
        benefits_illustration_table,
        documentation_url,
      }

      return body
    },
    [objectives]
  )

  const onFinishForm = async () => {
    try {
      const customizeFormValues = handleCustomizeFormValues(
        form.getFieldsValue()
      )

      const submitData =
        insuranceDetails?.insurance_category?.label === HEALTH_INSURANCE
          ? {
              ...insuranceDetails,
              ...form.getFieldsValue(),
              insurance_category: insuranceDetails?.insurance_category,
              additional_benefit: benefitSelected.filter(
                (item) => item !== null
              ),
              hospitals,
            }
          : {
              ...insuranceDetails,
              ...form.getFieldsValue(),
              ...customizeFormValues,
              insurance_category: insuranceDetails?.insurance_category,
            }

      const response = await updateInsurance(insuranceDetails?.id, submitData)

      if (response.status === 204) {
        notification.success({
          message: t('insurances:message:update_success'),
        })

        router.push('/insurances/admin')
      }
    } catch (error) {
      console.log('Error', error)
      notification.error({
        message: t('insurances:message:update_failed'),
      })
    }
  }

  const handleSubmit = async () => form.submit()

  useEffect(() => {
    if (!window.document.body.classList.value) {
      window.document.body.classList.add('form-insurance-body')
    }
    setIsLoaded(true)
  }, [])

  useEffect(() => {
    fetchInsuranceCategories()
  }, [fetchInsuranceCategories])

  useEffect(() => {
    fetchInsuranceDetails()
  }, [fetchInsuranceDetails])

  // useEffect(() => {
  //   form.setFieldsValue(HEALTH_FORM_INITIAL_VALUES)
  // }, [form])

  return (
    <>
      <Head>
        <title>Create Insurance - Sosanh24</title>
      </Head>
      {!isLoaded && (
        <div className="flex h-screen items-center justify-center">
          <Spin size="large" />
        </div>
      )}

      {isLoaded && (
        <div className="h-full overflow-hidden overflow-y-auto px-6 py-4">
          <div className="mb-10">
            <div className="mb-2 text-8 font-bold leading-12 text-arsenic">
              {t('insurances:form:details:insurance_type')}
            </div>

            <div className="flex items-center justify-between">
              <Select
                placeholder={t('insurances:form:placeholder:select')}
                options={handleOptions(insuranceCategories)}
                className="w-1/3"
                value={insuranceDetails?.insurance_category?.label}
                size="large"
                disabled
              />

              <Button
                type="primary"
                className="h-full bg-ultramarine-blue px-10 py-3 text-base font-bold"
                onClick={handleSubmit}
              >
                Lưu thay đổi
              </Button>
            </div>
          </div>

          {insuranceDetails?.insurance_category?.label &&
            handleRenderComponent()}
        </div>
      )}
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  const [resultCompanies, objectives] = await Promise.all([
    getCompanies(),
    getInsuranceObjectives(),
  ])

  const companies = resultCompanies.map((item) => ({
    value: item.id,
    label: item.long_name,
  }))

  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'insurances',
        'common',
        'vendor',
      ])),
      companies,
      objectives,
    },
  }
}

export default UpdateInsurance
