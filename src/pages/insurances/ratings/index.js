import React from 'react'
import { SearchOutlined } from '@ant-design/icons'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import CardRating from '@components/Cards/Rating'
import { Button, Empty, Input, Select } from 'antd'
import Head from 'next/head'
import { isEmpty } from 'lodash'
import CustomizePagination from 'components/Pagination'
import { useTranslation } from 'next-i18next'
import { useLifeAndHealthRatingLogic } from '@hooks/useLifeAndHealthRatingLogic'

export const InsuranceRating = () => {
  const { t } = useTranslation(['vendor', 'common', 'insurances'])
  const {
    isLoading,
    pendingRatings,
    insuranceCategories,
    insuranceName,
    filterValues,
    pagination,
    handleChangeInsuranceName,
    handleOptions,
    handleChangeInsuranceType,
    handleResetFilter,
    handleChangePage,
  } = useLifeAndHealthRatingLogic()

  return (
    <>
      <Head>
        <title>Insurance Ratings - Sosanh24</title>
      </Head>

      <div className="no-scrollbar overflow-hidden overflow-y-auto px-6 py-10">
        {/* filter */}
        <div className="mb-8 flex items-center justify-between gap-4">
          <div className="flex w-2/3 items-center gap-4">
            <Input
              name="insurance_name"
              size="large"
              placeholder={t('insurances:filter:name')}
              prefix={<SearchOutlined className="text-nickel" />}
              className="w-full text-nickel placeholder:text-nickel"
              onChange={handleChangeInsuranceName}
              value={insuranceName}
            />

            <Select
              className="w-full text-nickel placeholder:text-nickel"
              placeholder={t('insurances:form:placeholder:select')}
              size="large"
              allowClear={true}
              options={handleOptions(insuranceCategories)}
              onChange={handleChangeInsuranceType}
              value={filterValues?.insurance_category_label}
            />
          </div>

          <Button
            type="primary"
            size="large"
            className="mr-0 rounded-lg py-2 px-6 text-base"
            onClick={handleResetFilter}
            disabled={
              isLoading ||
              (!filterValues.insurance_category_label &&
                !filterValues.insurance_name)
            }
          >
            Reset
          </Button>
        </div>

        {/* table header */}
        <div className="mb-4 flex items-center px-6 text-base font-bold text-metallic">
          <p className="w-3/12">{t('insurances:fields:insurance_name')}</p>
          <p className="w-4/12 pr-6">{t('insurances:fields:review')}</p>
          <p className="w-3/12">{t('insurances:fields:created_at')}</p>
          <p className="w-2/12" />
        </div>

        {/* table body */}
        {(isLoading || isEmpty(pendingRatings)) && <Empty className="mt-20" />}

        {!isLoading && !isEmpty(pendingRatings) && (
          <>
            <div className="[&>*:last-child]:mb-10">
              {pendingRatings.map((pendingRating) => (
                <CardRating
                  key={pendingRating?.id}
                  onRefresh={handleResetFilter}
                  {...pendingRating}
                />
              ))}
            </div>

            <CustomizePagination
              pagination={pagination}
              onChangePage={handleChangePage}
            />
          </>
        )}
      </div>
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'favorite',
        'insurances',
      ])),
    },
  }
}

export default InsuranceRating
