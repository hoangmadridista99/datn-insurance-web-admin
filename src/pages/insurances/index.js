import React from 'react'
import { SearchOutlined } from '@ant-design/icons'
import { Input, Button, Select, Empty } from 'antd'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import Image from 'next/image'
import { useTranslation } from 'next-i18next'
import CardInsurance from '@components/Cards/Insurance'
import { INSURANCE_STATUSES, ROLES } from '@utils/constants/insurances'
import useInsurancesLogic from '@hooks/useInsurancesLogic'
import { getCompanies } from '@services/companies'
import { getInsuranceObjectives } from '@services/insurance-objectives'
import Head from 'next/head'
import classNames from 'classnames'
import { isEmpty } from 'lodash'
import { useInputNumber } from '@hooks/useInputNumber'
import CustomizePagination from 'components/Pagination'

function Insurances({ role }) {
  const { t } = useTranslation(['common', 'insurances'])
  const { onKeyDown } = useInputNumber()
  const {
    insurances,
    pagination,
    isLoading,
    inputFieldValues,
    isFilter,
    filterValues,
    insuranceCategories,
    handleChangePage,
    handleChangeInputField,
    handleSelectValue,
    handleOpenMoreFilters,
    handleResetFilters,
    onRemoveSuccess,
    onUpdateStatusSuccess,
  } = useInsurancesLogic(role)

  return (
    <>
      <Head>
        <title>Insurances - Sosanh24</title>
      </Head>

      <div className="no-scrollbar flex flex-col justify-between overflow-hidden overflow-y-auto px-6 py-10">
        {/* filter */}
        <div className="flex items-center justify-between">
          <Input
            name="name"
            size="large"
            placeholder={t('insurances:filter.name')}
            prefix={<SearchOutlined className="text-nickel" />}
            className="w-100 text-base"
            onChange={handleChangeInputField}
            value={inputFieldValues.insuranceName}
            disabled={isLoading}
          />
          <Button
            type="primary"
            ghost
            size="large"
            icon={
              <Image
                src={isFilter ? '/svg/filter.svg' : '/svg/no-filter.svg'}
                width={18}
                height={18}
                alt="Filter"
                className="mr-2"
              />
            }
            onClick={handleOpenMoreFilters}
            className="flex items-center justify-center border-nickel py-2 px-4 text-base font-normal text-nickel aria-checked:border-ultramarine-blue aria-checked:text-ultramarine-blue aria-checked:hover:border-ultramarine-blue aria-checked:hover:text-ultramarine-blue"
            aria-checked={isFilter}
            loading={isLoading}
          >
            {t('insurances:filter.button')}
          </Button>
        </div>

        {/* expand filter */}
        <div
          className="hidden overflow-hidden aria-checked:block"
          aria-checked={isFilter}
        >
          <div className="mt-6 flex items-center justify-between gap-4">
            <Select
              name="type"
              className="w-full text-arsenic"
              placeholder={t('insurances:form:placeholder:select')}
              size="large"
              allowClear={true}
              options={insuranceCategories}
              onChange={(value, _) => handleSelectValue(value, _, 'type')}
              value={filterValues?.insurance_type}
              disabled={isLoading}
            />

            <Input
              name="price"
              placeholder={t('insurances:filter.price')}
              className="w-full"
              size="large"
              onChange={handleChangeInputField}
              value={inputFieldValues?.price}
              disabled={isLoading}
              onKeyDown={(event) => onKeyDown(event, 'filter_insurance_price')}
            />

            {role === ROLES.VENDOR && (
              <Select
                name="status"
                placeholder={t('insurances:form:placeholder:select')}
                options={INSURANCE_STATUSES}
                allowClear={false}
                className="w-full text-nickel placeholder:text-nickel"
                size="large"
                onChange={(value, _) => handleSelectValue(value, _, 'status')}
                value={filterValues?.status}
                disabled={isLoading}
              />
            )}

            <Button
              type="primary"
              size="large"
              className="mr-0 rounded-lg py-2 px-6 text-base"
              onClick={handleResetFilters}
              disabled={isLoading}
            >
              Reset
            </Button>
          </div>
        </div>

        {/* body content */}
        <div className="mb-2 mt-8 flex items-center px-4 text-base font-bold text-metallic">
          <p className="w-4/12">{t('insurances:fields:insurance_name')}</p>
          <p className="w-3/12">{t('insurances:fields:expenses')}</p>
          <p className="w-2/12">{t('insurances:fields:created_at')}</p>

          {role === ROLES.VENDOR && (
            <p className="w-2/12">{t('insurances:fields:status')}</p>
          )}
          <p
            className={classNames('w-2/12', {
              'w-3/12': role !== ROLES.VENDOR,
            })}
          />
        </div>

        {(isLoading || isEmpty(insurances)) && <Empty className="mt-20" />}

        {!isLoading && !isEmpty(insurances) && (
          <>
            <div className={'[&>*:last-child]:mb-10'}>
              {insurances?.map((item) => (
                <CardInsurance
                  key={item.id}
                  {...item}
                  isLoading={isLoading}
                  onUpdateStatusSuccess={onUpdateStatusSuccess}
                  onRemoveSuccess={onRemoveSuccess}
                />
              ))}
            </div>

            <CustomizePagination
              pagination={pagination}
              onChangePage={handleChangePage}
            />
          </>
        )}
      </div>
    </>
  )
}

export const getServerSideProps = async ({ locale, query }) => {
  const { role } = query

  if (!role || (role !== ROLES.ADMIN && role !== ROLES.VENDOR))
    return {
      notFound: true,
    }

  const [resultCompanies, objectives] = await Promise.all([
    await getCompanies(),
    await getInsuranceObjectives(),
  ])

  const companies = [...resultCompanies].map((item) => ({
    label: item.short_name,
    value: item.id,
  }))

  return {
    props: {
      ...(await serverSideTranslations(locale, ['common', 'insurances'])),
      companies,
      objectives,
      role,
    },
  }
}

export default Insurances
