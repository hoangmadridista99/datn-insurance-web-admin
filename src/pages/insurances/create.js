import React, { useCallback, useEffect, useState } from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import {
  INSURANCE_TYPES_DEFAULT,
  HEALTH_INSURANCE,
  LIFE_INSURANCE,
  HEALTH_FORM_INITIAL_VALUES,
  LIFE_FORM_INITIAL_VALUES,
} from '@utils/constants/insurances'
import { getInsuranceObjectives } from '@services/insurance-objectives'
import { getCompanies } from '@services/companies'
import { Button, Form, Segmented, Spin } from 'antd'
import Head from 'next/head'
import { useTranslation } from 'next-i18next'
import { createInsurance, getInsuranceCategories } from '@services/insurances'
import { notification } from 'antd'
import HealthInsuranceForm from '../../components/Preview/Health/create'
import LifeInsuranceForm from 'components/Preview/Life/create'

function CreateInsurance({ companies, objectives }) {
  const [form] = Form.useForm()
  const { t } = useTranslation(['insurances', 'common'])

  const [isLoaded, setIsLoaded] = useState(false)
  const [insuranceCategories, setInsuranceCategories] = useState([])
  const [insuranceType, setInsuranceType] = useState(
    insuranceCategories[0]?.label || HEALTH_INSURANCE
  )
  const [benefitSelected, setBenefitSelected] = useState([])
  const [hospitals, setHospitals] = useState([])

  const handleSelectInsuranceType = (value) => {
    form.resetFields()
    setInsuranceType(value)
  }

  const handleRenderComponent = () => {
    switch (insuranceType) {
      case LIFE_INSURANCE:
        return (
          <LifeInsuranceForm
            form={form}
            companies={companies}
            objectives={objectives}
            onFinishForm={onFinishForm}
            insurances={INSURANCE_TYPES_DEFAULT}
            benefitSelected={benefitSelected}
            setBenefitSelected={setBenefitSelected}
          />
        )

      case HEALTH_INSURANCE:
        return (
          <HealthInsuranceForm
            form={form}
            companies={companies}
            objectives={objectives}
            onFinishForm={onFinishForm}
            benefitSelected={benefitSelected}
            setBenefitSelected={setBenefitSelected}
            hospitals={hospitals}
            setHospitals={setHospitals}
          />
        )

      default:
        return null
    }
  }

  const handleOptions = (list) => {
    if (!list || list.length < 1) return

    return list.map((item) => ({
      value: item.label,
      label: (
        <div className="py-2 px-6 text-base">
          <p className="font-bold">
            {t(`insurances:form:options:${item.label}`)}
          </p>
        </div>
      ),
    }))
  }

  const fetchInsuranceCategories = useCallback(async () => {
    try {
      const response = await getInsuranceCategories()

      setInsuranceCategories(response)
    } catch (error) {
      console.log('Error', error)
    }
  }, [])

  const handleCustomizeFormValues = useCallback(
    (data) => {
      const {
        objective_of_insurance: objective,
        benefits_illustration_table: document_benefits,
        documentation_url: document,
        ...dataValues
      } = data

      const objective_of_insurance = [...(objective ?? [])].map((id) => {
        const findObjective = objectives.find((item) => item.id === id)

        return findObjective
      })

      let documentation_url = null
      let benefits_illustration_table = null

      if (document_benefits) {
        benefits_illustration_table = Array.isArray(document_benefits)
          ? document_benefits[0].url
          : document_benefits.file.xhr.responseURL
      }

      if (document) {
        documentation_url = Array.isArray(document)
          ? document[0].url
          : document.file.xhr.responseURL
      }

      const body = {
        ...dataValues,
        objective_of_insurance,
        benefits_illustration_table,
        documentation_url,
      }

      return body
    },
    [objectives]
  )

  const onFinishForm = async () => {
    try {
      const customizeFormValues = handleCustomizeFormValues(
        form.getFieldsValue()
      )

      const submitData =
        insuranceType === HEALTH_INSURANCE
          ? {
              ...HEALTH_FORM_INITIAL_VALUES,
              ...form.getFieldsValue(),
              insurance_category: insuranceCategories.filter(
                (category) => category.label === insuranceType
              )[0],
              additional_benefit: benefitSelected,
              hospitals,
            }
          : {
              ...LIFE_FORM_INITIAL_VALUES,
              ...customizeFormValues,
              insurance_category: insuranceCategories.filter(
                (category) => category.label === insuranceType
              )[0],
            }

      const response = await createInsurance(submitData)

      if (response.status === 204) {
        form.resetFields()
        setInsuranceType(null)

        notification.success({
          message: t('insurances:message:create_success'),
        })
      }
    } catch (error) {
      console.log('Error', error)
      notification.error({
        message: t(`common:error:${error?.response?.data?.error_code}`),
      })
    }
  }

  const handleSubmit = async () => form.submit()

  useEffect(() => {
    if (!window.document.body.classList.value) {
      window.document.body.classList.add('form-insurance-body')
    }
    setIsLoaded(true)
  }, [])

  useEffect(() => {
    fetchInsuranceCategories()
  }, [fetchInsuranceCategories])

  return (
    <>
      <Head>
        <title>Create Insurance - Sosanh24</title>
      </Head>
      {!isLoaded && (
        <div className="flex h-screen items-center justify-center">
          <Spin size="large" />
        </div>
      )}

      {isLoaded && (
        <div className="h-full overflow-hidden overflow-y-auto">
          <div className="mb-10 p-6">
            <div className="mb-4 text-8 font-bold leading-12 text-arsenic">
              {t('insurances:form:details:insurance_type')}
            </div>

            <div className="flex items-center justify-between">
              <Segmented
                options={handleOptions(insuranceCategories)}
                value={insuranceType}
                onChange={handleSelectInsuranceType}
              />

              <Button
                type="primary"
                className="h-full bg-ultramarine-blue px-10 py-3 text-base font-bold"
                onClick={handleSubmit}
              >
                {t('insurances:form:button:save')}
              </Button>
            </div>
          </div>

          {handleRenderComponent()}
        </div>
      )}
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  const [resultCompanies, objectives] = await Promise.all([
    getCompanies(),
    getInsuranceObjectives(),
  ])

  const companies = resultCompanies.map((item) => ({
    value: item.id,
    label: item.long_name,
  }))

  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'insurances',
        'common',
        'vendor',
      ])),
      companies,
      objectives,
    },
  }
}

export default CreateInsurance
