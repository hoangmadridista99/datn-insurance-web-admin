import Head from 'next/head'
import React from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
import { Button, Empty, Input, Select } from 'antd'
import { SearchOutlined } from '@ant-design/icons'
import CardRatingFavor from 'components/Cards/Rating/CardRatingFavor'
import { isEmpty } from 'lodash'
import { SortAscendingSvg, SortDescendingSvg } from '@utils/icons'
import CustomizePagination from 'components/Pagination'
import { useLifeAndHealthFavoriteLogic } from '@hooks/useLifeAndHealthFavoriteLogic'

export default function Index() {
  const { t } = useTranslation(['vendor', 'common', 'insurances'])
  const {
    isLoading,
    insuranceName,
    insuranceCategories,
    filterValues,
    interestList,
    pagination,
    handleChangeInsuranceName,
    handleOptions,
    handleChangeInsuranceType,
    handleSortValues,
    handleResetFilter,
    handleChangePage,
  } = useLifeAndHealthFavoriteLogic()

  return (
    <>
      <Head>
        <title>Favorite - Sosanh24</title>
      </Head>

      <div className="no-scrollbar flex h-full w-full flex-col justify-between overflow-hidden overflow-y-auto px-6 py-10">
        <div className="min-h-100">
          <div className="mb-8 block overflow-hidden">
            <div className="flex items-center justify-between [&>*]:mr-4">
              <div className="flex w-2/3 items-center gap-4">
                <Input
                  name="insurance_name"
                  size="large"
                  placeholder={t('insurances:filter.name')}
                  prefix={<SearchOutlined className="text-nickel" />}
                  className="w-full text-base"
                  onChange={handleChangeInsuranceName}
                  value={insuranceName}
                />

                <Select
                  className="w-full text-nickel placeholder:text-nickel"
                  placeholder={t('insurances:form:placeholder:select')}
                  size="large"
                  allowClear={true}
                  options={handleOptions(insuranceCategories)}
                  onChange={handleChangeInsuranceType}
                  value={filterValues?.insurance_category_label}
                />
              </div>

              <Button
                type="primary"
                size="large"
                className={`${
                  (isLoading ||
                    (!filterValues?.insurance_name &&
                      !filterValues?.insurance_category_label)) &&
                  'border border-solid border-platinum bg-platinum text-metallic'
                } rounded-lg px-6 text-base`}
                onClick={handleResetFilter}
                disabled={
                  isLoading ||
                  (filterValues?.insurance_name &&
                    !filterValues?.insurance_category_label)
                }
              >
                Reset
              </Button>
            </div>
          </div>

          <div className="mb-4 flex items-center px-6 text-base font-bold text-metallic">
            <p className="w-1/4 text-start">
              {t('insurances:fields:insurance_name')}
            </p>

            <p className="flex w-1/3 items-center justify-between pr-6 text-start">
              {t('insurances:fields:amount')}

              <button
                type="ghost"
                className="cursor-pointer border-none bg-transparent"
                onClick={handleSortValues}
                disabled={isLoading || isEmpty(interestList)}
              >
                {filterValues.total_interest === 'DESC' ? (
                  <SortDescendingSvg />
                ) : (
                  <SortAscendingSvg />
                )}
              </button>
            </p>

            <p className="w-1/3 text-start">
              {t('insurances:fields:latest_update')}
            </p>
          </div>

          {(isLoading || isEmpty(interestList)) && <Empty className="mt-20" />}

          {!isLoading && !isEmpty(interestList) && (
            <>
              <div className="[&>*:last-child]:mb-10">
                {interestList?.map((interestItem) => (
                  <CardRatingFavor
                    key={interestItem.id}
                    interestItem={interestItem}
                  />
                ))}
              </div>

              <CustomizePagination
                pagination={pagination}
                onChangePage={handleChangePage}
              />
            </>
          )}
        </div>
      </div>
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'favorite',
        'insurances',
      ])),
    },
  }
}
