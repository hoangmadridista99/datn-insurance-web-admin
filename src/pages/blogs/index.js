import React from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { Button, Input, DatePicker, Select, Empty } from 'antd'
import { SearchOutlined } from '@ant-design/icons'
import dayjs from 'dayjs'
import CardBlog from '@components/Cards/Blog'
import { CREATE_AT, KEY_MODAL, OptionBlogStatus } from '@utils/constants/blog'
import { useBlogsLogic } from '@hooks/useBlogsLogic'
import Head from 'next/head'
import { ROLES } from '@utils/constants/insurances'
import FilterIconSvg from 'components/Icon/Filter'
import NoFilterIconSvg from 'components/Icon/NoFilter'
import { useTranslation } from 'next-i18next'
import { isEmpty } from 'lodash'
import CustomizePagination from 'components/Pagination'
import CreateBlogModal from 'components/Modals/Blog/Create'
import UpdateBlogModal from 'components/Modals/Blog/Update'
import PreviewBlogModal from 'components/Modals/Blog/Preview'
import ReasonForRefusalBlogModal from 'components/Modals/Blog/ReasonForRefusal'
import RemoveBlogModal from 'components/Modals/Blog/Remove'

const Blogs = (props) => {
  const { t } = useTranslation(['blogs'])
  const {
    isLoading,
    isShowFilter,
    isDisableButtonReset,
    blog,
    blogs,
    categories,
    blogName,
    filterValues,
    pagination,
    openModals,
    onOpenModal,
    onCloseModal,
    onToggleFilter,
    onClickResetFilter,
    onUpdateSuccess,
    onChangeTitle,
    onSelectFilter,
    onChangePage,
    handleGetBlogs,
  } = useBlogsLogic(props)

  return (
    <>
      <Head>
        <title>Blogs - Sosanh24</title>
      </Head>

      <div className="no-scrollbar w-full overflow-scroll bg-white px-6 py-10">
        <div className="flex items-center justify-between">
          <Input
            size="large"
            prefix={<SearchOutlined className="text-nickel" />}
            className="w-100 text-base"
            placeholder={t('blogs:placeholder:search-title')}
            onChange={onChangeTitle}
            value={blogName}
          />
          <div className="flex items-center gap-4">
            {props.role === ROLES.ADMIN && (
              <Button
                size="large"
                type="primary"
                onClick={() => onOpenModal(KEY_MODAL.CREATE)}
                className="px-6 text-base font-bold text-ghost-white"
              >
                {t('blogs:button:create')}
              </Button>
            )}

            <Button
              type="primary"
              ghost
              size="large"
              icon={
                isShowFilter ? (
                  <FilterIconSvg className="mr-2" />
                ) : (
                  <NoFilterIconSvg className="mr-2" />
                )
              }
              aria-checked={isShowFilter}
              onClick={onToggleFilter}
              className="flex items-center justify-center border-nickel px-4 text-base font-normal text-nickel aria-checked:border-ultramarine-blue aria-checked:text-ultramarine-blue aria-checked:hover:border-ultramarine-blue aria-checked:hover:text-ultramarine-blue"
            >
              {t('blogs:button:filter')}
            </Button>
          </div>
        </div>

        <div
          className="hidden overflow-hidden aria-checked:block"
          aria-checked={isShowFilter}
        >
          <div className="mt-6 flex items-center justify-between gap-4">
            <DatePicker
              allowClear={false}
              className="w-full text-nickel placeholder:text-nickel"
              size="large"
              placeholder={t('blogs:placeholder:created-at')}
              onChange={(value) => onSelectFilter(CREATE_AT, value)}
              format={'YYYY-MM-DD'}
              value={
                filterValues.created_at
                  ? dayjs(filterValues.created_at)
                  : undefined
              }
            />

            <Select
              options={categories}
              allowClear={true}
              className="w-full text-nickel placeholder:text-nickel"
              size="large"
              placeholder={t('blogs:placeholder:category')}
              loading={!categories}
              disabled={!categories}
              onChange={(value) => onSelectFilter('blog_category_id', value)}
              value={filterValues.blog_category_id}
            />

            {props?.role === ROLES.VENDOR && (
              <Select
                options={OptionBlogStatus.map((item) => ({
                  label: t(`blogs:status:${item}`),
                  value: item,
                }))}
                allowClear={true}
                className="w-full text-nickel placeholder:text-nickel"
                size="large"
                onChange={(value) => onSelectFilter('status', value)}
                value={filterValues.status}
                placeholder={t('blogs:placeholder:status')}
              />
            )}

            <Button
              type="primary"
              size="large"
              className="rounded-lg py-2 px-6 text-base"
              disabled={isDisableButtonReset}
              onClick={onClickResetFilter}
            >
              Reset
            </Button>
          </div>
        </div>

        <div className="mb-4 mt-8 flex items-center px-4 text-base font-bold text-metallic">
          <p className="mr-6 w-2/12">{t('blogs:banner')}</p>
          <p className={props.role === ROLES.VENDOR ? 'w-4/12' : 'w-5/12'}>
            {t('blogs:title')}
          </p>
          <p className={props.role === ROLES.VENDOR ? 'w-2/12' : 'w-3/12'}>
            {t('blogs:created-at')}
          </p>
          {props.role === ROLES.VENDOR && (
            <p className="w-2/12">{t('blogs:placeholder:status')}</p>
          )}
          <p className="w-2/12" />
        </div>

        {(isLoading || isEmpty(blogs)) && <Empty className="mt-20" />}

        {!isLoading && !isEmpty(blogs) && (
          <>
            <div className="[&>*:last-child]:mb-10">
              {blogs.map((item) => (
                <CardBlog
                  key={item.id}
                  handleOpenModal={onOpenModal}
                  onUpdateSuccess={onUpdateSuccess}
                  isLoading={isLoading}
                  data={item}
                  locale={props.locale}
                />
              ))}
            </div>

            <CustomizePagination
              pagination={pagination}
              onChangePage={onChangePage}
            />
          </>
        )}
      </div>

      {openModals.create && (
        <CreateBlogModal
          isOpen={openModals.create}
          onClose={() => onCloseModal(KEY_MODAL.CREATE)}
          categories={categories}
          onRefreshing={handleGetBlogs}
        />
      )}

      {blog && openModals.update && (
        <UpdateBlogModal
          isOpen={openModals.update}
          blogId={blog?.id || null}
          onClose={() => onCloseModal(KEY_MODAL.UPDATE)}
          categories={categories}
          onUpdateSuccess={onUpdateSuccess}
        />
      )}

      {blog && openModals.preview && (
        <PreviewBlogModal
          isOpen={openModals.preview}
          blog={blog && openModals.preview ? blog : null}
          onClose={() => onCloseModal(KEY_MODAL.PREVIEW)}
        />
      )}

      {blog && openModals.reject && (
        <ReasonForRefusalBlogModal
          isOpen={openModals.reject}
          blogId={blog?.id || null}
          onClose={() => onCloseModal(KEY_MODAL.REJECT)}
          onUpdateSuccess={onUpdateSuccess}
        />
      )}

      {blog && openModals.remove && (
        <RemoveBlogModal
          blog={blog && openModals.remove ? blog : false}
          onClose={() => onCloseModal(KEY_MODAL.REMOVE)}
          onRefreshing={handleGetBlogs}
        />
      )}
    </>
  )
}

export const getServerSideProps = async ({ locale, query }) => {
  const { role } = query

  if (!role || (role !== ROLES.ADMIN && role !== ROLES.VENDOR))
    return {
      notFound: true,
    }

  return {
    props: {
      ...(await serverSideTranslations(locale, ['vendor', 'common', 'blogs'])),
      role,
      locale,
    },
  }
}

export default Blogs
