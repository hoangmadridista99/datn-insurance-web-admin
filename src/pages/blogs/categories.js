import React from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { Button, Empty, Input } from 'antd'
import useBlogCategoriesLogic from '@hooks/useBlogCategoriesLogic'
import Head from 'next/head'
import { useTranslation } from 'next-i18next'
import { SearchOutlined } from '@ant-design/icons'
import CardBlogCategory from 'components/Cards/BlogCategory'
import { isEmpty } from 'lodash'
import CustomizePagination from 'components/Pagination'
import CreateOrUpdateBlogCategoryModal from 'components/Modals/BlogCateogory/CreateOrUpdate'

const BlogCategories = () => {
  const { t } = useTranslation(['blogs'])
  const {
    isLoading,
    blogCategories,
    categoryName,
    isCreateModalVisible,
    isUpdateModalVisible,
    categoryDetails,
    pagination,
    handleChangeCategoryName,
    handleOpenCreateModal,
    handleCloseCreateModal,
    handleSubmitCreate,
    handleOpenUpdateModal,
    handleCloseUpdateModal,
    handleSubmitUpdate,
    handleDeleteCategory,
    handleChangePage,
  } = useBlogCategoriesLogic()

  return (
    <>
      <Head>
        <title>Blog Categories - Sosanh24</title>
      </Head>

      <div className="h-full w-full bg-white p-4">
        <div className="no-scrollbar w-full justify-between overflow-scroll bg-white px-6 py-10">
          <div className="flex items-center justify-between">
            <Input
              size="large"
              prefix={<SearchOutlined className="text-nickel" />}
              className="w-100 text-base"
              placeholder={t('blogs:placeholder:blog_category')}
              onChange={handleChangeCategoryName}
              value={categoryName}
            />

            <Button onClick={handleOpenCreateModal} type="primary" size="large">
              {t('blogs:button:create_category')}
            </Button>
          </div>

          <div className="mb-4 mt-8 flex items-center px-4 text-base font-bold text-metallic">
            <p className="mr-6 w-2/5">{t('blogs:vietnamese_title')}</p>
            <p className="w-2/5">{t('blogs:english_title')}</p>
            <p className="w-1/5" />
          </div>

          {(isLoading || isEmpty(blogCategories)) && (
            <Empty className="mt-20" />
          )}

          {!isLoading && !isEmpty(blogCategories) && (
            <>
              <div className="[&>*:last-child]:mb-10">
                {blogCategories.map((category) => (
                  <CardBlogCategory
                    key={category.id}
                    isLoading={isLoading}
                    blogCategory={category}
                    onClickUpdate={handleOpenUpdateModal}
                    onClickDelete={handleDeleteCategory}
                  />
                ))}
              </div>

              <CustomizePagination
                pagination={pagination}
                onChangePage={handleChangePage}
              />
            </>
          )}
        </div>
      </div>

      {isCreateModalVisible && (
        <CreateOrUpdateBlogCategoryModal
          isModalVisible={isCreateModalVisible}
          onSubmit={handleSubmitCreate}
          onCancel={handleCloseCreateModal}
        />
      )}

      {isUpdateModalVisible && categoryDetails && (
        <CreateOrUpdateBlogCategoryModal
          isModalVisible={isUpdateModalVisible}
          initialValues={categoryDetails}
          onSubmit={handleSubmitUpdate}
          onCancel={handleCloseUpdateModal}
        />
      )}
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['blogs', 'common'])),
    },
  }
}

export default BlogCategories
