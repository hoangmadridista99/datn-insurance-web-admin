import Link from 'next/link'
import React from 'react'
import { getServerSession } from 'next-auth/next'
import { authOptions } from './api/auth/[...nextauth]'
import { useRouter } from 'next/router'
import { useTranslation } from 'next-i18next'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { Select, Button } from 'antd'
import Head from 'next/head'

export default function Home() {
  const route = useRouter()
  const { t } = useTranslation(['common', 'auth'])

  const handleClickLocale = (value) => {
    if (route.locale !== value) {
      route.push(route.asPath, undefined, { locale: value })
    }
  }

  return (
    <>
      <Head>
        <title>Portal - Sosanh24</title>
      </Head>
      <div className="fixed inset-0 h-20 bg-white shadow-lg">
        <div className="container flex h-full items-center justify-between gap-4 px-10">
          <Select
            value={route.locale ?? 'vi'}
            options={[
              {
                value: 'vi',
                label: t('common:vi'),
                disabled: route.locale === 'vi',
              },
              {
                value: 'en',
                label: t('common:en'),
                disabled: route.locale === 'en',
              },
            ]}
            onChange={handleClickLocale}
            size="large"
          />
          <Button type="primary">
            <Link href="/auth/login" locale={route.locale}>
              {t('auth:login')}
            </Link>
          </Button>
        </div>
      </div>
    </>
  )
}

export const getServerSideProps = async ({ req, res, locale }) => {
  const session = await getServerSession(req, res, authOptions)

  if (session)
    return {
      redirect: {
        destination: '/dashboard',
        permanent: true,
      },
    }

  return {
    props: { ...(await serverSideTranslations(locale, ['auth', 'common'])) },
  }
}
