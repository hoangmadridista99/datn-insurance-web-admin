import React from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
import { Button, Empty, Input, Select } from 'antd'
import useVendorLogic from '@hooks/useVendorLogic'
import { getCompanies } from '@services/companies'
import Head from 'next/head'
import { isEmpty } from 'lodash'
import { SearchOutlined } from '@ant-design/icons'
import CardVendor from 'components/Cards/Vendor'
import { VENDOR_STATUSES } from '@utils/constants/insurances'
import CustomizePagination from 'components/Pagination'

function Vendor({ companies }) {
  const { t } = useTranslation(['vendor', 'common', 'modal'])

  const {
    isLoading,
    vendors,
    pagination,
    companyName,
    filterValues,
    convertStatusOptions,
    onChangeName,
    onResetFilter,
    onSelect,
    handleChangePage,
    handleGetVendors,
  } = useVendorLogic(companies)

  return (
    <>
      <Head>
        <title>Insurances - Sosanh24</title>
      </Head>

      <div className="no-scrollbar flex flex-col justify-between overflow-hidden overflow-y-auto px-6 py-10">
        <div className="min-h-100">
          {/* filter */}
          <div className="flex items-center justify-between">
            <div className="flex w-full items-center gap-4">
              <div className="flex w-full items-center gap-14">
                <Input
                  name="insurance_name"
                  size="large"
                  placeholder={t('vendor:placeholders.name')}
                  prefix={<SearchOutlined className="text-nickel" />}
                  className="w-1/2 text-base"
                  onChange={onChangeName}
                  value={companyName}
                  disabled={isLoading}
                  allowClear
                />

                <Select
                  className="w-full text-nickel placeholder:text-nickel"
                  placeholder={t('vendor:placeholders.select')}
                  size="large"
                  allowClear={true}
                  options={convertStatusOptions(Object.values(VENDOR_STATUSES))}
                  onChange={onSelect}
                  value={filterValues?.vendor_status}
                  disabled={isLoading}
                />
              </div>

              <Button
                type="primary"
                size="large"
                className="mr-0 rounded-lg py-2 px-6 text-base"
                onClick={onResetFilter}
                disabled={!companyName && !filterValues?.vendor_status}
              >
                Reset
              </Button>
            </div>
          </div>

          {/* body content */}
          <div className="mb-2 mt-8 flex items-center px-4 text-base font-bold text-metallic">
            <p className="w-3/12">{t('vendor:table_head:company')}</p>
            <p className="w-2/12">{t('vendor:table_head:person_info')}</p>
            <p className="w-3/12">{t('vendor:table_head:contact_info')}</p>
            <p className="w-2/12">{t('vendor:table_head:status')}</p>
            <p className="w-2/12" />
          </div>

          {(isLoading || isEmpty(vendors)) && <Empty className="mt-20" />}

          {!isLoading && !isEmpty(vendors) && (
            <>
              <div className={'[&>*:last-child]:mb-10'}>
                {vendors?.map((item) => (
                  <CardVendor
                    key={item.id}
                    {...item}
                    handleGetVendors={handleGetVendors}
                  />
                ))}
              </div>

              <CustomizePagination
                pagination={pagination}
                onChangePage={handleChangePage}
              />
            </>
          )}
        </div>
      </div>
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  const companies = await getCompanies()

  return {
    props: {
      ...(await serverSideTranslations(locale, ['vendor', 'common', 'modal'])),
      companies,
    },
  }
}

export default Vendor
