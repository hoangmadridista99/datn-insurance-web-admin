import React from 'react'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { Input, Button, Select, Empty } from 'antd'
import { SearchOutlined } from '@ant-design/icons'
import CardUser from '@components/Cards/User'
import { OptionStatus } from '@utils/constants/operator'
import Head from 'next/head'
import { isEmpty, isNil } from 'lodash'
import { useTranslation } from 'next-i18next'
import CustomizePagination from 'components/Pagination'
import CreateOrUpdateUserModal from 'components/Modals/User/CreateOrUpdate'
import { useOperatorLogic } from '@hooks/useOperatorLogic'

const Operators = () => {
  const { t } = useTranslation(['operators', 'common', 'insurances'])
  const {
    isLoading,
    operators,
    username,
    filterValues,
    pagination,
    isModalCreate,
    isModalUpdate,
    operatorDetails,
    handleOpenModalCreate,
    handleChangeAccount,
    handleOptions,
    handleSelectStatus,
    handleResetFilter,
    handleOpenModalUpdate,
    handleUpdateSuccess,
    handleGetData,
    handleChangePage,
    handleCloseModalCreate,
    handleCloseModalUpdate,
  } = useOperatorLogic()

  return (
    <>
      <Head>
        <title>Operators - Sosanh24</title>
      </Head>

      <div className="no-scrollbar overflow-hidden overflow-y-auto px-6 py-10">
        <div className="min-h-100">
          <div className="mb-6 flex justify-end">
            <Button
              type="primary"
              className="cursor-pointer rounded-lg font-bold text-ghost-white"
              onClick={handleOpenModalCreate}
              size="large"
            >
              {t('operators:button:create_new_account')}
            </Button>
          </div>

          <div className="block overflow-hidden ">
            <div className="flex items-center justify-between gap-4">
              <Input
                size="large"
                placeholder={t('operators:placeholders:enter_username')}
                prefix={<SearchOutlined className="text-nickel" />}
                className="w-full text-base"
                value={username}
                onChange={handleChangeAccount}
              />

              <Select
                options={handleOptions(OptionStatus)}
                placeholder={t('operators:placeholders:status')}
                allowClear
                className="w-full text-nickel placeholder:text-nickel"
                size="large"
                value={filterValues.is_blocked}
                onChange={handleSelectStatus}
              />

              <Button
                type="primary"
                size="large"
                className={`${
                  !filterValues.account &&
                  isNil(filterValues.is_blocked) &&
                  'border border-solid border-platinum bg-platinum text-metallic'
                } rounded-lg px-6 text-base`}
                onClick={handleResetFilter}
                disabled={
                  !filterValues.account && isNil(filterValues.is_blocked)
                }
              >
                Reset
              </Button>
            </div>
          </div>

          <div className="mb-4 mt-8 flex items-center px-4 text-base font-bold text-metallic">
            <p className="w-1/3">{t('operators:table:full_name')}</p>
            <p className="w-1/4">{t('operators:table:user_name')}</p>
            <p className="w-1/4">{t('operators:table:status')}</p>
            <p className="w-1/4" />
          </div>

          {(isLoading || isEmpty(operators)) && <Empty className="mt-20" />}

          {!isLoading && !isEmpty(operators) && (
            <>
              <div className="[&>*:last-child]:mb-10">
                {operators.map((item) => (
                  <CardUser
                    onUpdate={handleOpenModalUpdate}
                    onUpdateSuccess={handleUpdateSuccess}
                    handleGetData={handleGetData}
                    key={item.id}
                    {...item}
                  />
                ))}
              </div>

              <CustomizePagination
                pagination={pagination}
                onChangePage={handleChangePage}
              />
            </>
          )}
        </div>
      </div>

      {isModalCreate && (
        <CreateOrUpdateUserModal
          isOpenModal={isModalCreate}
          onClose={handleCloseModalCreate}
          handleGetData={handleGetData}
        />
      )}

      {isModalUpdate && operatorDetails && (
        <CreateOrUpdateUserModal
          isOpenModal={isModalUpdate}
          initialValues={operatorDetails}
          onClose={handleCloseModalUpdate}
          handleGetData={handleGetData}
        />
      )}
    </>
  )
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'common',
        'operators',
        'insurances',
      ])),
    },
  }
}

export default Operators
