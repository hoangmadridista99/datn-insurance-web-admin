import NextAuth from 'next-auth'
import CredentialsProvider from 'next-auth/providers/credentials'

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL
const ONE_DAY = 24 * 60 * 60
const ONE_YEAR = ONE_DAY * 365

export const authOptions = {
  // Configure one or more authentication providers
  providers: [
    CredentialsProvider({
      type: 'credentials',
      credentials: {},
      async authorize(credentials) {
        try {
          const { account, password } = credentials
          const authResponse = await fetch(
            `${BASE_URL}/v1/api/admin/auth/login`,
            {
              method: 'POST',
              body: JSON.stringify({
                account,
                password,
              }),
              headers: { 'Content-Type': 'application/json' },
            }
          )

          const user = await authResponse.json()

          if (!authResponse.ok) throw user.error_code

          if (authResponse.ok) return user
          return null
        } catch (error) {
          throw Error(error)
        }
      },
    }),
  ],

  callbacks: {
    async jwt({ token, trigger, user, session }) {
      if (user) {
        const { accessToken, role, ...data } = user
        token = {
          accessToken,
          role,
          user: data,
        }
      }

      if (trigger === 'update') {
        token.user = session
      }
      return token
    },

    async session({ session, token }) {
      // Send properties to the client, like an access_token from a provider.
      session.accessToken = token.accessToken
      session.role = token.role
      session.user = token.user

      return session
    },

    async signIn() {
      return true
    },
  },

  session: {
    strategy: 'jwt',
    maxAge: ONE_YEAR,
    updateAge: ONE_DAY,
  },

  pages: {
    signIn: '/auth/login',
  },

  debug: true,
}

export default NextAuth(authOptions)
