const BaseUrl = process.env.NEXT_PUBLIC_BASE_URL
import axios from 'axios'

export const getRatingsInsurance = (token, { page, limit }) => {
  return axios.get(
    `${BaseUrl}/v1/api/admin/ratings/unverified?page=${page}&limit=${
      limit ?? 10
    }`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  )
}

export const getRatingsInsuranceId = (token, { page, id, limit }) => {
  return axios.get(
    `${BaseUrl}/v1/api/admin/ratings/insurance/${id}?page=${page}&limit=${
      limit ?? 5
    }`,
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  )
}

export const patchRatingsInsurance = (token, { id, is_verified_by_admin }) => {
  return axios.patch(
    `${BaseUrl}/v1/api/admin/ratings/${id}/verify`,
    { is_verified_by_admin },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  )
}

export const deleteRatingsInsurance = (token, { id }) => {
  return axios.delete(`${BaseUrl}/v1/api/admin/ratings/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
}
