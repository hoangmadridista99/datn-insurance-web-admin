/** @type {import('next').NextConfig} */
const { i18n } = require('./next-i18next.config')

const nextConfig = {
  reactStrictMode: true,
  i18n,
  images: {
    domains: ['localhost', 'dev.api.insurance.just.engineer'],
  },
  async rewrites() {
    return [
      {
        source: '/insurances/:role',
        destination: '/insurances',
      },
      {
        source: '/blogs/:role',
        destination: '/blogs',
      },
      {
        source: '/insurance-price/physical-car/:id/:type',
        destination: '/insurance-price/physical-car/:id?type=:type',
      },
    ]
  },
}

module.exports = nextConfig
